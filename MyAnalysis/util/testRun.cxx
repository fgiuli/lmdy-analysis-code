#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>

#include "MyAnalysis/MyxAODAnalysis.h"

int main( int argc, char* argv[] ) {

  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  //std::string sampleName = "";
  if( argc > 1 ) submitDir = argv[ 1 ];
  //if( argc > 2 ) sampleName = argv[ 2 ];

  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // Construct the samples to run on:
  SH::SampleHandler sh;

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  //const char* inputFilePath = gSystem->ExpandPathName ("/data/atlas/atlasdata/giuli/Samples/lmDY/mc15_13TeV.361667.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_10M60.merge.DAOD_STDM7.e4770_s2726_r7772_r7676_p2669/");
  //SH::ScanDir().filePattern("DAOD_STDM7.08592161._000005.pool.root.1").scan(sh, inputFilePath);

  //SH::DiskListLocal list (inputFilePath);                                                                                                             
  //SH::ScanDir(sh, list, "DAOD_STDM7.08592161._000004.pool.root.1"); // specifying one particular file for testing       

  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/trunk/Samples/data16_13TeV.00297170.physics_Main.merge.DAOD_STDM7.f686_m1583_p2623_tid08308945_00");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/trunk/Samples/data15_13TeV.00281385.physics_Main.merge.DAOD_STDM7.r7562_p2521_p2614_tid08289493_00");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/trunk/Samples/mc15_13TeV.363834.Pythia8B_A14_NNPDF23LO_ccmu3p5mu3p5M6.merge.AOD.e4996_s2726_r7772_r7676_tid08227671_00");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/trunk/Samples/data15_13TeV.00283074.physics_Main.merge.DAOD_STDM7.r7562_p2521_p2614_tid08289522_00");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/trunk/Samples/data15_13TeV.00278970.physics_Main.merge.DAOD_STDM7.r7562_p2521_p2614_tid08289325_00");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/low_mass_DY_2016/trunk/Samples/data15_13TeV.00280977.physics_Main.merge.DAOD_EXOT0.r7562_p2521_p2614_tid08126272_00");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/low_mass_DY_2016/trunk/Samples/data15_13TeV.00279345.physics_Main.merge.DAOD_STDM7.r7562_p2521_p2614_tid08289347_00");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/low_mass_DY_2016/trunk/Samples/data15_13TeV.00281385.physics_Main.merge.DAOD_STDM7.r7562_p2521_p2614_tid08289493_00");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/low_mass_DY_2016/trunk/Samples/data15_13TeV.00276329.physics_Main.merge.DAOD_STDM7.r7562_p2521_p2667");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/mc15_13TeV.361633.MadGraphPythia8EvtGen_A14NNPDF23LO_Zmumu_lowMll_Np0.merge.DAOD_STDM7.e4442_s2608_s2183_r7772_r7676_p2669");
  
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/DB/mc15_13TeV.361096.Sherpa_CT10_ZqqZll_SHv21_improved.merge.DAOD_EXOT0.e4607_s2726_r7725_r7676_p2669");
  
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/lmDY/mc15_13TeV.363834.Pythia8B_A14_NNPDF23LO_ccmu3p5mu3p5M6.merge.DAOD_STDM7.e4996_s2726_r7772_r7676_p2669.1");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/lmDY/mc15_13TeV.363833.Pythia8B_A14_NNPDF23LO_bbmu3p5mu3p5M6.merge.DAOD_STDM7.e4996_s2726_r7772_r7676_p2669.1");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/lmDY/mc15_13TeV.361666.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_6M10.merge.DAOD_STDM7.e4770_s2726_r7772_r7676_p2669");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/lmDY/mc15_13TeV.361667.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_10M60.merge.DAOD_STDM7.e4770_s2726_r7772_r7676_p2669");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/lmDY/mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7725_r7676_p2669.7/");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/lmDY/mc15_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.merge.DAOD_STDM7.e3601_s2726_r7773_r7676_p2669.1");

  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/low_mass_DY_2016/trunk/data_2015/data15_13TeV.00280950.physics_Main.merge.DAOD_STDM7.r7562_p2521_p2667");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/lmDY/mc15_13TeV.361667.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_10M60.merge.DAOD_STDM7.e4770_s2726_r7772_r7676_p2669/");

  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/Zmumu");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/DY/");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/data.NEW/");
  
  SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/MC15_repro207/mc15_13TeV.364669.Sherpa_221_NN30NNLO_Zmm_Mll6_10.merge.DAOD_STDM7.e6478_s2726_r7773_r7676_p3314");

  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/MC15_repro207/mc15_13TeV.363676.Pythia8EvtGen_NNPDF23QED_ggTOmumu_18M60_LeptonFilter.merge.DAOD_STDM7.e4913_s2726_r7773_r7676_p3317");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/MC15_repro207/mc15_13TeV.361667.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_10M60.merge.DAOD_STDM7.e4770_s2726_r7773_r7676_p3317.1");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/MC15_repro207/mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.12");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/MC15_repro207/mc15_13TeV.361666.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_6M10.merge.DAOD_STDM7.e4770_s2726_r7773_r7676_p3317.6");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/MC15_repro207/mc15_13TeV.410503.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_dil.merge.DAOD_STDM7.e5475_s2726_r7773_r7676_p3317.11");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/MC15_repro207/mc15_13TeV.424106.Pythia8B_A14_CTEQ6L1_Upsilon3S_mu4mu4.merge.DAOD_STDM7.e5104_s2726_r7773_r7676_p3020.2");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/Data15_repro207/data15_13TeV.00280673.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/Data15_repro207/data15_13TeV.00276329.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/Data15_repro207/data15_13TeV.00276262.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/Data15_repro207/data15_13TeV.00284484.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/Data15_repro207/data15_13TeV.00280862.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/Data15_repro207/data15_13TeV.00280500.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/MC15_repro207/mc15_13TeV.363833.Pythia8B_A14_NNPDF23LO_bbmu3p5mu3p5M6.merge.DAOD_STDM7.e4996_s2726_r7773_r7676_p3317.2");

  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/MC15.NEW/mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3111.1");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/MC15.NEW/mc15_13TeV.363833.Pythia8B_A14_NNPDF23LO_bbmu3p5mu3p5M6.merge.DAOD_STDM7.e4996_s2726_r7773_r7676_p3111.2");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/MC15.NEW/mc15_13TeV.410503.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_dil.merge.DAOD_STDM7.e5475_s2726_r7773_r7676_p3111.11");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/MC15.NEW/mc15_13TeV.361667.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_10M60.merge.DAOD_STDM7.e4770_s2726_r7773_r7676_p3111.1");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/MC15.NEW/mc15_13TeV.361668.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_6M10.merge.DAOD_STDM7.e4784_s2726_r7773_r7676_p3111/");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/MC15.NEW/mc15_13TeV.361669.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_10M60.merge.DAOD_STDM7.e4770_s2726_r7773_r7676_p3111/");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/MC15.NEW/mc15_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.merge.DAOD_STDM7.e3601_s2726_r7773_r7676_p3111.1");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/MC15.NEW/mc15_13TeV.363698.HepMCAsciiEG_Lpair_BSY_SDiss_ggTOmumu_18M60_LeptonFilter.merge.DAOD_STDM7.e4909_s2726_r7773_r7676_p3111");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/Data15.NEW/data15_13TeV.00284484.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108");
  
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/ttbarZ_check/data15_13TeV.00276336.physics_Main.merge.DAOD_STDM3.f620_m1480_p2425/");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/ttbarZ_check/data15_13TeV.00276262.physics_Main.merge.DAOD_STDM3.r7562_p2521_p2950/");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/data_p2952/data15_13TeV.00276336.physics_Main.merge.DAOD_STDM7.r7562_p2521_p2950/");
  
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/lmDY/mc15_13TeV.363833.Pythia8B_A14_NNPDF23LO_bbmu3p5mu3p5M6.merge.DAOD_STDM7.e4996_s2726_r7773_r7676_p2952.1/");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/lmDY/mc15_13TeV.363834.Pythia8B_A14_NNPDF23LO_ccmu3p5mu3p5M6.merge.DAOD_STDM7.e4996_s2726_r7773_r7676_p2952/");
  
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/lmDY/mc15_13TeV.361667.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_10M60.merge.DAOD_STDM7.e4770_s2726_r7772_r7676_p2669.1/");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/lmDY/mc15_13TeV.361666.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_6M10.merge.DAOD_STDM7.e4770_s2726_r7772_r7676_p2669.1/");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/lmDY/mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p2952.12/");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/data_p2952/data15_13TeV.00283608.physics_Main.merge.DAOD_STDM7.r7562_p2521_p2950/");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/Samples/data_p2952/data15_13TeV.00276790.physics_Main.merge.DAOD_STDM7.r7562_p2521_p2950/");
  
  //SH::DiskListLocal list (sampleName);
  
  //SH::scanSingleDir(sh,list, "*.pool.root.1"); // specifying one particular file for testing                                   
  SH::scanSingleDir(sh, "Output", list);  //to add again if you run SH::DiskListLocal list      

  // Set the name of the input TTree. It's always "CollectionTree"
  // for xAOD files.
  sh.setMetaString( "nc_tree", "CollectionTree" );

  // Print what we found:
  sh.print();

  // Create an EventLoop job:
  EL::Job job;
  job.sampleHandler( sh );
  job.options()->setDouble (EL::Job::optMaxEvents, -1);
  //job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_athena);
											     
  // Options for systematic variations, set std::string Sys to the appropriate value
  /*if (Arg_Sys=="Nom") sys_name = "";
  else if (Arg_Sys=="Mu_ResID_Up") sys_name = "MUONS_ID__1up";
  else if (Arg_Sys=="Mu_ResID_Down") sys_name = "MUONS_ID__1down";
  else if (Arg_Sys=="Mu_ResMS_Up") sys_name = "MUONS_MS__1up";
  else if (Arg_Sys=="Mu_ResMS_Down") sys_name = "MUONS_MS__1down";
  else if (Arg_Sys=="Mu_Scale_Up") sys_name = "MUONS_SCALE__1up";
  else if (Arg_Sys=="Mu_Scale_Down") sys_name = "MUONS_SCALE__1down";
  else if (Arg_Sys=="Mu_Eff_IDUpSys") sys_name = "MUON_EFF_SYS__1up";
  else if (Arg_Sys=="Mu_Eff_IDDownSys") sys_name = "MUON_EFF_SYS__1down";
  else if (Arg_Sys=="Mu_Eff_IDUpStat") sys_name = "MUON_EFF_STAT__1up";
  else if (Arg_Sys=="Mu_Eff_IDDownStat") sys_name = "MUON_EFF_STAT__1down";
  else if (Arg_Sys=="Mu_Eff_IsoUpStat") sys_name = "MUON_ISO_STAT__1up";
  else if (Arg_Sys=="Mu_Eff_IsoDownStat") sys_name = "MUON_ISO_STAT__1down";
  else if (Arg_Sys=="Mu_Eff_IsoUpSys") sys_name = "MUON_ISO_SYS__1up";
  else if (Arg_Sys=="Mu_Eff_IsoDownSys") sys_name = "MUON_ISO_SYS__1down";
  else if (Arg_Sys=="Mu_Eff_TrigUpSys") sys_name = "MUON_EFF_TrigSystUncertainty__1up";
  else if (Arg_Sys=="Mu_Eff_TrigDownSys") sys_name = "MUON_EFF_TrigSystUncertainty__1down";
  else if (Arg_Sys=="Mu_Eff_TrigUpStat") sys_name = "MUON_EFF_TrigStatUncertainty__1up";
  else if (Arg_Sys=="Mu_Eff_TrigDownStat") sys_name = "MUON_EFF_TrigStatUncertainty__1down";*/

  std::string Sys = "Nom";
  //std::string Sys = "Mu_Eff_IsoUpSys";
  // Arguments to Job
  //Double_t isMC = 0;
  //std::string SampleName = "";
  //std::string Sys = "";

  // Add our analysis to the job:
  MyxAODAnalysis* alg = new MyxAODAnalysis();
  //alg->Arg_isMC = isMC;
  //alg->Arg_SampleName = SampleName;
  alg->Arg_Sys = Sys;
  job.algsAdd (alg);

  // Run the job using the local/direct driver:
  EL::DirectDriver driver;
  driver.submit( job, submitDir );

  return 0;
}
