#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoop/LocalDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>
#include "SampleHandler/ScanDir.h"

#include "MyAnalysis/MyxAODAnalysis.h"

int main( int argc, char* argv[] ) {

  // Take the submit directory from the input if provided:
  std::string submitDir = "";
  std::string sampleName = "";
  if( argc > 1 ) submitDir = argv[ 1 ];
  if( argc > 2 ) sampleName = argv[ 2 ];

  // Arguments to Job                                                                                                                                                        
  //Double_t isMC = -999;

  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // Construct the samples to run on:
  SH::SampleHandler sh;

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  //const char* inputFilePath = gSystem->ExpandPathName ("/tmp/mcanobre");
  //SH::ScanDir().sampleDepth(1).samplePattern("AOD.05352803._000031.pool.root.1").scan(sh, inputFilePath);

  //SH::DiskListLocal list (inputFilePath);                                                                                                             
  //SH::scanDir (sh, list, "*.pool.root.1"); // specifying one particular file for testing       

  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/trunk/Samples/data16_13TeV.00297170.physics_Main.merge.DAOD_STDM7.f686_m1583_p2623_tid08308945_00");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/trunk/Samples/data15_13TeV.00281385.physics_Main.merge.DAOD_STDM7.r7562_p2521_p2614_tid08289493_00");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/trunk/Samples/mc15_13TeV.363834.Pythia8B_A14_NNPDF23LO_ccmu3p5mu3p5M6.merge.AOD.e4996_s2726_r7772_r7676_tid08227671_00");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/trunk/Samples/data15_13TeV.00283074.physics_Main.merge.DAOD_STDM7.r7562_p2521_p2614_tid08289522_00");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/trunk/Samples/data15_13TeV.00278970.physics_Main.merge.DAOD_STDM7.r7562_p2521_p2614_tid08289325_00");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/low_mass_DY_2016/trunk/Samples/data15_13TeV.00280977.physics_Main.merge.DAOD_EXOT0.r7562_p2521_p2614_tid08126272_00");
  //SH::DiskListLocal list ("/data/atlas/atlasdata/giuli/low_mass_DY_2016/trunk/Samples/data16_13TeV.00297170.physics_Main.merge.DAOD_STDM7.f686_m1583_p2623_tid08308945_00");
  SH::DiskListLocal list (sampleName);

  //SH::scanDir (sh,list, "DAOD_STDM7.08281638.*.root.*"); // specifying one particular file for testing                                   
  //SH::ScanDir ();  //to add again if you run SH::DiskListLocal list      
  //SH::scanDir (sh,list,"DAOD_EXOT0.*.root.*"); // specifying one particular file for testing                                   
  SH::scanSingleDir (sh, "Output", list); // specifying one particular file for testing                                   

  Double_t isMC = 0;
  //isMC = 0;
  //SampleName = "data";
  std::string Sys = "Nom";
  //std::string Sys = "Mu_Eff_IsoUpStat";

  // Set the name of the input TTree. It's always "CollectionTree"
  // for xAOD files.
  sh.setMetaString( "nc_tree", "CollectionTree" );

  // Print what we found:
  sh.print();

  // Create an EventLoop job:
  EL::Job job;
  job.sampleHandler( sh );

  // Add our analysis to the job:
  MyxAODAnalysis* alg = new MyxAODAnalysis();

  job.options()->setDouble (EL::Job::optMaxEvents, -1);
  job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_class);

  // Options for systematic variations, set std::string Sys to the appropriate value
  /*if (Arg_Sys=="Nom") sys_name = "";
  else if (Arg_Sys=="Mu_ResID_Up") sys_name = "MUONS_ID__1up";
  else if (Arg_Sys=="Mu_ResID_Down") sys_name = "MUONS_ID__1down";
  else if (Arg_Sys=="Mu_ResMS_Up") sys_name = "MUONS_MS__1up";
  else if (Arg_Sys=="Mu_ResMS_Down") sys_name = "MUONS_MS__1down";
  else if (Arg_Sys=="Mu_Scale_Up") sys_name = "MUONS_SCALE__1up";
  else if (Arg_Sys=="Mu_Scale_Down") sys_name = "MUONS_SCALE__1down";
  else if (Arg_Sys=="Mu_Eff_IDUpSys") sys_name = "MUON_EFF_SYS__1up";
  else if (Arg_Sys=="Mu_Eff_IDDownSys") sys_name = "MUON_EFF_SYS__1down";
  else if (Arg_Sys=="Mu_Eff_IDUpStat") sys_name = "MUON_EFF_STAT__1up";
  else if (Arg_Sys=="Mu_Eff_IDDownStat") sys_name = "MUON_EFF_STAT__1down";
  else if (Arg_Sys=="Mu_Eff_IsoUpStat") sys_name = "MUON_ISO_STAT__1up";
  else if (Arg_Sys=="Mu_Eff_IsoDownStat") sys_name = "MUON_ISO_STAT__1down";
  else if (Arg_Sys=="Mu_Eff_IsoUpSys") sys_name = "MUON_ISO_SYS__1up";
  else if (Arg_Sys=="Mu_Eff_IsoDownSys") sys_name = "MUON_ISO_SYS__1down";
  else if (Arg_Sys=="Mu_Eff_TrigUpSys") sys_name = "MUON_EFF_TrigSystUncertainty__1up";
  else if (Arg_Sys=="Mu_Eff_TrigDownSys") sys_name = "MUON_EFF_TrigSystUncertainty__1down";
  else if (Arg_Sys=="Mu_Eff_TrigUpStat") sys_name = "MUON_EFF_TrigStatUncertainty__1up";
  else if (Arg_Sys=="Mu_Eff_TrigDownStat") sys_name = "MUON_EFF_TrigStatUncertainty__1down";*/

  // Pass Argument to Jobs                                                                                                                                
  //alg->Arg_isMC = isMC;
  //alg->Arg_SampleName = SampleName;
  alg->Arg_Sys = Sys;
  alg->Arg_isMC = isMC;
  job.algsAdd( alg );

  // Run the job using the local/direct driver:
  EL::DirectDriver driver;
  driver.submitOnly( job, submitDir );

  return 0;
}
