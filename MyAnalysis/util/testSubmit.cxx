#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>
#include "EventLoopGrid/PrunDriver.h"

#include "MyAnalysis/MyxAODAnalysis.h"

int main( int argc, char* argv[] ) {

  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  if( argc > 1 ) submitDir = argv[ 1 ];

  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // Construct the samples to run on:
  SH::SampleHandler sh;
  
  //MC15c samples
  SH::scanRucio( sh, "mc15_13TeV.363686.HerwigppEvtGen_BudnevQED_ggTOmumu_6M18_LeptonFilter.merge.DAOD_STDM7.e4717_s2726_r7773_r7676_p3317/");
  SH::scanRucio( sh, "mc15_13TeV.363687.HerwigppEvtGen_BudnevQED_ggTOmumu_18M60_LeptonFilter.merge.DAOD_STDM7.e4717_s2726_r7773_r7676_p3317/");
  SH::scanRucio( sh, "mc15_13TeV.363688.HerwigppEvtGen_BudnevQED_ggTOmumu_60M200_LeptonFilter.merge.DAOD_STDM7.e4717_s2726_r7725_r7676_p3317/");
  SH::scanRucio( sh, "mc15_13TeV.363675.Pythia8EvtGen_NNPDF23QED_ggTOmumu_6M18_LeptonFilter.merge.DAOD_STDM7.e4913_s2726_r7773_r7676_p3317/");
  SH::scanRucio( sh, "mc15_13TeV.363676.Pythia8EvtGen_NNPDF23QED_ggTOmumu_18M60_LeptonFilter.merge.DAOD_STDM7.e4913_s2726_r7773_r7676_p3317/");
  SH::scanRucio( sh, "mc15_13TeV.363677.Pythia8EvtGen_NNPDF23QED_ggTOmumu_60M200_LeptonFilter.merge.DAOD_STDM7.e4913_s2726_r7773_r7676_p3317/");
  SH::scanRucio( sh, "mc15_13TeV.363697.HepMCAsciiEG_Lpair_BSY_SDiss_ggTOmumu_6M18_LeptonFilter.merge.DAOD_STDM7.e4909_s2726_r7773_r7676_p3317/");
  SH::scanRucio( sh, "mc15_13TeV.363698.HepMCAsciiEG_Lpair_BSY_SDiss_ggTOmumu_18M60_LeptonFilter.merge.DAOD_STDM7.e4909_s2726_r7773_r7676_p3317/");
  SH::scanRucio( sh, "mc15_13TeV.363699.HepMCAsciiEG_Lpair_BSY_SDiss_ggTOmumu_60M200_LeptonFilter.merge.DAOD_STDM7.e4909_s2726_r7773_r7676_p3317/");
  SH::scanRucio( sh, "mc15_13TeV.363833.Pythia8B_A14_NNPDF23LO_bbmu3p5mu3p5M6.merge.DAOD_STDM7.e4996_s2726_r7773_r7676_p3317/");
  SH::scanRucio( sh, "mc15_13TeV.363834.Pythia8B_A14_NNPDF23LO_ccmu3p5mu3p5M6.merge.DAOD_STDM7.e4996_s2726_r7773_r7676_p3317/");
  SH::scanRucio( sh, "mc15_13TeV.361666.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_6M10.merge.DAOD_STDM7.e4770_s2726_r7773_r7676_p3317/");
  SH::scanRucio( sh, "mc15_13TeV.361667.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_10M60.merge.DAOD_STDM7.e4770_s2726_r7773_r7676_p3317/");
  SH::scanRucio( sh, "mc15_13TeV.361668.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_6M10.merge.DAOD_STDM7.e4784_s2726_r7773_r7676_p3317/");
  SH::scanRucio( sh, "mc15_13TeV.361669.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_10M60.merge.DAOD_STDM7.e4770_s2726_r7773_r7676_p3317/");
  SH::scanRucio( sh, "mc15_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317/");
  //SH::scanRucio( sh, "mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.DAOD_STDM7.e3698_s2608_s2183_r7773_r7676_p3317/");
  SH::scanRucio( sh, "mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317/");
  SH::scanRucio( sh, "mc15_13TeV.410503.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_dil.merge.DAOD_STDM7.e5475_s2726_r7773_r7676_p3317/");
  SH::scanRucio( sh, "mc15_13TeV.424105.Pythia8B_A14_CTEQ6L1_Upsilon2S_mu4mu4.merge.DAOD_STDM7.e5104_s2726_r7773_r7676_p3020/");
  SH::scanRucio( sh, "mc15_13TeV.424106.Pythia8B_A14_CTEQ6L1_Upsilon3S_mu4mu4.merge.DAOD_STDM7.e5104_s2726_r7773_r7676_p3020/");
  SH::scanRucio( sh, "mc15_13TeV.424102.Pythia8B_A14_CTEQ6L1_Upsilon1S_mu4mu4.merge.DAOD_STDM7.e4293_s2608_s2183_r7773_r7676_p3020/");
  
  //2015 data
  //SH::scanRucio( sh, "data15_13TeV.00279928.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00276954.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00279867.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00279345.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00279932.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00276790.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00276689.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00279598.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00276416.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00279169.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00276511.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00278968.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00276262.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00279515.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00276329.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00279685.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00279259.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00278880.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00279984.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00279813.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00279284.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00279279.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00276952.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00278912.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00276336.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00276778.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00280231.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00280273.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00280319.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00280368.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00280423.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00280464.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00280500.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00280520.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00280614.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00280673.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00280753.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00280853.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00280862.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00280950.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00280977.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00281070.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00281074.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00281075.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00281317.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00281385.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00281411.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00282625.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00282631.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00282712.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00282784.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00282992.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00283074.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00283155.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00283270.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00283429.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00283608.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00283780.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00284006.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00284154.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00284213.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00284285.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00284420.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00284427.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");
  //SH::scanRucio( sh, "data15_13TeV.00284484.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3108/");


  // Set the name of the input TTree. It's always "CollectionTree"
  // for xAOD files.
  sh.setMetaString( "nc_tree", "CollectionTree" );

  // Print what we found:
  sh.print();

  // Create an EventLoop job:
  EL::Job job;
  job.sampleHandler( sh );
  //job.options()->setDouble (EL::Job::optMaxEvents, 500);

  // Options for systematic variations, set std::string Sys to the appropriate value
  /*if (Arg_Sys=="Nom") sys_name = "";
  else if (Arg_Sys=="Mu_ResID_Up") sys_name = "MUON_ID__1up";
  else if (Arg_Sys=="Mu_ResID_Down") sys_name = "MUON_ID__1down";
  else if (Arg_Sys=="Mu_ResMS_Up") sys_name = "MUON_MS__1up";
  else if (Arg_Sys=="Mu_ResMS_Down") sys_name = "MUON_MS__1down";
  else if (Arg_Sys=="Mu_Scale_Up") sys_name = "MUON_SCALE__1up";
  else if (Arg_Sys=="Mu_Scale_Down") sys_name = "MUON_SCALE__1down";
  else if (Arg_Sys=="Mu_SagittaRho_Up") sys_name = "MUON_SAGITTA_RHO__1up";
  else if (Arg_Sys=="Mu_SagittaRho_Down") sys_name = "MUON_SAGITTA_RHO__1down";
  else if (Arg_Sys=="Mu_SagittaBias_Up") sys_name = "MUON_SAGITTA_RESBIAS__1up";
  else if (Arg_Sys=="Mu_SagittaBias_Down") sys_name = "MUON_SAGITTA_RESBIAS__1down";
  else if (Arg_Sys=="Mu_Eff_IDUpSys") sys_name = "MUON_EFF_SYS__1up";
  else if (Arg_Sys=="Mu_Eff_IDDownSys") sys_name = "MUON_EFF_SYS__1down";
  else if (Arg_Sys=="Mu_Eff_IDUpStat") sys_name = "MUON_EFF_STAT__1up";
  else if (Arg_Sys=="Mu_Eff_IDDownStat") sys_name = "MUON_EFF_STAT__1down";
  else if (Arg_Sys=="Mu_Eff_IDUpStat_lowpT") sys_name = "MUON_EFF_STAT_LOWPT__1up";
  else if (Arg_Sys=="Mu_Eff_IDDownStat_lowpT") sys_name = "MUON_EFF_STAT_LOWPT__1down";
  else if (Arg_Sys=="Mu_Eff_IDUpSys_lowpT") sys_name = "MUON_EFF_SYS_LOWPT__1up";
  else if (Arg_Sys=="Mu_Eff_IDDownSys_lowpT") sys_name = "MUON_EFF_SYS_LOWPT__1down";
  else if (Arg_Sys=="Mu_Eff_IsoUpStat") sys_name = "MUON_ISO_STAT__1up";
  else if (Arg_Sys=="Mu_Eff_IsoDownStat") sys_name = "MUON_ISO_STAT__1down";
  else if (Arg_Sys=="Mu_Eff_IsoUpSys") sys_name = "MUON_ISO_SYS__1up";
  else if (Arg_Sys=="Mu_Eff_IsoDownSys") sys_name = "MUON_ISO_SYS__1down";
  else if (Arg_Sys=="Mu_Eff_TrigUpSys") sys_name = "MUON_EFF_TrigSystUncertainty__1up";
  else if (Arg_Sys=="Mu_Eff_TrigDownSys") sys_name = "MUON_EFF_TrigSystUncertainty__1down";
  else if (Arg_Sys=="Mu_Eff_TrigUpStat") sys_name = "MUON_EFF_TrigStatUncertainty__1up";
  else if (Arg_Sys=="Mu_Eff_TrigDownStat") sys_name = "MUON_EFF_TrigStatUncertainty__1down";*/

  std::string Sys = "Nom";

  // Arguments to Job                                                                                                          
  //Double_t isMC = 0;
  //std::string SampleName = "";
  //std::string Sys = "";

  // Add our analysis to the job:                                                                                                                  
  MyxAODAnalysis* alg = new MyxAODAnalysis();
  //alg->Arg_isMC = isMC;
  //alg->Arg_SampleName = SampleName;
  alg->Arg_Sys = Sys;
  job.algsAdd (alg);  // Arguments to Job                                                                                                                                                        
  job.options()->setDouble (EL::Job::optMaxEvents,-1);
  job.options()->setDouble (EL::Job::optGridMergeOutput, 1);
  //job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_branch);                                                             
  job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_athena);
  //job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_class);

  // Run the job using the local/direct driver:
  //EL::DirectDriver driver;
  EL::PrunDriver driver;
  driver.options()->setString("nc_outputSampleName", "user.fgiuli.%in:name[1]%.%in:name[2]%.%in:name[3]%.%in:name[4]%.z0Down_EBreq");
  //driver.options()->setString("nc_nFilesPerJob", "1"); //2
  //driver.options()->setDouble(EL::Job::optGridNFilesPerJob, 1); // 1
  //driver.options()->setDouble(EL::Job::optGridMemory,2048); // 2 GB
  //driver.options()->setDouble(EL::Job::optGridNFilesPerJob, 7); // 2
  //driver.options()->setDouble("nc_nGBPerJob", 7);
  driver.submitOnly( job, submitDir );
  
  return 0;
}
