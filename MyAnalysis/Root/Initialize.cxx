#include <MyAnalysis/MyxAODAnalysis.h>

EL::StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  
  xAOD::TEvent* event = wk()->xaodEvent();

  // as a check, let's see the number of events in our xAOD
  Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int

  // count number of events
  m_eventCounter = 0;
  m_numCleanEvents = 0;  

  //Choose whether to apply corrections to electrons, jets and muons
  ApplyCorrections = true;
  m_FastSim = false;
  
  //m_muTMstring_1 = "HLT_mu4_iloose_mu4_11invm60_noos";
  //m_muTMstring_2 = "HLT_mu4_iloose_mu4_7invm9_noos";
  //m_muTMstring_3 = "HLT_mu6_iloose_mu6_11invm24_noos";
  //m_muTMstring_4 = "HLT_mu6_iloose_mu6_24invm60_noos";
  //m_muTMstring_5 = "HLT_mu24_imedium";
  //m_muTMstring_6 = "HLT_mu18_mu8noL1";
  m_muTMstring_1 = "HLT_mu24_imedium";
  m_muTMstring_2 = "HLT_mu18";
  m_muTMstring_3 = "HLT_mu8noL1";

  SelectedMuon    = new xAOD::MuonContainer;                                         
  SelectedMuonAux = new xAOD::MuonAuxContainer;   

  m_debug = false;  

  using CxxUtils::make_unique;

  // GRL
  m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
  const char* GRLFilePath_2015 = "$ROOTCOREBIN/data/MyAnalysis/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml";
  const char* fullGRLFilePath_2015 = gSystem->ExpandPathName (GRLFilePath_2015);
  //const char* GRLFilePath_2016 = "$ROOTCOREBIN/data/MyAnalysis/data16_13TeV.periodAllYear_DetStatus-v78-pro20-04_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml";
  //const char* fullGRLFilePath_2016 = gSystem->ExpandPathName (GRLFilePath_2016);
  std::vector<std::string> vecStringGRL;
  vecStringGRL.push_back(fullGRLFilePath_2015);
  //vecStringGRL.push_back(fullGRLFilePath_2016);
  EL_RETURN_CHECK("initialize()",m_grl->setProperty( "GoodRunsListVec", vecStringGRL));
  EL_RETURN_CHECK("initialize()",m_grl->setProperty("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
  EL_RETURN_CHECK("initialize()",m_grl->initialize());

  // initialize the muon calibration and smearing tool
  m_muonCalibrationAndSmearingTool = new CP::MuonCalibrationAndSmearingTool( "MuonCorrectionTool" );
  //m_muonCalibrationAndSmearingTool->msg().setLevel( MSG::DEBUG );
  m_muonCalibrationAndSmearingTool->setProperty( "Year","Data15");
  m_muonCalibrationAndSmearingTool->setProperty( "Release","Recs2016_15_07");
  m_muonCalibrationAndSmearingTool->setProperty( "StatComb",true);
  m_muonCalibrationAndSmearingTool->setProperty( "SagittaRelease","sagittaBiasDataAll_06_02_17");
  m_muonCalibrationAndSmearingTool->setProperty( "SagittaCorr",false);
  m_muonCalibrationAndSmearingTool->setProperty( "doSagittaMCDistortion",false);
  EL_RETURN_CHECK("initialize()",m_muonCalibrationAndSmearingTool->initialize());

  /*m_muonSelection_TIGHT = new CP::MuonSelectionTool("MuonSelections_TIGHT");
  //m_muonSelection_TIGHT->msg().setLevel( MSG::DEBUG );
  m_muonSelection_TIGHT->setProperty( "MaxEta", 2.4 );
  m_muonSelection_TIGHT->setProperty( "MuQuality", 0);
  EL_RETURN_CHECK("initialize()",m_muonSelection_TIGHT->initialize() );*/

  m_muonSelection_MEDIUM = new CP::MuonSelectionTool("MuonSelections_MEDIUM");
  //m_muonSelection_MEDIUM->msg().setLevel( MSG::DEBUG );
  m_muonSelection_MEDIUM->setProperty( "MaxEta", 2.4 );
  m_muonSelection_MEDIUM->setProperty( "MuQuality", 1);
  EL_RETURN_CHECK("initialize()",m_muonSelection_MEDIUM->initialize() );

  /*iso_muon_gradient = new CP::IsolationSelectionTool("iso_muon_gradient");
  iso_muon_gradient->setProperty("MuonWP","Gradient");
  EL_RETURN_CHECK( "initialize()" , iso_muon_gradient->initialize() );

  iso_muon_tight = new CP::IsolationSelectionTool("iso_muon_tight");
  iso_muon_tight->setProperty("MuonWP","FixedCutTightTrackOnly");
  EL_RETURN_CHECK( "initialize()" , iso_muon_tight->initialize() );*/

  iso_muon_fixed_tight = new CP::IsolationSelectionTool("iso_muon_fixed_tight");
  iso_muon_fixed_tight->setProperty("MuonWP","FixedCutTight");
  EL_RETURN_CHECK( "initialize()" , iso_muon_fixed_tight->initialize() );

  /*iso_muon_fixed_loose = new CP::IsolationSelectionTool("iso_muon_fixed_loose");
  iso_muon_fixed_loose->setProperty("MuonWP","FixedCutLoose");
  EL_RETURN_CHECK( "initialize()" , iso_muon_fixed_loose->initialize() );

  iso_muon_loose = new CP::IsolationSelectionTool("iso_muon_loose");
  iso_muon_loose->setProperty("MuonWP","LooseTrackOnly");
  EL_RETURN_CHECK( "initialize()" , iso_muon_loose->initialize() );*/

  // Trigger
  m_TrigConfigTool = new TrigConf::xAODConfigTool("xAODConfigTool");
  //EL_RETURN_CHECK("initialize()", m_TrigConfigTool->initialize() );
  ToolHandle<TrigConf::ITrigConfigTool> configHandle(m_TrigConfigTool);
  EL_RETURN_CHECK("initialize()", configHandle->initialize() );  
 
  m_TrigDecTool = new Trig::TrigDecisionTool("TrigDecisionTool");
  //ToolHandle<Trig::ITrigDecisionTool> m_trigDec("Trig::TrigDecisionTool/TrigDecisionTool");
  //ToolHandle<Trig::ITrigDecisionTool> m_trigDec(m_TrigDecTool);
  //ToolHandle<Trig::ITrigDecisionTool> m_trigDec(ToolSvc.TrigDecisionTool);
  //ToolHandle<Trig::ITrigDecisionTool> m_trigDec(ToolSvc.ToolSvc.TrigDecisionTool);
  //ToolHandle<Trig::ITrigDecisionTool> m_trigDec("Trig::TrigDecisionTool/TrigDecisionTool");
  //ToolHandle<Trig::ITrigDecisionTool> m_trigDec(Trig::TrigDecisionTool/ToolSvc.TrigDecisionTool);
  EL_RETURN_CHECK("initialize()",m_TrigDecTool->setProperty("ConfigTool",configHandle));
  EL_RETURN_CHECK("initialize()",m_TrigDecTool->setProperty("TrigDecisionKey","xTrigDecision"));
  EL_RETURN_CHECK("initialize()",m_TrigDecTool->initialize());
  //m_trigDec = m_TrigDecTool;

  m_match_Tool = new Trig::MatchingTool( "TestMatchClass" );
  //m_match_Tool->msg().setLevel( MSG::DEBUG );
  m_match_Tool->setProperty("TrigDecisionTool","Trig::TrigDecisionTool/TrigDecisionTool");
  EL_RETURN_CHECK("initialize()", m_match_Tool->initialize() );

  // configuration and initialization example
  m_Pileup  = new CP::PileupReweightingTool("Pileup");
  std::vector<std::string> confFiles;
  std::vector<std::string> lcalcFiles;
  confFiles.push_back("$ROOTCOREBIN/data/MyAnalysis/prw_mc15c_PU_mc15b.root");    //PRW full 2015
  confFiles.push_back("$ROOTCOREBIN/data/MyAnalysis/Upsilon_prw.root");    //Upsilon prw
  confFiles.push_back("$ROOTCOREBIN/data/MyAnalysis/Sherpa.prw.root");    //Sherpa-signal prw
  lcalcFiles.push_back("$ROOTCOREBIN/data/MyAnalysis/ilumicalc_histograms_HLT_mu24_imedium_276262-284484_OflLumi-13TeV-009.root");  
  //lcalcFiles.push_back("$ROOTCOREBIN/data/MyAnalysis/ilumicalc_histograms_HLT_mu4_iloose_mu4_7invm9_noos_276262-284484_OflLumi-13TeV-009.root:HLT_mu4_iloose_mu4_7invm9_noos");  //PUT THEM BACK IF OR TRIGGER
  //lcalcFiles.push_back("$ROOTCOREBIN/data/MyAnalysis/ilumicalc_histograms_HLT_mu4_iloose_mu4_11invm60_noos_276262-284484_OflLumi-13TeV-009.root:HLT_mu4_iloose_mu4_11invm60_noos");
  //lcalcFiles.push_back("$ROOTCOREBIN/data/MyAnalysis/ilumicalc_histograms_HLT_mu6_iloose_mu6_11invm24_noos_276262-284484_OflLumi-13TeV-009.root:HLT_mu6_iloose_mu6_11invm24_noos");
  //lcalcFiles.push_back("$ROOTCOREBIN/data/MyAnalysis/ilumicalc_histograms_HLT_mu6_iloose_mu6_24invm60_noos_276262-284484_OflLumi-13TeV-009.root:HLT_mu6_iloose_mu6_24invm60_noos");
  m_Pileup->setProperty("ConfigFiles",confFiles);
  m_Pileup->setProperty("LumiCalcFiles",lcalcFiles);
  m_Pileup->setProperty("UsePeriodConfig","MC15");
  m_Pileup->setProperty("DataScaleFactor",1.0/1.09);
  m_Pileup->setProperty("DataScaleFactorUP",1/1.0);
  m_Pileup->setProperty("DataScaleFactorDOWN",1/1.18);
  //m_Pileup->setProperty("TrigDecisionTool",m_TrigDecTool);
  m_Pileup->setProperty("TrigDecisionTool", "Trig::TrigDecisionTool/TrigDecisionTool");
  EL_RETURN_CHECK("initialize()", m_Pileup->initialize() );
  PRWToolHandle = m_Pileup;
  
  // Muon Scale Factors (medium)
  m_mueff_corr_Medium = new CP::MuonEfficiencyScaleFactors("SF_Medium");
  m_mueff_corr_Medium->setProperty("WorkingPoint","Medium");
  //m_mueff_corr_Medium->msg().setLevel( MSG::DEBUG );
  //m_mueff_corr_Medium->setProperty("CalibrationRelease","170303_Moriond");
  //m_mueff_corr_Medium->setProperty("PileupReweightingTool",PRWToolHandle);
  //m_mueff_corr_Medium->setProperty("CalibrationRelease","170410_Moriond");
  m_mueff_corr_Medium->setProperty("CalibrationRelease","180214_Moriond207");
  EL_RETURN_CHECK( "initialize->()", m_mueff_corr_Medium->initialize() );

  // Muon Scale Factors (tight)
  /*m_mueff_corr_Tight = new CP::MuonEfficiencyScaleFactors("SF_Tight");
  m_mueff_corr_Tight->setProperty("WorkingPoint","Tight");
  //m_mueff_corr_Tight->msg().setLevel( MSG::DEBUG );
  //m_mueff_corr_Tight->setProperty("CalibrationRelease","170303_Moriond");
  //m_mueff_corr_Tight->setProperty("PileupReweightingTool",PRWToolHandle);
  //m_mueff_corr_Tight->setProperty("CalibrationRelease","170410_Moriond");
  m_mueff_corr_Tight->setProperty("CalibrationRelease","180214_Moriond207");
  EL_RETURN_CHECK( "initialize->()", m_mueff_corr_Tight->initialize() );*/

  //Muon Trigger Scale Factors (medium)
  /*m_mutrig_sf_Medium = new CP::MuonTriggerScaleFactors("TrigSFClass_Medium");
  m_mutrig_sf_Medium->setProperty("MuonQuality","Medium");
  //m_mutrig_sf_Medium->msg().setLevel( MSG::DEBUG );
  m_mutrig_sf_Medium->setProperty("MC", "mc15c" );
  m_mutrig_sf_Medium->setProperty("Year", "2015" );
  //m_mutrig_sf_Medium->setProperty("Isolation", "IsoGradient" ); //Same SFs used for different isolation points
  //m_mutrig_sf_Medium->setProperty("PileupReweightingTool",PRWToolHandle);
  EL_RETURN_CHECK("initialize()", m_mutrig_sf_Medium->initialize() );*/

  //Muon Trigger Scale Factors (tight)
  /*m_mutrig_sf_Tight = new CP::MuonTriggerScaleFactors("TrigSFClass_Tight");
  m_mutrig_sf_Tight->setProperty("MuonQuality", "Tight" );
  m_mutrig_sf_Tight->setProperty("MC", "mc15c" );
  m_mutrig_sf_Tight->setProperty("Year", "2015" );
  m_mutrig_sf_Tight->setProperty("Isolation", "LooseTrackOnly" );
  EL_RETURN_CHECK("initialize()", m_mutrig_sf_Tight->initialize() );*/

  //Muon Iso Scale Factor (LooseTrackOnly)
  /*m_muiso_sf_Loose = new CP::MuonEfficiencyScaleFactors("IsoSF_Loose");
  m_muiso_sf_Loose->setProperty("WorkingPoint","LooseTrackOnlyIso");
  //m_muiso_sf_Loose->setProperty("PileupReweightingTool",PRWToolHandle);
  EL_RETURN_CHECK( "initialize->()", m_muiso_sf_Loose->initialize() );

  //Muon Iso Scale Factor (FixedCutLoose)
  m_muiso_sf_FixedCutLoose = new CP::MuonEfficiencyScaleFactors("IsoSF_FixedCutLoose");
  m_muiso_sf_FixedCutLoose->setProperty("WorkingPoint","FixedCutLooseIso");
  //m_muiso_sf_FixedCutLoose->setProperty("PileupReweightingTool",PRWToolHandle);
  EL_RETURN_CHECK( "initialize->()", m_muiso_sf_FixedCutLoose->initialize() );*/

  //Muon Iso Scale Factor (FixedCuTight)
  m_muiso_sf_FixedCutTight = new CP::MuonEfficiencyScaleFactors("IsoSF_FixedCutTight");
  m_muiso_sf_FixedCutTight->setProperty("WorkingPoint","FixedCutTightIso");
  //m_muiso_sf_FixedCutTight->setProperty("PileupReweightingTool",PRWToolHandle);
  EL_RETURN_CHECK( "initialize->()", m_muiso_sf_FixedCutTight->initialize() );

  //Muon Iso Scale Factor (FixedTightCutTrackOnly)
  /*m_muiso_sf_Tight = new CP::MuonEfficiencyScaleFactors("IsoSF_Tight");
  m_muiso_sf_Tight->setProperty("WorkingPoint","FixedCutTightTrackOnlyIso");
  //m_muiso_sf_Tight->setProperty("PileupReweightingTool",PRWToolHandle);
  EL_RETURN_CHECK( "initialize->()", m_muiso_sf_Tight->initialize() );*/

  //Muon Iso Scale Factor (FixedTightCutTrackOnly)
  /*m_muiso_sf_Gradient = new CP::MuonEfficiencyScaleFactors("IsoSF_Gradient");
  m_muiso_sf_Gradient->setProperty("WorkingPoint","GradientIso");
  //m_muiso_sf_Gradient->setProperty("PileupReweightingTool",PRWToolHandle);
  EL_RETURN_CHECK( "initialize->()", m_muiso_sf_Gradient->initialize() );*/

  //Muon TTVA
  /*m_TTVA_effi_corr = new CP::MuonEfficiencyScaleFactors("TTVA");
  m_TTVA_effi_corr->setProperty("WorkingPoint","TTVA");
  m_TTVA_effi_corr->setProperty("CalibrationRelease","170410_Moriond");
  //m_TTVA_effi_corr->setProperty("PileupReweightingTool",PRWToolHandle);
  EL_RETURN_CHECK("initialize()", m_TTVA_effi_corr->initialize() );*/

  // kFactor Tool
  m_kFTool = new LPXKfactorTool("LPXKfactorTool");
  m_kFTool->setProperty("isMC15",true);
  m_kFTool->setProperty("applyEWCorr",true);
  m_kFTool->setProperty("applyPICorr",false);
  m_kFTool->initialize();

  //Trigger SF (private class)
  m_sfclass = new LowPtMuonTriggerSF();
  
  //m_selTool_l = new InDet::InDetTrackSelectionTool("TrackSelection_l");
  //m_selTool_l->setProperty("CutLevel", "Loose");
  //m_selTool_l->msg().setLevel( MSG::DEBUG );
  //m_selTool_l->initialize();

  //InDetTrackSystematics tools
  //m_smearingTool_d0 = 0;
  m_smearingTool_d0 = new InDet::InDetTrackSmearingTool("SmearingTool_d0");
  m_smearingTool_d0->setProperty( "Seed", 1234 ); // set the seed of the RNG
  m_smearingTool_d0->msg().setLevel( MSG::WARNING );
  m_smearingTool_d0->initialize();
  CP::SystematicSet systSet_d0 = {InDet::TrackSystematicMap[InDet::TRK_RES_D0_MEAS]};      //Nominal run
  //CP::SystematicSet systSet_d0 = {InDet::TrackSystematicMap[InDet::TRK_RES_D0_MEAS_DOWN]};   //Up/down variation
  m_smearingTool_d0->applySystematicVariation(systSet_d0);

  m_smearingTool_z0 = new InDet::InDetTrackSmearingTool("SmearingTool_z0");
  m_smearingTool_z0->setProperty( "Seed", 4321 ); // set the seed of the RNG
  m_smearingTool_z0->msg().setLevel( MSG::WARNING );
  m_smearingTool_z0->initialize();
  CP::SystematicSet systSet_z0 = {InDet::TrackSystematicMap[InDet::TRK_RES_Z0_MEAS]};                                                                                          
  //CP::SystematicSet systSet_z0 = {InDet::TrackSystematicMap[InDet::TRK_RES_Z0_MEAS_DOWN]};    
  m_smearingTool_z0->applySystematicVariation(systSet_z0);   


  //m_smearingTool_z0 = 0;

  //m_smearingTool_z0->msg().setLevel( MSG::DEBUG );
  //m_smearingTool_z0->setProperty( "Seed", 1234 ); // set the seed of the RNG
  
  //m_smearingTool_z0->initialize();
  //CP::SystematicSet systSet_z0 = {InDet::TrackSystematicMap[InDet::TRK_RES_Z0_MEAS]};
  //m_smearingTool_z0->applySystematicVariation(systSet_z0);
 
  //m_biasingTool = new InDet::InDetTrackBiasingTool("BiasingTool");
  //m_biasingTool->msg().setLevel( MSG::DEBUG );
  //m_biasingTool->initialize();

  //m_originTool = new InDet::InDetTrackTruthOriginTool("originTool");
  //m_originTool->initialize();

  //m_filterTool = new InDet::InDetTrackTruthFilterTool("FilterTool");
  //m_filterTool->msg().setLevel( MSG::DEBUG );
  //m_filterTool->setProperty( "Seed", 1234 ); // set the seed of the RNG
  //m_filterTool->setProperty("trackOriginTool", ToolHandle< InDet::IInDetTrackTruthOriginTool >( m_originTool ));
  //m_filterTool->initialize();

  // Systematic Variations
  const CP::SystematicRegistry& registry = CP::SystematicRegistry::getInstance();
  const CP::SystematicSet& recommendedSystematics = registry.recommendedSystematics(); // get list of recommended systematics

  // this is the nominal set, no systematic
  m_sysList.push_back(CP::SystematicSet());
  
  // loop over recommended systematics
  for(CP::SystematicSet::const_iterator sysItr = recommendedSystematics.begin(); sysItr != recommendedSystematics.end(); ++sysItr){
    TString syst_name = TString(sysItr->name());
    //if( !syst_name.BeginsWith("MU") || !syst_name.BeginsWith("LPX") || !syst_name.BeginsWith("TRK") ) continue; 
    //if( !syst_name.BeginsWith("MU") ) continue; 
    std::cout << "Systematic: " << (*sysItr).name() << std::endl;
    m_sysList.push_back( CP::SystematicSet() );
    m_sysList.back().insert( *sysItr );
  } // end for loop over recommended systematics

  //m_CIReweight = new DYReweighting("DYReweighting");
  //m_CIReweight->initialize();

  std::cout<<"INITIALIZE"<<std::endl;

  return EL::StatusCode::SUCCESS;
} // end of initialise

