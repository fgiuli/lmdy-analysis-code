#include <MyAnalysis/LowPtMuonTriggerSF.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <iostream>
#include <cmath>

LowPtMuonTriggerSF::LowPtMuonTriggerSF(){
  m_eps=1.0e-6;
  Initialize("$ROOTCOREBIN/data/MyAnalysis/Z_jpsi_out.root"); 
}

LowPtMuonTriggerSF::~LowPtMuonTriggerSF(){
  m_file->Close();
}

bool LowPtMuonTriggerSF::Initialize(const std::string& filename){
 
  
  m_file = new TFile(filename.c_str());
  if (!m_file) {
    std::cout << "LowPtMuonTriggerSF    ERROR         File not found : " << filename << std::endl;
    return false;
  }
  // m_histogram[idata][izorpsi][itrig][isyst][icharge]

  // default parameters:
  m_pt_Z_min = 15;
  m_phi_modulation = true;
  m_interpolate = false;
  
  // NOM Z
  m_histogram[1][1][0][0][0] = (TH2D*) m_file->Get("data_Z_pteta_qp_mu4_0");
  m_histogram[1][1][1][0][0] = (TH2D*) m_file->Get("data_Z_pteta_qp_mu6_0");
  m_histogram[1][1][0][0][1] = (TH2D*) m_file->Get("data_Z_pteta_qm_mu4_0");
  m_histogram[1][1][1][0][1] = (TH2D*) m_file->Get("data_Z_pteta_qm_mu6_0");
  m_histogram[0][1][0][0][0] = (TH2D*) m_file->Get("mc_Z_pteta_qp_mu4_0");
  m_histogram[0][1][1][0][0] = (TH2D*) m_file->Get("mc_Z_pteta_qp_mu6_0");
  m_histogram[0][1][0][0][1] = (TH2D*) m_file->Get("mc_Z_pteta_qm_mu4_0");
  m_histogram[0][1][1][0][1] = (TH2D*) m_file->Get("mc_Z_pteta_qm_mu6_0");

  // syst1 UP Z histos
  m_histogram[1][1][0][1][0] = (TH2D*) m_file->Get("data_Z_pteta_qp_mu4_1");
  m_histogram[1][1][1][1][0] = (TH2D*) m_file->Get("data_Z_pteta_qp_mu6_1");
  m_histogram[1][1][0][1][1] = (TH2D*) m_file->Get("data_Z_pteta_qm_mu4_1");
  m_histogram[1][1][1][1][1] = (TH2D*) m_file->Get("data_Z_pteta_qm_mu6_1");
  m_histogram[0][1][0][1][0] = (TH2D*) m_file->Get("mc_Z_pteta_qp_mu4_1");
  m_histogram[0][1][1][1][0] = (TH2D*) m_file->Get("mc_Z_pteta_qp_mu6_1");
  m_histogram[0][1][0][1][1] = (TH2D*) m_file->Get("mc_Z_pteta_qm_mu4_1");
  m_histogram[0][1][1][1][1] = (TH2D*) m_file->Get("mc_Z_pteta_qm_mu6_1");

  // syst1 DOWN Z histos
  m_histogram[1][1][0][2][0] = (TH2D*) m_file->Get("data_Z_pteta_qp_mu4_2");
  m_histogram[1][1][1][2][0] = (TH2D*) m_file->Get("data_Z_pteta_qp_mu6_2");
  m_histogram[1][1][0][2][1] = (TH2D*) m_file->Get("data_Z_pteta_qm_mu4_2");
  m_histogram[1][1][1][2][1] = (TH2D*) m_file->Get("data_Z_pteta_qm_mu6_2");
  m_histogram[0][1][0][2][0] = (TH2D*) m_file->Get("mc_Z_pteta_qp_mu4_2");
  m_histogram[0][1][1][2][0] = (TH2D*) m_file->Get("mc_Z_pteta_qp_mu6_2");
  m_histogram[0][1][0][2][1] = (TH2D*) m_file->Get("mc_Z_pteta_qm_mu4_2");
  m_histogram[0][1][1][2][1] = (TH2D*) m_file->Get("mc_Z_pteta_qm_mu6_2");
    
  // syst2 UP Z histos
  m_histogram[1][1][0][3][0] = (TH2D*) m_file->Get("data_Z_pteta_qp_mu4_3");
  m_histogram[1][1][1][3][0] = (TH2D*) m_file->Get("data_Z_pteta_qp_mu6_3");
  m_histogram[1][1][0][3][1] = (TH2D*) m_file->Get("data_Z_pteta_qm_mu4_3");
  m_histogram[1][1][1][3][1] = (TH2D*) m_file->Get("data_Z_pteta_qm_mu6_3");
  m_histogram[0][1][0][3][0] = (TH2D*) m_file->Get("mc_Z_pteta_qp_mu4_3");
  m_histogram[0][1][1][3][0] = (TH2D*) m_file->Get("mc_Z_pteta_qp_mu6_3");
  m_histogram[0][1][0][3][1] = (TH2D*) m_file->Get("mc_Z_pteta_qm_mu4_3");
  m_histogram[0][1][1][3][1] = (TH2D*) m_file->Get("mc_Z_pteta_qm_mu6_3");

  // syst2 DOWN Z histos
  m_histogram[1][1][0][4][0] = (TH2D*) m_file->Get("data_Z_pteta_qp_mu4_4");
  m_histogram[1][1][1][4][0] = (TH2D*) m_file->Get("data_Z_pteta_qp_mu6_4");
  m_histogram[1][1][0][4][1] = (TH2D*) m_file->Get("data_Z_pteta_qm_mu4_4");
  m_histogram[1][1][1][4][1] = (TH2D*) m_file->Get("data_Z_pteta_qm_mu6_4");
  m_histogram[0][1][0][4][0] = (TH2D*) m_file->Get("mc_Z_pteta_qp_mu4_4");
  m_histogram[0][1][1][4][0] = (TH2D*) m_file->Get("mc_Z_pteta_qp_mu6_4");
  m_histogram[0][1][0][4][1] = (TH2D*) m_file->Get("mc_Z_pteta_qm_mu4_4");
  m_histogram[0][1][1][4][1] = (TH2D*) m_file->Get("mc_Z_pteta_qm_mu6_4");

  // nom JPSI
  m_histogram[1][0][0][0][0] = (TH2D*) m_file->Get("data_PSI_pteta_qp_mu4_0");
  m_histogram[1][0][1][0][0] = (TH2D*) m_file->Get("data_PSI_pteta_qp_mu6_0");
  m_histogram[1][0][0][0][1] = (TH2D*) m_file->Get("data_PSI_pteta_qm_mu4_0");
  m_histogram[1][0][1][0][1] = (TH2D*) m_file->Get("data_PSI_pteta_qm_mu6_0");
  m_histogram[0][0][0][0][0] = (TH2D*) m_file->Get("mc_PSI_pteta_qp_mu4_0");
  m_histogram[0][0][1][0][0] = (TH2D*) m_file->Get("mc_PSI_pteta_qp_mu6_0");
  m_histogram[0][0][0][0][1] = (TH2D*) m_file->Get("mc_PSI_pteta_qm_mu4_0");
  m_histogram[0][0][1][0][1] = (TH2D*) m_file->Get("mc_PSI_pteta_qm_mu6_0");

    // sys1 up JPSI
  m_histogram[1][0][0][1][0] = (TH2D*) m_file->Get("data_PSI_pteta_qp_mu4_1");
  m_histogram[1][0][1][1][0] = (TH2D*) m_file->Get("data_PSI_pteta_qp_mu6_1");
  m_histogram[1][0][0][1][1] = (TH2D*) m_file->Get("data_PSI_pteta_qm_mu4_1");
  m_histogram[1][0][1][1][1] = (TH2D*) m_file->Get("data_PSI_pteta_qm_mu6_1");
  m_histogram[0][0][0][1][0] = (TH2D*) m_file->Get("mc_PSI_pteta_qp_mu4_1");
  m_histogram[0][0][1][1][0] = (TH2D*) m_file->Get("mc_PSI_pteta_qp_mu6_1");
  m_histogram[0][0][0][1][1] = (TH2D*) m_file->Get("mc_PSI_pteta_qm_mu4_1");
  m_histogram[0][0][1][1][1] = (TH2D*) m_file->Get("mc_PSI_pteta_qm_mu6_1");

  // sys1 down JPSI
  m_histogram[1][0][0][2][0] = (TH2D*) m_file->Get("data_PSI_pteta_qp_mu4_2");
  m_histogram[1][0][1][2][0] = (TH2D*) m_file->Get("data_PSI_pteta_qp_mu6_2");
  m_histogram[1][0][0][2][1] = (TH2D*) m_file->Get("data_PSI_pteta_qm_mu4_2");
  m_histogram[1][0][1][2][1] = (TH2D*) m_file->Get("data_PSI_pteta_qm_mu6_2");
  m_histogram[0][0][0][2][0] = (TH2D*) m_file->Get("mc_PSI_pteta_qp_mu4_2");
  m_histogram[0][0][1][2][0] = (TH2D*) m_file->Get("mc_PSI_pteta_qp_mu6_2");
  m_histogram[0][0][0][2][1] = (TH2D*) m_file->Get("mc_PSI_pteta_qm_mu4_2");
  m_histogram[0][0][1][2][1] = (TH2D*) m_file->Get("mc_PSI_pteta_qm_mu6_2");

  // sys2 up JPSI
  m_histogram[1][0][0][3][0] = (TH2D*) m_file->Get("data_PSI_pteta_qp_mu4_3");
  m_histogram[1][0][1][3][0] = (TH2D*) m_file->Get("data_PSI_pteta_qp_mu6_3");
  m_histogram[1][0][0][3][1] = (TH2D*) m_file->Get("data_PSI_pteta_qm_mu4_3");
  m_histogram[1][0][1][3][1] = (TH2D*) m_file->Get("data_PSI_pteta_qm_mu6_3");
  m_histogram[0][0][0][3][0] = (TH2D*) m_file->Get("mc_PSI_pteta_qp_mu4_3");
  m_histogram[0][0][1][3][0] = (TH2D*) m_file->Get("mc_PSI_pteta_qp_mu6_3");
  m_histogram[0][0][0][3][1] = (TH2D*) m_file->Get("mc_PSI_pteta_qm_mu4_3");
  m_histogram[0][0][1][3][1] = (TH2D*) m_file->Get("mc_PSI_pteta_qm_mu6_3");

  // sys2 down JPSI
  m_histogram[1][0][0][4][0] = (TH2D*) m_file->Get("data_PSI_pteta_qp_mu4_4");
  m_histogram[1][0][1][4][0] = (TH2D*) m_file->Get("data_PSI_pteta_qp_mu6_4");
  m_histogram[1][0][0][4][1] = (TH2D*) m_file->Get("data_PSI_pteta_qm_mu4_4");
  m_histogram[1][0][1][4][1] = (TH2D*) m_file->Get("data_PSI_pteta_qm_mu6_4");
  m_histogram[0][0][0][4][0] = (TH2D*) m_file->Get("mc_PSI_pteta_qp_mu4_4");
  m_histogram[0][0][1][4][0] = (TH2D*) m_file->Get("mc_PSI_pteta_qp_mu6_4");
  m_histogram[0][0][0][4][1] = (TH2D*) m_file->Get("mc_PSI_pteta_qm_mu4_4");
  m_histogram[0][0][1][4][1] = (TH2D*) m_file->Get("mc_PSI_pteta_qm_mu6_4");
  
  std::cout <<"LowPtMuonTriggerSF    INFO                 Using Z maps above pt= " << m_pt_Z_min<<  std::endl;


  // phi modulation
  std::cout <<"LowPtMuonTriggerSF    INFO                 Phi Modulation is active " <<  std::endl;

  
  m_phimod[1][0] =  (TH2D*) m_file->Get("data_Z_phieta_mu4_0");
  m_phimod[0][0] =  (TH2D*) m_file->Get("mc_Z_phieta_mu4_0");
  m_phimod[1][1] =  (TH2D*) m_file->Get("data_Z_phieta_mu6_0");
  m_phimod[0][1] =  (TH2D*) m_file->Get("mc_Z_phieta_mu6_0");
  
  for (int itrig=0; itrig<2; itrig++){
    for (int idata=0; idata<2; idata++){
      for (int ieta=1; ieta<=m_phimod[idata][itrig]->GetNbinsX(); ieta++){
	double sum = 0;
	for (int iphi=1; iphi<=m_phimod[idata][itrig]->GetNbinsY(); iphi++){
	  sum += m_phimod[idata][itrig]->GetBinContent(ieta,iphi);
	}
	if (sum>m_eps){
	  double oneovercmean = m_phimod[idata][itrig]->GetNbinsY() / sum;	  
	  for (int iphi=1; iphi<=m_phimod[idata][itrig]->GetNbinsY(); iphi++){
	    double c=oneovercmean*m_phimod[idata][itrig]->GetBinContent(ieta,iphi);
	    m_phimod[idata][itrig]->SetBinContent(ieta,iphi,c);
	  }	
	} else {
	  std::cout << "LowPtMuonTriggerSF        WARNING           Phi-eta map contains empty eta reqions " << sum <<  std::endl;
	  for (int iphi=1; iphi<=m_phimod[idata][itrig]->GetNbinsY(); iphi++){
	    m_phimod[idata][itrig]->SetBinContent(ieta,iphi,1.0);
	  }
	}
      }
    }
  }

  // id efficiency
  m_apply_id_eff = true;
  m_id_eff[0][0] = 0.9988; // barrel MC
  m_id_eff[0][1] = 0.9982; // barrel data
  m_id_eff[1][0] = 0.9979; // EC MC
  m_id_eff[1][1] = 0.9953; // EC data

  
  std::cout <<"LowPtMuonTriggerSF  version 0.7  Inizialized" << std::endl;    
  return true;
}

bool  LowPtMuonTriggerSF::getMuonEfficiency(double &eff, const TLorentzVector mu, const int charge, const std::string& trigger, const std::string& systematic, const std::string& dataormc){

  int idata=0;
  int itrig=0;
  int isyst=0;
  int izorpsi=0;
  int icharge=0;
  int ierr=0;
  
  eff=0;
  //cout << "getMuonEfficiency CALLED" << endl;
  
  if (dataormc=="data"||dataormc=="DATA"){
    idata=1;
  }else if (dataormc=="mc"||dataormc=="MC"){
    idata=0;
  } else {
    std::cout << "LowPtMuonTriggerSF    ERROR         Data Type supported : " << dataormc << std::endl;
    return false;
  }
  
  if (trigger=="mu4"){
    itrig=0;
  }else if (trigger=="mu6"){
    itrig=1;
  }else{
    std::cout << "LowPtMuonTriggerSF    ERROR         Trigger not supported : " << trigger << std::endl;
    return false;
  }

  if (systematic==""||systematic=="nominal"||systematic=="NOMINAL"){
    isyst=0;
  }else if (systematic=="syst1_up"||systematic=="SYST1_UP"){
    isyst=1;
  }else if (systematic=="syst1_down"||systematic=="SYST1_DOWN"){
    isyst=2;
  }else if (systematic=="syst2_up"||systematic=="SYST2_UP"){
    isyst=3;
  }else if (systematic=="syst2_down"||systematic=="SYST2_DOWN"){
    isyst=4;
  }else if (systematic=="stat_up"||systematic=="STAT_UP"){
    isyst=0;
    ierr=+1;
  }else if (systematic=="stat_down"||systematic=="STAT_DOWN"){
    isyst=0;
    ierr=-1;
  }else if (systematic=="stat_error"||systematic=="STAT_ERROR"){
    isyst=0;
    ierr=999;
  }else{
    std::cout << "LowPtMuonTriggerSF    ERROR         Error Type not supported : " << systematic << std::endl;
    return false;
  }

  if (charge==-1){
    icharge=1; 
  } else if (charge==1) {
    icharge=0;
  } else {
    std::cout << "LowPtMuonTriggerSF    ERROR         Charge not supported : " << charge << std::endl;
    return false;
  }



  
  double pt=mu.Pt();  // all pts are in GeV
  double eta=mu.Eta();
  if (charge<0) eta=-eta; // histograms are as a function of q*eta
  double phi=mu.Phi();
  if (phi>M_PI) phi-=2*M_PI;  // histograms are between -Pi and Pi
  
  if (pt>m_pt_Z_min) {
    izorpsi=1;
  }else{
    izorpsi=0;
  }
  
  // for pt overflow use highest-pt bin
  if (izorpsi==1&&pt>125) pt=124;
  if (izorpsi==0&&pt>20) pt=19;
  
  if (eta>2.4||eta<-2.4){
   std::cout <<"LowPtMuonTriggerSF    ERROR         efficiency not available for |eta|>2.4p" << eta << std::endl;  
    return false;
  }
  
  if (pt<4&&idata==0){
    std::cout <<"LowPtMuonTriggerSF    ERROR         MC efficiency not available for pt<4 GeV " << pt << std::endl;  
    return false;
  }
  
  if (!m_histogram[idata][izorpsi][itrig][isyst][icharge])  {
    std::cout <<"LowPtMuonTriggerSF    ERROR         Histogram not found [idata][izorpsi][itrig][isyst][icharge] = "
	      << idata   << " "
	      << izorpsi << " "
      	      << itrig   << " "
      	      << isyst   << " "
      	      << icharge << std::endl;
    return false;
  }

  /*
   std::cout <<"[idata][izorpsi][itrig][isyst][icharge] = "<< idata << " "
	      << izorpsi << " "
      	      << itrig << " "
      	      << isyst << " "
      	      << icharge << std::endl;
 */

  int bin = m_histogram[idata][izorpsi][itrig][isyst][icharge]->FindFixBin(eta,pt);
  if (!m_interpolate){
    eff = m_histogram[idata][izorpsi][itrig][isyst][icharge]->GetBinContent(bin); 
  } else {
    eff = m_histogram[idata][izorpsi][itrig][isyst][icharge]->Interpolate(eta,pt); 
  }

 

  if (ierr!=0) {
    if (ierr>99){
      eff = m_histogram[idata][izorpsi][itrig][isyst][icharge]->GetBinError(bin);
    } else {
      eff += ierr*m_histogram[idata][izorpsi][itrig][isyst][icharge]->GetBinError(bin);  
    }
  }


  // apply id efficiency
  if (m_apply_id_eff&&izorpsi==0){
    // correct for ID efficiency is measurement is based on J/Psi
    if (abs(mu.Eta())<1.05){
      eff *= m_id_eff[0][idata];
    } else {
      eff *= m_id_eff[1][idata];      
    }
    // if data add +- 3 per mille uncertainty to SYST1 (linearly for simplicity)
    if (idata==1&&isyst==1) eff *= 1.003;
    if (idata==1&&isyst==2) eff *= 0.997;
  }

 
  if (m_phi_modulation){
    int bin = m_phimod[idata][itrig]->FindFixBin(eta,phi);
    //std::cout << bin << " " <<  m_phimod[idata][itrig]->GetBinContent(bin) << endl;
    eff *=  m_phimod[idata][itrig]->GetBinContent(bin); 
  }
  
  
  //  std::cout << "eff = "<< eff << std::endl;
  return true;
}


bool LowPtMuonTriggerSF::getTriggerSF(double &sf, const TLorentzVector mu, const int charge, const std::string& trigger,  const std::string& systematic){
 
  double eff_data=1;
  double eff_mc=1;

  sf=1;
  

  // STAT ERROR:
  if (systematic=="stat_up"||systematic=="STAT_UP"||systematic=="stat_down"||systematic=="STAT_DOWN"||
      systematic=="stat_error"||systematic=="STAT_ERROR"){

    double err_data =0;
    if (!getMuonEfficiency(eff_data, mu, charge, trigger, "nominal", "data")) return false;
    if (!getMuonEfficiency(err_data, mu, charge, trigger, "stat_error", "data")) return false;;
    
    double err_mc =0;
    if (!getMuonEfficiency(eff_mc, mu, charge, trigger, "nominal", "mc")) return false;;
    if (!getMuonEfficiency(err_mc, mu, charge, trigger, "stat_error", "mc")) return false;;

    
    
    if (eff_mc>m_eps){
      sf = eff_data / eff_mc;
      double sf_error = sf;
      if (eff_data>m_eps) sf_error = sf * pow( err_data*err_data/eff_data/eff_data + err_mc*err_mc/eff_mc/eff_mc,   0.5);
      if (systematic=="stat_up"||systematic=="STAT_UP") {
	sf+=sf_error;
      }else if (systematic=="stat_down"||systematic=="STAT_DOWN"){
	if (sf_error<sf) {
	  sf-=sf_error;
	}else{
	  sf=0;
	}
      }else{
	sf = sf_error;
      }
    } else {
      sf=1;
    }
    // SYSTEMATICS :
  } else {				            
    if (!getMuonEfficiency(eff_data, mu, charge, trigger, systematic, "data")) return false;
    if (!getMuonEfficiency(eff_mc, mu, charge, trigger, systematic, "mc")) return false;
    if (eff_mc>m_eps){
      sf = eff_data / eff_mc;
    }else{
      sf=1;
    }
  }

  /*
  int itrig=0;
  if (m_phi_modulation){
    if (trigger=="mu4"){
      itrig=0;
    }else if (trigger=="mu6"){
      itrig=1;
    }else{
      std::cout << "LowPtMuonTriggerSF    ERROR         Trigger not supported : " << trigger << std::endl;
      return false;
    }
    int bin = m_phimod[itrig]->FindFixBin(mu.Eta(),mu.Phi());
    //std::cout << bin << " " <<  m_phimod[itrig]->GetBinContent(bin) << endl;
    sf *=  m_phimod[itrig]->GetBinContent(bin); 
  }
  */

  
  if (sf<0||sf>99) std::cout << "  LowPtMuonTriggerSF   WARNING  : SF = " << sf << "eff_data, eff_mc = " << eff_data <<" , "<< eff_mc
			     << " eta,phi,pt = " << mu.Eta() <<","<<mu.Phi()<<","<<mu.Pt() << std::endl;
  
  return true;
}

bool LowPtMuonTriggerSF::getDimuonSF(double &sf,
				     const TLorentzVector mu1, const int charge1,
				     const TLorentzVector mu2, const int charge2,
				     const std::string& trigger,  const std::string& systematic){

  sf=1.0;
  if (trigger=="2mu4"){
    double sf1 = 1;
    getTriggerSF(sf1, mu1, charge1, "mu4", systematic);
    sf*=sf1;
    double sf2 = 1;
    getTriggerSF(sf2, mu2, charge2, "mu4", systematic);
    sf*=sf2;
  } else if  (trigger=="2mu6"){
    double sf1 = 1;
    getTriggerSF(sf1, mu1, charge1, "mu6", systematic);
    sf*=sf1;
    double sf2 = 1;
    getTriggerSF(sf2, mu2, charge2, "mu6", systematic);
    sf*=sf2;
  } else {
    std::cout <<"LowPtMuonTriggerSF    ERROR         Dimuon trigger not available: " << trigger << std::endl;  
    return false;
  }
  return true;
}

