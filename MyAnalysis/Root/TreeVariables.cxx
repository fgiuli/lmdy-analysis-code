#include <MyAnalysis/MyxAODAnalysis.h>

void MyxAODAnalysis :: SetTreeBranches(){

  //dimuonTree->Branch( "Run Number"         ,  &Run_Number         );
  //dimuonTree->Branch( "Event Number"         ,  &Event_Number      );
  //dimuonTree->Branch( "Lumi block"         ,  &Lumi_Block         );

  /*dimuonTree->Branch( "PDF_X1"         ,  &PDF_X1         );
  dimuonTree->Branch( "PDF_X2"         ,  &PDF_X2         );
  dimuonTree->Branch( "PDF_Q2"         ,  &PDF_Q2         );  
  dimuonTree->Branch( "PDGID1"         ,  &PDGID1         );
  dimuonTree->Branch( "PDGID2"         ,  &PDGID2         );
  dimuonTree->Branch( "PDFID1"         ,  &PDFID1         );
  dimuonTree->Branch( "PDFID2"         ,  &PDFID2         );
  dimuonTree->Branch( "BornMass"         ,  &bornMass         );
  dimuonTree->Branch( "eventNumber"         ,  &eventNumber         );
  dimuonTree->Branch( "run"           ,  &run           );
  dimuonTree->Branch( "lbn"           ,  &lbn           );
  dimuonTree->Branch( "mcChannelNumber"           ,  &mcChannelNumber           );
  dimuonTree->Branch( "avgIntxCross"  ,  &avgIntxCross  );
  dimuonTree->Branch( "actIntxCross"  ,  &actIntxCross  );
  dimuonTree->Branch( "mcweight"      ,  &mcweight      );
  dimuonTree->Branch( "pileupweight"  ,  &pileupweight  );
  dimuonTree->Branch( "kfactorweight"  ,  &kfactorweight  );
  dimuonTree->Branch( "weight"        ,  &weight        );
  dimuonTree->Branch( "isEvtGood"     ,  &isEvtGood     );
  dimuonTree->Branch( "isEvtClean"     ,  &isEvtClean     );
  dimuonTree->Branch( "isVertexGood"  ,  &isVertexGood  );
  dimuonTree->Branch( "isTrigGood"    ,  &isTrigGood    );
  dimuonTree->Branch( "isOSGood"      ,  &isOSGood      );
  dimuonTree->Branch( "dilepMass"     ,  &dilepMass     );
  dimuonTree->Branch( "dilepPt"       ,  &dilepPt       );
  dimuonTree->Branch( "dilepEta"      ,  &dilepEta      );
  dimuonTree->Branch( "dilepPhi"      ,  &dilepPhi      );
  //dimuonTree->Branch( "HLT_mu50" ,  &HLT_mu50 );
  //dimuonTree->Branch( "HLT_mu26i" ,  &HLT_mu26i );

  dimuonTree->Branch( "HLT_mu4_iloose_mu4_11invm60_noos" , &HLT_mu4_iloose_mu4_11invm60_noos);
  dimuonTree->Branch( "HLT_mu4_iloose_mu4_7invm9_noos",&HLT_mu4_iloose_mu4_7invm9_noos);
  dimuonTree->Branch( "HLT_mu4_iloose_mu4_11invm60_noos_L1MU6_2MU4", &HLT_mu4_iloose_mu4_11invm60_noos_L1MU6_2MU4);
  dimuonTree->Branch( "HLT_mu4_iloose_mu4_7invm9_noos_L1MU6_2MU4" , &HLT_mu4_iloose_mu4_7invm9_noos_L1MU6_2MU4);
  dimuonTree->Branch( "HLT_mu6_iloose_mu6_11invm24_noos" , &HLT_mu6_iloose_mu6_11invm24_noos); 
  dimuonTree->Branch( "HLT_mu6_iloose_mu6_24invm60_noos" , &HLT_mu6_iloose_mu6_24invm60_noos);*/
				   		     
  /* Leptons */			   		     
  /*dimuonTree->Branch( "Mu_TriggerMatched"        ,  &Mu_TriggerMatched        );
  //dimuonTree->Branch( "Mu_Quality"        ,  &Mu_Quality        ); 
  dimuonTree->Branch( "Mu_Quality_TIGHT"        ,  &Mu_Quality_TIGHT        ); 
  dimuonTree->Branch( "Mu_Quality_MEDIUM"        ,  &Mu_Quality_MEDIUM        ); 
  dimuonTree->Branch( "Mu_Charge"        ,  &Mu_Charge        );
  //dimuonTree->Branch( "Mu_isHighPt"        ,  &Mu_isHighPt        );
  dimuonTree->Branch( "Mu_isHighPt_TIGHT"        ,  &Mu_isHighPt_TIGHT        );
  dimuonTree->Branch( "Mu_isHighPt_MEDIUM"        ,  &Mu_isHighPt_MEDIUM        );
  dimuonTree->Branch( "Mu_pt"        ,  &Mu_pt        );
  dimuonTree->Branch( "Mu_eta"       ,  &Mu_eta       );
  dimuonTree->Branch( "Mu_phi"       ,  &Mu_phi       );
  dimuonTree->Branch( "Mu_d0"        ,  &Mu_d0        );
  dimuonTree->Branch( "Mu_d0sig"     ,  &Mu_d0sig     );
  dimuonTree->Branch( "Mu_z0"        ,  &Mu_z0        );
  dimuonTree->Branch( "Mu_deltaz0"   ,  &Mu_deltaz0   );
  //dimuonTree->Branch( "Mu_Iso"       ,  &Mu_Iso       );
  dimuonTree->Branch( "Mu_Iso_fixed"       ,  &Mu_Iso_fixed       );
  dimuonTree->Branch( "Mu_Iso_track"       ,  &Mu_Iso_track       );

  dimuonTree->Branch( "pTvar_trk_20"       ,  &pTvar_trk_20       );
  dimuonTree->Branch( "pTvar_trk_30"       ,  &pTvar_trk_30       );

  dimuonTree->Branch( "Mu_TrigSF"    ,  &Mu_TrigSF    );
  dimuonTree->Branch( "Mu_RecoSF"    ,  &Mu_RecoSF    );
  dimuonTree->Branch( "Mu_IsoSF"     ,  &Mu_IsoSF     );*/
}

void MyxAODAnalysis :: ResetVariables(){

  //Run_Number = -1;
  //Event_Number = -1;
  //Lumi_Block = -1;

  /*PDFID2        = 0.0;
  PDFID1        = 0.0;
  PDGID2        = 0.0;
  PDGID1        = 0.0;
  PDF_X1 	= 0.0;
  PDF_X2	= 0.0;
  PDF_Q2	= 0.0;
  eventNumber   = -1;
  run           = -1;
  lbn           = -1;
  mcweight      =  1;
  pileupweight  =  1;
  weight        =  1;
  xsec          =  1;
  avgIntxCross  = -1;
  actIntxCross  = -1;
  isEvtGood     = false;
  isEvtClean     = true;
  isVertexGood  = false;
  isTrigGood    = false;
  isOSGood      = false;
  //HLT_mu50     = false;
  //HLT_mu26i     = false;
  HLT_mu4_iloose_mu4_11invm60_noos = false;                                                                                       
  HLT_mu4_iloose_mu4_7invm9_noos = false;                                                                                                        
  HLT_mu4_iloose_mu4_11invm60_noos_L1MU6_2MU4 = false;                                                                                        
  HLT_mu4_iloose_mu4_7invm9_noos_L1MU6_2MU4 = false;                                                                               
  HLT_mu6_iloose_mu6_11invm24_noos = false;                                                                                 
  HLT_mu6_iloose_mu6_24invm60_noos = false;
  dilepMass     = -999;
  dilepPt       = -999;
  dilepEta      = -999;
  dilepPhi      = -999;

  //Mu_isHighPt.clear();
  Mu_isHighPt_TIGHT.clear();
  Mu_isHighPt_MEDIUM.clear();
  Mu_TriggerMatched.clear();
  //Mu_Quality.clear();
  Mu_Quality_TIGHT.clear();
  Mu_Quality_MEDIUM.clear();
  Mu_Charge.clear();
  Mu_pt.clear();
  Mu_eta.clear();
  Mu_phi.clear();
  Mu_d0.clear();
  Mu_d0sig.clear();
  Mu_z0.clear();
  Mu_deltaz0.clear();
  //Mu_Iso.clear();
  Mu_Iso_track.clear();
  Mu_Iso_fixed.clear();
  Mu_TrigSF.clear();
  Mu_RecoSF.clear();
  Mu_IsoSF.clear();*/

}
