#include <MyAnalysis/MyxAODAnalysis.h>
#include <stdio.h>      /* printf */
#include <math.h> 

struct muon{
  int ID;
  double charge;
  double pT;
  double eta;
  double phi;
  double z0;
  double d0;
  double d0_significance;
  double pT_var20;
  double pT_var30;
  bool qual_Tight;
  bool qual_Medium;
  bool iso_Track;
  bool iso_Fixed;
  bool TriggerDecision_mu4_1;
  bool TriggerDecision_mu4_2;
  bool TriggerDecision_mu4_1_2016;
  bool TriggerDecision_mu4_2_2016;
  bool TriggerDecision_mu6_1;
  bool TriggerDecision_mu6_2;  
};

MyxAODAnalysis :: MyxAODAnalysis () 
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



EL::StatusCode MyxAODAnalysis :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you

  // let's initialize the algorithm to use the xAODRootAccess package
  job.useXAOD ();
  //xAOD::Init(); // call before opening first file
  EL_RETURN_CHECK( "setupJob()", xAOD::Init() ); // call before opening first file

  std::cout<<"BELLA!"<<std::endl;

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysis :: fileExecute ()
{

  xAOD::TEvent* m_event = wk()->xaodEvent();
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  NewFile = true;
  // get the MetaData tree once a new file is opened, with
  TTree* MetaData = dynamic_cast<TTree*>(wk()->inputFile()->Get("MetaData"));
  if (!MetaData) {
    Error("fileExecute()", "MetaData not found! Exiting.");
    return EL::StatusCode::FAILURE;
  }
  MetaData->LoadTree(0);

  //check if file is from a DxAOD
  m_isDerivation = !MetaData->GetBranch("StreamAOD");
  
  //std::cout<<"BELLA!"<<std::endl;

  if(m_isDerivation ){

    std::cout << "***** NOTE ***** You Are Running On A Derivation ***** NOTE *****" << std::endl;
    
    // check for corruption
    const xAOD::CutBookkeeperContainer* incompleteCBC = nullptr;
    if(!m_event->retrieveMetaInput(incompleteCBC, "IncompleteCutBookkeepers").isSuccess()){
      Error("initializeEvent()","Failed to retrieve IncompleteCutBookkeepers from MetaData! Exiting.");
      return EL::StatusCode::FAILURE;
    }
    if ( incompleteCBC->size() != 0 ) {
      //Error("initializeEvent()","Found incomplete Bookkeepers! Check file for corruption.");
      //return EL::StatusCode::FAILURE;
    }
    
    // Now, let's find the actual information
    const xAOD::CutBookkeeperContainer* completeCBC = 0;
    if(!m_event->retrieveMetaInput(completeCBC, "CutBookkeepers").isSuccess()){
      Error("initializeEvent()","Failed to retrieve CutBookkeepers from MetaData! Exiting.");
      return EL::StatusCode::FAILURE;
    }
    
    // Find the smallest cycle number, the original first processing step/cycle
    int minCycle = 10000;
    for ( auto cbk : *completeCBC ) {
      if ( ! cbk->name().empty()  && minCycle > cbk->cycle() ){ minCycle = cbk->cycle(); }
    }
    
    // Now, find the right one that contains all the needed info...
    const xAOD::CutBookkeeper* allEventsCBK=0;
    for ( auto cbk :  *completeCBC ) {
      if ( minCycle == cbk->cycle() && cbk->name() == "AllExecutedEvents" ){
        allEventsCBK = cbk;
        break;
      }
    }
    
    m_nEventsProcessed  = allEventsCBK->nAcceptedEvents();
    m_sumOfWeights        = allEventsCBK->sumOfEventWeights();
    m_sumOfWeightsSquared = allEventsCBK->sumOfEventWeightsSquared();
    
  } // end of is derivation  
  
  return EL::StatusCode::SUCCESS;
  
} // end of if file execute

EL::StatusCode MyxAODAnalysis :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysis :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  //xAOD::TEvent* event = wk()->xaodEvent();

  //NOMINAL ANALYSIS
  std::cout << "Common Cut Flow: " << std::endl;
  std::cout << "All (skimmed):     " << h_CutFlow_Common->GetBinContent(1) << std::endl;
  std::cout << "GRL:     " << h_CutFlow_Common->GetBinContent(2) << " " << "Eff: " << h_CutFlow_Common->GetBinContent(2)/h_CutFlow_Common->GetBinContent(1) <<std::endl;
  std::cout << "Event Cleaning:     " << h_CutFlow_Common->GetBinContent(3) << " " << "Eff: " << h_CutFlow_Common->GetBinContent(3)/h_CutFlow_Common->GetBinContent(2) <<std::endl;
  std::cout << ">=2 Muons:     " << h_CutFlow_Common->GetBinContent(4) << " " << "Eff: " << h_CutFlow_Common->GetBinContent(4)/h_CutFlow_Common->GetBinContent(3) <<std::endl;
  std::cout << "Primary Vertex:     " << h_CutFlow_Common->GetBinContent(5) << " " << "Eff: " << h_CutFlow_Common->GetBinContent(5)/h_CutFlow_Common->GetBinContent(4) <<std::endl;
  std::cout << "Trigger:  "<< h_CutFlow_Common->GetBinContent(6) << " " << "Eff: " << h_CutFlow_Common->GetBinContent(6)/h_CutFlow_Common->GetBinContent(5) <<std::endl;
  std::cout << "Muon |d0 sig.|:     " <<h_CutFlow_Common->GetBinContent(7) << " " << "Eff: " << h_CutFlow_Common->GetBinContent(7)/h_CutFlow_Common->GetBinContent(6) <<std::endl;
  std::cout << "Muon |z0*sin(theta)|:           " << h_CutFlow_Common->GetBinContent(8) << " " << "Eff: " << h_CutFlow_Common->GetBinContent(8)/h_CutFlow_Common->GetBinContent(7) <<std::endl;
  std::cout << "Muon pT:           " << h_CutFlow_Common->GetBinContent(9) << " " << "Eff: " << h_CutFlow_Common->GetBinContent(9)/h_CutFlow_Common->GetBinContent(8) <<std::endl;
  std::cout << "Muon Eta:     " <<h_CutFlow_Common->GetBinContent(10) << " " << "Eff: " << h_CutFlow_Common->GetBinContent(10)/h_CutFlow_Common->GetBinContent(9) <<std::endl;
  std::cout << "Muon Quality (Medium):     " <<h_CutFlow_Common->GetBinContent(11) << " " << "Eff: " << h_CutFlow_Common->GetBinContent(11)/h_CutFlow_Common->GetBinContent(10) <<std::endl;
  std::cout << "Muon Isolation (LooseTrackOnly):     " <<h_CutFlow_Common->GetBinContent(12) << " " << "Eff: " << h_CutFlow_Common->GetBinContent(12)/h_CutFlow_Common->GetBinContent(11) <<std::endl;
  std::cout << "Muon Trigger Matched:     " <<h_CutFlow_Common->GetBinContent(13) << " " << "Eff: " << h_CutFlow_Common->GetBinContent(13)/h_CutFlow_Common->GetBinContent(12) <<std::endl;
  std::cout << "Invariant Mass:     " <<h_CutFlow_Common->GetBinContent(14) << " " << "Eff: " << h_CutFlow_Common->GetBinContent(14)/h_CutFlow_Common->GetBinContent(13) <<std::endl;
  std::cout << "OS muons:     " <<h_CutFlow_Common->GetBinContent(15) << " " << "Eff: " << h_CutFlow_Common->GetBinContent(15)/h_CutFlow_Common->GetBinContent(14) <<std::endl;
  std::cout << ">= 2 muons (after):     " <<h_CutFlow_Common->GetBinContent(16) << " " << "Eff: " << h_CutFlow_Common->GetBinContent(16)/h_CutFlow_Common->GetBinContent(15) <<std::endl;

  if (m_grl) {
    delete m_grl;
    m_grl = 0;
  }

  if(m_muonCalibrationAndSmearingTool){
    delete m_muonCalibrationAndSmearingTool;
    m_muonCalibrationAndSmearingTool = 0;
  }

  if (m_muonSelection_MEDIUM) {
    delete m_muonSelection_MEDIUM;
    m_muonSelection_MEDIUM = 0;
  }

  if (iso_muon_fixed_tight) {
    delete iso_muon_fixed_tight;
    iso_muon_fixed_tight = 0;
  }

  if (m_TrigConfigTool){
    delete m_TrigConfigTool;
    m_TrigConfigTool = 0;
  }

  if (m_TrigDecTool) {
    delete m_TrigDecTool;
    m_TrigDecTool = 0;
  }

  if (m_match_Tool) {
    delete m_match_Tool;
    m_match_Tool = 0;
  }

  if (m_Pileup) {
    delete m_Pileup;
    m_Pileup = 0;
  }

  if (m_kFTool) {
    delete m_kFTool;
    m_kFTool = 0;
  }

  if (m_mueff_corr_Medium){
    delete m_mueff_corr_Medium;
    m_mueff_corr_Medium = 0;
  }

  if (m_muiso_sf_FixedCutTight){
    delete m_muiso_sf_FixedCutTight;
    m_muiso_sf_FixedCutTight = 0;
  }

  if (m_smearingTool_d0){
    delete m_smearingTool_d0;
    m_smearingTool_d0 = 0;
  }

  if (m_smearingTool_z0){
    delete m_smearingTool_z0;
    m_smearingTool_z0 = 0;
  }

  delete SelectedMuon;
  delete SelectedMuonAux;

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.


  return EL::StatusCode::SUCCESS;
}

// Function to normalise to luminosity, still developing 
double MyxAODAnalysis::NormalisationWeight( double mc_channel_number, double Lumi ){ // Define Lumi in fb^-1

  double Weight = 1.0;

  //Ztautau
  if( mc_channel_number == 147408 ) Weight = 1.9000E+06/15027084.0;   

  //ttbar samples 
  if( mc_channel_number == 410000.0 ) Weight = 0.54316*831760.0/996213.0;
  if( mc_channel_number == 410013.0 ) Weight = 34009.0/996213.0;
  if( mc_channel_number == 410014.0 ) Weight = 33989.0/996213.0;
  if( mc_channel_number == 302917.0 ) Weight = 4790.0/199800.0;
  if( mc_channel_number == 302918.0 ) Weight = 1852.0/49900.0;
  if( mc_channel_number == 302919.0 ) Weight = 58.171/48500.0;
  if( mc_channel_number == 302920.0 ) Weight = 2.647/49900.0;
  if( mc_channel_number == 302921.0 ) Weight = 0.026/50000.0;
  if( mc_channel_number == 302922.0 ) Weight = 0.00714830/50000.0;
  if( mc_channel_number == 302923.0 ) Weight = 0.000018980/50000.0;
  if( mc_channel_number == 302924.0 ) Weight = 0.000008980/48000.0;

  //Diboson samples
  if( mc_channel_number == 361063.0 ) Weight = 12583.0/990000;
  if( mc_channel_number == 361064.0 ) Weight = 1844.6/149600;
  if( mc_channel_number == 361065.0 ) Weight = 3623.5/299800;
  if( mc_channel_number == 361066.0 ) Weight = 2565.5/199400;
  if( mc_channel_number == 361067.0 ) Weight = 5016.9/399800;
  if( mc_channel_number == 361068.0 ) Weight = 14022.0/997400;
  if( mc_channel_number == 361084.0 ) Weight = 3758.0/249400.0;
  if( mc_channel_number == 361086.0 ) Weight = 16590.0/99400.0;

  //Drell-Yan Pythia samples
  if( mc_channel_number == 301560.0 ) Weight = 1430.1E+03/249200;
  if( mc_channel_number == 301561.0 ) Weight = 13.846E+03/250000;
  if( mc_channel_number == 301562.0 ) Weight = 2.3311E+03/249000;
  if( mc_channel_number == 301563.0 ) Weight = 0.86684E+03/150000;
  if( mc_channel_number == 301564.0 ) Weight = 0.15621E+03/99600;
  if( mc_channel_number == 301565.0 ) Weight = 0.029571E+03/49600;
  if( mc_channel_number == 301566.0 ) Weight = 0.0083307E+03/50000;
  if( mc_channel_number == 301567.0 ) Weight = 0.0033162E+03/50000;
  if( mc_channel_number == 301568.0 ) Weight = 0.001098E+03/50000;
  if( mc_channel_number == 301569.0 ) Weight = 0.0004183E+03/50000;
  if( mc_channel_number == 301570.0 ) Weight = 0.0001762E+03/49600;
  if( mc_channel_number == 301571.0 ) Weight = 7.9698E-02/50000;
  if( mc_channel_number == 301572.0 ) Weight = 3.8193e-02/50000;
  if( mc_channel_number == 301573.0 ) Weight = 1.9148e-02/50000;
  if( mc_channel_number == 301574.0 ) Weight = 9.8945e-03/49800;
  if( mc_channel_number == 301575.0 ) Weight = 8.0138e-03/50000;
  if( mc_channel_number == 301576.0 ) Weight = 2.4223e-03/50000;
  if( mc_channel_number == 301577.0 ) Weight = 7.5614e-04/49800;
  if( mc_channel_number == 301578.0 ) Weight = 2.4256e-04/49200;
  if( mc_channel_number == 301579.0 ) Weight = 1.1636e-04/50000;

  //Drell-Yan Powheg samples
  if( mc_channel_number == 361107.0 ) Weight = 1.9012E+06/19962997;
  if( mc_channel_number == 301020.0 ) Weight = 0.017478E+06/498600;  
  if( mc_channel_number == 301021.0 ) Weight = 0.0029212E+06/249600;
  if( mc_channel_number == 301022.0 ) Weight = 0.001082E+06/149164;
  if( mc_channel_number == 301023.0 ) Weight = 0.0001955E+06/99000;
  if( mc_channel_number == 301024.0 ) Weight = 0.000037399+06/48400;
  if( mc_channel_number == 301025.0 ) Weight = 0.000010607E+06/50000;
  if( mc_channel_number == 301026.0 ) Weight = 0.0000042582E+06/50000;
  if( mc_channel_number == 301027.0 ) Weight = 0.0000014219E+06/49800;
  if( mc_channel_number == 301028.0 ) Weight = 0.00000054521E+06/49600;
  if( mc_channel_number == 301029.0 ) Weight = 0.00000022991E+06/50000;
  if( mc_channel_number == 301030.0 ) Weight = 0.00000010387E+06/50000;
  if( mc_channel_number == 301031.0 ) Weight = 0.0000000494E+06/49800;
  if( mc_channel_number == 301032.0 ) Weight = 0.000000024452E+06/49800;
  if( mc_channel_number == 301033.0 ) Weight = 0.000000012487E+06/50000;
  if( mc_channel_number == 301034.0 ) Weight = 0.000000010029E+06/50000;
  if( mc_channel_number == 301035.0 ) Weight = 0.0000000029342E+06/50000;
  if( mc_channel_number == 301036.0 ) Weight = 0.00000000089764E+06/49000;
  if( mc_channel_number == 301037.0 ) Weight = 0.00000000028071E+06/50000;
  if( mc_channel_number == 301038.0 ) Weight = 0.00000000012649E+06/50000;

  //CI Samples
  if( mc_channel_number == 301728.0 ) Weight = 507.47/49800;
  if( mc_channel_number == 301729.0 ) Weight = 38.863/30000;
  if( mc_channel_number == 301730.0 ) Weight = 3.3016/50000;
  if( mc_channel_number == 301731.0 ) Weight = 0.23309/50000;
  if( mc_channel_number == 301752.0 ) Weight = 511.82/50000;
  if( mc_channel_number == 301753.0 ) Weight = 39.189/49800;
  if( mc_channel_number == 301754.0 ) Weight = 2.315/50000;
  if( mc_channel_number == 301755.0 ) Weight = 0.07277/49800;
  if( mc_channel_number == 301776.0 ) Weight = 513.52/50000;
  if( mc_channel_number == 301777.0 ) Weight = 39.525/49800;
  if( mc_channel_number == 301778.0 ) Weight = 2.1389/50000;
  if( mc_channel_number == 301779.0 ) Weight = 0.031769/49800;
  if( mc_channel_number == 301800.0 ) Weight = 513.64/100000;
  if( mc_channel_number == 301801.0 ) Weight = 39.960/49800;
  if( mc_channel_number == 301802.0 ) Weight = 2.099/50000;
  if( mc_channel_number == 301803.0 ) Weight = 0.01844/99800;

  if( mc_channel_number == 301724.0 ) Weight = 531.26/50000;
  if( mc_channel_number == 301725.0 ) Weight = 47.741/50000;
  if( mc_channel_number == 301726.0 ) Weight = 5.4164/50000;
  if( mc_channel_number == 301727.0 ) Weight = 0.30782/50000;
  if( mc_channel_number == 301748.0 ) Weight = 525.61/49800;
  if( mc_channel_number == 301749.0 ) Weight = 44.216/50000;
  if( mc_channel_number == 301750.0 ) Weight = 3.4866/50000;
  if( mc_channel_number == 301751.0 ) Weight = 0.11497/50000;
  if( mc_channel_number == 301772.0 ) Weight = 521.96/30000;
  if( mc_channel_number == 301773.0 ) Weight = 42.931/49800;
  if( mc_channel_number == 301774.0 ) Weight = 2.8924/49800;
  if( mc_channel_number == 301775.0 ) Weight = 0.05856/50000;
  if( mc_channel_number == 301796.0 ) Weight = 522.78/50000;
  if( mc_channel_number == 301797.0 ) Weight = 42.211/30000;
  if( mc_channel_number == 301798.0 ) Weight = 2.6315/49800;
  if( mc_channel_number == 301799.0 ) Weight = 0.037017/50000;

  return Lumi*Weight;

} // end of NormalisationWeight


//pT sorting
void MyxAODAnalysis::bubblesort(std::vector<double> &array,std::vector<int> &array2)
{
  const int n=array.size();
  
  for(int i=0;i<n;i++){
    for(int j=n-1;j>i;j--){

      if(array[j-1] < array[j])
	{
	  double temp = array[j-1];
	  array[j-1] = array[j];
	  array[j] = temp;
	  int itemp = array2[j-1];
	  array2[j-1] = array2[j];
	  array2[j] = itemp;
	
	}
    }    
  }
}

double MyxAODAnalysis :: GetMuonRecoSF_Medium(const xAOD::IParticle *p){

  float m_sf = 1;

  const xAOD::Muon* mu = dynamic_cast<const xAOD::Muon*> (p);
  if( m_mueff_corr_Medium->getEfficiencyScaleFactor(*mu,m_sf) == CP::CorrectionCode::Error ){
     Info( "GetMuonRecoSF()", "MuonEfficiencyScaleFactors returns Error CorrectionCode in getting SF");
  }

  if( m_debug ) Info( "GetMuonRecoSF()" , "Muon Reco SF = %f ", m_sf );
  return (double)m_sf;

} // end of GetMuonRecoSF (medium)

double MyxAODAnalysis :: GetMuonIsoSF_FixedCutTight(const xAOD::IParticle *p){

  float m_sf = 1;
  const xAOD::Muon* mu = dynamic_cast<const xAOD::Muon*> (p);
  if( m_muiso_sf_FixedCutTight->getEfficiencyScaleFactor(*mu,m_sf) == CP::CorrectionCode::Error ){
     Info( "GetMuonIsoSF()", "MuonEfficiencyScaleFactors returns Error CorrectionCode in getting SF");
  }

  if( m_debug ) Info( "GetMuonIsoSF()" , "Muon Iso SF = %f (FixedCutTight)", m_sf );
  return (double)m_sf;

} // end of GetMuonIsoSF (FixedCutTight)

double MyxAODAnalysis :: GetZReweighting(double pT){ //Z pT reweighting considering the whole mass range

  double weight_ZpT = 1;

  weight_ZpT = 7.80389e-01 + 2.83357e-02*pT - 4.35534e-04*pT*pT + 3.58178e-06*pT*pT*pT - 1.04605e-08*pT*pT*pT*pT;

  if(pT > 135.) {
    weight_ZpT = 1.92663;
  } 
  
  return weight_ZpT;

}

double MyxAODAnalysis :: GetZReweighting_CR1(double pT){ //Z pT reweighting in the Z-peak control region

  double weight_ZpT_CR1 = 1;
  weight_ZpT_CR1 = 9.56524e-01 + 5.61071e-03*pT - 2.48368e-04*pT*pT + 5.02965e-06*pT*pT*pT - 4.47794e-08*pT*pT*pT*pT + 1.81221e-10*pT*pT*pT*pT*pT - 2.72804e-13*pT*pT*pT*pT*pT*pT; //MINE
  
  return weight_ZpT_CR1;

}

double MyxAODAnalysis :: GetZReweighting_A(double pT){ //7M9 

  double weight_ZpT = 1;

  weight_ZpT = 1.394e+00 + 6.60554e-02*pT - 4.92183e-04*pT*pT;

  if(pT >= 8. && pT < 10.) {
    weight_ZpT = 0.449518;
  } 

  if(pT >= 80.) {
    weight_ZpT = 5.20845;
  } 
  

  return weight_ZpT;

}

double MyxAODAnalysis :: GetZReweighting_B(double pT){ //12M14

  double weight_ZpT = 1;

  weight_ZpT = 5.38825e-01 + 7.26362e-02*pT - 1.21679e-03*pT*pT + 7.39181e-06*pT*pT*pT;

  if(pT >= 90.) {
    weight_ZpT = 3.17537;
  } 
  

  return weight_ZpT;

}

double MyxAODAnalysis :: GetZReweighting_C(double pT){ //14M17

  double weight_ZpT = 1;

  weight_ZpT = 7.31267e-01 + 3.78985e-02*pT - 2.94854e-04*pT*pT + 6.41026e-07*pT*pT*pT; 

  if(pT >= 140.) {
    weight_ZpT = 4.02926;
  } 
  

  return weight_ZpT;

}

double MyxAODAnalysis :: GetZReweighting_D(double pT){  //17M22

  double weight_ZpT = 1;

  weight_ZpT = 7.66022e-01 + 2.45164e-02*pT - 1.11745e-04*pT*pT; 
  
  if(pT >= 130.) {
    weight_ZpT = 2.23546;
  }

  return weight_ZpT;

}

double MyxAODAnalysis :: GetZReweighting_E(double pT){ //22M28

  double weight_ZpT = 1;

  weight_ZpT = 7.91803e-01 + 1.83948e-02*pT - 7.34319e-05*pT*pT;
  
  if(pT >= 140.) {
    weight_ZpT = 1.67985;
  }

  return weight_ZpT;

}

double MyxAODAnalysis :: GetZReweighting_F(double pT){ //28M36

  double weight_ZpT = 1;

  weight_ZpT = 9.75314e-01 + 5.80027e-03*pT - 2.43521e-05*pT*pT;
  
  if(pT >= 140.) {
    weight_ZpT = 1.45677;
  } 

  return weight_ZpT;

}

double MyxAODAnalysis :: GetZReweighting_G(double pT){ //36M46

  double weight_ZpT = 1;

  weight_ZpT = 9.43552e-01 + 4.61083e-03*pT;
  
  if(pT >= 160.) {
    weight_ZpT = 2.9811;
  } 

  return weight_ZpT;

}

double MyxAODAnalysis :: GetZReweighting_H(double pT){  //46M60

  double weight_ZpT = 1;

  weight_ZpT = 9.55045e-01 + 4.59468e-03*pT - 1.53192e-05*pT*pT;
  
  if(pT >= 160.) {
    weight_ZpT = 1.89932;
  }

  return weight_ZpT;

}
