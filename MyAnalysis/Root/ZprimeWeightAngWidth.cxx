#include <iostream>
#include "TH1.h"
#include <complex>
#include <string>
#include <iostream>
#include "TROOT.h"
#include "TChain.h"
#include "TCanvas.h"
#include "TLorentzVector.h"
#include "TClonesArray.h"
#include "TFile.h"

// Header file
#include <MyAnalysis/MyxAODAnalysis.h>

using namespace std;

double MyxAODAnalysis::MassOnly_CrossSection( double ZprimeMass, double TruthMass, double quarktype, int model, int mode){

  Double_t XS = 1;
  double mZprime = ZprimeMass;
  double sHat = TruthMass*TruthMass;

  double mpi = 3.14159;  
  double sigma = 0;

  // define coupling constants, e = 1 by convention
  const float x = 0.231; // sin^2 theta_W
  const float gZ = sqrt ( 1.0 / ( x * (1.0 - x) ) ) ; 
  const float gTheta = sqrt ( 5.0 / ( 3.0 * (1-x) ) ) ; 

  double thetaE6 = 0;

  if (model==0)       thetaE6 = 0.0 ;		// SM DY
  else if (model==1)  thetaE6 = 1.97717;	// sq model //E6 model angles need to be double checked!!!!
  else if (model==2)  thetaE6 = -0.261;		// iota model
  else if (model==3)  thetaE6 = 3.0750;		// N model
  else if (model==4)  thetaE6 = mpi;		// E6 model for Z'_psi 
  else if (model==5)  thetaE6 = mpi/2;		// E6 model for Z'_chi
  else if (model==6)  thetaE6 = 0.65939;	// E6 model for Z'_eta 
  else if (model==7)  thetaE6 = 0.0;		// SM Z couplings for Zprime 
 
  double A = cos (thetaE6) / ( 2.0*sqrt(6.0) ) ;
  double B = sin (thetaE6) / ( 2.0*sqrt(10.0) ) ;

  int quarkType = 0;
  if(quarktype == 2 || quarktype == 4 || quarktype == 6) quarkType = 0;
  else if(quarktype == 1 || quarktype == 3 || quarktype == 5) quarkType = 1;

  // u quark (first two entries) and d quark (next two entries)
  double photonCQuark[4] = { 2.0/3.0, 2.0/3.0, -1.0/3.0, -1.0/3.0 } ; // (lu,ru,ld,rd)
  double zCQuark[4] = { gZ * (-0.5 + 2.0*x/3.0 ) , gZ * ( 2.0*x/3.0 ) , gZ * (0.5 - x/3.0 ) , gZ * ( -x/3.0 ) } ; 
  double zPrimeCQuark[4] = { -gTheta * (A+B) ,  gTheta * (A+B) , -gTheta * (A+B) ,  gTheta * (A-3.0*B) } ;

  // muon
  double photonCMuon[2] = { -1.0, -1.0 } ; // (le,re)
  double zCMuon[2] = { gZ * (0.5-x) ,  gZ * (-x) } ; 
  double zPrimeCMuon[2] = { gTheta * (3.0*B-A) , gTheta * (A+B) } ; 

  // parton luminosity fraction for u=0 and d=1 quark
  double quarkLuminosity[2] = { 2.0/3.0 , 1.0/3.0 } ;

  // photon amplitude
  complex<double> photonAmplitude(1.0, 0);

  // Z amplitude
  double  mZ = 91.18760;
  double gammaZ = 2.49520;

  double massDependentZwidth =  sHat * gammaZ / mZ ;

  complex<double> zPropagator( sHat - mZ*mZ, massDependentZwidth ) ;
  complex<double> zAmplitude(0,0) ;
  zAmplitude = sHat / zPropagator ;
  
  // Sum over the square of the couplings
  double WidthEle = 0; double WidthQuark = 0;
  for (int i = 0; i < 2; i++)
    { WidthEle += zPrimeCMuon[i]*zPrimeCMuon[i]; }
  for (int k =0; k < 4; k++)
    { WidthQuark += zPrimeCQuark[k]*zPrimeCQuark[k]; } 
  
  double CouplingFactor = 1;
  if (model!=7){	
    CouplingFactor = (WidthEle + WidthQuark)/3;
  }
 
  // Zprime amplitude
  double gammaZprime = mZprime * gammaZ / mZ ;
  double massDependentZprimeWidth = 0;
  if(mZprime < 375) massDependentZprimeWidth = sHat * gammaZprime / mZprime ;
  else massDependentZprimeWidth = sHat * gammaZprime / mZprime *8/7  * CouplingFactor;
   
  complex<double> zPrimePropagator( sHat - mZprime*mZprime, massDependentZprimeWidth ) ;
  complex<double> zPrimeAmplitude(0,0) ;
  zPrimeAmplitude = sHat / zPrimePropagator;

  // helicity amplitude is sum over gauge bosons
  complex<double> helicityamplitude(0,0);
  double normAij = 0; // norm of helicity amplitude

  // amplitudes with couplings
  complex<double> Agamma(0,0);
  complex<double> Az(0,0);
  complex<double> Azprime(0,0);

  float helicitySwitch[2][2]; // [quark helicity, muon helicity]
  helicitySwitch[0][0] = 1;  //  LL
  helicitySwitch[0][1] = 1;  //  LR
  helicitySwitch[1][0] = 1;  //  RL
  helicitySwitch[1][1] = 1;  //  RR
 
   // loop over quark and muon helicities
  for (int muonHelicity=0; muonHelicity<= 1; muonHelicity++) {
    for (int quarkHelicity=0; quarkHelicity<= 1; quarkHelicity++) {
     
      int quarkOffset = 2*quarkType;
      
      double photonCoupling = photonCQuark[quarkOffset+quarkHelicity] * photonCMuon[muonHelicity] * helicitySwitch[quarkHelicity][muonHelicity];
      double zCoupling = zCQuark[quarkOffset+quarkHelicity] * zCMuon[muonHelicity] * helicitySwitch[quarkHelicity][muonHelicity] ;
      double zPrimeCoupling = zPrimeCQuark[quarkOffset+quarkHelicity] * zPrimeCMuon[muonHelicity] * helicitySwitch[quarkHelicity][muonHelicity];
      
      if (model==7) zPrimeCoupling = zCoupling;
      
      Agamma = photonCoupling*photonAmplitude;
      Az = zCoupling*zAmplitude ;
      if(model == 0) Azprime = 0;
      else Azprime = zPrimeCoupling*zPrimeAmplitude ;
      
      if(mode == 0) helicityamplitude = Agamma + Az + Azprime ;
      else if(mode == 1) helicityamplitude = Azprime;
      normAij = abs(helicityamplitude);
      normAij *= normAij; //square
      
      sigma += quarkLuminosity[quarkType] * normAij;
    }
  }

  sigma /= sHat ; // put in the global photon pole, which was factorized out from all amplitudes

  XS = sigma;
  return XS;
}

double MyxAODAnalysis::Full_CrossSection( double ZprimeMass, TLorentzVector vec_q1, TLorentzVector vec_q2, TLorentzVector vec_l1, TLorentzVector vec_l2, double quarktype, int model, int mode, double UserAngle){

  Double_t XS = 1;
  double mZprime = ZprimeMass;

  double mpi = 3.14159;  
  double sigma = 0;

  double sHat = (vec_q1 + vec_q2)*(vec_q1 + vec_q2);
  double tHat = (vec_q1 - vec_l1)*(vec_q1 - vec_l1);
  double uHat = (vec_q1 - vec_l2)*(vec_q1 - vec_l2);

  // define coupling constants, e = 1 by convention
  const float x = 0.231; // sin^2 theta_W
  const float gZ = sqrt ( 1.0 / ( x * (1.0 - x) ) ) ; 
  const float gTheta = sqrt ( 5.0 / ( 3.0 * (1-x) ) ) ; 
  double thetaE6 = 0;

  if (model==0)       thetaE6 = 0.0 ;		// SM DY
  else if (model==1)  thetaE6 = 1.97717;	// sq model //E6 model angles need to be double checked!!!!
  else if (model==2)  thetaE6 = -0.261;		// iota model
  else if (model==3)  thetaE6 = 3.0750;		// N model
  else if (model==4)  thetaE6 = mpi;		// E6 model for Z'_psi 
  else if (model==5)  thetaE6 = mpi/2;		// E6 model for Z'_chi
  else if (model==6)  thetaE6 = 0.65939;	// E6 model for Z'_eta 
  else if (model==7)  thetaE6 = 0.0;		// SM Z couplings for Zprime 
  else if (model==8)  thetaE6 = UserAngle;	// User Inputs the Angle 
 
  double A = cos (thetaE6) / ( 2.0*sqrt(6.0) ) ;
  double B = sin (thetaE6) / ( 2.0*sqrt(10.0) ) ;
  
  // define charges for helicities L=0, R=1

  int quarkType = 0;
  //up quark
  if(quarktype == 2 || quarktype == 4 || quarktype == 6) quarkType = 0;
  else if(quarktype == 1 || quarktype == 3 || quarktype == 5) quarkType = 1;

  // u quark (first two entries) and d quark (next two entries)
  double photonCQuark[4] = { 2.0/3.0, 2.0/3.0, -1.0/3.0, -1.0/3.0 } ; // (lu,ru,ld,rd)
  double zCQuark[4] = { gZ * (-0.5 + 2.0*x/3.0 ) , gZ * ( 2.0*x/3.0 ) , gZ * (0.5 - x/3.0 ) , gZ * ( -x/3.0 ) } ; 
  double zPrimeCQuark[4] = { -gTheta * (A+B) ,  gTheta * (A+B) , -gTheta * (A+B) ,  gTheta * (A-3.0*B) } ;

  // muon
  double photonCMuon[2] = { -1.0, -1.0 } ; // (le,re)
  double zCMuon[2] = { gZ * (0.5-x) ,  gZ * (-x) } ; 
  double zPrimeCMuon[2] = { gTheta * (3.0*B-A) , gTheta * (A+B) } ; 

  // photon amplitude
  complex<double> photonAmplitude(1.0, 0);

  // Z amplitude
  double  mZ = 91.18760;
  double gammaZ = 2.49520;

  double massDependentZwidth =  sHat * gammaZ / mZ ;

  complex<double> zPropagator( sHat - mZ*mZ, massDependentZwidth ) ;
  complex<double> zAmplitude(0,0) ;
  zAmplitude = sHat / zPropagator ;
  
  // Sum over the square of the couplings
  double WidthEle = 0; double WidthQuark = 0;
  for (int i = 0; i < 2; i++)
  	{ WidthEle += zPrimeCMuon[i]*zPrimeCMuon[i]; }
  for (int k =0; k < 4; k++)
   	{ WidthQuark += zPrimeCQuark[k]*zPrimeCQuark[k]; } 

  double CouplingFactor = 1;
  if (model!=7){	
    CouplingFactor = (WidthEle + WidthQuark)/3;
  }

  // Zprime amplitude
  double gammaZprime = mZprime * gammaZ / mZ ;
  double massDependentZprimeWidth = 0;
  if(mZprime < 375) massDependentZprimeWidth = sHat * gammaZprime / mZprime ;
  else massDependentZprimeWidth = sHat * gammaZprime / mZprime *8/7  * CouplingFactor;
   
  complex<double> zPrimePropagator( sHat - mZprime*mZprime, massDependentZprimeWidth ) ;
  complex<double> zPrimeAmplitude(0,0) ;
  zPrimeAmplitude = sHat / zPrimePropagator;

  // helicity amplitude is sum over gauge bosons
  complex<double> helicityamplitude(0,0);
  double normAij = 0; // norm of helicity amplitude

  // amplitudes with couplings
  complex<double> Agamma(0,0);
  complex<double> Az(0,0);
  complex<double> Azprime(0,0);

  double prefac = 1/(2*mpi*mpi);
  float helicitySwitch[2][2]; // [quark helicity, muon helicity]
  helicitySwitch[0][0] = prefac * uHat;  //  LL
  helicitySwitch[0][1] = prefac * tHat;  //  LR
  helicitySwitch[1][0] = prefac * tHat;  //  RL
  helicitySwitch[1][1] = prefac * uHat;  //  RR
 
   // loop over quark and muon helicities
  for (int muonHelicity=0; muonHelicity<= 1; muonHelicity++) {
    for (int quarkHelicity=0; quarkHelicity<= 1; quarkHelicity++) {
     
     int quarkOffset = 2*quarkType;

  	double photonCoupling = photonCQuark[quarkOffset+quarkHelicity] * photonCMuon[muonHelicity] * helicitySwitch[quarkHelicity][muonHelicity];
  	double zCoupling = zCQuark[quarkOffset+quarkHelicity] * zCMuon[muonHelicity] * helicitySwitch[quarkHelicity][muonHelicity] ;
  	double zPrimeCoupling = zPrimeCQuark[quarkOffset+quarkHelicity] * zPrimeCMuon[muonHelicity] * helicitySwitch[quarkHelicity][muonHelicity];
  	
  	if (model==7) zPrimeCoupling = zCoupling;

  	Agamma = photonCoupling*photonAmplitude;
  	Az = zCoupling*zAmplitude ;
	if(model == 0) Azprime = 0;
	else Azprime = zPrimeCoupling*zPrimeAmplitude ;
  	
	//	if (zPrimeInterference) {
	if(mode == 0) helicityamplitude = Agamma + Az + Azprime ;
	else if(mode == 1) helicityamplitude = Azprime;
	normAij = abs(helicityamplitude);
	normAij *= normAij; //square
	
  	sigma += normAij;
    }
  }

  sigma /= sHat ; // put in the global photon pole, which was factorized out from all amplitudes

  XS = sigma;
  return XS;
}
