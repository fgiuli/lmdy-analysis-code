#include <MyAnalysis/MyxAODAnalysis.h>
#include <vector>
#include <utility>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>

// this is needed to distribute the algorithm to the workers
ClassImp(MyxAODAnalysis)

using namespace std;

EL::StatusCode MyxAODAnalysis :: execute ()
{

  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  xAOD::TEvent* event = wk()->xaodEvent();

  //ResetVariables();

  // Define Global variables
  double mc_channel_number = 0.0;
  const xAOD::TruthEventContainer* xTruthEventContainer = NULL;
  const xAOD::TruthParticleContainer* mc_particle = NULL;
  double GeV = 0.001;
  TLorentzVector Muon1(0,0,0,0), Muon2(0,0,0,0),  Propagator(0,0,0,0), Propagator_sel(0,0,0,0);
  TLorentzVector Propagator_CR1(0,0,0,0), Propagator_CR2(0,0,0,0);

  //Muon pair
  std::vector<std::pair<int,int>> mupairs[1][3];    //creating a vector of pairs and then cleaning it
  
  //CLearing the vector of pairs...
  for(int j=0; j < 1; j++){                        //just MEDIUM quality muons
    for(int k=0; k < 3; k++){
      mupairs[j][k].clear();
    }
  }

  const double MuonPDGMass = 105.658367e-3;
  double EventWeight = 1.0; // Initialise to one  
  double EventWeight_noPU = 1.0; // Initialise to one  
  double nVtx = 0;
  bool primVtx = false;
  double KFactorWeight = 1.0;
  double PileUpWeight = 1.0, MCWeight = 1.0, MCWeight_xSec = 1.0; //double bornDeltaPhi=0.0;
  double PileUpWeight_B = 1.0;

  // Pre Skim Number of Events
  if (m_isDerivation && NewFile){
    h_CutFlow_Common->Fill(0.0,m_nEventsProcessed);
    NewFile = false;
  }
  else if (!m_isDerivation){
    h_CutFlow_Common->Fill(0.0);
  }

  //Count Events and print info every 1000 events
  m_eventCounter++;

  //if( (m_eventCounter % 1000) ==0 ) Info("execute()", "Event number = %i", m_eventCounter );
  if( (m_eventCounter % 1) ==0 ) Info("execute()", "Event number = %i", m_eventCounter );

  // Event Information and GRL

  // Retrieve Event information
  const xAOD::EventInfo* eventInfo = 0;
  EL_RETURN_CHECK("execute()",event->retrieve( eventInfo, "EventInfo" ));

  // check if the event is data or MC (many tools are applied either to data or MC)
  bool isMC = false;
  // check if the event is MC
  if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
    isMC = true; // can do something with this later
  }   

  //std::cout<<"isMC: "<<isMC<<std::endl;

  int rNum=0.0;
  //if data check if event passes GRL
  if(!(isMC)){ // it's data!
    if(!m_grl->passRunLB(*eventInfo)){
      return EL::StatusCode::SUCCESS; // go to next event
    }
  } // end if not MC
  else{

    mc_channel_number = eventInfo->mcChannelNumber();

    //EventWeight = NormalisationWeight( mc_channel_number, Luminosity );  //It could be uncommented
    PRWToolHandle->apply(*eventInfo);
    PileUpWeight = eventInfo->auxdata<Float_t >("PileupWeight");

    PileUpWeight_B = PRWToolHandle->getCombinedWeight(*eventInfo);

    rNum = PRWToolHandle->getRandomRunNumber(*eventInfo);
    if (rNum == 0) rNum = PRWToolHandle->getRandomRunNumber(*eventInfo);

    MCWeight = eventInfo->mcEventWeights().at(0); //Retrieve Generator Weight
  
    EventWeight*=(PileUpWeight*MCWeight); //CORRECT!

    EventWeight_noPU*=MCWeight; //no PU 

    // Retrieve Truth container                                                                        
    EL_RETURN_CHECK("execute()", event->retrieve( xTruthEventContainer, "TruthEvents" ) );
    // Retrieve Truth Particle Container
    EL_RETURN_CHECK("execute()", event->retrieve( mc_particle, "TruthParticles" ) ); 
  } // end of if Monte Carlo

  MCWeight_xSec = MCWeight;

  uint32_t runNumber;
  runNumber = eventInfo->runNumber();
 
  //PileUpWeight distribution
  h_PileUpWeight->Fill(PileUpWeight, EventWeight);

  h_CutFlow_Common->Fill(20.5,MCWeight_xSec);
  h_CutFlow_Common->Fill(22.5,PileUpWeight);
  h_CutFlow_Common->Fill(23.5,PileUpWeight*MCWeight);

  //Truth quantities
  Double_t Truth_V_Mass = 0, BornMass_muons = 0, BornMass_muons_BF = 0, BareMass_muons_BF = 0, BornPt_muons = 0, Truth_V_pT = 0;
  Double_t BornPt_muon1 = 0, BornPt_muon2 = 0, BornEta_muon1 = 0, BornEta_muon2 = 0, BornEnergy_muon1 = 0,  BornEnergy_muon2 = 0;
  Double_t BarePt_muon1 = 0, BarePt_muon2 = 0, BareEta_muon1 = 0, BareEta_muon2 = 0, BareEnergy_muon1 = 0,  BareEnergy_muon2 = 0;
  Double_t BornRapidity_muons = 0;
  bool FoundBornMuon=false, FoundBornAntiMuon=false;    
  bool FoundBornTau=false, FoundBornAntiTau=false;    

  Double_t BareMass_muons = 0, BarePt_muons = 0;
  bool FoundBareMuon=false, FoundBareAntiMuon=false;    
  bool FoundBareTau=false, FoundBareAntiTau=false;    

   Double_t weight_ZpT = 1.;

  // Initialize variables
  TLorentzVector BornMuon1, BornMuon2, BornPropagator, BornPropagator_BF, BarePropagator_BF; 
  BornMuon1.SetPtEtaPhiM( 0,0,0,0 ); 
  BornMuon2.SetPtEtaPhiM( 0,0,0,0 ); 
  BornPropagator.SetPtEtaPhiM( 0,0,0,0 );
  BornPropagator_BF.SetPtEtaPhiM( 0,0,0,0 );
  BarePropagator_BF.SetPtEtaPhiM( 0,0,0,0 );
  double BornCharge1 = 0, BornCharge2 = 0, TruthCharge = 0;
  int NumberBornMuons = 0;

  TLorentzVector BareMuon1, BareMuon2, BarePropagator; 
  BareMuon1.SetPtEtaPhiM( 0,0,0,0 ); 
  BareMuon2.SetPtEtaPhiM( 0,0,0,0 ); 
  BarePropagator.SetPtEtaPhiM( 0,0,0,0 );
  double BareCharge1 = 0, BareCharge2 = 0;
  int NumberBareMuons = 0;

  // Retrieve containers 
 
  // Vertex Container Retrieval
  const xAOD::VertexContainer* vxContainer = 0;
  EL_RETURN_CHECK("execute()",event->retrieve( vxContainer, "PrimaryVertices" ));

  // get muon container of interest
  const xAOD::MuonContainer* muons = 0;
  EL_RETURN_CHECK("execute()",event->retrieve( muons, "Muons" ));

  //Initialize variables
  bool DYSample = false;
  bool DYtautau = false;
  
  // PowhegPY8 DY samples
  if( (int)mc_channel_number == 361107 || (int)mc_channel_number == 361666 || (int)mc_channel_number == 361667 || ( (int)mc_channel_number > 364099 && (int)mc_channel_number < 364113 ) || ( (int)mc_channel_number > 364197 && (int)mc_channel_number < 364204 ) || (int)mc_channel_number == 364669 || (int)mc_channel_number == 364675 ){
    
    DYSample = true;
  
  }

  // PowhegPY8 DY samples
  if( (int)mc_channel_number == 361108 || (int)mc_channel_number == 361668 || (int)mc_channel_number == 361669){
    
    DYtautau = true;
  
  }

  //////////////////////////////////////////////////////////////////                                                                       
  // Event pre-selection                                                                                                                                   
  /////////////////////////////////////////////////////////////////                                                                                       
  // Trigger Selection                                                                                                                                  
  /////////////////////////////////////////////////////////////////                                                           
  
  h_CutFlow_Common->Fill(1.0, EventWeight);
  
  
  //Trigger menu output                                                              
  if(m_eventCounter == 1){                                                                                                               
    auto chainGroups = m_TrigDecTool->getChainGroup("HLT_mu.*");
    
    std::map<std::string,int> triggerCounts;

    for(auto &trig : chainGroups->getListOfTriggers()) {                                            
      std::cout<< "  "<<trig<<std::endl;    
      auto cg = m_TrigDecTool->getChainGroup(trig);
      std::string thisTrig = trig;
      Info( "execute()", "%30s chain passed(1)/failed(0): %d total chain prescale (L1*HLT): %.1f", thisTrig.c_str(), cg->isPassed(), cg->getPrescale() );
    } // end of loop                                                                                                                                    
  } // enf of First Event
 
  int Counter_TRUTH = 0;

  if (isMC) { //Studies at TRUTH level - e.g. find the Born mass of the muon pair to apply NNLO QCD + NLO QED kFs

    xAOD::TruthEventContainer::const_iterator truthE_itr = xTruthEventContainer->begin();
    xAOD::TruthEventContainer::const_iterator truthE_end = xTruthEventContainer->end(); 

    if( DYSample ){

      for (; truthE_itr != truthE_end; ++truthE_itr){
      
	int nPart = (*truthE_itr)->nTruthParticles();
      
	for(int iPart = 0;iPart<nPart;iPart++){
	  const xAOD::TruthParticle* particle = (*truthE_itr)->truthParticle(iPart);
	  int PDG_ID_Z = 0, Status_Z = 0; 
	
	  if (particle) PDG_ID_Z = (particle)->pdgId();
	  if (particle) Status_Z = (particle)->status();

	  if( Status_Z == 62 && PDG_ID_Z == 23 ){ // Born Z in DY samples

	    Truth_V_Mass = (particle)->m() * GeV;
	    Truth_V_pT = (particle)->pt() * GeV;	    

	  }
	}
      }

      for( auto truth = mc_particle->begin(); truth!=mc_particle->end(); ++truth ){	
	
	Counter_TRUTH++;
	
	int PDG_ID = (*truth)->pdgId();
	int Status = (*truth)->status();
	int BarCode = (*truth)->barcode();

	if( PDG_ID == 13 ){ TruthCharge = -1.0; }
	if( PDG_ID == -13 ){ TruthCharge = 1.0; }


	if( Status == 11 && fabs(PDG_ID) == 13 ){ // Born Muons in DY MC samples
	  
	  NumberBornMuons++;
	  if( PDG_ID == 13 ) FoundBornMuon=true;  
	  if( PDG_ID == -13 ) FoundBornAntiMuon=true;  

	  if( (*truth)->pt() * GeV > BornMuon1.Pt() ){ BornMuon2 = BornMuon1; BornCharge2 = BornCharge1; BornMuon1.SetPtEtaPhiM( (*truth)->pt() * GeV, (*truth)->eta(), (*truth)->phi(), MuonPDGMass  ); BornCharge1 = TruthCharge; }
	  else if( (*truth)->pt() * GeV > BornMuon2.Pt() && TruthCharge != BornCharge1 ){ BornMuon2.SetPtEtaPhiM( (*truth)->pt() * GeV, (*truth)->eta(), (*truth)->phi(), MuonPDGMass  ); BornCharge2 = TruthCharge; }
 
	  BornPropagator_BF = BornMuon1 + BornMuon1;
	  BornMass_muons_BF = BornPropagator_BF.Mag();
	  

	}
		
	//Bare muons
	if( Status == 1 && fabs(PDG_ID) == 13 ){ // Bare muons in DY sample
	  
	  NumberBareMuons++;
          if( PDG_ID == 13 ) FoundBareMuon=true;
          if( PDG_ID == -13 ) FoundBareAntiMuon=true;
	  
	  if( (*truth)->pt() * GeV > BareMuon1.Pt() ){ BareMuon2 = BareMuon1; BareCharge2 = BareCharge1; BareMuon1.SetPtEtaPhiM( (*truth)->pt() * GeV, (*truth)->eta(), (*truth)->phi(),MuonPDGMass  ); BareCharge1 = TruthCharge; }
	  else if( (*truth)->pt() * GeV > BareMuon2.Pt() && TruthCharge != BareCharge1 ){ BareMuon2.SetPtEtaPhiM( (*truth)->pt() * GeV, (*truth)->eta(), (*truth)->phi(), MuonPDGMass  ); BareCharge2 = TruthCharge; }

	  BarePropagator_BF = BareMuon1 + BareMuon1;
	  BareMass_muons_BF = BarePropagator_BF.Mag();
	  
	}
	
      }
    }
  }
    
  if( FoundBornMuon && FoundBornAntiMuon ) {
    if (BornMuon1.Pt() > 3.5 && BornMuon2.Pt() > 3.5 && fabs(BornMuon1.Eta()) < 2.7 && fabs(BornMuon2.Eta()) < 2.7) {
    
      BornPropagator = BornMuon1 + BornMuon2;
      BornMass_muons = BornPropagator.Mag();
      BornPt_muons = BornPropagator.Pt();

      BornPt_muon1 = BornMuon1.Pt();
      BornPt_muon2 = BornMuon2.Pt();
      BornEta_muon1 = BornMuon1.Eta();
      BornEta_muon2 = BornMuon2.Eta();    
      
      BornEnergy_muon1 = BornMuon1.E();
      BornEnergy_muon2 = BornMuon2.E();

      BornRapidity_muons = BornPropagator.Rapidity();

      h_CutFlow_Common->Fill(21.5,MCWeight);
    	  
    }	
  } 

  if ( (FoundBareMuon && FoundBareAntiMuon) ) {
    if (BareMuon1.Pt() > 3.5 && BareMuon2.Pt() > 3.5 && fabs(BareMuon1.Eta()) < 2.7 && fabs(BareMuon2.Eta()) < 2.7) {

    BarePropagator = BareMuon1 + BareMuon2;
    BareMass_muons = BarePropagator.Mag();
    BarePt_muons = BarePropagator.Pt();

    BarePt_muon1 = BareMuon1.Pt();
    BarePt_muon2 = BareMuon2.Pt();
    BareEta_muon1 = BareMuon1.Eta();
    BareEta_muon2 = BareMuon2.Eta();    
    
    BareEnergy_muon1 = BareMuon1.E();
    BareEnergy_muon2 = BareMuon2.E();

    if ( !(FoundBornMuon && FoundBornAntiMuon) ) {

      BornPropagator = BarePropagator;
      BornMass_muons = BareMass_muons;
      BornPt_muons = BarePt_muons;

      BornPt_muon1 = BareMuon1.Pt();
      BornPt_muon2 = BareMuon2.Pt();
      BornEta_muon1 = BareMuon1.Eta();
      BornEta_muon2 = BareMuon2.Eta();    
      
      BornEnergy_muon1 = BareMuon1.E();
      BornEnergy_muon2 = BareMuon2.E();

      BornRapidity_muons = BarePropagator.Rapidity();
    }
    }    
  }

  //if (Truth_V_Mass != BornMass_muons) std::cout<<Truth_V_Mass - BornMass_muons<<std::endl;
  //if (Truth_V_pT != BornPt_muons) std::cout<<Truth_V_pT - BornPt_muons<<std::endl;

  if (BornMass_muons != 0.0){
    h_Truth_V_Mass->Fill(BornMass_muons, MCWeight);
    h_Truth_V_Mass_NEW->Fill(BornMass_muons, MCWeight);	 
    h_BosonPt->Fill(BornPt_muons, MCWeight);
  }

  // Define truth propagators - KfactorTool
  if( DYSample ){

    m_kFTool->execute();
    KFactorWeight = eventInfo->auxdata< double >( "KfactorWeight" );
    
  } // end of if DY Sample
  
  //Validity range stops at 10 GeV for kFs!
  if (Truth_V_Mass < 10.) {
    KFactorWeight = 1;
  }
  
  h_kF->Fill(Truth_V_Mass, KFactorWeight);
  h_Dummy->Fill(Truth_V_Mass);
  h_kFweight->Fill(KFactorWeight);

  EventWeight*=KFactorWeight; 
  EventWeight_noPU*=KFactorWeight; 

  MCWeight*=PileUpWeight;

  MCWeight*=KFactorWeight; //CORRECT! (for TRUTH events)

  h_CutFlow_Common->Fill(24.5,MCWeight);

  bool Born_0 = false;
  bool Born_1 = false;
  bool Born_2 = false;
  bool Born_3 = false;
  bool Born_4 = false;
  bool Born_5 = false;
  bool Born_6 = false;
  bool Born_7 = false;

  bool Bare_0 = false;
  bool Bare_1 = false;
  bool Bare_2 = false;
  bool Bare_3 = false;
  bool Bare_4 = false;
  bool Bare_5 = false;
  bool Bare_6 = false;
  bool Bare_7 = false;

  if( DYSample && BornPt_muons != 0.0){  //Z pT reweighting starting from BORN muons - NOT IF RECO level pT
    
    if(BornMass_muons > 7 && BornMass_muons < 9) {
      if (BornPt_muons > 8) {
	weight_ZpT = GetZReweighting_A(BornPt_muons);
	h_ZpT_A->Fill(BornPt_muons, MCWeight);
      }
      weight_ZpT*=1.76092;
    }
    if(BornMass_muons >= 12 && BornMass_muons < 14) {
      weight_ZpT = GetZReweighting_B(BornPt_muons);
      h_ZpT_B->Fill(BornPt_muons, MCWeight);
      
      weight_ZpT*=0.986201;
    }
    if(BornMass_muons >= 14 && BornMass_muons < 17) {
      weight_ZpT = GetZReweighting_C(BornPt_muons);
      h_ZpT_C->Fill(BornPt_muons, MCWeight);
      
      weight_ZpT*=0.956739;
    }
    if(BornMass_muons >= 17 && BornMass_muons < 22) {
      weight_ZpT = GetZReweighting_D(BornPt_muons);
      h_ZpT_D->Fill(BornPt_muons, MCWeight);
      
      weight_ZpT*=0.943885;
    }
    if(BornMass_muons >= 22 && BornMass_muons < 28) {
      weight_ZpT = GetZReweighting_E(BornPt_muons);
      h_ZpT_E->Fill(BornPt_muons, MCWeight);
      
      weight_ZpT*=0.951644;
    }
    if(BornMass_muons >= 28 && BornMass_muons < 36) {
      weight_ZpT = GetZReweighting_F(BornPt_muons);
      h_ZpT_F->Fill(BornPt_muons, MCWeight);
      
      weight_ZpT*=1.03506;
    }
    if(BornMass_muons >= 36 && BornMass_muons < 46) {
      weight_ZpT = GetZReweighting_G(BornPt_muons);
      h_ZpT_G->Fill(BornPt_muons, MCWeight);
      
      weight_ZpT*=1.00919;
    }
    if(BornMass_muons >= 46 && BornMass_muons < 60) {
      weight_ZpT = GetZReweighting_H(BornPt_muons);
      h_ZpT_H->Fill(BornPt_muons, MCWeight);
      
      weight_ZpT*=1.02745;
    }
    //}
  }
  
  MCWeight*=weight_ZpT; 
  
  if( DYSample && BornPt_muons != 0.0){  //NOT IF RECO level pT
    
    if(BornMass_muons > 7 && BornMass_muons < 9) {
      if (BornPt_muons > 8) {
	h_ZpT_A_After->Fill(BornPt_muons, MCWeight);
      }
    }
    if(BornMass_muons >= 12 && BornMass_muons < 14) {
      h_ZpT_B_After->Fill(BornPt_muons, MCWeight);
    }
    if(BornMass_muons >= 14 && BornMass_muons < 17) {
      h_ZpT_C_After->Fill(BornPt_muons, MCWeight);
	}
    if(BornMass_muons >= 17 && BornMass_muons < 22) {
      h_ZpT_D_After->Fill(BornPt_muons, MCWeight);
    }
    if(BornMass_muons >= 22 && BornMass_muons < 28) {
      h_ZpT_E_After->Fill(BornPt_muons, MCWeight);
    }
    if(BornMass_muons >= 28 && BornMass_muons < 36) {
      h_ZpT_F_After->Fill(BornPt_muons, MCWeight);
    }
    if(BornMass_muons >= 36 && BornMass_muons < 46) {
      h_ZpT_G_After->Fill(BornPt_muons, MCWeight);
    }
    if(BornMass_muons >= 46 && BornMass_muons < 60) {
      h_ZpT_H_After->Fill(BornPt_muons, MCWeight);
    }
    //}	
  }
  
  if (DYSample) { //xsection extraction

    //Z-peak cross section at BORN level
    if (BornPt_muon1 > 25. && BornPt_muon2 > 25. && fabs(BornEta_muon1) < 2.5 && fabs(BornEta_muon2) < 2.5) {
      if (BornMass_muons >= 66. && BornMass_muons <= 116.) {
	h_Invariant_Mass_Z[0][2]->Fill(BornMass_muons,MCWeight_xSec);
      }
    }

    if (BornPt_muon1 > 4.5 && BornPt_muon2 > 4.5  && fabs(BornEta_muon1) < 2.4 && fabs(BornEta_muon2) < 2.4) {
      
      if (BornMass_muons > 7. && BornMass_muons < 9.) {
	if (BornPt_muons > 8) h_Invariant_Mass_TRUTH_B[0][2]->Fill(8.0,MCWeight_xSec);
      }

      if (BornMass_muons >= 12. && BornMass_muons < 14.) h_Invariant_Mass_TRUTH_B[0][2]->Fill(13.0,MCWeight_xSec);
      if (BornMass_muons >= 14. && BornMass_muons < 17.) h_Invariant_Mass_TRUTH_B[0][2]->Fill(15.5,MCWeight_xSec);
      if (BornMass_muons >= 17. && BornMass_muons < 22.) h_Invariant_Mass_TRUTH_B[0][2]->Fill(19.5,MCWeight_xSec);
      if (BornMass_muons >= 22. && BornMass_muons < 28.) h_Invariant_Mass_TRUTH_B[0][2]->Fill(25.0,MCWeight_xSec);
      if (BornMass_muons >= 28. && BornMass_muons < 36.) h_Invariant_Mass_TRUTH_B[0][2]->Fill(32.0,MCWeight_xSec);
      if (BornMass_muons >= 36. && BornMass_muons < 46.) h_Invariant_Mass_TRUTH_B[0][2]->Fill(41.0,MCWeight_xSec);
      if (BornMass_muons >= 46. && BornMass_muons < 60.) h_Invariant_Mass_TRUTH_B[0][2]->Fill(53.0,MCWeight_xSec);

      if (BornMass_muons > 7. && BornMass_muons < 9.) {
	if (BornPt_muons > 8) {
	  Born_0 = true;
	  
	  if (fabs(BornRapidity_muons) > 0. && fabs(BornRapidity_muons) < 0.4) h_Muon_Rapidity_TRUTH[0][0][2]->Fill(0.2, MCWeight_xSec);
	  if (fabs(BornRapidity_muons) >= 0.4 && fabs(BornRapidity_muons) < 0.8) h_Muon_Rapidity_TRUTH[0][0][2]->Fill(0.6, MCWeight_xSec);
	  if (fabs(BornRapidity_muons) >= 0.8 && fabs(BornRapidity_muons) < 1.2) h_Muon_Rapidity_TRUTH[0][0][2]->Fill(1.0, MCWeight_xSec);
	  if (fabs(BornRapidity_muons) >= 1.2 && fabs(BornRapidity_muons) < 1.6) h_Muon_Rapidity_TRUTH[0][0][2]->Fill(1.4, MCWeight_xSec);
	  if (fabs(BornRapidity_muons) >= 1.6 && fabs(BornRapidity_muons) < 2.0) h_Muon_Rapidity_TRUTH[0][0][2]->Fill(1.8, MCWeight_xSec);
	  if (fabs(BornRapidity_muons) >= 2.0 && fabs(BornRapidity_muons) < 2.4) h_Muon_Rapidity_TRUTH[0][0][2]->Fill(2.2, MCWeight_xSec);
	}
      }

      if (BornMass_muons >= 12. && BornMass_muons < 14.) {
      
	Born_1 = true;

	if (fabs(BornRapidity_muons) > 0. && fabs(BornRapidity_muons) < 0.4) h_Muon_Rapidity_TRUTH[0][1][2]->Fill(0.2, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 0.4 && fabs(BornRapidity_muons) < 0.8) h_Muon_Rapidity_TRUTH[0][1][2]->Fill(0.6, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 0.8 && fabs(BornRapidity_muons) < 1.2) h_Muon_Rapidity_TRUTH[0][1][2]->Fill(1.0, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 1.2 && fabs(BornRapidity_muons) < 1.6) h_Muon_Rapidity_TRUTH[0][1][2]->Fill(1.4, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 1.6 && fabs(BornRapidity_muons) < 2.0) h_Muon_Rapidity_TRUTH[0][1][2]->Fill(1.8, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 2.0 && fabs(BornRapidity_muons) < 2.4) h_Muon_Rapidity_TRUTH[0][1][2]->Fill(2.2, MCWeight_xSec);

      }

      if (BornMass_muons >= 14. && BornMass_muons < 17.) {
      
	Born_2 = true;

	if (fabs(BornRapidity_muons) > 0. && fabs(BornRapidity_muons) < 0.4) h_Muon_Rapidity_TRUTH[0][2][2]->Fill(0.2, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 0.4 && fabs(BornRapidity_muons) < 0.8) h_Muon_Rapidity_TRUTH[0][2][2]->Fill(0.6, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 0.8 && fabs(BornRapidity_muons) < 1.2) h_Muon_Rapidity_TRUTH[0][2][2]->Fill(1.0, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 1.2 && fabs(BornRapidity_muons) < 1.6) h_Muon_Rapidity_TRUTH[0][2][2]->Fill(1.4, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 1.6 && fabs(BornRapidity_muons) < 2.0) h_Muon_Rapidity_TRUTH[0][2][2]->Fill(1.8, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 2.0 && fabs(BornRapidity_muons) < 2.4) h_Muon_Rapidity_TRUTH[0][2][2]->Fill(2.2, MCWeight_xSec);

      }

      if (BornMass_muons >= 17. && BornMass_muons < 22.) {
      
	Born_3 = true;

	if (fabs(BornRapidity_muons) > 0. && fabs(BornRapidity_muons) < 0.4) h_Muon_Rapidity_TRUTH[0][3][2]->Fill(0.2, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 0.4 && fabs(BornRapidity_muons) < 0.8) h_Muon_Rapidity_TRUTH[0][3][2]->Fill(0.6, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 0.8 && fabs(BornRapidity_muons) < 1.2) h_Muon_Rapidity_TRUTH[0][3][2]->Fill(1.0, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 1.2 && fabs(BornRapidity_muons) < 1.6) h_Muon_Rapidity_TRUTH[0][3][2]->Fill(1.4, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 1.6 && fabs(BornRapidity_muons) < 2.0) h_Muon_Rapidity_TRUTH[0][3][2]->Fill(1.8, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 2.0 && fabs(BornRapidity_muons) < 2.4) h_Muon_Rapidity_TRUTH[0][3][2]->Fill(2.2, MCWeight_xSec);

      }

      if (BornMass_muons >= 22. && BornMass_muons < 28.) {
      
	Born_4 = true;

	if (fabs(BornRapidity_muons) > 0. && fabs(BornRapidity_muons) < 0.4) h_Muon_Rapidity_TRUTH[0][4][2]->Fill(0.2, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 0.4 && fabs(BornRapidity_muons) < 0.8) h_Muon_Rapidity_TRUTH[0][4][2]->Fill(0.6, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 0.8 && fabs(BornRapidity_muons) < 1.2) h_Muon_Rapidity_TRUTH[0][4][2]->Fill(1.0, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 1.2 && fabs(BornRapidity_muons) < 1.6) h_Muon_Rapidity_TRUTH[0][4][2]->Fill(1.4, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 1.6 && fabs(BornRapidity_muons) < 2.0) h_Muon_Rapidity_TRUTH[0][4][2]->Fill(1.8, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 2.0 && fabs(BornRapidity_muons) < 2.4) h_Muon_Rapidity_TRUTH[0][4][2]->Fill(2.2, MCWeight_xSec);

      }

      if (BornMass_muons >= 28. && BornMass_muons < 36.) {
      
	Born_5 = true;

	if (fabs(BornRapidity_muons) > 0. && fabs(BornRapidity_muons) < 0.4) h_Muon_Rapidity_TRUTH[0][5][2]->Fill(0.2, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 0.4 && fabs(BornRapidity_muons) < 0.8) h_Muon_Rapidity_TRUTH[0][5][2]->Fill(0.6, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 0.8 && fabs(BornRapidity_muons) < 1.2) h_Muon_Rapidity_TRUTH[0][5][2]->Fill(1.0, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 1.2 && fabs(BornRapidity_muons) < 1.6) h_Muon_Rapidity_TRUTH[0][5][2]->Fill(1.4, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 1.6 && fabs(BornRapidity_muons) < 2.0) h_Muon_Rapidity_TRUTH[0][5][2]->Fill(1.8, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 2.0 && fabs(BornRapidity_muons) < 2.4) h_Muon_Rapidity_TRUTH[0][5][2]->Fill(2.2, MCWeight_xSec);

      }

      if (BornMass_muons >= 36. && BornMass_muons < 46.) {
      
	Born_6 = true;

	if (fabs(BornRapidity_muons) > 0. && fabs(BornRapidity_muons) < 0.4) h_Muon_Rapidity_TRUTH[0][6][2]->Fill(0.2, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 0.4 && fabs(BornRapidity_muons) < 0.8) h_Muon_Rapidity_TRUTH[0][6][2]->Fill(0.6, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 0.8 && fabs(BornRapidity_muons) < 1.2) h_Muon_Rapidity_TRUTH[0][6][2]->Fill(1.0, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 1.2 && fabs(BornRapidity_muons) < 1.6) h_Muon_Rapidity_TRUTH[0][6][2]->Fill(1.4, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 1.6 && fabs(BornRapidity_muons) < 2.0) h_Muon_Rapidity_TRUTH[0][6][2]->Fill(1.8, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 2.0 && fabs(BornRapidity_muons) < 2.4) h_Muon_Rapidity_TRUTH[0][6][2]->Fill(2.2, MCWeight_xSec);

      }

      if (BornMass_muons >= 46. && BornMass_muons < 60.) {
      
	Born_7 = true;

	if (fabs(BornRapidity_muons) > 0. && fabs(BornRapidity_muons) < 0.4) h_Muon_Rapidity_TRUTH[0][7][2]->Fill(0.2, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 0.4 && fabs(BornRapidity_muons) < 0.8) h_Muon_Rapidity_TRUTH[0][7][2]->Fill(0.6, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 0.8 && fabs(BornRapidity_muons) < 1.2) h_Muon_Rapidity_TRUTH[0][7][2]->Fill(1.0, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 1.2 && fabs(BornRapidity_muons) < 1.6) h_Muon_Rapidity_TRUTH[0][7][2]->Fill(1.4, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 1.6 && fabs(BornRapidity_muons) < 2.0) h_Muon_Rapidity_TRUTH[0][7][2]->Fill(1.8, MCWeight_xSec);
	if (fabs(BornRapidity_muons) >= 2.0 && fabs(BornRapidity_muons) < 2.4) h_Muon_Rapidity_TRUTH[0][7][2]->Fill(2.2, MCWeight_xSec);

      }
    }
    
    //Bare muons studies - extracting the xsection  at BARE level (if needed)
    if (BareMuon1.Pt() > 4.5 && BareMuon2.Pt() > 4.5  && fabs(BareMuon1.Eta()) < 2.4 && fabs(BareMuon2.Eta()) < 2.4) {

      
      if (BareMass_muons > 7. && BareMass_muons < 9.) {
	if (BarePt_muons > 8) h_Invariant_Mass_TRUTH_BARE_B[0][2]->Fill(8.0,MCWeight_xSec);
      }
      if (BareMass_muons >= 12. && BareMass_muons < 14.) h_Invariant_Mass_TRUTH_BARE_B[0][2]->Fill(13.0,MCWeight_xSec);
      if (BareMass_muons >= 14. && BareMass_muons < 17.) h_Invariant_Mass_TRUTH_BARE_B[0][2]->Fill(15.5,MCWeight_xSec);
      if (BareMass_muons >= 17. && BareMass_muons < 22.) h_Invariant_Mass_TRUTH_BARE_B[0][2]->Fill(19.5,MCWeight_xSec);
      if (BareMass_muons >= 22. && BareMass_muons < 28.) h_Invariant_Mass_TRUTH_BARE_B[0][2]->Fill(25.0,MCWeight_xSec);
      if (BareMass_muons >= 28. && BareMass_muons < 36.) h_Invariant_Mass_TRUTH_BARE_B[0][2]->Fill(32.0,MCWeight_xSec);
      if (BareMass_muons >= 36. && BareMass_muons < 46.) h_Invariant_Mass_TRUTH_BARE_B[0][2]->Fill(41.0,MCWeight_xSec);
      if (BareMass_muons >= 46. && BareMass_muons < 60.) h_Invariant_Mass_TRUTH_BARE_B[0][2]->Fill(53.0,MCWeight_xSec);

      if (BareMass_muons > 7. && BareMass_muons < 9.) {
	if (BarePt_muons > 8) {
	  
	  Bare_0 = true;
	  
	  if (fabs(BarePropagator.Rapidity()) > 0. && fabs(BarePropagator.Rapidity()) < 0.4) h_Muon_Rapidity_TRUTH_BARE[0][0][2]->Fill(0.2, MCWeight_xSec);
	  if (fabs(BarePropagator.Rapidity()) >= 0.4 && fabs(BarePropagator.Rapidity()) < 0.8) h_Muon_Rapidity_TRUTH_BARE[0][0][2]->Fill(0.6, MCWeight_xSec);
	  if (fabs(BarePropagator.Rapidity()) >= 0.8 && fabs(BarePropagator.Rapidity()) < 1.2) h_Muon_Rapidity_TRUTH_BARE[0][0][2]->Fill(1.0, MCWeight_xSec);
	  if (fabs(BarePropagator.Rapidity()) >= 1.2 && fabs(BarePropagator.Rapidity()) < 1.6) h_Muon_Rapidity_TRUTH_BARE[0][0][2]->Fill(1.4, MCWeight_xSec);
	  if (fabs(BarePropagator.Rapidity()) >= 1.6 && fabs(BarePropagator.Rapidity()) < 2.0) h_Muon_Rapidity_TRUTH_BARE[0][0][2]->Fill(1.8, MCWeight_xSec);
	  if (fabs(BarePropagator.Rapidity()) >= 2.0 && fabs(BarePropagator.Rapidity()) < 2.4) h_Muon_Rapidity_TRUTH_BARE[0][0][2]->Fill(2.2, MCWeight_xSec);
	  
	}
      }

      if (BareMass_muons >= 12. && BareMass_muons < 14.) {
      
	Bare_1 = true;

	if (fabs(BarePropagator.Rapidity()) > 0. && fabs(BarePropagator.Rapidity()) < 0.4) h_Muon_Rapidity_TRUTH_BARE[0][1][2]->Fill(0.2, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 0.4 && fabs(BarePropagator.Rapidity()) < 0.8) h_Muon_Rapidity_TRUTH_BARE[0][1][2]->Fill(0.6, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 0.8 && fabs(BarePropagator.Rapidity()) < 1.2) h_Muon_Rapidity_TRUTH_BARE[0][1][2]->Fill(1.0, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 1.2 && fabs(BarePropagator.Rapidity()) < 1.6) h_Muon_Rapidity_TRUTH_BARE[0][1][2]->Fill(1.4, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 1.6 && fabs(BarePropagator.Rapidity()) < 2.0) h_Muon_Rapidity_TRUTH_BARE[0][1][2]->Fill(1.8, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 2.0 && fabs(BarePropagator.Rapidity()) < 2.4) h_Muon_Rapidity_TRUTH_BARE[0][1][2]->Fill(2.2, MCWeight_xSec);

      }

      if (BareMass_muons >= 14. && BareMass_muons < 17.) {
      
	Bare_2 = true;

	if (fabs(BarePropagator.Rapidity()) > 0. && fabs(BarePropagator.Rapidity()) < 0.4) h_Muon_Rapidity_TRUTH_BARE[0][2][2]->Fill(0.2, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 0.4 && fabs(BarePropagator.Rapidity()) < 0.8) h_Muon_Rapidity_TRUTH_BARE[0][2][2]->Fill(0.6, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 0.8 && fabs(BarePropagator.Rapidity()) < 1.2) h_Muon_Rapidity_TRUTH_BARE[0][2][2]->Fill(1.0, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 1.2 && fabs(BarePropagator.Rapidity()) < 1.6) h_Muon_Rapidity_TRUTH_BARE[0][2][2]->Fill(1.4, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 1.6 && fabs(BarePropagator.Rapidity()) < 2.0) h_Muon_Rapidity_TRUTH_BARE[0][2][2]->Fill(1.8, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 2.0 && fabs(BarePropagator.Rapidity()) < 2.4) h_Muon_Rapidity_TRUTH_BARE[0][2][2]->Fill(2.2, MCWeight_xSec);

      }

      if (BareMass_muons >= 17. && BareMass_muons < 22.) {
      
	Bare_3 = true;

	if (fabs(BarePropagator.Rapidity()) > 0. && fabs(BarePropagator.Rapidity()) < 0.4) h_Muon_Rapidity_TRUTH_BARE[0][3][2]->Fill(0.2, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 0.4 && fabs(BarePropagator.Rapidity()) < 0.8) h_Muon_Rapidity_TRUTH_BARE[0][3][2]->Fill(0.6, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 0.8 && fabs(BarePropagator.Rapidity()) < 1.2) h_Muon_Rapidity_TRUTH_BARE[0][3][2]->Fill(1.0, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 1.2 && fabs(BarePropagator.Rapidity()) < 1.6) h_Muon_Rapidity_TRUTH_BARE[0][3][2]->Fill(1.4, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 1.6 && fabs(BarePropagator.Rapidity()) < 2.0) h_Muon_Rapidity_TRUTH_BARE[0][3][2]->Fill(1.8, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 2.0 && fabs(BarePropagator.Rapidity()) < 2.4) h_Muon_Rapidity_TRUTH_BARE[0][3][2]->Fill(2.2, MCWeight_xSec);

      }

      if (BareMass_muons >= 22. && BareMass_muons < 28.) {
      
	Bare_4 = true;

	if (fabs(BarePropagator.Rapidity()) > 0. && fabs(BarePropagator.Rapidity()) < 0.4) h_Muon_Rapidity_TRUTH_BARE[0][4][2]->Fill(0.2, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 0.4 && fabs(BarePropagator.Rapidity()) < 0.8) h_Muon_Rapidity_TRUTH_BARE[0][4][2]->Fill(0.6, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 0.8 && fabs(BarePropagator.Rapidity()) < 1.2) h_Muon_Rapidity_TRUTH_BARE[0][4][2]->Fill(1.0, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 1.2 && fabs(BarePropagator.Rapidity()) < 1.6) h_Muon_Rapidity_TRUTH_BARE[0][4][2]->Fill(1.4, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 1.6 && fabs(BarePropagator.Rapidity()) < 2.0) h_Muon_Rapidity_TRUTH_BARE[0][4][2]->Fill(1.8, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 2.0 && fabs(BarePropagator.Rapidity()) < 2.4) h_Muon_Rapidity_TRUTH_BARE[0][4][2]->Fill(2.2, MCWeight_xSec);

      }

      if (BareMass_muons >= 28. && BareMass_muons < 36.) {
      
	Bare_5 = true;

	if (fabs(BarePropagator.Rapidity()) > 0. && fabs(BarePropagator.Rapidity()) < 0.4) h_Muon_Rapidity_TRUTH_BARE[0][5][2]->Fill(0.2, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 0.4 && fabs(BarePropagator.Rapidity()) < 0.8) h_Muon_Rapidity_TRUTH_BARE[0][5][2]->Fill(0.6, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 0.8 && fabs(BarePropagator.Rapidity()) < 1.2) h_Muon_Rapidity_TRUTH_BARE[0][5][2]->Fill(1.0, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 1.2 && fabs(BarePropagator.Rapidity()) < 1.6) h_Muon_Rapidity_TRUTH_BARE[0][5][2]->Fill(1.4, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 1.6 && fabs(BarePropagator.Rapidity()) < 2.0) h_Muon_Rapidity_TRUTH_BARE[0][5][2]->Fill(1.8, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 2.0 && fabs(BarePropagator.Rapidity()) < 2.4) h_Muon_Rapidity_TRUTH_BARE[0][5][2]->Fill(2.2, MCWeight_xSec);

      }

      if (BareMass_muons >= 36. && BareMass_muons < 46.) {
      
	Bare_6 = true;

	if (fabs(BarePropagator.Rapidity()) > 0. && fabs(BarePropagator.Rapidity()) < 0.4) h_Muon_Rapidity_TRUTH_BARE[0][6][2]->Fill(0.2, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 0.4 && fabs(BarePropagator.Rapidity()) < 0.8) h_Muon_Rapidity_TRUTH_BARE[0][6][2]->Fill(0.6, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 0.8 && fabs(BarePropagator.Rapidity()) < 1.2) h_Muon_Rapidity_TRUTH_BARE[0][6][2]->Fill(1.0, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 1.2 && fabs(BarePropagator.Rapidity()) < 1.6) h_Muon_Rapidity_TRUTH_BARE[0][6][2]->Fill(1.4, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 1.6 && fabs(BarePropagator.Rapidity()) < 2.0) h_Muon_Rapidity_TRUTH_BARE[0][6][2]->Fill(1.8, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 2.0 && fabs(BarePropagator.Rapidity()) < 2.4) h_Muon_Rapidity_TRUTH_BARE[0][6][2]->Fill(2.2, MCWeight_xSec);

      }

      if (BareMass_muons >= 46. && BareMass_muons < 60.) {
      
	Bare_7 = true;

	if (fabs(BarePropagator.Rapidity()) > 0. && fabs(BarePropagator.Rapidity()) < 0.4) h_Muon_Rapidity_TRUTH_BARE[0][7][2]->Fill(0.2, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 0.4 && fabs(BarePropagator.Rapidity()) < 0.8) h_Muon_Rapidity_TRUTH_BARE[0][7][2]->Fill(0.6, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 0.8 && fabs(BarePropagator.Rapidity()) < 1.2) h_Muon_Rapidity_TRUTH_BARE[0][7][2]->Fill(1.0, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 1.2 && fabs(BarePropagator.Rapidity()) < 1.6) h_Muon_Rapidity_TRUTH_BARE[0][7][2]->Fill(1.4, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 1.6 && fabs(BarePropagator.Rapidity()) < 2.0) h_Muon_Rapidity_TRUTH_BARE[0][7][2]->Fill(1.8, MCWeight_xSec);
	if (fabs(BarePropagator.Rapidity()) >= 2.0 && fabs(BarePropagator.Rapidity()) < 2.4) h_Muon_Rapidity_TRUTH_BARE[0][7][2]->Fill(2.2, MCWeight_xSec);
	
      }
      
    }
  }
  
  EventWeight*=weight_ZpT; //GET THE CORRECT ACCEPTANCE
  
  if(!isMC){
    if( (eventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error ) || (eventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error ) || (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18) )  )  return EL::StatusCode::SUCCESS; // go to the next event if not clean data event                                                                   
  } // end if the event is data
  
  m_numCleanEvents++;
  h_CutFlow_Common->Fill(2.0, EventWeight);
  
  uint mu_n = 0;
  mu_n = muons->size();
  
  // >= 2 Muons in the Event.                                                                                                           
  if (mu_n < 2) return EL::StatusCode::SUCCESS;
  h_CutFlow_Common->Fill(3.0, EventWeight);

  // Primary Vertex                                                                                                                               
  for (unsigned int i=0; i<vxContainer->size(); i++){
    const xAOD::Vertex* vxcand = vxContainer->at(i);
    if ( vxcand->nTrackParticles() > 2 ){
      primVtx = true;
      nVtx++;
    }
  }

  if (!primVtx) return EL::StatusCode::SUCCESS;
  h_CutFlow_Common->Fill(4.0, EventWeight);

  Int_t NPV = vxContainer->size();

  Double_t Mu = PRWToolHandle->getCorrectedMu(*eventInfo);

  //vector containing the corrected energy                                                                                           
  vector<double> muE(mu_n,0);
  vector<double> mupT;


  bool TriggerDecision_mu6[3] = {false, false, false};
  TriggerDecision_mu6[0] = m_TrigDecTool->isPassed("HLT_mu6_iloose_mu6_11invm24_noos");
  TriggerDecision_mu6[1] = m_TrigDecTool->isPassed("HLT_mu6_iloose_mu6_24invm60_noos");
  TriggerDecision_mu6[2] = m_TrigDecTool->isPassed("HLT_2mu6_10invm30_pt2_z10");

  bool muon_Trigger_dec[5] = {false, false, false, false, false};

  muon_Trigger_dec[0] = m_TrigDecTool->isPassed("HLT_mu4_iloose_mu4_7invm9_noos");
  
  muon_Trigger_dec[1] = m_TrigDecTool->isPassed("HLT_mu4_iloose_mu4_11invm60_noos");

  //OR of all the 2mu6 triggers
  muon_Trigger_dec[2] = (TriggerDecision_mu6[0] || TriggerDecision_mu6[1]);
 	    
  // Set systematic variation
  if (Arg_Sys=="Nom") sys_name = "";
  else if (Arg_Sys=="Mu_ResID_Up") sys_name = "MUON_ID__1up";
  else if (Arg_Sys=="Mu_ResID_Down") sys_name = "MUON_ID__1down";
  else if (Arg_Sys=="Mu_ResMS_Up") sys_name = "MUON_MS__1up";
  else if (Arg_Sys=="Mu_ResMS_Down") sys_name = "MUON_MS__1down";
  else if (Arg_Sys=="Mu_Scale_Up") sys_name = "MUON_SCALE__1up";
  else if (Arg_Sys=="Mu_Scale_Down") sys_name = "MUON_SCALE__1down";
  else if (Arg_Sys=="Mu_SagittaRho_Up") sys_name = "MUON_SAGITTA_RHO__1up";
  else if (Arg_Sys=="Mu_SagittaRho_Down") sys_name = "MUON_SAGITTA_RHO__1down";
  else if (Arg_Sys=="Mu_SagittaBias_Up") sys_name = "MUON_SAGITTA_RESBIAS__1up";
  else if (Arg_Sys=="Mu_SagittaBias_Down") sys_name = "MUON_SAGITTA_RESBIAS__1down";
  else if (Arg_Sys=="Mu_Eff_IDUpSys") sys_name = "MUON_EFF_SYS__1up";
  else if (Arg_Sys=="Mu_Eff_IDDownSys") sys_name = "MUON_EFF_SYS__1down";
  else if (Arg_Sys=="Mu_Eff_IDUpStat") sys_name = "MUON_EFF_STAT__1up";
  else if (Arg_Sys=="Mu_Eff_IDDownStat") sys_name = "MUON_EFF_STAT__1down";
  else if (Arg_Sys=="Mu_Eff_IDUpStat_lowpT") sys_name = "MUON_EFF_STAT_LOWPT__1up";
  else if (Arg_Sys=="Mu_Eff_IDDownStat_lowpT") sys_name = "MUON_EFF_STAT_LOWPT__1down";
  else if (Arg_Sys=="Mu_Eff_IDUpSys_lowpT") sys_name = "MUON_EFF_SYS_LOWPT__1up";
  else if (Arg_Sys=="Mu_Eff_IDDownSys_lowpT") sys_name = "MUON_EFF_SYS_LOWPT__1down";
  else if (Arg_Sys=="Mu_Eff_IsoUpStat") sys_name = "MUON_ISO_STAT__1up";
  else if (Arg_Sys=="Mu_Eff_IsoDownStat") sys_name = "MUON_ISO_STAT__1down";
  else if (Arg_Sys=="Mu_Eff_IsoUpSys") sys_name = "MUON_ISO_SYS__1up";
  else if (Arg_Sys=="Mu_Eff_IsoDownSys") sys_name = "MUON_ISO_SYS__1down";
  else if (Arg_Sys=="Mu_Eff_TrigUpSys") sys_name = "MUON_EFF_TrigSystUncertainty__1up";
  else if (Arg_Sys=="Mu_Eff_TrigDownSys") sys_name = "MUON_EFF_TrigSystUncertainty__1down";
  else if (Arg_Sys=="Mu_Eff_TrigUpStat") sys_name = "MUON_EFF_TrigStatUncertainty__1up";
  else if (Arg_Sys=="Mu_Eff_TrigDownStat") sys_name = "MUON_EFF_TrigStatUncertainty__1down";
  else if (Arg_Sys=="Mu_Trk_d0_Up") sys_name = "TRK_RES_D0_MEAS_UP";
  else if (Arg_Sys=="Mu_Trk_d0_Down") sys_name = "TRK_RES_D0_MEAS_DOWN";
  else if (Arg_Sys=="Mu_Trk_z0_Up") sys_name = "TRK_RES_Z0_MEAS_UP";
  else if (Arg_Sys=="Mu_Trk_z0_Down") sys_name = "TRK_RES_Z0_MEAS_DOWN";
  else{
    return EL::StatusCode::FAILURE;
  }

  //////////////////////////////////////////////////////////////////////
  //
  //              MUON SELECTION
  //
  //////////////////////////////////////////////////////////////////////

  bool MuonCB_bool = false, MuonPt_bool = false, Muond0_bool = false, MuonEta_bool = false, Muonz0_bool = false; 
  //int TwoMuonsPt=0, TwoMuonsd0=0, TwoMuonsEta=0, TwoMuonsz0=0, NumberCombinedMuons=0;
  int TwoMuonsPt=0, TwoMuonsEta=0;

  xAOD::TStore* store = wk()->xaodStore();

  // create a shallow copy of the muons container
  std::pair< xAOD::MuonContainer*, xAOD::ShallowAuxContainer* > muons_shallowCopy = xAOD::shallowCopyContainer( *muons );

  // iterate over our shallow copy
  xAOD::MuonContainer::iterator muonSC_itr = (muons_shallowCopy.first)->begin();
  xAOD::MuonContainer::iterator muonSC_end = (muons_shallowCopy.first)->end();

  xAOD::MuonContainer* muonsCorr = muons_shallowCopy.first;

  EL_RETURN_CHECK( "GetMuonsForAnalysis()", store->record( muons_shallowCopy.first, "ShallowCopiedMuons" ) );
  EL_RETURN_CHECK( "GetMuonsForAnalysis()", store->record( muons_shallowCopy.second, "ShallowCopiedMuonsAux." ) );

  //Index vector
  std::vector<int> index;
  (index).reserve(mu_n);

  //Vector of structs ---> declaration
  Int_t Counter = -1;

  for( ; muonSC_itr != muonSC_end; ++muonSC_itr ) {

    Counter++;

    //Applying Systematic Uncertainties
    for (sysListItr = m_sysList.begin(); sysListItr != m_sysList.end(); ++sysListItr){
      if (!((*sysListItr).name()==sys_name)) continue;
	  
      if( m_muonCalibrationAndSmearingTool->applySystematicVariation( *sysListItr ) == CP::SystematicCode::Unsupported ) {
	Error("execute()", "Cannot configure MuonMomentumCorretions tool for systematic" );
      } // end check that systematic applied ok
	  	  
      if( m_muiso_sf_FixedCutTight->applySystematicVariation( *sysListItr ) == CP::SystematicCode::Unsupported ) {
	Error("execute()", "Cannot configure iso muon SF tool for systematic (FixedTight)" );
      } // end check that systematic applied ok
	  
      if( m_mueff_corr_Medium->applySystematicVariation( *sysListItr ) == CP::SystematicCode::Unsupported ) {
	Error("execute()", "Cannot configure muon reco SF tool for systematic" );
      } // end check that systematic applied ok
	  	  
    } // end for loop over systematics
	
    if(m_muonCalibrationAndSmearingTool->applyCorrection(**muonSC_itr) == CP::CorrectionCode::Error){ // apply correction and check return code
      // Can have CorrectionCode values of Ok, OutOfValidityRange, or Error. Here only checking for Error.
      // If OutOfValidityRange is returned no modification is made and the original muon values are taken.
      Error("execute()", "MuonCalibrationAndSmearingTool returns Error CorrectionCode");
    } // end of error message

    //Combined Muon
    //if( (*muonSC_itr)->auxdata< unsigned short >( "muonType" ) == xAOD::Muon::MuonType::Combined ) {
    //NumberCombinedMuons++; 
    //if( NumberCombinedMuons > 1 ) MuonCB_bool = true;
    //if (MuonCB_bool == true && NumberCombinedMuons == 2 ) {
    //h_CutFlow_Common->Fill(5);
    //}
    //}
    
    //Define Impact Parameter quantities - d0, z0
    const xAOD::TrackParticle* trk = (*muonSC_itr)->trackParticle(xAOD::Muon::TrackParticleType::InnerDetectorTrackParticle);

    if (isMC) {
      if( trk ){
	if(xAOD::TrackingHelpers::hasValidCov( trk )){
      
	  //smeared track particle in muon shallow copy
	  trk_Cpy_d0 = new xAOD::TrackParticle;
	  trk_Cpy_z0 = new xAOD::TrackParticle;
	
	  //d0 and z0 smearing	
	  if (m_smearingTool_d0->correctedCopy(*trk, trk_Cpy_d0) == CP::CorrectionCode::Error ) {                                                                                 
	    Error("execute()", "Could not apply smearing correction (d0)!" );                                                                                             
	  }                                                                                                                                                                 
	
	  if (m_smearingTool_z0->correctedCopy(*trk, trk_Cpy_z0) == CP::CorrectionCode::Error ) {                                                                        
	    Error("execute()", "Could not apply smearing correction (z0)!" );                                                                                          
	  }                              
	
	  (*muonSC_itr)->auxdata<float>("smeared_d0") = trk_Cpy_d0->d0();
	  (*muonSC_itr)->auxdata<float>("smeared_z0") = trk_Cpy_z0->z0();
	
	  delete trk_Cpy_d0;
	  delete trk_Cpy_z0;
    
	} else {
	  (*muonSC_itr)->auxdata<float>("smeared_d0") = 0.;

	  (*muonSC_itr)->auxdata<float>("smeared_z0") = 0.;      

	  std::cout<<"No valid cov matrix"<<std::endl;
	}
      } else {  
	(*muonSC_itr)->auxdata<float>("smeared_d0") = 0.;

	(*muonSC_itr)->auxdata<float>("smeared_z0") = 0.;      

      }
    }

    // Muon pT Requirement 
    if( (*muonSC_itr)->pt() * GeV  < 4.5 ) continue;     
    if( (*muonSC_itr)->pt() * GeV  > 4.5 ) {
      TwoMuonsPt++;     
      if( TwoMuonsPt > 1 ) MuonPt_bool = true; 
      if (MuonCB_bool == true && Muond0_bool == true && Muonz0_bool == true && MuonPt_bool == true && TwoMuonsPt == 2 ) {
	h_CutFlow_Common->Fill(8.0, EventWeight);
      }
    }

    // Muon Eta requirement
    if( fabs((*muonSC_itr)->eta()) > 2.4 ) continue;      
    if( fabs((*muonSC_itr)->eta()) < 2.4 ) { 
      TwoMuonsEta++;      
      if( TwoMuonsEta > 1 ) MuonEta_bool = true;
      if (MuonCB_bool == true && Muond0_bool == true && Muonz0_bool == true && MuonPt_bool == true && MuonEta_bool == true && TwoMuonsEta == 2) {
	h_CutFlow_Common->Fill(9.0, EventWeight);
      }
    }

    //Quality requirement - we want to select MEDIUM-quality muons
    if (!m_muonSelection_MEDIUM->accept(*(*muonSC_itr))) continue;

    //Storing the index of the muon passing the common selection
    index.push_back(Counter);

    mupT.push_back(((*muonSC_itr)->pt())*GeV);

  } //fisrt loop on the shallow copy (common selection) ends

  //sorting (so the first element of the muon container is the one with the highest pT
  bubblesort(mupT, index);

  int index_n = index.size();

  bool TwoMuons = false;

  //Passing JUST the low mass DY triggers 
  if(!muon_Trigger_dec[0] && !muon_Trigger_dec[1] && !muon_Trigger_dec[2]) return EL::StatusCode::SUCCESS; //Analysis triggers

  //Pre-selections 
  //We consider event with two muons at least
  if(index_n < 2) return EL::StatusCode::SUCCESS; 
  if(index_n >= 2) TwoMuons = true;
  if ( MuonPt_bool == true && MuonEta_bool == true && TwoMuons == true) {
    h_CutFlow_Common->Fill(10.0, EventWeight);
  }

  std::vector<bool> muon_SelectionM;
  std::vector<bool> muon_SelectionT;
  std::vector<bool> muon_Iso1;
  std::vector<bool> muon_Iso2;
  std::vector<bool> muon_Iso3;
  std::vector<bool> muon_Iso4;
  std::vector<bool> muon_Iso5;
  muon_SelectionM.reserve(index_n);  
  muon_SelectionT.reserve(index_n);  
  muon_Iso1.reserve(index_n);  
  muon_Iso2.reserve(index_n);   
  muon_Iso3.reserve(index_n);  
  muon_Iso4.reserve(index_n);   
  muon_Iso5.reserve(index_n);  
  
  //Second loop over the muons which have already passed minimum selection (first loop)
  for(int count = 0; count < index_n; count++){
    
    Int_t Selected = index.at(count);
    
    
    muon_SelectionM[count] = true; //Quality requirement already in the loop over the shallow copy 

    muon_Iso1[count] = true; //NO ISOLATION

    muon_Iso2[count] = true; //NO ISOLATION

    muon_Iso3[count] = true; //NO ISOLATION

    muon_Iso4[count] = true; //NO ISOLATION

    if(!muon_Iso1[count] && !muon_Iso2[count] && !muon_Iso3[count] && !muon_Iso4[count] && !muon_Iso5[count]) {   
      return EL::StatusCode::SUCCESS;
    } 
  }
   
  //Vector for trigger matching
  std::vector<const xAOD::IParticle*> myParticles;
  
  int ilead = 0;
  int isub = 0;

  //Trigger Matching booleans
  std::vector<bool> matchTrigger(5, false); // Creates a vector with 5 elements, each set to false
  bool MuonTriggerMatched = false;
        
  std::vector<bool> matchSingle(2, false); // Creates a vector with 5 elements, each set to false

  // Build the propagator at reconstructed level
  TLorentzVector p1(0,0,0,0), p2(0,0,0,0);
  TLorentzVector p1_CR1(0,0,0,0), p2_CR1(0,0,0,0);
  TLorentzVector p1_CR2(0,0,0,0), p2_CR2(0,0,0,0);
  TLorentzVector p1_sel(0,0,0,0), p2_sel(0,0,0,0);

  bool iso_dec[ 6 ] = {false, false, false, false, false, false};
  iso_dec[ 0 ] = (muon_Iso1[0] && muon_Iso1[1]);
  iso_dec[ 1 ] = (muon_Iso2[0] && muon_Iso2[1]);
  iso_dec[ 2 ] = (muon_Iso3[0] && muon_Iso3[1]);
  iso_dec[ 3 ] = (muon_Iso4[0] && muon_Iso4[1]);
  iso_dec[ 4 ] = (muon_Iso5[0] && muon_Iso5[1]);
  iso_dec[ 5 ] = (muon_Iso3[0] || muon_Iso3[1]);

  bool qual_dec[ 2 ] = { false, false };
  qual_dec[ 0 ] = (muon_SelectionM[0] && muon_SelectionM[1]);
  qual_dec[ 1 ] = (muon_SelectionT[0] && muon_SelectionT[1]);

  for( unsigned int j = 0; j < 1; j++ ) {      //just MEDIUM quality muons
    for( unsigned int i = 2; i < 3; i++ ) {      //OR of the analysis trigger
      if(qual_dec [ j ]) {
	for(int m = 0; m < index_n; m++) {
	  for(int n = m+1; n < index_n; n++) {
	      
	    p1_sel.SetPtEtaPhiM( (muonsCorr)->at(index.at(m))->pt() * GeV, (muonsCorr)->at(index.at(m))->eta() , (muonsCorr)->at(index.at(m))->phi() , MuonPDGMass);
	    p2_sel.SetPtEtaPhiM( (muonsCorr)->at(index.at(n))->pt() * GeV, (muonsCorr)->at(index.at(n))->eta() , (muonsCorr)->at(index.at(n))->phi() , MuonPDGMass);
	      
	    Propagator_sel = p1_sel+p2_sel;
	      
	    myParticles.clear();
	    myParticles.push_back(muonsCorr->at(index.at(m)));
	    myParticles.push_back(muonsCorr->at(index.at(n)));
	      
	    //BPHYSICS TRIGGER FOR UPSILON CR
	    //matchTrigger.at(0) = m_match_Tool->match(myParticles,"HLT_2mu4_bUpsimumu",0.1);
	      
	    //NOMINAL TRIGGERS IN THE ANALYSIS
	    matchTrigger.at(0) = m_match_Tool->match(myParticles,"HLT_mu4_iloose_mu4_7invm9_noos",0.1);
	    matchTrigger.at(1) = m_match_Tool->match(myParticles,"HLT_mu4_iloose_mu4_11invm60_noos",0.1);

	      
	    //SUM OF THE TRIGGERS --> RIGHT STUFF TO DO
	    matchTrigger.at(i) = (matchTrigger.at(0) || matchTrigger.at(1) || (( m_match_Tool->match(myParticles,"HLT_mu6_iloose_mu6_11invm24_noos",0.1)) || (m_match_Tool->match(myParticles,"HLT_mu6_iloose_mu6_24invm60_noos",0.1)))); //Analysis triggers

	    //matchTrigger.at(i) = (( m_match_Tool->match(myParticles,"HLT_mu6_iloose_mu6_11invm24_noos",0.1)) || (m_match_Tool->match(myParticles,"HLT_mu6_iloose_mu6_24invm60_noos",0.1))); //2mu6
	    //matchTrigger.at(i) = (matchTrigger.at(0) || matchTrigger.at(1));  //2mu4

	    //Single-muon triggers (reference)
	    matchSingle.at(0) = m_match_Tool->match(*(muonsCorr->at(index.at(m))),"HLT_mu20_iloose_L1MU15",0.1);
	    if (!(matchSingle.at(0))) {
	      matchSingle.at(0) = m_match_Tool->match(*(muonsCorr->at(index.at(n))),"HLT_mu20_iloose_L1MU15",0.1);
	    }

	    matchSingle.at(1) = m_match_Tool->match(*(muonsCorr->at(index.at(m))),"HLT_mu50",0.1);
	    if (!(matchSingle.at(1))) {
	      matchSingle.at(1) = m_match_Tool->match(*(muonsCorr->at(index.at(n))),"HLT_mu50",0.1);
	    }

	    matchTrigger.at(3) = matchSingle.at(0) || matchSingle.at(1);

	    matchTrigger.at(4) = m_match_Tool->match(myParticles,"HLT_mu18_mu8noL1",0.1);

	    if(matchTrigger.at(3) || matchTrigger.at(4)){
	      MuonTriggerMatched = true;	
	    }

	    //Requiring trigger matching
	    if (matchTrigger.at(i)) {  //ALL THE ANALYSIS TRIGGER
	    //if (matchTrigger.at(0)) { //7M9 2mu4
	    	
	      if((muonsCorr)->at(index.at(m))->pt() > (muonsCorr)->at(index.at(n))->pt() ) {
		ilead=m;
		isub=n;
	      } else if((muonsCorr)->at(index.at(m))->pt() < (muonsCorr)->at(index.at(n))->pt() ){
		ilead=n;
		isub=m;
	      }
		
	      std::pair<int,int> mupair(ilead,isub); //crea il pair 
		
	      mupairs[j][i].push_back(mupair);  //mette il pair nel vettore elemento dell'array
	    }
	  }
	}
      }
    }
  }
  if( MuonPt_bool == true && MuonEta_bool == true && MuonTriggerMatched == true && TwoMuons == true) h_CutFlow_Common->Fill(11.0, EventWeight);

  float d0_significance_lead=0.0;
  float d0_significance_sub=0.0;
  float delta_z0=0.0;  
  float delta_z0_significance=0.0;  
  float delta_z0_lead=0.0;
  float delta_z0_sub=0.0;

  float d0_significance_BOTH=0.0;
  float sigma_d0_lead=0.0;
  float sigma_d0_sub=0.0;

  float chi2 = 0;

  Double_t DeltaPhi_ll = 0.;
  Double_t DeltaR_ll = 0.;

  for( unsigned int j = 0; j < 1; j++ ) {           //just MEDIUM-quality muons
    for( unsigned int i = 2; i < 3; i++ ) {         //OR of the analysis triggers
	  
      Double_t local_EventWeight = EventWeight;
      Double_t local_EventWeight_noPU = EventWeight_noPU;
      Double_t ReWeight = local_EventWeight * weight_ZpT;

      //SFs weights
      Double_t w_Trig_SF_Fir = 1.;
      Double_t w_Trig_SF_Sec = 1.;
      Double_t w_Trig_SF_dimuon = 1.;
      Double_t w_Iso_SF_Fir = 1.;
      Double_t w_Iso_SF_Sec = 1.;
      Double_t w_Reco_SF_Fir = 1.;
      Double_t w_Reco_SF_Sec = 1.;
      Double_t SF_weights_Lead = 1.;
      Double_t SF_weights_Sub = 1.;

      double sf_lead=0;
      double sf_sub=0;
      double sf_dimuon=0;

      int charge_lead=0;
      int charge_sub=0;

      int l = 0;

      //PreScale Efficicency (here I'm taking into account the different luminosity recorded by mu6 and mu4... Then I rescale by mu6 luminosity --> 1280.28 pb-1)
      if(isMC) {
	if ((muon_Trigger_dec[ 0 ] || muon_Trigger_dec[ 1 ]) && !muon_Trigger_dec[ i ]) local_EventWeight*=0.25;
	if ((muon_Trigger_dec[ 0 ] || muon_Trigger_dec[ 1 ]) && !muon_Trigger_dec[ i ]) local_EventWeight_noPU*=0.25;
      }
      
      //Creating the muon pairs objects
      if (mupairs[j][i].size() != 0) {
	for (uint icoppia=0; icoppia<1; icoppia++){
	  int LEAD=index.at(mupairs[j][i].at(icoppia).first);  //Leading muon
	  int SUB=index.at(mupairs[j][i].at(icoppia).second);  //Sub-leading muon

	  //TLorentzVectors for the two muons
	  p1.SetPtEtaPhiM( (muonsCorr)->at(LEAD)->pt() * GeV, (muonsCorr)->at(LEAD)->eta() , (muonsCorr)->at(LEAD)->phi() , MuonPDGMass);
	  p2.SetPtEtaPhiM( (muonsCorr)->at(SUB)->pt() * GeV, (muonsCorr)->at(SUB)->eta() , (muonsCorr)->at(SUB)->phi() , MuonPDGMass);

	  //TLorentzVector for the Z/gamma*
	  Propagator = p1+p2;

	  const xAOD::TrackParticle* trk_lead = (muonsCorr)->at(LEAD)->trackParticle( xAOD::Muon::TrackParticleType::InnerDetectorTrackParticle );
	  const xAOD::TrackParticle* trk_sub = (muonsCorr)->at(SUB)->trackParticle( xAOD::Muon::TrackParticleType::InnerDetectorTrackParticle );

	  //Applying cuts at RECO level
	  if( Propagator.Mag() > 7. && (Propagator.Mag() < 9. || Propagator.Mag() >= 12.) && p1.Pt() > 4.5 && p2.Pt() > 4.5 && fabs(p1.Eta()) < 2.4 && fabs(p2.Eta()) < 2.4  ){
	  //if( Propagator.Mag() > 7. && (Propagator.Mag() < 9. || Propagator.Mag() >= 12.) && p1.Pt() > 4.5 && p2.Pt() > 4.5 && fabs(p1.Eta()) < 2.4 && fabs(p2.Eta()) < 2.4  && Propagator.Pt() > 8.){ //7M9 GeV bin
	    //if( (Propagator.Mag() >= 7.5 && Propagator.Mag() <= 11.5) && p1.Pt() > 4.5 && p2.Pt() > 4.5 && fabs(p1.Eta()) < 2.4 && fabs(p2.Eta()) < 2.4  ){ //UPSILON CR

	    Double_t RapZ_Abs = fabs(Propagator.Rapidity());
		
	    if(muon_Trigger_dec[ 0 ] || muon_Trigger_dec[ 1 ] || (muon_Trigger_dec[ 2 ] && p1.Pt() > 6.0 && p2.Pt() > 6.0)) {
	    //if(muon_Trigger_dec[ 0 ]) {   //2mu4 7M9 events only
	  
	      //Z pT > 8 GeV if event triggerd by the 2mu4_7M9 trigger only
	      if (muon_Trigger_dec[ 0 ] && !(muon_Trigger_dec[ 1 ] || muon_Trigger_dec[ 2 ])) {
		if (Propagator.Pt() < 8.) continue;
	      }

	      //if MC, apply reconstruction Scale Factor (SF) and trigger SF
  	      if (isMC) {
		w_Reco_SF_Fir = GetMuonRecoSF_Medium((muonsCorr->at(LEAD)));
		w_Reco_SF_Sec = GetMuonRecoSF_Medium((muonsCorr->at(SUB)));
		  		
		if (muonsCorr->at(LEAD)->charge() < 0) {
		  charge_lead = -1;
		} else {
		  charge_lead = 1;
		}
		
		if (muonsCorr->at(SUB)->charge() < 0) {
		  charge_sub = -1;
		} else {
		  charge_sub = 1;
		}

		//Trigger SF
		if((muon_Trigger_dec[ 0 ] || muon_Trigger_dec[ 1 ]) && !muon_Trigger_dec[ i ]) {
		  sf_lead = m_sfclass->getTriggerSF(w_Trig_SF_Fir, p1, charge_lead, "mu4", "nominal");
		  sf_sub = m_sfclass->getTriggerSF(w_Trig_SF_Sec, p2, charge_sub, "mu4", "nominal");

		  sf_dimuon = m_sfclass->getDimuonSF(w_Trig_SF_dimuon, p1, charge_lead, p2, charge_sub, "2mu4", "nominal");

		} else if (muon_Trigger_dec[ i ] && p1.Pt() > 6.0 && p2.Pt() > 6.0) {
		  sf_lead = m_sfclass->getTriggerSF(w_Trig_SF_Fir, p1, charge_lead, "mu6", "nominal");
		  sf_sub = m_sfclass->getTriggerSF(w_Trig_SF_Sec, p2, charge_sub, "mu6", "nominal");
		  
		  sf_dimuon = m_sfclass->getDimuonSF(w_Trig_SF_dimuon, p1, charge_lead, p2, charge_sub, "2mu6", "nominal");
		}

		SF_weights_Lead *= w_Reco_SF_Fir;
		SF_weights_Sub *= w_Reco_SF_Sec;
				  
	      }
				
	      local_EventWeight *= SF_weights_Lead * SF_weights_Sub; 
	      local_EventWeight_noPU *= SF_weights_Lead * SF_weights_Sub; 

	      //Applying the correct normalisation to LO PY8 b-bbar/c-cbar MC samples
	      if( (int)mc_channel_number == 363834 || (int)mc_channel_number ==  363833){
		if (Propagator.Mag() > 7. && Propagator.Mag() < 9.) {
		  local_EventWeight *= 0.548995;
		}
		if (Propagator.Mag() >= 12. && Propagator.Mag() < 14.) {
		  local_EventWeight *= 0.320993;
		}
		if (Propagator.Mag() >= 14. && Propagator.Mag() < 17.) {
		  local_EventWeight *= 0.34783;
		}
		if (Propagator.Mag() >= 17. && Propagator.Mag() < 22.) {
		  local_EventWeight *= 0.409328;
		}
		if (Propagator.Mag() >= 22. && Propagator.Mag() < 28.) {
		  local_EventWeight *= 0.473099;
		}
		if (Propagator.Mag() >= 28. && Propagator.Mag() < 36.) {
		  local_EventWeight *= 0.356787;
		}
		if (Propagator.Mag() >= 36. && Propagator.Mag() < 46.) {
		  local_EventWeight *= 0.4420;
		}
		if (Propagator.Mag() >= 46. && Propagator.Mag() < 60.) {
		  local_EventWeight *= 0.40463;
		}
	      }
		    	      
	      if ((muonsCorr->at(LEAD)->charge()*muonsCorr->at(SUB)->charge())<0) {    //OS muon selection
	       
		//Retrieving the smeared IP quantities for MC (we need to smear them in order to reproduce shapes observed in data)
		if (isMC) {

		  if( trk_lead && trk_sub){
		    if( xAOD::TrackingHelpers::hasValidCov( trk_lead ) && xAOD::TrackingHelpers::hasValidCov( trk_sub )){
		      for (unsigned int i=0; i<vxContainer->size(); i++){
			const xAOD::Vertex* vtx = vxContainer->at(i);
			if ( vtx->vertexType() == xAOD::VxType::PriVtx ){
			  delta_z0_lead = fabs(((muonsCorr)->at(LEAD)->auxdata<float>("smeared_z0") + trk_lead->vz() - vtx->z() ) * TMath::Sin( trk_lead->theta()));
			  delta_z0_sub = fabs(((muonsCorr)->at(SUB)->auxdata<float>("smeared_z0") + trk_sub->vz() - vtx->z() ) * TMath::Sin( trk_sub->theta()));
			}
		      }
		      
		      d0_significance_lead  =  TMath::Abs(xAOD::TrackingHelpers::d0significance( trk_lead, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY() ));
		      d0_significance_sub =  TMath::Abs(xAOD::TrackingHelpers::d0significance( trk_sub, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY() ));
		      
		    }
		  }		  
		  
		  delta_z0 = (muonsCorr)->at(LEAD)->auxdata<float>("smeared_z0") - (muonsCorr)->at(SUB)->auxdata<float>("smeared_z0");		  

		  delta_z0_significance = (fabs(delta_z0) / (sqrt(trk_lead->definingParametersCovMatrixVec().at(2) + trk_sub->definingParametersCovMatrixVec().at(2))));
		    
		  sigma_d0_lead = trk_lead->d0() / d0_significance_lead;
		  sigma_d0_sub = trk_sub->d0() / d0_significance_sub;
		  d0_significance_BOTH = (fabs((muonsCorr)->at(LEAD)->auxdata<float>("smeared_d0") + (muonsCorr)->at(SUB)->auxdata<float>("smeared_d0"))) / (sqrt((sigma_d0_lead * sigma_d0_lead)+(sigma_d0_sub * sigma_d0_sub)));
		    
		  chi2 = pow(((muonsCorr)->at(LEAD)->auxdata<float>("smeared_d0")/sigma_d0_lead),2) + pow(((muonsCorr)->at(SUB)->auxdata<float>("smeared_d0")/sigma_d0_sub),2) + pow((delta_z0_significance),2);
		    
		} else {

		  if( trk_lead && trk_sub ){
		    if( xAOD::TrackingHelpers::hasValidCov( trk_lead ) && xAOD::TrackingHelpers::hasValidCov( trk_sub )){
		      for (unsigned int i=0; i<vxContainer->size(); i++){
			const xAOD::Vertex* vtx = vxContainer->at(i);
			if ( vtx->vertexType() == xAOD::VxType::PriVtx ){
			  delta_z0_lead = fabs((trk_lead->z0() + trk_lead->vz() - vtx->z() ) * TMath::Sin( trk_lead->theta()));
			  delta_z0_sub = fabs((trk_sub->z0() + trk_sub->vz() - vtx->z() ) * TMath::Sin( trk_sub->theta()));
			}
		      }
		    
		      d0_significance_lead  =  TMath::Abs(xAOD::TrackingHelpers::d0significance( trk_lead, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY() ));
		      d0_significance_sub =  TMath::Abs(xAOD::TrackingHelpers::d0significance( trk_sub, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY() ));

		    }
		  }		  
		
		  delta_z0 = trk_lead->z0() - trk_sub->z0();
		
		  sigma_d0_lead = trk_lead->d0() / d0_significance_lead;
		  sigma_d0_sub = trk_sub->d0() / d0_significance_sub;
		  d0_significance_BOTH = (fabs(trk_lead->d0() + trk_sub->d0())) / (sqrt((sigma_d0_lead * sigma_d0_lead)+(sigma_d0_sub * sigma_d0_sub)));

		  delta_z0_significance = (fabs(delta_z0) / (sqrt(trk_lead->definingParametersCovMatrixVec().at(2) + trk_sub->definingParametersCovMatrixVec().at(2))));

		  chi2 = pow((trk_lead->d0()/sigma_d0_lead),2) + pow((trk_sub->d0()/sigma_d0_sub),2) + pow((delta_z0_significance),2);

		}
		  
		if(chi2 < 16.) { //Applying chi2_QCD cuts < 16

		  //From here we have different isoltaion selections
		  //Here the case where both the muons pass the FixedCutTigh WP requirement (ii)
		  if ((iso_muon_fixed_tight->accept(*(muonsCorr)->at(LEAD))) && (iso_muon_fixed_tight->accept(*(muonsCorr)->at(SUB)))) {
		   
		    l = 3;
		    
		    //Applying isolation SF to both the muons (iso-iso case)
		    if(isMC) {
		      w_Iso_SF_Fir = GetMuonIsoSF_FixedCutTight((muonsCorr->at(LEAD)));
		      w_Iso_SF_Sec = GetMuonIsoSF_FixedCutTight((muonsCorr->at(SUB)));
		    }

		    local_EventWeight *= w_Iso_SF_Fir * w_Iso_SF_Sec;
		    local_EventWeight_noPU *= w_Iso_SF_Fir * w_Iso_SF_Sec;		    

		    h_N_vtx_BF->Fill(NPV, local_EventWeight_noPU);
		      
		    h_N_vtx_A->Fill(NPV, PileUpWeight);
		    h_N_vtx_B->Fill(NPV, local_EventWeight);
		    h_PU->Fill(PRWToolHandle->getCorrectedAverageInteractionsPerCrossing(*eventInfo, true), local_EventWeight);
		    h_PU_BF->Fill(eventInfo->averageInteractionsPerCrossing(), local_EventWeight_noPU);

		    DeltaPhi_ll = ( (muonsCorr)->at(LEAD)->p4() ).DeltaPhi( (muonsCorr)->at(SUB)->p4() );
					      
		    //Testing the impact of trigger SF
		    //BF applying trigger SF
		    if (fabs(p1.Eta()) <= 1.05  && fabs(p2.Eta()) <= 1.05) {
		      h_MuonEta_Pair_A_BF[0][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_A_BF[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_A_BF[0][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_A_BF[0][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_A_BF[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_A_BF[0][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_DeltaPhi_A_BF[0][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		      h_MuonRap_Inc_A_BF[0][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_A_BF[0][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		    } else if (fabs(p1.Eta()) > 1.05  && fabs(p2.Eta()) > 1.05) {
		      h_MuonEta_Pair_B_BF[0][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_B_BF[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_B_BF[0][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_B_BF[0][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_B_BF[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_B_BF[0][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_DeltaPhi_B_BF[0][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		      h_MuonRap_Inc_B_BF[0][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_B_BF[0][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		    } else if (fabs(p1.Eta()) <= 1.05  && fabs(p2.Eta()) > 1.05) {
		      h_MuonEta_Pair_C_BF[0][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_C_BF[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_C_BF[0][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_C_BF[0][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_C_BF[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_C_BF[0][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_DeltaPhi_C_BF[0][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		      h_MuonRap_Inc_C_BF[0][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_C_BF[0][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		    } else if (fabs(p1.Eta()) > 1.05  && fabs(p2.Eta()) <= 1.05) {
		      h_MuonEta_Pair_D_BF[0][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_D_BF[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_D_BF[0][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_D_BF[0][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_D_BF[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_D_BF[0][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_DeltaPhi_D_BF[0][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		      h_MuonRap_Inc_D_BF[0][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_D_BF[0][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		    }

		    if (p1.Pt() < 6.) {
		      h_MuonEta_Pair_Fir_BF[0][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_Fir_BF[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_Fir_BF[0][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_Fir_BF[0][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_Fir_BF[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_Fir_BF[0][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_MuonRap_Inc_Fir_BF[0][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_Fir_BF[0][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		      h_MuonEta_Inc_Fir_BF[0][l][j][i]->Fill(p1.Eta(), local_EventWeight);
		      h_MuonEta_Inc_Fir_BF[0][l][j][i]->Fill(p2.Eta(), local_EventWeight);
		      h_MuonPhi_Inc_Fir_BF[0][l][j][i]->Fill(p1.Phi(), local_EventWeight);
		      h_MuonPhi_Inc_Fir_BF[0][l][j][i]->Fill(p2.Phi(), local_EventWeight);
		      h_DeltaPhi_Fir_BF[0][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		    } else if (p1.Pt() > 6. && p1.Pt() < 10.) {
		      h_MuonEta_Pair_Sec_BF[0][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_Sec_BF[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_Sec_BF[0][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_Sec_BF[0][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_Sec_BF[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_Sec_BF[0][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_MuonRap_Inc_Sec_BF[0][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_Sec_BF[0][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		      h_MuonEta_Inc_Sec_BF[0][l][j][i]->Fill(p1.Eta(), local_EventWeight);
		      h_MuonEta_Inc_Sec_BF[0][l][j][i]->Fill(p2.Eta(), local_EventWeight);
		      h_MuonPhi_Inc_Sec_BF[0][l][j][i]->Fill(p1.Phi(), local_EventWeight);
		      h_MuonPhi_Inc_Sec_BF[0][l][j][i]->Fill(p2.Phi(), local_EventWeight);
		      h_DeltaPhi_Sec_BF[0][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);			      
		    } else if (p1.Pt() > 10. && p1.Pt() < 15.) {
		      h_MuonEta_Pair_Trd_BF[0][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_Trd_BF[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_Trd_BF[0][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_Trd_BF[0][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_Trd_BF[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_Trd_BF[0][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_MuonRap_Inc_Trd_BF[0][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_Trd_BF[0][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		      h_MuonEta_Inc_Trd_BF[0][l][j][i]->Fill(p1.Eta(), local_EventWeight);
		      h_MuonEta_Inc_Trd_BF[0][l][j][i]->Fill(p2.Eta(), local_EventWeight);
		      h_MuonPhi_Inc_Trd_BF[0][l][j][i]->Fill(p1.Phi(), local_EventWeight);
		      h_MuonPhi_Inc_Trd_BF[0][l][j][i]->Fill(p2.Phi(), local_EventWeight);
		      h_DeltaPhi_Trd_BF[0][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		    }  else if (p1.Pt() > 15) {
		      h_MuonEta_Pair_Fou_BF[0][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_Fou_BF[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_Fou_BF[0][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_Fou_BF[0][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_Fou_BF[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_Fou_BF[0][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_MuonRap_Inc_Fou_BF[0][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_Fou_BF[0][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		      h_MuonEta_Inc_Fou_BF[0][l][j][i]->Fill(p1.Eta(), local_EventWeight);
		      h_MuonEta_Inc_Fou_BF[0][l][j][i]->Fill(p2.Eta(), local_EventWeight);
		      h_MuonPhi_Inc_Fou_BF[0][l][j][i]->Fill(p1.Phi(), local_EventWeight);
		      h_MuonPhi_Inc_Fou_BF[0][l][j][i]->Fill(p2.Phi(), local_EventWeight);
		      h_DeltaPhi_Fou_BF[0][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		    }

		    //Applying trigger SF
		    local_EventWeight *= w_Trig_SF_Fir * w_Trig_SF_Sec;

		    //After applying trigger SF
		    if (fabs(p1.Eta()) <= 1.05  && fabs(p2.Eta()) <= 1.05) {
		      h_MuonEta_Pair_A[0][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_A[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_A[0][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_A[0][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_A[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_A[0][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_DeltaPhi_A[0][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		      h_MuonRap_Inc_A[0][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_A[0][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		    } else if (fabs(p1.Eta()) > 1.05  && fabs(p2.Eta()) > 1.05) {
		      h_MuonEta_Pair_B[0][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_B[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_B[0][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_B[0][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_B[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_B[0][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_DeltaPhi_B[0][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		      h_MuonRap_Inc_B[0][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_B[0][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		    } else if (fabs(p1.Eta()) <= 1.05  && fabs(p2.Eta()) > 1.05) {
		      h_MuonEta_Pair_C[0][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_C[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_C[0][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_C[0][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_C[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_C[0][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_DeltaPhi_C[0][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		      h_MuonRap_Inc_C[0][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_C[0][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		    } else if (fabs(p1.Eta()) > 1.05  && fabs(p2.Eta()) <= 1.05) {
		      h_MuonEta_Pair_D[0][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_D[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_D[0][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_D[0][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_D[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_D[0][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_DeltaPhi_D[0][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		      h_MuonRap_Inc_D[0][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_D[0][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		    }

		    if (p1.Pt() < 6.) {
		      h_MuonEta_Pair_Fir[0][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_Fir[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_Fir[0][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_Fir[0][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_Fir[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_Fir[0][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_MuonRap_Inc_Fir[0][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_Fir[0][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		      h_MuonEta_Inc_Fir[0][l][j][i]->Fill(p1.Eta(), local_EventWeight);
		      h_MuonEta_Inc_Fir[0][l][j][i]->Fill(p2.Eta(), local_EventWeight);
		      h_MuonPhi_Inc_Fir[0][l][j][i]->Fill(p1.Phi(), local_EventWeight);
		      h_MuonPhi_Inc_Fir[0][l][j][i]->Fill(p2.Phi(), local_EventWeight);
		      h_DeltaPhi_Fir[0][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		    } else if (p1.Pt() > 6. && p1.Pt() < 10.) {
		      h_MuonEta_Pair_Sec[0][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_Sec[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_Sec[0][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_Sec[0][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_Sec[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_Sec[0][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_MuonRap_Inc_Sec[0][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_Sec[0][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		      h_MuonEta_Inc_Sec[0][l][j][i]->Fill(p1.Eta(), local_EventWeight);
		      h_MuonEta_Inc_Sec[0][l][j][i]->Fill(p2.Eta(), local_EventWeight);
		      h_MuonPhi_Inc_Sec[0][l][j][i]->Fill(p1.Phi(), local_EventWeight);
		      h_MuonPhi_Inc_Sec[0][l][j][i]->Fill(p2.Phi(), local_EventWeight);
		      h_DeltaPhi_Sec[0][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);			      
		    } else if (p1.Pt() > 10. && p1.Pt() < 15.) {
		      h_MuonEta_Pair_Trd[0][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_Trd[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_Trd[0][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_Trd[0][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_Trd[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_Trd[0][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_MuonRap_Inc_Trd[0][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_Trd[0][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		      h_MuonEta_Inc_Trd[0][l][j][i]->Fill(p1.Eta(), local_EventWeight);
		      h_MuonEta_Inc_Trd[0][l][j][i]->Fill(p2.Eta(), local_EventWeight);
		      h_MuonPhi_Inc_Trd[0][l][j][i]->Fill(p1.Phi(), local_EventWeight);
		      h_MuonPhi_Inc_Trd[0][l][j][i]->Fill(p2.Phi(), local_EventWeight);
		      h_DeltaPhi_Trd[0][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		    }  else if (p1.Pt() > 15) {
		      h_MuonEta_Pair_Fou[0][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_Fou[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_Fou[0][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_Fou[0][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_Fou[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_Fou[0][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_MuonRap_Inc_Fou[0][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_Fou[0][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		      h_MuonEta_Inc_Fou[0][l][j][i]->Fill(p1.Eta(), local_EventWeight);
		      h_MuonEta_Inc_Fou[0][l][j][i]->Fill(p2.Eta(), local_EventWeight);
		      h_MuonPhi_Inc_Fou[0][l][j][i]->Fill(p1.Phi(), local_EventWeight);
		      h_MuonPhi_Inc_Fou[0][l][j][i]->Fill(p2.Phi(), local_EventWeight);
		      h_DeltaPhi_Fou[0][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		    }

		    if (isMC) {
		      h_d0_mu1mu2[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->auxdata<float>("smeared_d0"), (muonsCorr)->at(SUB)->auxdata<float>("smeared_d0"));
		      h_d0_mu1[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->auxdata<float>("smeared_d0"), local_EventWeight);
		      h_d0_mu1[0][l][j][i]->Fill((muonsCorr)->at(SUB)->auxdata<float>("smeared_d0"), local_EventWeight);
		      h_z0_mu1[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->auxdata<float>("smeared_z0"), local_EventWeight);
		      h_z0_mu1[0][l][j][i]->Fill((muonsCorr)->at(SUB)->auxdata<float>("smeared_z0"), local_EventWeight);

		      h_d0_mu1_BF[0][l][j][i]->Fill(trk_lead->d0(), local_EventWeight);
		      h_d0_mu1_BF[0][l][j][i]->Fill(trk_sub->d0(), local_EventWeight);
		      h_z0_mu1_BF[0][l][j][i]->Fill(trk_lead->z0(), local_EventWeight);
		      h_z0_mu1_BF[0][l][j][i]->Fill(trk_sub->z0(), local_EventWeight);

		    } else {
		      h_d0_mu1mu2[0][l][j][i]->Fill(trk_lead->d0(), trk_sub->d0());
		      h_d0_mu1[0][l][j][i]->Fill(trk_lead->d0(), local_EventWeight);
		      h_d0_mu1[0][l][j][i]->Fill(trk_sub->d0(), local_EventWeight);
		      h_z0_mu1[0][l][j][i]->Fill(trk_lead->z0(), local_EventWeight);
		      h_z0_mu1[0][l][j][i]->Fill(trk_sub->z0(), local_EventWeight);

		      h_d0_mu1_BF[0][l][j][i]->Fill(trk_lead->d0(), local_EventWeight);
		      h_d0_mu1_BF[0][l][j][i]->Fill(trk_sub->d0(), local_EventWeight);
		      h_z0_mu1_BF[0][l][j][i]->Fill(trk_lead->z0(), local_EventWeight);
		      h_z0_mu1_BF[0][l][j][i]->Fill(trk_sub->z0(), local_EventWeight);
		    }
		   
		    h_delta_z0[0][l][j][i]->Fill(delta_z0, local_EventWeight);
			  
		    h_EventWeight[0][l][j][i]->Fill(local_EventWeight);
		    h_Multiplicity[0][l][j][i]->Fill(mupairs[j][i].size());
		    h_kF_InvMass[0][l][j][i]->Fill(Propagator.Mag(),KFactorWeight);

		    //Filling plots with relevant kinematic quantities
		    h_BornMass[0][l][j][i]->Fill(Truth_V_Mass, local_EventWeight);

		    h_chi2_ISO[0][l][j][i]->Fill(chi2, local_EventWeight);
		    h_chi2_Prob_ISO[0][l][j][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		    h_chi2_Prob_noweight[0][l][j][i]->Fill(TMath::Prob(chi2,3));
		    h_d0sig_deltaz0sig[0][l][j][i]->Fill(d0_significance_BOTH, delta_z0_significance);

		    h_InvMass[0][l][j][i]->Fill(Propagator.Mag(), local_EventWeight);

		    if (Propagator.Mag() > 7. && Propagator.Mag() < 9.) {
		      if (Propagator.Pt() > 8.) {
			h_MuonPt_Pair_A[0][l][j][i]->Fill(Propagator.Pt(), local_EventWeight);

			h_InvMass_low[0][l][j][i]->Fill(Propagator.Mag(), local_EventWeight);

		      }
		    } else if (Propagator.Mag() >= 12. && Propagator.Mag() < 14.) {
		      h_MuonPt_Pair_B[0][l][j][i]->Fill(Propagator.Pt(), local_EventWeight);
		    } else if (Propagator.Mag() >= 14. && Propagator.Mag() < 17.) {
		      h_MuonPt_Pair_C[0][l][j][i]->Fill(Propagator.Pt(), local_EventWeight);
		    } else if (Propagator.Mag() >= 17. && Propagator.Mag() < 22.) {
		      h_MuonPt_Pair_D[0][l][j][i]->Fill(Propagator.Pt(), local_EventWeight);
		    } else if (Propagator.Mag() >= 22. && Propagator.Mag() < 28.) {
		      h_MuonPt_Pair_E[0][l][j][i]->Fill(Propagator.Pt(), local_EventWeight);
		    } else if (Propagator.Mag() >= 28. && Propagator.Mag() < 36.) {
		      h_MuonPt_Pair_F[0][l][j][i]->Fill(Propagator.Pt(), local_EventWeight);
		    } else if (Propagator.Mag() >= 36. && Propagator.Mag() < 46.) {
		      h_MuonPt_Pair_G[0][l][j][i]->Fill(Propagator.Pt(), local_EventWeight);
		    } else if (Propagator.Mag() >= 46. && Propagator.Mag() < 60.) {
		      h_MuonPt_Pair_H[0][l][j][i]->Fill(Propagator.Pt(), local_EventWeight);
		    }

		    h_MuonPt_Pair[0][l][j][i]->Fill(Propagator.Pt(), local_EventWeight);
		    h_MuonPt_Lead[0][l][j][i]->Fill(p1.Pt(), local_EventWeight);
		    h_MuonPt_Sub[0][l][j][i]->Fill(p2.Pt(), local_EventWeight);
		    h_Pt_muons[0][l][j][i]->Fill(p1.Pt(), p2.Pt());
	      
		    h_MuonEta_Pair[0][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		    h_MuonEta_Lead[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		    h_MuonEta_Sub[0][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		    h_MuonPhi_Pair[0][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		    h_MuonPhi_Lead[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		    h_MuonPhi_Sub[0][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);

		    h_MuonPt_Inc[0][l][j][i]->Fill(p1.Pt(), local_EventWeight);
		    h_MuonPt_Inc[0][l][j][i]->Fill(p2.Pt(), local_EventWeight);
		    h_MuonRap_Inc[0][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		    h_MuonRap_Inc[0][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		    h_MuonEta_Inc[0][l][j][i]->Fill(p1.Eta(), local_EventWeight);
		    h_MuonEta_Inc[0][l][j][i]->Fill(p2.Eta(), local_EventWeight);
		    h_MuonPhi_Inc[0][l][j][i]->Fill(p1.Phi(), local_EventWeight);
		    h_MuonPhi_Inc[0][l][j][i]->Fill(p2.Phi(), local_EventWeight);

		    DeltaR_ll = ( (muonsCorr)->at(LEAD)->p4() ).DeltaR( (muonsCorr)->at(SUB)->p4() );

		    h_DeltaPhi[0][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);

		    h_DeltaR[0][l][j][i]->Fill(DeltaR_ll, local_EventWeight);

		    //Studies with DeltaR
		    /*if (DeltaR_ll >= 0.2) {
		      h_InvMass_DeltaR[0][l][j][i]->Fill(Propagator.Mag(), local_EventWeight);
		      h_MuonPt_Pair_DeltaR[0][l][j][i]->Fill(Propagator.Pt(), local_EventWeight);
		      h_MuonPt_Lead_DeltaR[0][l][j][i]->Fill(p1.Pt(), local_EventWeight);
		      h_MuonPt_Sub_DeltaR[0][l][j][i]->Fill(p2.Pt(), local_EventWeight);
		      h_MuonEta_Lead_DeltaR[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_DeltaR[0][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Lead_DeltaR[0][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_DeltaR[0][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		    }*/

		    h_DeltaEta[0][l][j][i]->Fill(((muonsCorr)->at(LEAD)->eta())-((muonsCorr)->at(SUB)->eta()), local_EventWeight);
		    h_Rapidity[0][l][j][i]->Fill(Propagator.Rapidity(), local_EventWeight);

		    h_SF_Iso_Lead[0][l][j][i]->Fill(w_Iso_SF_Fir, 1);
		    h_SF_Iso_Sub[0][l][j][i]->Fill(w_Iso_SF_Sec, 1);
		    h_SF_Eff_Lead[0][l][j][i]->Fill(w_Reco_SF_Fir, 1);
		    h_SF_Eff_Sub[0][l][j][i]->Fill(w_Reco_SF_Sec, 1);
		    h_SF_Trig_Lead[0][l][j][i]->Fill(w_Trig_SF_Fir, 1);
		    h_SF_Trig_Sub[0][l][j][i]->Fill(w_Trig_SF_Sec, 1);

		    h_d0_significance_BOTH_ISO_Sig[0][l][j][i]->Fill(d0_significance_BOTH, local_EventWeight);
		    h_delta_z0_significance_ISO_Sig[0][l][j][i]->Fill(delta_z0_significance, local_EventWeight);
						    
		    //Filling the discriminating variable Prob(chi2_QCD, n.d.f. = 3) in each invaraint mass and rapidity bins where we extract the xsection
		    if (Propagator.Mag() > 7. && Propagator.Mag() < 9.) {
		      h_d0_significance_BOTH_ISO[0][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			    
		      h_SF_BOTH_ISO[0][l][0][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_ISO_rapA[0][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapA[0][l][0][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_ISO_rapB[0][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapB[0][l][0][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_ISO_rapC[0][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapC[0][l][0][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_ISO_rapD[0][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapD[0][l][0][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_ISO_rapE[0][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapE[0][l][0][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_ISO_rapF[0][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapF[0][l][0][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }

		    }			

		    if (Propagator.Mag() >= 12. && Propagator.Mag() < 14.) {
		      h_d0_significance_BOTH_ISO[0][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			    
		      h_SF_BOTH_ISO[0][l][1][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_ISO_rapA[0][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapA[0][l][1][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));		       
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_ISO_rapB[0][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapB[0][l][1][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_ISO_rapC[0][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapC[0][l][1][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_ISO_rapD[0][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapD[0][l][1][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_ISO_rapE[0][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapE[0][l][1][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_ISO_rapF[0][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapF[0][l][1][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }

		    }
		    if (Propagator.Mag() >= 14. && Propagator.Mag() < 17.) {
		      h_d0_significance_BOTH_ISO[0][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      h_SF_BOTH_ISO[0][l][2][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_ISO_rapA[0][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapA[0][l][2][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_ISO_rapB[0][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapB[0][l][2][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_ISO_rapC[0][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapC[0][l][2][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_ISO_rapD[0][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapD[0][l][2][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_ISO_rapE[0][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapE[0][l][2][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_ISO_rapF[0][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapF[0][l][2][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }

		    }
		    if (Propagator.Mag() >= 17. && Propagator.Mag() < 22.) {
		      h_d0_significance_BOTH_ISO[0][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      h_SF_BOTH_ISO[0][l][3][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_ISO_rapA[0][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapA[0][l][3][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_ISO_rapB[0][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapB[0][l][3][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_ISO_rapC[0][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapC[0][l][3][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_ISO_rapD[0][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapD[0][l][3][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_ISO_rapE[0][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapE[0][l][3][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_ISO_rapF[0][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapF[0][l][3][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }

		    }
		    if (Propagator.Mag() >= 22. && Propagator.Mag() < 28.) {
		      h_d0_significance_BOTH_ISO[0][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      h_SF_BOTH_ISO[0][l][4][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_ISO_rapA[0][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapA[0][l][4][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_ISO_rapB[0][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapB[0][l][4][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_ISO_rapC[0][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapC[0][l][4][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_ISO_rapD[0][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapD[0][l][4][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_ISO_rapE[0][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapE[0][l][4][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_ISO_rapF[0][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapF[0][l][4][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }

		    }
		    if (Propagator.Mag() >= 28. && Propagator.Mag() < 36.) {
		      h_d0_significance_BOTH_ISO[0][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      h_SF_BOTH_ISO[0][l][5][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_ISO_rapA[0][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapA[0][l][5][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_ISO_rapB[0][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapB[0][l][5][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_ISO_rapC[0][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapC[0][l][5][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_ISO_rapD[0][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapD[0][l][5][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_ISO_rapE[0][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapE[0][l][5][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_ISO_rapF[0][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapF[0][l][5][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }

		    }
		    if (Propagator.Mag() >= 36. && Propagator.Mag() < 46.) {
		      h_d0_significance_BOTH_ISO[0][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      h_SF_BOTH_ISO[0][l][6][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_ISO_rapA[0][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapA[0][l][6][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_ISO_rapB[0][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapB[0][l][6][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_ISO_rapC[0][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapC[0][l][6][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_ISO_rapD[0][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapD[0][l][6][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_ISO_rapE[0][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapE[0][l][6][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_ISO_rapF[0][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapF[0][l][6][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }

		    }
		    if (Propagator.Mag() >= 46. && Propagator.Mag() < 60.) {
		      h_d0_significance_BOTH_ISO[0][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      h_SF_BOTH_ISO[0][l][7][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_ISO_rapA[0][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapA[0][l][7][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_ISO_rapB[0][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapB[0][l][7][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_ISO_rapC[0][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapC[0][l][7][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_ISO_rapD[0][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapD[0][l][7][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_ISO_rapE[0][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapE[0][l][7][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_ISO_rapF[0][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			h_SF_BOTH_ISO_rapF[0][l][7][i]->Fill(0.5*(w_Iso_SF_Fir+w_Iso_SF_Sec));
		      }
		    }

		    //Here we consider another isolation selection - BOTH the muons are NOT ISOLATED (nn)  
		  } else if (!(iso_muon_fixed_tight->accept(*(muonsCorr)->at(LEAD))) && !(iso_muon_fixed_tight->accept(*(muonsCorr)->at(SUB)))) {

		    l = 2;
			    
		    local_EventWeight *= w_Trig_SF_Fir * w_Trig_SF_Sec;

		    h_d0_significance_BOTH_NOISO_Sig[0][l][j][i]->Fill(d0_significance_BOTH, local_EventWeight);
		    h_delta_z0_significance_NOISO_Sig[0][l][j][i]->Fill(delta_z0_significance, local_EventWeight);
		    h_chi2_NOISO[0][l][j][i]->Fill(chi2, local_EventWeight);
		    h_chi2_Prob_NOISO[0][l][j][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		    if (Propagator.Mag() > 7. && Propagator.Mag() < 9.) {
		      h_d0_significance_BOTH_NOISO[0][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_NOISO_rapA[0][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_NOISO_rapB[0][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_NOISO_rapC[0][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_NOISO_rapD[0][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_NOISO_rapE[0][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_NOISO_rapF[0][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		    }
		    if (Propagator.Mag() >= 12. && Propagator.Mag() < 14.) {
		      h_d0_significance_BOTH_NOISO[0][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			    
		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_NOISO_rapA[0][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_NOISO_rapB[0][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_NOISO_rapC[0][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_NOISO_rapD[0][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_NOISO_rapE[0][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_NOISO_rapF[0][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 14. && Propagator.Mag() < 17.) {
		      h_d0_significance_BOTH_NOISO[0][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_NOISO_rapA[0][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_NOISO_rapB[0][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_NOISO_rapC[0][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_NOISO_rapD[0][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_NOISO_rapE[0][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_NOISO_rapF[0][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 17. && Propagator.Mag() < 22.) {
		      h_d0_significance_BOTH_NOISO[0][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_NOISO_rapA[0][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_NOISO_rapB[0][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_NOISO_rapC[0][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_NOISO_rapD[0][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_NOISO_rapE[0][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_NOISO_rapF[0][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 22. && Propagator.Mag() < 28.) {
		      h_d0_significance_BOTH_NOISO[0][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_NOISO_rapA[0][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_NOISO_rapB[0][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_NOISO_rapC[0][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_NOISO_rapD[0][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_NOISO_rapE[0][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_NOISO_rapF[0][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 28. && Propagator.Mag() < 36.) {
		      h_d0_significance_BOTH_NOISO[0][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_NOISO_rapA[0][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_NOISO_rapB[0][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_NOISO_rapC[0][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_NOISO_rapD[0][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_NOISO_rapE[0][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_NOISO_rapF[0][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 36. && Propagator.Mag() < 46.) {
		      h_d0_significance_BOTH_NOISO[0][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_NOISO_rapA[0][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_NOISO_rapB[0][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_NOISO_rapC[0][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_NOISO_rapD[0][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_NOISO_rapE[0][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_NOISO_rapF[0][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 46. && Propagator.Mag() < 60.) {
		      h_d0_significance_BOTH_NOISO[0][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_NOISO_rapA[0][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_NOISO_rapB[0][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_NOISO_rapC[0][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_NOISO_rapD[0][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_NOISO_rapE[0][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_NOISO_rapF[0][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }			  

		    //Last isolation selection - one muon is isolated and the other is not (in case) - needed for 3-histigrams fit in QCD multijet background estimate
		  } else if (((iso_muon_fixed_tight->accept(*(muonsCorr)->at(LEAD))) && !(iso_muon_fixed_tight->accept(*(muonsCorr)->at(SUB)))) || (!(iso_muon_fixed_tight->accept(*(muonsCorr)->at(LEAD))) && (iso_muon_fixed_tight->accept(*(muonsCorr)->at(SUB))))) {

		    l = 0;
			 
		    if (isMC) {
		      if ((iso_muon_fixed_tight->accept(*(muonsCorr)->at(LEAD)))) {
			w_Iso_SF_Fir = GetMuonIsoSF_FixedCutTight((muonsCorr->at(LEAD)));
		      } else {
			w_Iso_SF_Sec = GetMuonIsoSF_FixedCutTight((muonsCorr->at(SUB)));
		      }
		    }

		    local_EventWeight *= w_Iso_SF_Fir * w_Iso_SF_Sec;

		    local_EventWeight *= w_Trig_SF_Fir * w_Trig_SF_Sec;

		    h_d0_significance_BOTH_HALF_Sig[0][l][j][i]->Fill(d0_significance_BOTH, local_EventWeight);
		    h_delta_z0_significance_HALF_Sig[0][l][j][i]->Fill(delta_z0_significance, local_EventWeight);
		    h_chi2_HALF[0][l][j][i]->Fill(chi2, local_EventWeight);
		    h_chi2_Prob_HALF[0][l][j][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
 
		    if (Propagator.Mag() > 7. && Propagator.Mag() < 9.) {
		      h_d0_significance_BOTH_HALF[0][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_HALF_rapA[0][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_HALF_rapB[0][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_HALF_rapC[0][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_HALF_rapD[0][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_HALF_rapE[0][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_HALF_rapF[0][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		    }

		    if (Propagator.Mag() >= 12. && Propagator.Mag() < 14.) {
		      h_d0_significance_BOTH_HALF[0][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			  
		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_HALF_rapA[0][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_HALF_rapB[0][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_HALF_rapC[0][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_HALF_rapD[0][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_HALF_rapE[0][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_HALF_rapF[0][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 14. && Propagator.Mag() < 17.) {
		      h_d0_significance_BOTH_HALF[0][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_HALF_rapA[0][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_HALF_rapB[0][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_HALF_rapC[0][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_HALF_rapD[0][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_HALF_rapE[0][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_HALF_rapF[0][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 17. && Propagator.Mag() < 22.) {
		      h_d0_significance_BOTH_HALF[0][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_HALF_rapA[0][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_HALF_rapB[0][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_HALF_rapC[0][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_HALF_rapD[0][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_HALF_rapE[0][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_HALF_rapF[0][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 22. && Propagator.Mag() < 28.) {
		      h_d0_significance_BOTH_HALF[0][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_HALF_rapA[0][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_HALF_rapB[0][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_HALF_rapC[0][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_HALF_rapD[0][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_HALF_rapE[0][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_HALF_rapF[0][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 28. && Propagator.Mag() < 36.) {
		      h_d0_significance_BOTH_HALF[0][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_HALF_rapA[0][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_HALF_rapB[0][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_HALF_rapC[0][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_HALF_rapD[0][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_HALF_rapE[0][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_HALF_rapF[0][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 36. && Propagator.Mag() < 46.) {
		      h_d0_significance_BOTH_HALF[0][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_HALF_rapA[0][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_HALF_rapB[0][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_HALF_rapC[0][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_HALF_rapD[0][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_HALF_rapE[0][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_HALF_rapF[0][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 46. && Propagator.Mag() < 60.) {
		      h_d0_significance_BOTH_HALF[0][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_HALF_rapA[0][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_HALF_rapB[0][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_HALF_rapC[0][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_HALF_rapD[0][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_HALF_rapE[0][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_HALF_rapF[0][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		    }
		  }
		} //end of the OS selection
		  
		//Same-Sign (SS) selection
	      } else {
		 
		//In the following the same steps performed in the OS selection are repetead
		//1: Obtain smeared IP quantities needed for computing the discriminating variable in the SuperFitter method
		//2: cut on chi2_QCD < 16
		//3: select muons depending on isolation requirements

		//Step 1
		if (isMC) {

		  if( trk_lead && trk_sub){
		    if( xAOD::TrackingHelpers::hasValidCov( trk_lead ) && xAOD::TrackingHelpers::hasValidCov( trk_sub ) && xAOD::TrackingHelpers::hasValidCov( trk_lead ) && xAOD::TrackingHelpers::hasValidCov( trk_sub )){
		      for (unsigned int i=0; i<vxContainer->size(); i++){
			const xAOD::Vertex* vtx = vxContainer->at(i);
			if ( vtx->vertexType() == xAOD::VxType::PriVtx ){
			  delta_z0_lead = fabs(((muonsCorr)->at(LEAD)->auxdata<float>("smeared_z0") + trk_lead->vz() - vtx->z() ) * TMath::Sin( trk_lead->theta()));
			  delta_z0_sub = fabs(((muonsCorr)->at(SUB)->auxdata<float>("smeared_z0") + trk_sub->vz() - vtx->z() ) * TMath::Sin( trk_sub->theta()));
			}
		      }
			
		      d0_significance_lead  =  TMath::Abs(xAOD::TrackingHelpers::d0significance( trk_lead, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY() ));
		      d0_significance_sub =  TMath::Abs(xAOD::TrackingHelpers::d0significance( trk_sub, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY() ));
			
		    }
		  }		  

		  delta_z0 = (muonsCorr)->at(LEAD)->auxdata<float>("smeared_z0") - (muonsCorr)->at(SUB)->auxdata<float>("smeared_z0");

		  sigma_d0_lead = trk_lead->d0() / d0_significance_lead;
		  sigma_d0_sub = trk_sub->d0() / d0_significance_sub;
		  d0_significance_BOTH = (fabs((muonsCorr)->at(LEAD)->auxdata<float>("smeared_d0") + (muonsCorr)->at(SUB)->auxdata<float>("smeared_d0"))) / (sqrt((sigma_d0_lead * sigma_d0_lead)+(sigma_d0_sub * sigma_d0_sub)));

		  delta_z0_significance = (fabs(delta_z0) / (sqrt(trk_lead->definingParametersCovMatrixVec().at(2) + trk_sub->definingParametersCovMatrixVec().at(2))));

		  chi2 = pow(((muonsCorr)->at(LEAD)->auxdata<float>("smeared_d0")/sigma_d0_lead),2) + pow(((muonsCorr)->at(SUB)->auxdata<float>("smeared_d0")/sigma_d0_sub),2) + pow((delta_z0_significance),2);
		    
		} else { //data

		  if( trk_lead && trk_sub ){
		    if( xAOD::TrackingHelpers::hasValidCov( trk_lead ) && xAOD::TrackingHelpers::hasValidCov( trk_sub )){
		      for (unsigned int i=0; i<vxContainer->size(); i++){
			const xAOD::Vertex* vtx = vxContainer->at(i);
			if ( vtx->vertexType() == xAOD::VxType::PriVtx ){
			  delta_z0_lead = fabs((trk_lead->z0() + trk_lead->vz() - vtx->z() ) * TMath::Sin( trk_lead->theta()));
			  delta_z0_sub = fabs((trk_sub->z0() + trk_sub->vz() - vtx->z() ) * TMath::Sin( trk_sub->theta()));
			}
		      }
		    
		      d0_significance_lead  =  TMath::Abs(xAOD::TrackingHelpers::d0significance( trk_lead, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY() ));
		      d0_significance_sub =  TMath::Abs(xAOD::TrackingHelpers::d0significance( trk_sub, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY() ));
		    }
		  }		  
		
		  delta_z0 = trk_lead->z0() - trk_sub->z0();

		  sigma_d0_lead = trk_lead->d0() / d0_significance_lead;
		  sigma_d0_sub = trk_sub->d0() / d0_significance_sub;
		  d0_significance_BOTH = (fabs(trk_lead->d0() + trk_sub->d0())) / (sqrt((sigma_d0_lead * sigma_d0_lead)+(sigma_d0_sub * sigma_d0_sub)));

		  delta_z0_significance = (fabs(delta_z0) / (sqrt(trk_lead->definingParametersCovMatrixVec().at(2) + trk_sub->definingParametersCovMatrixVec().at(2))));

		  chi2 = pow((trk_lead->d0()/sigma_d0_lead),2) + pow((trk_sub->d0()/sigma_d0_sub),2) + pow((delta_z0_significance),2);
		}
		  
		//Cut on chi2_QCD < 16
		if(chi2 < 16.) {

		  //Isolated-isolated case
		  if ((iso_muon_fixed_tight->accept(*(muonsCorr)->at(LEAD))) && (iso_muon_fixed_tight->accept(*(muonsCorr)->at(SUB)))) {
			
		    l = 3;
			
		    if (isMC) {
		      w_Iso_SF_Fir = GetMuonIsoSF_FixedCutTight((muonsCorr->at(LEAD)));
		      w_Iso_SF_Sec = GetMuonIsoSF_FixedCutTight((muonsCorr->at(SUB)));
		    }

		    local_EventWeight *= w_Iso_SF_Fir * w_Iso_SF_Sec; 
			
		    DeltaPhi_ll = ( (muonsCorr)->at(LEAD)->p4() ).DeltaPhi( (muonsCorr)->at(SUB)->p4() );

		    //Before applying trigger SF
		    if (fabs(p1.Eta()) <= 1.05  && fabs(p2.Eta()) <= 1.05) {
		      h_MuonEta_Pair_A_BF[1][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_A_BF[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_A_BF[1][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_A_BF[1][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_A_BF[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_A_BF[1][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_DeltaPhi_A_BF[1][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		      h_MuonRap_Inc_A_BF[1][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_A_BF[1][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		    } else if (fabs(p1.Eta()) > 1.05  && fabs(p2.Eta()) > 1.05) {
		      h_MuonEta_Pair_B_BF[1][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_B_BF[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_B_BF[1][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_B_BF[1][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_B_BF[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_B_BF[1][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_DeltaPhi_B_BF[1][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		      h_MuonRap_Inc_B_BF[1][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_B_BF[1][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		    } else if (fabs(p1.Eta()) <= 1.05  && fabs(p2.Eta()) > 1.05) {
		      h_MuonEta_Pair_C_BF[1][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_C_BF[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_C_BF[1][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_C_BF[1][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_C_BF[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_C_BF[1][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_DeltaPhi_C_BF[1][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		      h_MuonRap_Inc_C_BF[1][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_C_BF[1][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);			      
		    } else if (fabs(p1.Eta()) > 1.05  && fabs(p2.Eta()) <= 1.05) {
		      h_MuonEta_Pair_D_BF[1][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_D_BF[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_D_BF[1][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_D_BF[1][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_D_BF[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_D_BF[1][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_DeltaPhi_D_BF[1][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		      h_MuonRap_Inc_D_BF[1][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_D_BF[1][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		    }

		    if (p1.Pt() < 6.) {
		      h_MuonEta_Pair_Fir_BF[1][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_Fir_BF[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_Fir_BF[1][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_Fir_BF[1][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_Fir_BF[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_Fir_BF[1][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_MuonRap_Inc_Fir_BF[1][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_Fir_BF[1][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		      h_MuonEta_Inc_Fir_BF[1][l][j][i]->Fill(p1.Eta(), local_EventWeight);
		      h_MuonEta_Inc_Fir_BF[1][l][j][i]->Fill(p2.Eta(), local_EventWeight);
		      h_MuonPhi_Inc_Fir_BF[1][l][j][i]->Fill(p1.Phi(), local_EventWeight);
		      h_MuonPhi_Inc_Fir_BF[1][l][j][i]->Fill(p2.Phi(), local_EventWeight);
		      h_DeltaPhi_Fir_BF[1][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		    } else if (p1.Pt() > 6. && p1.Pt() < 10.) {
		      h_MuonEta_Pair_Sec_BF[1][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_Sec_BF[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_Sec_BF[1][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_Sec_BF[1][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_Sec_BF[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_Sec_BF[1][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_MuonRap_Inc_Sec_BF[1][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_Sec_BF[1][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		      h_MuonEta_Inc_Sec_BF[1][l][j][i]->Fill(p1.Eta(), local_EventWeight);
		      h_MuonEta_Inc_Sec_BF[1][l][j][i]->Fill(p2.Eta(), local_EventWeight);
		      h_MuonPhi_Inc_Sec_BF[1][l][j][i]->Fill(p1.Phi(), local_EventWeight);
		      h_MuonPhi_Inc_Sec_BF[1][l][j][i]->Fill(p2.Phi(), local_EventWeight);
		      h_DeltaPhi_Sec_BF[1][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);			      
		    } else if (p1.Pt() > 10. && p1.Pt() < 15.) {
		      h_MuonEta_Pair_Trd_BF[1][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_Trd_BF[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_Trd_BF[1][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_Trd_BF[1][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_Trd_BF[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_Trd_BF[1][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_MuonRap_Inc_Trd_BF[1][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_Trd_BF[1][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		      h_MuonEta_Inc_Trd_BF[1][l][j][i]->Fill(p1.Eta(), local_EventWeight);
		      h_MuonEta_Inc_Trd_BF[1][l][j][i]->Fill(p2.Eta(), local_EventWeight);
		      h_MuonPhi_Inc_Trd_BF[1][l][j][i]->Fill(p1.Phi(), local_EventWeight);
		      h_MuonPhi_Inc_Trd_BF[1][l][j][i]->Fill(p2.Phi(), local_EventWeight);
		      h_DeltaPhi_Trd_BF[1][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		    }  else if (p1.Pt() > 15) {
		      h_MuonEta_Pair_Fou_BF[1][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_Fou_BF[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_Fou_BF[1][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_Fou_BF[1][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_Fou_BF[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_Fou_BF[1][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_MuonRap_Inc_Fou_BF[1][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_Fou_BF[1][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		      h_MuonEta_Inc_Fou_BF[1][l][j][i]->Fill(p1.Eta(), local_EventWeight);
		      h_MuonEta_Inc_Fou_BF[1][l][j][i]->Fill(p2.Eta(), local_EventWeight);
		      h_MuonPhi_Inc_Fou_BF[1][l][j][i]->Fill(p1.Phi(), local_EventWeight);
		      h_MuonPhi_Inc_Fou_BF[1][l][j][i]->Fill(p2.Phi(), local_EventWeight);
		      h_DeltaPhi_Fou_BF[1][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		      }			    

		    //Applying trigger SF
		    local_EventWeight *= w_Trig_SF_Fir * w_Trig_SF_Sec;

		    if (fabs(p1.Eta()) <= 1.05  && fabs(p2.Eta()) <= 1.05) {
		      h_MuonEta_Pair_A[1][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_A[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_A[1][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_A[1][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_A[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_A[1][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_DeltaPhi_A[1][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		      h_MuonRap_Inc_A[1][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_A[1][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);			      
		    } else if (fabs(p1.Eta()) > 1.05  && fabs(p2.Eta()) > 1.05) {
		      h_MuonEta_Pair_B[1][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_B[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_B[1][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_B[1][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_B[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_B[1][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_DeltaPhi_B[1][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		      h_MuonRap_Inc_B[1][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_B[1][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);			      
		    } else if (fabs(p1.Eta()) <= 1.05  && fabs(p2.Eta()) > 1.05) {
		      h_MuonEta_Pair_C[1][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_C[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_C[1][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_C[1][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_C[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_C[1][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_DeltaPhi_C[1][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		      h_MuonRap_Inc_C[1][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_C[1][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);			      
		    } else if (fabs(p1.Eta()) > 1.05  && fabs(p2.Eta()) <= 1.05) {
		      h_MuonEta_Pair_D[1][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_D[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_D[1][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_D[1][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_D[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_D[1][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_DeltaPhi_D[1][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		      h_MuonRap_Inc_D[1][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_D[1][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);			      
		    }

		    if (p1.Pt() < 6.) {
		      h_MuonEta_Pair_Fir[1][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_Fir[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_Fir[1][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_Fir[1][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_Fir[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_Fir[1][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_MuonRap_Inc_Fir[1][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_Fir[1][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		      h_MuonEta_Inc_Fir[1][l][j][i]->Fill(p1.Eta(), local_EventWeight);
		      h_MuonEta_Inc_Fir[1][l][j][i]->Fill(p2.Eta(), local_EventWeight);
		      h_MuonPhi_Inc_Fir[1][l][j][i]->Fill(p1.Phi(), local_EventWeight);
		      h_MuonPhi_Inc_Fir[1][l][j][i]->Fill(p2.Phi(), local_EventWeight);
		      h_DeltaPhi_Fir[1][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		    } else if (p1.Pt() > 6. && p1.Pt() < 10.) {
		      h_MuonEta_Pair_Sec[1][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_Sec[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_Sec[1][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_Sec[1][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_Sec[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_Sec[1][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_MuonRap_Inc_Sec[1][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_Sec[1][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		      h_MuonEta_Inc_Sec[1][l][j][i]->Fill(p1.Eta(), local_EventWeight);
		      h_MuonEta_Inc_Sec[1][l][j][i]->Fill(p2.Eta(), local_EventWeight);
		      h_MuonPhi_Inc_Sec[1][l][j][i]->Fill(p1.Phi(), local_EventWeight);
		      h_MuonPhi_Inc_Sec[1][l][j][i]->Fill(p2.Phi(), local_EventWeight);
		      h_DeltaPhi_Sec[1][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);			      
		    } else if (p1.Pt() > 10. && p1.Pt() < 15.) {
		      h_MuonEta_Pair_Trd[1][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_Trd[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_Trd[1][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_Trd[1][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_Trd[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_Trd[1][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_MuonRap_Inc_Trd[1][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_Trd[1][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		      h_MuonEta_Inc_Trd[1][l][j][i]->Fill(p1.Eta(), local_EventWeight);
		      h_MuonEta_Inc_Trd[1][l][j][i]->Fill(p2.Eta(), local_EventWeight);
		      h_MuonPhi_Inc_Trd[1][l][j][i]->Fill(p1.Phi(), local_EventWeight);
		      h_MuonPhi_Inc_Trd[1][l][j][i]->Fill(p2.Phi(), local_EventWeight);
		      h_DeltaPhi_Trd[1][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		    }  else if (p1.Pt() > 15) {
		      h_MuonEta_Pair_Fou[1][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		      h_MuonEta_Lead_Fou[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		      h_MuonEta_Sub_Fou[1][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		      h_MuonPhi_Pair_Fou[1][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		      h_MuonPhi_Lead_Fou[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		      h_MuonPhi_Sub_Fou[1][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
		      h_MuonRap_Inc_Fou[1][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		      h_MuonRap_Inc_Fou[1][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		      h_MuonEta_Inc_Fou[1][l][j][i]->Fill(p1.Eta(), local_EventWeight);
		      h_MuonEta_Inc_Fou[1][l][j][i]->Fill(p2.Eta(), local_EventWeight);
		      h_MuonPhi_Inc_Fou[1][l][j][i]->Fill(p1.Phi(), local_EventWeight);
		      h_MuonPhi_Inc_Fou[1][l][j][i]->Fill(p2.Phi(), local_EventWeight);
		      h_DeltaPhi_Fou[1][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		    }

		    if (isMC) {
		      h_d0_mu1mu2[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->auxdata<float>("smeared_d0"), (muonsCorr)->at(SUB)->auxdata<float>("smeared_d0"));
		      h_d0_mu1[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->auxdata<float>("smeared_d0"), local_EventWeight);
		      h_d0_mu1[1][l][j][i]->Fill((muonsCorr)->at(SUB)->auxdata<float>("smeared_d0"), local_EventWeight);
		      h_z0_mu1[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->auxdata<float>("smeared_z0"), local_EventWeight);
		      h_z0_mu1[1][l][j][i]->Fill((muonsCorr)->at(SUB)->auxdata<float>("smeared_z0"), local_EventWeight);

		      h_d0_mu1_BF[1][l][j][i]->Fill(trk_lead->d0(), local_EventWeight);
		      h_d0_mu1_BF[1][l][j][i]->Fill(trk_sub->d0(), local_EventWeight);
		      h_z0_mu1_BF[1][l][j][i]->Fill(trk_lead->z0(), local_EventWeight);
		      h_z0_mu1_BF[1][l][j][i]->Fill(trk_sub->z0(), local_EventWeight);

		    } else {
		      h_d0_mu1mu2[1][l][j][i]->Fill(trk_lead->d0(), trk_sub->d0());
		      h_d0_mu1[1][l][j][i]->Fill(trk_lead->d0(), local_EventWeight);
		      h_d0_mu1[1][l][j][i]->Fill(trk_sub->d0(), local_EventWeight);
		      h_z0_mu1[1][l][j][i]->Fill(trk_lead->z0(), local_EventWeight);
		      h_z0_mu1[1][l][j][i]->Fill(trk_sub->z0(), local_EventWeight);

		      h_d0_mu1_BF[1][l][j][i]->Fill(trk_lead->d0(), local_EventWeight);
		      h_d0_mu1_BF[1][l][j][i]->Fill(trk_sub->d0(), local_EventWeight);
		      h_z0_mu1_BF[1][l][j][i]->Fill(trk_lead->z0(), local_EventWeight);
		      h_z0_mu1_BF[1][l][j][i]->Fill(trk_sub->z0(), local_EventWeight);
		    }

		    h_delta_z0[1][l][j][i]->Fill(delta_z0, local_EventWeight);
		    
		    h_EventWeight[1][l][j][i]->Fill(local_EventWeight);
		    h_Multiplicity[1][l][j][i]->Fill(mupairs[j][i].size());
		    h_kF_InvMass[1][l][j][i]->Fill(Propagator.Mag(),KFactorWeight);
	      
		    h_BornMass[1][l][j][i]->Fill(Truth_V_Mass, local_EventWeight);
	      
		    h_chi2_ISO[1][l][j][i]->Fill(chi2, local_EventWeight);
		    h_chi2_Prob_ISO[1][l][j][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		    h_chi2_Prob_noweight[1][l][j][i]->Fill(TMath::Prob(chi2,3));
		    h_d0sig_deltaz0sig[1][l][j][i]->Fill(d0_significance_BOTH, delta_z0_significance);

		    h_InvMass[1][l][j][i]->Fill(Propagator.Mag(), local_EventWeight);

		    if ( (muonsCorr->at(LEAD)->charge() > 0) && (muonsCorr->at(SUB)->charge() > 0) ) {
		      h_InvMass_pp_ISO[1][l][j][i]->Fill(Propagator.Mag(), local_EventWeight);
		      h_chi2_Prob_pp_ISO[1][l][j][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		    } else if ( (muonsCorr->at(LEAD)->charge() < 0) && (muonsCorr->at(SUB)->charge() < 0) ) {
		      h_InvMass_mm_ISO[1][l][j][i]->Fill(Propagator.Mag(), local_EventWeight);
		      h_chi2_Prob_mm_ISO[1][l][j][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		    }

		    if (Propagator.Mag() > 7. && Propagator.Mag() < 9.) {
		      h_MuonPt_Pair_A[1][l][j][i]->Fill(Propagator.Pt(), local_EventWeight);

		      h_InvMass_low[1][l][j][i]->Fill(Propagator.Mag(), local_EventWeight);

		    } else if (Propagator.Mag() >= 12. && Propagator.Mag() < 14.) {
		      h_MuonPt_Pair_B[1][l][j][i]->Fill(Propagator.Pt(), local_EventWeight);
		    } else if (Propagator.Mag() >= 14. && Propagator.Mag() < 17.) {
		      h_MuonPt_Pair_C[1][l][j][i]->Fill(Propagator.Pt(), local_EventWeight);
		    } else if (Propagator.Mag() >= 17. && Propagator.Mag() < 22.) {
		      h_MuonPt_Pair_D[1][l][j][i]->Fill(Propagator.Pt(), local_EventWeight);
		    } else if (Propagator.Mag() >= 22. && Propagator.Mag() < 28.) {
		      h_MuonPt_Pair_E[1][l][j][i]->Fill(Propagator.Pt(), local_EventWeight);
		    } else if (Propagator.Mag() >= 28. && Propagator.Mag() < 36.) {
		      h_MuonPt_Pair_F[1][l][j][i]->Fill(Propagator.Pt(), local_EventWeight);
		    } else if (Propagator.Mag() >= 36. && Propagator.Mag() < 46.) {
		      h_MuonPt_Pair_G[1][l][j][i]->Fill(Propagator.Pt(), local_EventWeight);
		    } else if (Propagator.Mag() >= 46. && Propagator.Mag() < 60.) {
		      h_MuonPt_Pair_H[1][l][j][i]->Fill(Propagator.Pt(), local_EventWeight);
		    }

		    h_MuonPt_Pair[1][l][j][i]->Fill(Propagator.Pt(), local_EventWeight);
		    h_MuonPt_Lead[1][l][j][i]->Fill(p1.Pt(), local_EventWeight);
		    h_MuonPt_Sub[1][l][j][i]->Fill(p2.Pt(), local_EventWeight);
		    h_Pt_muons[1][l][j][i]->Fill(p1.Pt(), p2.Pt());
	      
		    h_MuonEta_Pair[1][l][j][i]->Fill(Propagator.Eta(), local_EventWeight);
		    h_MuonEta_Lead[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->eta(), local_EventWeight);
		    h_MuonEta_Sub[1][l][j][i]->Fill((muonsCorr)->at(SUB)->eta(), local_EventWeight);
		    h_MuonPhi_Pair[1][l][j][i]->Fill(Propagator.Phi(), local_EventWeight);
		    h_MuonPhi_Lead[1][l][j][i]->Fill((muonsCorr)->at(LEAD)->phi(), local_EventWeight);
		    h_MuonPhi_Sub[1][l][j][i]->Fill((muonsCorr)->at(SUB)->phi(), local_EventWeight);
	
		    h_MuonPt_Inc[1][l][j][i]->Fill(p1.Pt(), local_EventWeight);
		    h_MuonPt_Inc[1][l][j][i]->Fill(p2.Pt(), local_EventWeight);
		    h_MuonRap_Inc[1][l][j][i]->Fill(p1.Rapidity(), local_EventWeight);
		    h_MuonRap_Inc[1][l][j][i]->Fill(p2.Rapidity(), local_EventWeight);
		    h_MuonEta_Inc[1][l][j][i]->Fill(p1.Eta(), local_EventWeight);
		    h_MuonEta_Inc[1][l][j][i]->Fill(p2.Eta(), local_EventWeight);
		    h_MuonPhi_Inc[1][l][j][i]->Fill(p1.Phi(), local_EventWeight);
		    h_MuonPhi_Inc[1][l][j][i]->Fill(p2.Phi(), local_EventWeight);
	      
		    DeltaR_ll = ( (muonsCorr)->at(LEAD)->p4() ).DeltaR( (muonsCorr)->at(SUB)->p4() );

		    h_DeltaPhi[1][l][j][i]->Fill(DeltaPhi_ll, local_EventWeight);
		    h_DeltaR[1][l][j][i]->Fill(DeltaR_ll, local_EventWeight);

		    h_DeltaEta[1][l][j][i]->Fill(((muonsCorr)->at(LEAD)->eta())-((muonsCorr)->at(SUB)->eta()), local_EventWeight);
		    h_Rapidity[1][l][j][i]->Fill(Propagator.Rapidity(), local_EventWeight);
		
		    h_d0_significance_BOTH_ISO_Sig[1][l][j][i]->Fill(d0_significance_BOTH, local_EventWeight);
		    h_delta_z0_significance_ISO_Sig[1][l][j][i]->Fill(delta_z0_significance, local_EventWeight);

		    //Exctrating the cross section in SS selection (needed for OS - SS subtarction)
		    if (Propagator.Mag() > 7. && Propagator.Mag() < 9.) {
		      h_d0_significance_BOTH_ISO[1][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			    
		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_ISO_rapA[1][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_ISO_rapB[1][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_ISO_rapC[1][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_ISO_rapD[1][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_ISO_rapE[1][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_ISO_rapF[1][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }			

		    if (Propagator.Mag() >= 12. && Propagator.Mag() < 14.) {
		      h_d0_significance_BOTH_ISO[1][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_ISO_rapA[1][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_ISO_rapB[1][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_ISO_rapC[1][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_ISO_rapD[1][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_ISO_rapE[1][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_ISO_rapF[1][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 14. && Propagator.Mag() < 17.) {
		      h_d0_significance_BOTH_ISO[1][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_ISO_rapA[1][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_ISO_rapB[1][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_ISO_rapC[1][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_ISO_rapD[1][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_ISO_rapE[1][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_ISO_rapF[1][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 17. && Propagator.Mag() < 22.) {
		      h_d0_significance_BOTH_ISO[1][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_ISO_rapA[1][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_ISO_rapB[1][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_ISO_rapC[1][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_ISO_rapD[1][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_ISO_rapE[1][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_ISO_rapF[1][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 22. && Propagator.Mag() < 28.) {
		      h_d0_significance_BOTH_ISO[1][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_ISO_rapA[1][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_ISO_rapB[1][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_ISO_rapC[1][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_ISO_rapD[1][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_ISO_rapE[1][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_ISO_rapF[1][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 28. && Propagator.Mag() < 36.) {
		      h_d0_significance_BOTH_ISO[1][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_ISO_rapA[1][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_ISO_rapB[1][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_ISO_rapC[1][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_ISO_rapD[1][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_ISO_rapE[1][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_ISO_rapF[1][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 36. && Propagator.Mag() < 46.) {
		      h_d0_significance_BOTH_ISO[1][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_ISO_rapA[1][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_ISO_rapB[1][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_ISO_rapC[1][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_ISO_rapD[1][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_ISO_rapE[1][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_ISO_rapF[1][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 46. && Propagator.Mag() < 60.) {
		      h_d0_significance_BOTH_ISO[1][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_ISO_rapA[1][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_ISO_rapB[1][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_ISO_rapC[1][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_ISO_rapD[1][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_ISO_rapE[1][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_ISO_rapF[1][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		    }

		    //not iso-not iso case
		  }  else if (!(iso_muon_fixed_tight->accept(*(muonsCorr)->at(LEAD))) && !(iso_muon_fixed_tight->accept(*(muonsCorr)->at(SUB)))) {

		    l = 2;

		    local_EventWeight *= w_Trig_SF_Fir * w_Trig_SF_Sec;

		    h_chi2_Prob_NOISO[1][l][j][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		    h_d0_significance_BOTH_NOISO_Sig[1][l][j][i]->Fill(d0_significance_BOTH, local_EventWeight);
		    h_delta_z0_significance_NOISO_Sig[1][l][j][i]->Fill(delta_z0_significance, local_EventWeight);
		    h_chi2_NOISO[1][l][j][i]->Fill(chi2, local_EventWeight);

		    if ( (muonsCorr->at(LEAD)->charge() > 0) && (muonsCorr->at(SUB)->charge() > 0) ) {
		      h_InvMass_pp_NOISO[1][l][j][i]->Fill(Propagator.Mag(), local_EventWeight);
		      h_chi2_Prob_pp_NOISO[1][l][j][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		    } else if ( (muonsCorr->at(LEAD)->charge() < 0) && (muonsCorr->at(SUB)->charge() < 0) ) {
		      h_InvMass_mm_NOISO[1][l][j][i]->Fill(Propagator.Mag(), local_EventWeight);
		      h_chi2_Prob_mm_NOISO[1][l][j][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		    }

		    if (Propagator.Mag() > 7. && Propagator.Mag() < 9.) {
		      h_d0_significance_BOTH_NOISO[1][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_NOISO_rapA[1][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_NOISO_rapB[1][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_NOISO_rapC[1][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_NOISO_rapD[1][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_NOISO_rapE[1][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_NOISO_rapF[1][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		    }
		    if (Propagator.Mag() >= 12. && Propagator.Mag() < 14.) {
		      h_d0_significance_BOTH_NOISO[1][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			    
		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_NOISO_rapA[1][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_NOISO_rapB[1][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_NOISO_rapC[1][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_NOISO_rapD[1][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_NOISO_rapE[1][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_NOISO_rapF[1][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 14. && Propagator.Mag() < 17.) {
		      h_d0_significance_BOTH_NOISO[1][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_NOISO_rapA[1][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_NOISO_rapB[1][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_NOISO_rapC[1][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_NOISO_rapD[1][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_NOISO_rapE[1][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_NOISO_rapF[1][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 17. && Propagator.Mag() < 22.) {
		      h_d0_significance_BOTH_NOISO[1][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_NOISO_rapA[1][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_NOISO_rapB[1][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_NOISO_rapC[1][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_NOISO_rapD[1][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_NOISO_rapE[1][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_NOISO_rapF[1][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 22. && Propagator.Mag() < 28.) {
		      h_d0_significance_BOTH_NOISO[1][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_NOISO_rapA[1][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_NOISO_rapB[1][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_NOISO_rapC[1][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_NOISO_rapD[1][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_NOISO_rapE[1][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_NOISO_rapF[1][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 28. && Propagator.Mag() < 36.) {
		      h_d0_significance_BOTH_NOISO[1][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_NOISO_rapA[1][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_NOISO_rapB[1][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_NOISO_rapC[1][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_NOISO_rapD[1][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_NOISO_rapE[1][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_NOISO_rapF[1][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 36. && Propagator.Mag() < 46.) {
		      h_d0_significance_BOTH_NOISO[1][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_NOISO_rapA[1][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_NOISO_rapB[1][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_NOISO_rapC[1][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_NOISO_rapD[1][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_NOISO_rapE[1][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_NOISO_rapF[1][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 46. && Propagator.Mag() < 60.) {
		      h_d0_significance_BOTH_NOISO[1][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_NOISO_rapA[1][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_NOISO_rapB[1][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_NOISO_rapC[1][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_NOISO_rapD[1][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_NOISO_rapE[1][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_NOISO_rapF[1][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		    }			  

		    //one muon is isolated and the other is not (iso-not iso case)
		  } else if (((iso_muon_fixed_tight->accept(*(muonsCorr)->at(LEAD))) && !(iso_muon_fixed_tight->accept(*(muonsCorr)->at(SUB)))) || (!(iso_muon_fixed_tight->accept(*(muonsCorr)->at(LEAD))) && (iso_muon_fixed_tight->accept(*(muonsCorr)->at(SUB))))) {

		    l = 0; 

		    if (isMC) {
		      if ((iso_muon_fixed_tight->accept(*(muonsCorr)->at(LEAD)))) {
			w_Iso_SF_Fir = GetMuonIsoSF_FixedCutTight((muonsCorr->at(LEAD)));
		      } else {
			w_Iso_SF_Sec = GetMuonIsoSF_FixedCutTight((muonsCorr->at(SUB)));
		      }
		    }

		    local_EventWeight *= w_Iso_SF_Fir * w_Iso_SF_Sec;
		    
		    local_EventWeight *= w_Trig_SF_Fir * w_Trig_SF_Sec;

		    h_d0_significance_BOTH_HALF_Sig[1][l][j][i]->Fill(d0_significance_BOTH, local_EventWeight);
		    h_delta_z0_significance_HALF_Sig[1][l][j][i]->Fill(delta_z0_significance, local_EventWeight);
		    h_chi2_HALF[1][l][j][i]->Fill(chi2, local_EventWeight);
		    h_chi2_Prob_HALF[1][l][j][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		    if (Propagator.Mag() > 7. && Propagator.Mag() < 9.) {
		      h_d0_significance_BOTH_HALF[1][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_HALF_rapA[1][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_HALF_rapB[1][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_HALF_rapC[1][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_HALF_rapD[1][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_HALF_rapE[1][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_HALF_rapF[1][l][0][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		    }

		    if (Propagator.Mag() >= 12. && Propagator.Mag() < 14.) {
		      h_d0_significance_BOTH_HALF[1][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
			  
		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_HALF_rapA[1][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_HALF_rapB[1][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_HALF_rapC[1][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_HALF_rapD[1][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_HALF_rapE[1][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_HALF_rapF[1][l][1][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 14. && Propagator.Mag() < 17.) {
		      h_d0_significance_BOTH_HALF[1][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_HALF_rapA[1][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_HALF_rapB[1][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_HALF_rapC[1][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_HALF_rapD[1][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_HALF_rapE[1][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_HALF_rapF[1][l][2][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 17. && Propagator.Mag() < 22.) {
		      h_d0_significance_BOTH_HALF[1][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_HALF_rapA[1][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_HALF_rapB[1][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_HALF_rapC[1][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_HALF_rapD[1][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_HALF_rapE[1][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_HALF_rapF[1][l][3][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 22. && Propagator.Mag() < 28.) {
		      h_d0_significance_BOTH_HALF[1][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_HALF_rapA[1][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_HALF_rapB[1][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_HALF_rapC[1][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_HALF_rapD[1][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_HALF_rapE[1][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_HALF_rapF[1][l][4][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 28. && Propagator.Mag() < 36.) {
		      h_d0_significance_BOTH_HALF[1][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_HALF_rapA[1][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_HALF_rapB[1][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_HALF_rapC[1][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_HALF_rapD[1][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_HALF_rapE[1][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_HALF_rapF[1][l][5][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 36. && Propagator.Mag() < 46.) {
		      h_d0_significance_BOTH_HALF[1][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_HALF_rapA[1][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_HALF_rapB[1][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_HALF_rapC[1][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_HALF_rapD[1][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_HALF_rapE[1][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_HALF_rapF[1][l][6][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }

		    }
		    if (Propagator.Mag() >= 46. && Propagator.Mag() < 60.) {
		      h_d0_significance_BOTH_HALF[1][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);

		      if (RapZ_Abs > 0. && RapZ_Abs < 0.4) {
			h_d0_significance_BOTH_HALF_rapA[1][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.4 && RapZ_Abs < 0.8) {
			h_d0_significance_BOTH_HALF_rapB[1][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 0.8 && RapZ_Abs < 1.2) {
			h_d0_significance_BOTH_HALF_rapC[1][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.2 && RapZ_Abs < 1.6) {
			h_d0_significance_BOTH_HALF_rapD[1][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 1.6 && RapZ_Abs < 2.0) {
			h_d0_significance_BOTH_HALF_rapE[1][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		      if (RapZ_Abs >= 2.0 && RapZ_Abs < 2.4) {
			h_d0_significance_BOTH_HALF_rapF[1][l][7][i]->Fill(TMath::Prob(chi2,3), local_EventWeight);
		      }
		    }
		  }
		}  
	      }//end of the SS selection
		    
	    }
	  }
	}
      }
    }
  }
    
  return EL::StatusCode::SUCCESS;
} // end of execute

