
#include <MyAnalysis/MyxAODAnalysis.h>


EL::StatusCode MyxAODAnalysis :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected

  std::cout<<"HISTOGRAMMING! - begin..."<<std::endl;

  //m_f = new TFile("file.root","RECREATE");

  //dimuonTree = new TTree("dimuonTree","Z' lfv tree");
  //lfvTree->SetDirectory(lfvFile);
  //SetTreeBranches();
  //wk()->addOutput(dimuonTree);

  TH1::SetDefaultSumw2(kTRUE);

  h_N_vtx_BF = new TH1D("h_N_vtx_BF", "", 50, 0., 50.);
  wk()->addOutput (h_N_vtx_BF);

  h_N_vtx_A = new TH1D("h_N_vtx_A", "", 50, 0., 50.);
  wk()->addOutput (h_N_vtx_A);

  h_N_vtx_B = new TH1D("h_N_vtx_B", "", 50, 0., 50.);
  wk()->addOutput (h_N_vtx_B);

  h_PileUpWeight = new TH1D("h_PileUpWeight", "", 900, -10., 80.);
  wk()->addOutput (h_PileUpWeight);

  h_kF = new TH1D("h_kF", "", 115, 6., 121.);
  wk()->addOutput (h_kF);

  h_kFweight = new TH1D("h_kFweight", "h_kFweight", 115, 6., 121.);
  wk()->addOutput (h_kFweight);

  h_Dummy = new TH1D("h_Dummy", "", 115, 6., 121.);
  wk()->addOutput (h_Dummy);

  h_PU = new TH1D("h_PU", "h_PU", 100, 0., 100.);
  wk()->addOutput (h_PU);

  h_PU_BF = new TH1D("h_PU_BF", "h_PU_BF", 100, 0., 100.);
  wk()->addOutput (h_PU_BF);

  int nxbins = 72;
  double xmin = 80;
  double xmax = 10000;
  double logxmin = TMath::Log10(xmin);
  double logxmax = TMath::Log10(xmax);
  double binwidth = (logxmax-logxmin)/double(nxbins);
  double xbins[73]; double xbins2[66];
  xbins[0] = xmin;
  for (int i=1;i<=nxbins;i++){
    xbins[i] = TMath::Power(10,logxmin+i*binwidth);
    if( i > 6 ){ xbins2[i-7] = TMath::Power(10,logxmin+i*binwidth); }
  } // end of for loop

  //Variable for histos' names                                                                                     
  char histname[256];
  char htitle[256];

  //CutFlow common selection                                                                                                     
  h_CutFlow_Common = new TH1D("h_CutFlow_Common","h_CutFlow_Common",25,0.,25.);
  wk()->addOutput (h_CutFlow_Common);

  h_TRY = new TH1D("h_TRY","h_TRY",55, 6., 61.);
  wk()->addOutput (h_TRY);

  h_TRY_2 = new TH1D("h_TRY_2","h_TRY_2",55, 6., 61.);
  wk()->addOutput (h_TRY_2);

  h_Truth_V_Mass = new TH1D("h_Truth_V_Mass","h_Truth_V_Mass",115, 6., 121.);
  wk()->addOutput (h_Truth_V_Mass);

  h_Truth_V_Mass_NEW = new TH1D("h_Truth_V_Mass_NEW","h_Truth_V_Mass_NEW",230, 6., 121.);
  wk()->addOutput (h_Truth_V_Mass_NEW);
  
  h_ZpT = new TH1D("h_ZpT","h_ZpT",200, 0.0, 2.0);
  wk()->addOutput (h_ZpT);
  
  h_BosonPt = new TH1D("h_BosonPt", "h_BosonPt", 50, 0., 250.);
  wk()->addOutput (h_BosonPt);

  Double_t mass_bins_B[] = {7, 9, 12, 14, 17, 22, 28, 36, 46, 60};
  Int_t mass_binnum_B = sizeof(mass_bins_B)/sizeof(Double_t) - 1;

  h_ZpT_A = new TH1D("h_ZpT_A", "h_ZpT_A", 200, 0., 200.);
  wk()->addOutput (h_ZpT_A);

  h_ZpT_B = new TH1D("h_ZpT_B", "h_ZpT_B", 200, 0., 200.);
  wk()->addOutput (h_ZpT_B);

  h_ZpT_C = new TH1D("h_ZpT_C", "h_ZpT_C", 200, 0., 200.);
  wk()->addOutput (h_ZpT_C);

  h_ZpT_D = new TH1D("h_ZpT_D", "h_ZpT_D", 200, 0., 200.);
  wk()->addOutput (h_ZpT_D);

  h_ZpT_E = new TH1D("h_ZpT_E", "h_ZpT_E", 200, 0., 200.);
  wk()->addOutput (h_ZpT_E);

  h_ZpT_F = new TH1D("h_ZpT_F", "h_ZpT_F", 200, 0., 200.);
  wk()->addOutput (h_ZpT_F);

  h_ZpT_G = new TH1D("h_ZpT_G", "h_ZpT_G", 200, 0., 200.);
  wk()->addOutput (h_ZpT_G);

  h_ZpT_H = new TH1D("h_ZpT_H", "h_ZpT_H", 200, 0., 200.);
  wk()->addOutput (h_ZpT_H);

  h_ZpT_A_After = new TH1D("h_ZpT_A_After", "h_ZpT_A_After", 200, 0., 200.);
  wk()->addOutput (h_ZpT_A_After);

  h_ZpT_B_After = new TH1D("h_ZpT_B_After", "h_ZpT_B_After", 200, 0., 200.);
  wk()->addOutput (h_ZpT_B_After);

  h_ZpT_C_After = new TH1D("h_ZpT_C_After", "h_ZpT_C_After", 200, 0., 200.);
  wk()->addOutput (h_ZpT_C_After);

  h_ZpT_D_After = new TH1D("h_ZpT_D_After", "h_ZpT_D_After", 200, 0., 200.);
  wk()->addOutput (h_ZpT_D_After);

  h_ZpT_E_After = new TH1D("h_ZpT_E_After", "h_ZpT_E_After", 200, 0., 200.);
  wk()->addOutput (h_ZpT_E_After);

  h_ZpT_F_After = new TH1D("h_ZpT_F_After", "h_ZpT_F_After", 200, 0., 200.);
  wk()->addOutput (h_ZpT_F_After);

  h_ZpT_G_After = new TH1D("h_ZpT_G_After", "h_ZpT_G_After", 200, 0., 200.);
  wk()->addOutput (h_ZpT_G_After);

  h_ZpT_H_After = new TH1D("h_ZpT_H_After", "h_ZpT_H_After", 200, 0., 200.);
  wk()->addOutput (h_ZpT_H_After);

  //My histos                                                                                                              
  for(int i = 0; i < 2; i++) {     // 0 ---> OS; 1 ---> SS                                        
    for(int j = 0; j <6; j++) {      // 5 isolation WPs - we use JUST FixedCutTight                       
      for(int n = 0; n < 1; n++) {       // 0 ---> medium; 1 ---> tight                                     
	for(int k = 0; k < 3; k++) {        // 0 ---> 2mu4_1; 1 ---> 2mu4_2; 2 ---> 2mu6_1_OR_2mu6_2               

	  sprintf(htitle, "kF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_kF_InvMass_%i_%i_%i_%i",i,j,n,k);
	  h_kF_InvMass[i][j][n][k] = new TH2D(histname, htitle, 115, 6., 121., 20, 0.9, 1.1);
	  wk()->addOutput (h_kF_InvMass[i][j][n][k]);

	  sprintf(htitle, "Muon pairs Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_Multiplicity_%i_%i_%i_%i",i,j,n,k);
	  h_Multiplicity[i][j][n][k] = new TH1D(histname, htitle, 10, 0.,10.);
	  wk()->addOutput (h_Multiplicity[i][j][n][k]);	  

 	  sprintf(htitle, "Rapidity Inc Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonRap_Inc_%i_%i_%i_%i",i,j,n,k);
	  h_MuonRap_Inc[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonRap_Inc[i][j][n][k]);	                        

	  sprintf(htitle, "Eta Inc Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonEta_Inc_%i_%i_%i_%i",i,j,n,k);
	  h_MuonEta_Inc[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonEta_Inc[i][j][n][k]);
	  
	  sprintf(htitle, "Phi Inc Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonPhi_Inc_%i_%i_%i_%i",i,j,n,k);
	  h_MuonPhi_Inc[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
	  wk()->addOutput (h_MuonPhi_Inc[i][j][n][k]);
	  	
	  sprintf(htitle, "Dimuon Rapidity Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_Rapidity_%i_%i_%i_%i",i,j,n,k);
	  h_Rapidity[i][j][n][k] = new TH1D(histname, htitle, 26, -2.5, 2.5);
	  wk()->addOutput (h_Rapidity[i][j][n][k]);	  

	  sprintf(htitle, "Event Weight Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_EventWeight_%i_%i_%i_%i",i,j,n,k);
	  h_EventWeight[i][j][n][k] = new TH1D(histname,htitle,600,-5.,55.);
	  wk()->addOutput (h_EventWeight[i][j][n][k]);

	  sprintf(htitle, "Invariant Mass low Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_InvMass_low_%i_%i_%i_%i",i,j,n,k);
	  h_InvMass_low[i][j][n][k] = new TH1D(histname, htitle, 20, 7., 9.);
	  wk()->addOutput (h_InvMass_low[i][j][n][k]);

	  sprintf(htitle, "Invariant Mass Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_InvMass_%i_%i_%i_%i",i,j,n,k);
	  h_InvMass[i][j][n][k] = new TH1D(histname, htitle, mass_binnum_B, mass_bins_B);
	  wk()->addOutput (h_InvMass[i][j][n][k]);

	  sprintf(htitle, "Invariant Mass pp ISO Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_InvMass_pp_ISO_%i_%i_%i_%i",i,j,n,k);
	  h_InvMass_pp_ISO[i][j][n][k] = new TH1D(histname, htitle, mass_binnum_B, mass_bins_B);
	  wk()->addOutput (h_InvMass_pp_ISO[i][j][n][k]);

	  sprintf(htitle, "Invariant Mass mm ISO Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_InvMass_mm_ISO_%i_%i_%i_%i",i,j,n,k);
	  h_InvMass_mm_ISO[i][j][n][k] = new TH1D(histname, htitle, mass_binnum_B, mass_bins_B);
	  wk()->addOutput (h_InvMass_mm_ISO[i][j][n][k]);

	  sprintf(htitle, "Invariant Mass pp NOISO Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_InvMass_pp_NOISO_%i_%i_%i_%i",i,j,n,k);
	  h_InvMass_pp_NOISO[i][j][n][k] = new TH1D(histname, htitle, mass_binnum_B, mass_bins_B);
	  wk()->addOutput (h_InvMass_pp_NOISO[i][j][n][k]);

	  sprintf(htitle, "Invariant Mass mm NOISO Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_InvMass_mm_NOISO_%i_%i_%i_%i",i,j,n,k);
	  h_InvMass_mm_NOISO[i][j][n][k] = new TH1D(histname, htitle, mass_binnum_B, mass_bins_B);
	  wk()->addOutput (h_InvMass_mm_NOISO[i][j][n][k]);

	  sprintf(htitle, "Invariant Mass Up Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_InvMass_Up_%i_%i_%i_%i",i,j,n,k);
	  h_InvMass_Up[i][j][n][k] = new TH1D(histname, htitle, mass_binnum_B, mass_bins_B);
	  wk()->addOutput (h_InvMass_Up[i][j][n][k]);

	  sprintf(htitle, "Invariant Mass Down Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_InvMass_Down_%i_%i_%i_%i",i,j,n,k);
	  h_InvMass_Down[i][j][n][k] = new TH1D(histname, htitle, mass_binnum_B, mass_bins_B);
	  wk()->addOutput (h_InvMass_Down[i][j][n][k]);

	  sprintf(htitle, "Invariant Mass Down Iso Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_InvMass_DownIso_%i_%i_%i_%i",i,j,n,k);
	  h_InvMass_DownIso[i][j][n][k] = new TH1D(histname, htitle, mass_binnum_B, mass_bins_B);
	  wk()->addOutput (h_InvMass_DownIso[i][j][n][k]);

	  sprintf(htitle, "Invariant Mass Up Iso Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_InvMass_UpIso_%i_%i_%i_%i",i,j,n,k);
	  h_InvMass_UpIso[i][j][n][k] = new TH1D(histname, htitle, mass_binnum_B, mass_bins_B);
	  wk()->addOutput (h_InvMass_UpIso[i][j][n][k]);

	  sprintf(htitle, "Invariant Mass Down AntiIso Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_InvMass_DownAntiIso_%i_%i_%i_%i",i,j,n,k);
	  h_InvMass_DownAntiIso[i][j][n][k] = new TH1D(histname, htitle, mass_binnum_B, mass_bins_B);
	  wk()->addOutput (h_InvMass_DownAntiIso[i][j][n][k]);

	  sprintf(htitle, "Invariant Mass Up AntiIso Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_InvMass_UpAntiIso_%i_%i_%i_%i",i,j,n,k);
	  h_InvMass_UpAntiIso[i][j][n][k] = new TH1D(histname, htitle, mass_binnum_B, mass_bins_B);
	  wk()->addOutput (h_InvMass_UpAntiIso[i][j][n][k]);

	  sprintf(htitle, "Truth Mass Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_BornMass_%i_%i_%i_%i",i,j,n,k);
	  h_BornMass[i][j][n][k] = new TH1D(histname, htitle, 115, 6., 121.);
	  wk()->addOutput (h_BornMass[i][j][n][k]);

	  sprintf(htitle, "Pt_muons Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_Pt_muons_%i_%i_%i_%i",i,j,n,k);
	  h_Pt_muons[i][j][n][k] = new TH2D(histname, htitle, 96,4.,100.,96,4.,100.);
	  wk()->addOutput (h_Pt_muons[i][j][n][k]);

          sprintf(htitle, "Pt_Eta Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_PtEta_%i_%i_%i_%i",i,j,n,k);
          h_PtEta[i][j][n][k] = new TH2D(histname, htitle, 96,4.,100.,13,-2.5,2.5);
          wk()->addOutput (h_PtEta[i][j][n][k]);

          sprintf(htitle, "Pt_Eta_lead Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_PtEta_Lead_%i_%i_%i_%i",i,j,n,k);
          h_PtEta_Lead[i][j][n][k] = new TH2D(histname, htitle, 96,4.,100.,13,-2.5,2.5);
          wk()->addOutput (h_PtEta_Lead[i][j][n][k]);

          sprintf(htitle, "Pt_Eta_sub Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_PtEta_Sub_%i_%i_%i_%i",i,j,n,k);
          h_PtEta_Sub[i][j][n][k] = new TH2D(histname, htitle, 96,4.,100.,13,-2.5,2.5);
          wk()->addOutput (h_PtEta_Sub[i][j][n][k]);

	  sprintf(htitle, "Pt_Phi Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_PtPhi_%i_%i_%i_%i",i,j,n,k);
          h_PtPhi[i][j][n][k] = new TH2D(histname, htitle, 96,4.,100.,20,-4.,4.);
          wk()->addOutput (h_PtPhi[i][j][n][k]);

          sprintf(htitle, "Pt_Phi_lead Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_PtPhi_Lead_%i_%i_%i_%i",i,j,n,k);
          h_PtPhi_Lead[i][j][n][k] = new TH2D(histname, htitle, 96,4.,100.,20,-4.,4.);
          wk()->addOutput (h_PtPhi_Lead[i][j][n][k]);

          sprintf(htitle, "Pt_Phi_sub Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_PtPhi_Sub_%i_%i_%i_%i",i,j,n,k);
          h_PtPhi_Sub[i][j][n][k] = new TH2D(histname, htitle, 96,4.,100.,20,-4.,4.);
          wk()->addOutput (h_PtPhi_Sub[i][j][n][k]);

          sprintf(htitle, "Pt pair Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPt_Pair_%i_%i_%i_%i",i,j,n,k);
          h_MuonPt_Pair[i][j][n][k] = new TH1D(histname, htitle, 200, 0., 200.);
          wk()->addOutput (h_MuonPt_Pair[i][j][n][k]);

          sprintf(htitle, "Pt pair_A Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPt_Pair_A_%i_%i_%i_%i",i,j,n,k);
          h_MuonPt_Pair_A[i][j][n][k] = new TH1D(histname, htitle, 200, 0., 200.);
          wk()->addOutput (h_MuonPt_Pair_A[i][j][n][k]);

          sprintf(htitle, "Pt pair_B Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPt_Pair_B_%i_%i_%i_%i",i,j,n,k);
          h_MuonPt_Pair_B[i][j][n][k] = new TH1D(histname, htitle, 200, 0., 200.);
          wk()->addOutput (h_MuonPt_Pair_B[i][j][n][k]);

          sprintf(htitle, "Pt pair_C Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPt_Pair_C_%i_%i_%i_%i",i,j,n,k);
          h_MuonPt_Pair_C[i][j][n][k] = new TH1D(histname, htitle, 200, 0., 200.);
          wk()->addOutput (h_MuonPt_Pair_C[i][j][n][k]);

          sprintf(htitle, "Pt pair_D Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPt_Pair_D_%i_%i_%i_%i",i,j,n,k);
          h_MuonPt_Pair_D[i][j][n][k] = new TH1D(histname, htitle, 200, 0., 200.);
          wk()->addOutput (h_MuonPt_Pair_D[i][j][n][k]);

          sprintf(htitle, "Pt pair_E Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPt_Pair_E_%i_%i_%i_%i",i,j,n,k);
          h_MuonPt_Pair_E[i][j][n][k] = new TH1D(histname, htitle, 200, 0., 200.);
          wk()->addOutput (h_MuonPt_Pair_E[i][j][n][k]);

          sprintf(htitle, "Pt pair_F Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPt_Pair_F_%i_%i_%i_%i",i,j,n,k);
          h_MuonPt_Pair_F[i][j][n][k] = new TH1D(histname, htitle, 200, 0., 200.);
          wk()->addOutput (h_MuonPt_Pair_F[i][j][n][k]);

          sprintf(htitle, "Pt pair_G Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPt_Pair_G_%i_%i_%i_%i",i,j,n,k);
          h_MuonPt_Pair_G[i][j][n][k] = new TH1D(histname, htitle, 200, 0., 200.);
          wk()->addOutput (h_MuonPt_Pair_G[i][j][n][k]);

          sprintf(htitle, "Pt pair_H Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPt_Pair_H_%i_%i_%i_%i",i,j,n,k);
          h_MuonPt_Pair_H[i][j][n][k] = new TH1D(histname, htitle, 200, 0., 200.);
          wk()->addOutput (h_MuonPt_Pair_H[i][j][n][k]);

	  sprintf(htitle, "Pt lead Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPt_Lead_%i_%i_%i_%i",i,j,n,k);
          h_MuonPt_Lead[i][j][n][k] = new TH1D(histname, htitle, 200, 0., 200.);
          wk()->addOutput (h_MuonPt_Lead[i][j][n][k]);

          sprintf(htitle, "Pt sub Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPt_Sub_%i_%i_%i_%i",i,j,n,k);
          h_MuonPt_Sub[i][j][n][k] = new TH1D(histname, htitle, 200, 0., 200.);
          wk()->addOutput (h_MuonPt_Sub[i][j][n][k]);
	  
	  sprintf(htitle, "Pt Inc Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonPt_Inc_%i_%i_%i_%i",i,j,n,k);
	  h_MuonPt_Inc[i][j][n][k] = new TH1D(histname, htitle, 200, 0., 200.);
	  wk()->addOutput (h_MuonPt_Inc[i][j][n][k]);
	  
	  sprintf(htitle, "Eta pair Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Pair_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Pair[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Pair[i][j][n][k]);

          sprintf(htitle, "Eta lead Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Lead_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Lead[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Lead[i][j][n][k]);

          sprintf(htitle, "Eta sub Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Sub_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Sub[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Sub[i][j][n][k]);

	  sprintf(htitle, "Delta Eta Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_DeltaEta_%i_%i_%i_%i",i,j,n,k);
          h_DeltaEta[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_DeltaEta[i][j][n][k]);

	  sprintf(htitle, "Delta R Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_DeltaR_%i_%i_%i_%i",i,j,n,k);
          h_DeltaR[i][j][n][k] = new TH1D(histname, htitle, 241, -6., 6.);
          wk()->addOutput (h_DeltaR[i][j][n][k]);

          sprintf(htitle, "Phi pair Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Pair_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Pair[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
          wk()->addOutput (h_MuonPhi_Pair[i][j][n][k]);

	  sprintf(htitle, "Phi lead Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Lead_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Lead[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
          wk()->addOutput (h_MuonPhi_Lead[i][j][n][k]);

          sprintf(htitle, "Phi sub Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Sub_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Sub[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
          wk()->addOutput (h_MuonPhi_Sub[i][j][n][k]);

	  sprintf(htitle, "Delta Phi Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_DeltaPhi_%i_%i_%i_%i",i,j,n,k);
          h_DeltaPhi[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
          wk()->addOutput (h_DeltaPhi[i][j][n][k]);

	  sprintf(htitle, "Delta Phi A Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_DeltaPhi_A_%i_%i_%i_%i",i,j,n,k);
          h_DeltaPhi_A[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
          wk()->addOutput (h_DeltaPhi_A[i][j][n][k]);

	  sprintf(htitle, "Delta Phi A BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_DeltaPhi_A_BF_%i_%i_%i_%i",i,j,n,k);
          h_DeltaPhi_A_BF[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
          wk()->addOutput (h_DeltaPhi_A_BF[i][j][n][k]);

	  sprintf(htitle, "Rap Inc A Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonRap_Inc_A_%i_%i_%i_%i",i,j,n,k);
	  h_MuonRap_Inc_A[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonRap_Inc_A[i][j][n][k]);
	  
	  sprintf(htitle, "Rap Inc A BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonRap_Inc_A_BF_%i_%i_%i_%i",i,j,n,k);
	  h_MuonRap_Inc_A_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonRap_Inc_A_BF[i][j][n][k]);

	  sprintf(htitle, "Eta pair A BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Pair_A_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Pair_A_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Pair_A_BF[i][j][n][k]);

          sprintf(htitle, "Eta lead A BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Lead_A_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Lead_A_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Lead_A_BF[i][j][n][k]);

          sprintf(htitle, "Eta sub A BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Sub_A_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Sub_A_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Sub_A_BF[i][j][n][k]);

	  sprintf(htitle, "Eta pair A Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Pair_A_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Pair_A[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Pair_A[i][j][n][k]);

          sprintf(htitle, "Eta lead A Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Lead_A_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Lead_A[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Lead_A[i][j][n][k]);

          sprintf(htitle, "Eta sub A Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Sub_A_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Sub_A[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Sub_A[i][j][n][k]);

	  sprintf(htitle, "Phi pair A BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Pair_A_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Pair_A_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Pair_A_BF[i][j][n][k]);

          sprintf(htitle, "Phi lead A BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Lead_A_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Lead_A_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Lead_A_BF[i][j][n][k]);

          sprintf(htitle, "Phi sub A BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Sub_A_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Sub_A_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Sub_A_BF[i][j][n][k]);

	  sprintf(htitle, "Phi pair A Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Pair_A_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Pair_A[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Pair_A[i][j][n][k]);

          sprintf(htitle, "Phi lead A Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Lead_A_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Lead_A[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Lead_A[i][j][n][k]);

          sprintf(htitle, "Phi sub A Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Sub_A_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Sub_A[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Sub_A[i][j][n][k]);

	  sprintf(htitle, "Delta Phi B Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_DeltaPhi_B_%i_%i_%i_%i",i,j,n,k);
          h_DeltaPhi_B[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
          wk()->addOutput (h_DeltaPhi_B[i][j][n][k]);

	  sprintf(htitle, "Delta Phi B BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_DeltaPhi_B_BF_%i_%i_%i_%i",i,j,n,k);
          h_DeltaPhi_B_BF[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
          wk()->addOutput (h_DeltaPhi_B_BF[i][j][n][k]);

	  sprintf(htitle, "Rap Inc B Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonRap_Inc_B_%i_%i_%i_%i",i,j,n,k);
	  h_MuonRap_Inc_B[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonRap_Inc_B[i][j][n][k]);
	  
	  sprintf(htitle, "Rap Inc B BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonRap_Inc_B_BF_%i_%i_%i_%i",i,j,n,k);
	  h_MuonRap_Inc_B_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonRap_Inc_B_BF[i][j][n][k]);

	  sprintf(htitle, "Eta pair B BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Pair_B_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Pair_B_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Pair_B_BF[i][j][n][k]);

          sprintf(htitle, "Eta lead B BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Lead_B_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Lead_B_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Lead_B_BF[i][j][n][k]);

          sprintf(htitle, "Eta sub B BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Sub_B_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Sub_B_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Sub_B_BF[i][j][n][k]);

	  sprintf(htitle, "Eta pair B Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Pair_B_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Pair_B[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Pair_B[i][j][n][k]);

          sprintf(htitle, "Eta lead B Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Lead_B_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Lead_B[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Lead_B[i][j][n][k]);

          sprintf(htitle, "Eta sub B Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Sub_B_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Sub_B[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Sub_B[i][j][n][k]);

	  sprintf(htitle, "Phi pair B BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Pair_B_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Pair_B_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Pair_B_BF[i][j][n][k]);

          sprintf(htitle, "Phi lead B BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Lead_B_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Lead_B_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Lead_B_BF[i][j][n][k]);

          sprintf(htitle, "Phi sub B BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Sub_B_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Sub_B_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Sub_B_BF[i][j][n][k]);

	  sprintf(htitle, "Phi pair B Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Pair_B_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Pair_B[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Pair_B[i][j][n][k]);

          sprintf(htitle, "Phi lead B Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Lead_B_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Lead_B[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Lead_B[i][j][n][k]);

          sprintf(htitle, "Phi sub B Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Sub_B_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Sub_B[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Sub_B[i][j][n][k]);

	  sprintf(htitle, "Delta Phi C Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_DeltaPhi_C_%i_%i_%i_%i",i,j,n,k);
          h_DeltaPhi_C[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
          wk()->addOutput (h_DeltaPhi_C[i][j][n][k]);

	  sprintf(htitle, "Delta Phi C BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_DeltaPhi_C_BF_%i_%i_%i_%i",i,j,n,k);
          h_DeltaPhi_C_BF[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
          wk()->addOutput (h_DeltaPhi_C_BF[i][j][n][k]);

	  sprintf(htitle, "Rap Inc C Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonRap_Inc_C_%i_%i_%i_%i",i,j,n,k);
	  h_MuonRap_Inc_C[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonRap_Inc_C[i][j][n][k]);
	  
	  sprintf(htitle, "Rap Inc C BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonRap_Inc_C_BF_%i_%i_%i_%i",i,j,n,k);
	  h_MuonRap_Inc_C_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonRap_Inc_C_BF[i][j][n][k]);

	  sprintf(htitle, "Eta pair C BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Pair_C_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Pair_C_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Pair_C_BF[i][j][n][k]);

          sprintf(htitle, "Eta lead C BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Lead_C_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Lead_C_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Lead_C_BF[i][j][n][k]);

          sprintf(htitle, "Eta sub C BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Sub_C_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Sub_C_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Sub_C_BF[i][j][n][k]);

	  sprintf(htitle, "Eta pair C Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Pair_C_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Pair_C[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Pair_C[i][j][n][k]);

          sprintf(htitle, "Eta lead C Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Lead_C_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Lead_C[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Lead_C[i][j][n][k]);

          sprintf(htitle, "Eta sub C Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Sub_C_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Sub_C[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Sub_C[i][j][n][k]);

	  sprintf(htitle, "Phi pair C BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Pair_C_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Pair_C_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Pair_C_BF[i][j][n][k]);

          sprintf(htitle, "Phi lead C BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Lead_C_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Lead_C_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Lead_C_BF[i][j][n][k]);

          sprintf(htitle, "Phi sub C BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Sub_C_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Sub_C_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Sub_C_BF[i][j][n][k]);

	  sprintf(htitle, "Phi pair C Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Pair_C_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Pair_C[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Pair_C[i][j][n][k]);

          sprintf(htitle, "Phi lead C Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Lead_C_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Lead_C[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Lead_C[i][j][n][k]);

          sprintf(htitle, "Phi sub C Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Sub_C_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Sub_C[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Sub_C[i][j][n][k]);

	  sprintf(htitle, "Delta Phi D Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_DeltaPhi_D_%i_%i_%i_%i",i,j,n,k);
          h_DeltaPhi_D[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
          wk()->addOutput (h_DeltaPhi_D[i][j][n][k]);

	  sprintf(htitle, "Delta Phi D BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_DeltaPhi_D_BF_%i_%i_%i_%i",i,j,n,k);
          h_DeltaPhi_D_BF[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
          wk()->addOutput (h_DeltaPhi_D_BF[i][j][n][k]);

	  sprintf(htitle, "Rap Inc D Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonRap_Inc_D_%i_%i_%i_%i",i,j,n,k);
	  h_MuonRap_Inc_D[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonRap_Inc_D[i][j][n][k]);
	  
	  sprintf(htitle, "Rap Inc D BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonRap_Inc_D_BF_%i_%i_%i_%i",i,j,n,k);
	  h_MuonRap_Inc_D_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonRap_Inc_D_BF[i][j][n][k]);

	  sprintf(htitle, "Eta pair D BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Pair_D_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Pair_D_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Pair_D_BF[i][j][n][k]);

          sprintf(htitle, "Eta lead D BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Lead_D_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Lead_D_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Lead_D_BF[i][j][n][k]);

          sprintf(htitle, "Eta sub D BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Sub_D_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Sub_D_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Sub_D_BF[i][j][n][k]);

	  sprintf(htitle, "Eta pair D Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Pair_D_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Pair_D[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Pair_D[i][j][n][k]);

          sprintf(htitle, "Eta lead D Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Lead_D_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Lead_D[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Lead_D[i][j][n][k]);

          sprintf(htitle, "Eta sub D Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Sub_D_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Sub_D[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Sub_D[i][j][n][k]);

	  sprintf(htitle, "Phi pair D BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Pair_D_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Pair_D_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Pair_D_BF[i][j][n][k]);

          sprintf(htitle, "Phi lead D BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Lead_D_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Lead_D_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Lead_D_BF[i][j][n][k]);

          sprintf(htitle, "Phi sub D BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Sub_D_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Sub_D_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Sub_D_BF[i][j][n][k]);

	  sprintf(htitle, "Phi pair D Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Pair_D_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Pair_D[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Pair_D[i][j][n][k]);

          sprintf(htitle, "Phi lead D Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Lead_D_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Lead_D[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Lead_D[i][j][n][k]);

          sprintf(htitle, "Phi sub D Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Sub_D_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Sub_D[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Sub_D[i][j][n][k]);

	  sprintf(htitle, "Delta Phi Fir Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_DeltaPhi_Fir_%i_%i_%i_%i",i,j,n,k);
          h_DeltaPhi_Fir[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
          wk()->addOutput (h_DeltaPhi_Fir[i][j][n][k]);

	  sprintf(htitle, "Delta Phi Fir BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_DeltaPhi_Fir_BF_%i_%i_%i_%i",i,j,n,k);
          h_DeltaPhi_Fir_BF[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
          wk()->addOutput (h_DeltaPhi_Fir_BF[i][j][n][k]);

	  sprintf(htitle, "Rapidity Inc Fir Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonRap_Inc_Fir_%i_%i_%i_%i",i,j,n,k);
	  h_MuonRap_Inc_Fir[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonRap_Inc_Fir[i][j][n][k]);	                        

	  sprintf(htitle, "Eta Inc Fir Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonEta_Inc_Fir_%i_%i_%i_%i",i,j,n,k);
	  h_MuonEta_Inc_Fir[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonEta_Inc_Fir[i][j][n][k]);
	  
	  sprintf(htitle, "Phi Inc Fir Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonPhi_Inc_Fir_%i_%i_%i_%i",i,j,n,k);
	  h_MuonPhi_Inc_Fir[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
	  wk()->addOutput (h_MuonPhi_Inc_Fir[i][j][n][k]);
	  
	  sprintf(htitle, "Rapidity Inc Fir BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonRap_Inc_Fir_BF_%i_%i_%i_%i",i,j,n,k);
	  h_MuonRap_Inc_Fir_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonRap_Inc_Fir_BF[i][j][n][k]);	                        

	  sprintf(htitle, "Eta Inc Fir BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonEta_Inc_Fir_BF_%i_%i_%i_%i",i,j,n,k);
	  h_MuonEta_Inc_Fir_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonEta_Inc_Fir_BF[i][j][n][k]);

	  sprintf(htitle, "Phi Inc Fir BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonPhi_Inc_Fir_BF_%i_%i_%i_%i",i,j,n,k);
	  h_MuonPhi_Inc_Fir_BF[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
	  wk()->addOutput (h_MuonPhi_Inc_Fir_BF[i][j][n][k]);

	  sprintf(htitle, "Eta pair Fir BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Pair_Fir_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Pair_Fir_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Pair_Fir_BF[i][j][n][k]);

          sprintf(htitle, "Eta lead Fir BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Lead_Fir_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Lead_Fir_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Lead_Fir_BF[i][j][n][k]);

          sprintf(htitle, "Eta sub Fir BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Sub_Fir_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Sub_Fir_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Sub_Fir_BF[i][j][n][k]);

	  sprintf(htitle, "Eta pair Fir Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Pair_Fir_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Pair_Fir[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Pair_Fir[i][j][n][k]);

          sprintf(htitle, "Eta lead Fir Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Lead_Fir_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Lead_Fir[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Lead_Fir[i][j][n][k]);

          sprintf(htitle, "Eta sub Fir Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Sub_Fir_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Sub_Fir[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Sub_Fir[i][j][n][k]);

	  sprintf(htitle, "Phi pair Fir BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Pair_Fir_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Pair_Fir_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Pair_Fir_BF[i][j][n][k]);

          sprintf(htitle, "Phi lead Fir BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Lead_Fir_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Lead_Fir_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Lead_Fir_BF[i][j][n][k]);

          sprintf(htitle, "Phi sub Fir BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Sub_Fir_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Sub_Fir_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Sub_Fir_BF[i][j][n][k]);

	  sprintf(htitle, "Phi pair Fir Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Pair_Fir_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Pair_Fir[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Pair_Fir[i][j][n][k]);

          sprintf(htitle, "Phi lead Fir Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Lead_Fir_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Lead_Fir[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Lead_Fir[i][j][n][k]);

          sprintf(htitle, "Phi sub Fir Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Sub_Fir_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Sub_Fir[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Sub_Fir[i][j][n][k]);

	  sprintf(htitle, "Delta Phi Sec Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_DeltaPhi_Sec_%i_%i_%i_%i",i,j,n,k);
          h_DeltaPhi_Sec[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
          wk()->addOutput (h_DeltaPhi_Sec[i][j][n][k]);

	  sprintf(htitle, "Delta Phi Sec BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_DeltaPhi_Sec_BF_%i_%i_%i_%i",i,j,n,k);
          h_DeltaPhi_Sec_BF[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
          wk()->addOutput (h_DeltaPhi_Sec_BF[i][j][n][k]);

	  sprintf(htitle, "Rapidity Inc Sec Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonRap_Inc_Sec_%i_%i_%i_%i",i,j,n,k);
	  h_MuonRap_Inc_Sec[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonRap_Inc_Sec[i][j][n][k]);	                        

	  sprintf(htitle, "Eta Inc Sec Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonEta_Inc_Sec_%i_%i_%i_%i",i,j,n,k);
	  h_MuonEta_Inc_Sec[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonEta_Inc_Sec[i][j][n][k]);
	  
	  sprintf(htitle, "Phi Inc Sec Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonPhi_Inc_Sec_%i_%i_%i_%i",i,j,n,k);
	  h_MuonPhi_Inc_Sec[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
	  wk()->addOutput (h_MuonPhi_Inc_Sec[i][j][n][k]);
	  
	  sprintf(htitle, "Rapidity Inc Sec BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonRap_Inc_Sec_BF_%i_%i_%i_%i",i,j,n,k);
	  h_MuonRap_Inc_Sec_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonRap_Inc_Sec_BF[i][j][n][k]);	                        

	  sprintf(htitle, "Eta Inc Sec BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonEta_Inc_Sec_BF_%i_%i_%i_%i",i,j,n,k);
	  h_MuonEta_Inc_Sec_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonEta_Inc_Sec_BF[i][j][n][k]);

	  sprintf(htitle, "Phi Inc Sec BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonPhi_Inc_Sec_BF_%i_%i_%i_%i",i,j,n,k);
	  h_MuonPhi_Inc_Sec_BF[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
	  wk()->addOutput (h_MuonPhi_Inc_Sec_BF[i][j][n][k]);

	  sprintf(htitle, "Eta pair Sec BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Pair_Sec_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Pair_Sec_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Pair_Sec_BF[i][j][n][k]);

          sprintf(htitle, "Eta lead Sec BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Lead_Sec_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Lead_Sec_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Lead_Sec_BF[i][j][n][k]);

          sprintf(htitle, "Eta sub Sec BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Sub_Sec_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Sub_Sec_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Sub_Sec_BF[i][j][n][k]);

	  sprintf(htitle, "Eta pair Sec Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Pair_Sec_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Pair_Sec[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Pair_Sec[i][j][n][k]);

          sprintf(htitle, "Eta lead Sec Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Lead_Sec_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Lead_Sec[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Lead_Sec[i][j][n][k]);

          sprintf(htitle, "Eta sub Sec Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Sub_Sec_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Sub_Sec[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Sub_Sec[i][j][n][k]);

	  sprintf(htitle, "Phi pair Sec BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Pair_Sec_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Pair_Sec_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Pair_Sec_BF[i][j][n][k]);

          sprintf(htitle, "Phi lead Sec BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Lead_Sec_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Lead_Sec_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Lead_Sec_BF[i][j][n][k]);

          sprintf(htitle, "Phi sub Sec BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Sub_Sec_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Sub_Sec_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Sub_Sec_BF[i][j][n][k]);

	  sprintf(htitle, "Phi pair Sec Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Pair_Sec_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Pair_Sec[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Pair_Sec[i][j][n][k]);

          sprintf(htitle, "Phi lead Sec Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Lead_Sec_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Lead_Sec[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Lead_Sec[i][j][n][k]);

          sprintf(htitle, "Phi sub Sec Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Sub_Sec_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Sub_Sec[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Sub_Sec[i][j][n][k]);

	  sprintf(htitle, "Delta Phi Trd Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_DeltaPhi_Trd_%i_%i_%i_%i",i,j,n,k);
          h_DeltaPhi_Trd[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
          wk()->addOutput (h_DeltaPhi_Trd[i][j][n][k]);

	  sprintf(htitle, "Delta Phi Trd BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_DeltaPhi_Trd_BF_%i_%i_%i_%i",i,j,n,k);
          h_DeltaPhi_Trd_BF[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
          wk()->addOutput (h_DeltaPhi_Trd_BF[i][j][n][k]);

	  sprintf(htitle, "Rapidity Inc Trd Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonRap_Inc_Trd_%i_%i_%i_%i",i,j,n,k);
	  h_MuonRap_Inc_Trd[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonRap_Inc_Trd[i][j][n][k]);	                        

	  sprintf(htitle, "Eta Inc Trd Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonEta_Inc_Trd_%i_%i_%i_%i",i,j,n,k);
	  h_MuonEta_Inc_Trd[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonEta_Inc_Trd[i][j][n][k]);
	  
	  sprintf(htitle, "Phi Inc Trd Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonPhi_Inc_Trd_%i_%i_%i_%i",i,j,n,k);
	  h_MuonPhi_Inc_Trd[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
	  wk()->addOutput (h_MuonPhi_Inc_Trd[i][j][n][k]);
	  
	  sprintf(htitle, "Rapidity Inc Trd BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonRap_Inc_Trd_BF_%i_%i_%i_%i",i,j,n,k);
	  h_MuonRap_Inc_Trd_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonRap_Inc_Trd_BF[i][j][n][k]);	                        

	  sprintf(htitle, "Eta Inc Trd BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonEta_Inc_Trd_BF_%i_%i_%i_%i",i,j,n,k);
	  h_MuonEta_Inc_Trd_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonEta_Inc_Trd_BF[i][j][n][k]);

	  sprintf(htitle, "Phi Inc Trd BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonPhi_Inc_Trd_BF_%i_%i_%i_%i",i,j,n,k);
	  h_MuonPhi_Inc_Trd_BF[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
	  wk()->addOutput (h_MuonPhi_Inc_Trd_BF[i][j][n][k]);

	  sprintf(htitle, "Eta pair Trd BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Pair_Trd_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Pair_Trd_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Pair_Trd_BF[i][j][n][k]);

          sprintf(htitle, "Eta lead Trd BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Lead_Trd_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Lead_Trd_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Lead_Trd_BF[i][j][n][k]);

          sprintf(htitle, "Eta sub Trd BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Sub_Trd_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Sub_Trd_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Sub_Trd_BF[i][j][n][k]);

	  sprintf(htitle, "Eta pair Trd Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Pair_Trd_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Pair_Trd[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Pair_Trd[i][j][n][k]);

          sprintf(htitle, "Eta lead Trd Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Lead_Trd_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Lead_Trd[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Lead_Trd[i][j][n][k]);

          sprintf(htitle, "Eta sub Trd Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Sub_Trd_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Sub_Trd[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Sub_Trd[i][j][n][k]);

	  sprintf(htitle, "Phi pair Trd BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Pair_Trd_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Pair_Trd_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Pair_Trd_BF[i][j][n][k]);

          sprintf(htitle, "Phi lead Trd BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Lead_Trd_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Lead_Trd_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Lead_Trd_BF[i][j][n][k]);

          sprintf(htitle, "Phi sub Trd BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Sub_Trd_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Sub_Trd_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Sub_Trd_BF[i][j][n][k]);

	  sprintf(htitle, "Phi pair Trd Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Pair_Trd_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Pair_Trd[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Pair_Trd[i][j][n][k]);

          sprintf(htitle, "Phi lead Trd Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Lead_Trd_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Lead_Trd[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Lead_Trd[i][j][n][k]);

          sprintf(htitle, "Phi sub Trd Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Sub_Trd_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Sub_Trd[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Sub_Trd[i][j][n][k]);

	  sprintf(htitle, "Delta Phi Fou Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_DeltaPhi_Fou_%i_%i_%i_%i",i,j,n,k);
          h_DeltaPhi_Fou[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
          wk()->addOutput (h_DeltaPhi_Fou[i][j][n][k]);

	  sprintf(htitle, "Delta Phi Fou BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_DeltaPhi_Fou_BF_%i_%i_%i_%i",i,j,n,k);
          h_DeltaPhi_Fou_BF[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
          wk()->addOutput (h_DeltaPhi_Fou_BF[i][j][n][k]);

	  sprintf(htitle, "Rapidity Inc Fou Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonRap_Inc_Fou_%i_%i_%i_%i",i,j,n,k);
	  h_MuonRap_Inc_Fou[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonRap_Inc_Fou[i][j][n][k]);	                        

	  sprintf(htitle, "Eta Inc Fou Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonEta_Inc_Fou_%i_%i_%i_%i",i,j,n,k);
	  h_MuonEta_Inc_Fou[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonEta_Inc_Fou[i][j][n][k]);
	  
	  sprintf(htitle, "Phi Inc Fou Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonPhi_Inc_Fou_%i_%i_%i_%i",i,j,n,k);
	  h_MuonPhi_Inc_Fou[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
	  wk()->addOutput (h_MuonPhi_Inc_Fou[i][j][n][k]);
	  
	  sprintf(htitle, "Rapidity Inc Fou BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonRap_Inc_Fou_BF_%i_%i_%i_%i",i,j,n,k);
	  h_MuonRap_Inc_Fou_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonRap_Inc_Fou_BF[i][j][n][k]);	                        

	  sprintf(htitle, "Eta Inc Fou BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonEta_Inc_Fou_BF_%i_%i_%i_%i",i,j,n,k);
	  h_MuonEta_Inc_Fou_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
	  wk()->addOutput (h_MuonEta_Inc_Fou_BF[i][j][n][k]);

	  sprintf(htitle, "Phi Inc Fou BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_MuonPhi_Inc_Fou_BF_%i_%i_%i_%i",i,j,n,k);
	  h_MuonPhi_Inc_Fou_BF[i][j][n][k] = new TH1D(histname, htitle, 80, -4., 4.);
	  wk()->addOutput (h_MuonPhi_Inc_Fou_BF[i][j][n][k]);

	  sprintf(htitle, "Eta pair Fou BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Pair_Fou_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Pair_Fou_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Pair_Fou_BF[i][j][n][k]);

          sprintf(htitle, "Eta lead Fou BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Lead_Fou_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Lead_Fou_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Lead_Fou_BF[i][j][n][k]);

          sprintf(htitle, "Eta sub Fou BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Sub_Fou_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Sub_Fou_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Sub_Fou_BF[i][j][n][k]);

	  sprintf(htitle, "Eta pair Fou Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Pair_Fou_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Pair_Fou[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Pair_Fou[i][j][n][k]);

          sprintf(htitle, "Eta lead Fou Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Lead_Fou_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Lead_Fou[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Lead_Fou[i][j][n][k]);

          sprintf(htitle, "Eta sub Fou Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonEta_Sub_Fou_%i_%i_%i_%i",i,j,n,k);
          h_MuonEta_Sub_Fou[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonEta_Sub_Fou[i][j][n][k]);

	  sprintf(htitle, "Phi pair Fou BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Pair_Fou_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Pair_Fou_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Pair_Fou_BF[i][j][n][k]);

          sprintf(htitle, "Phi lead Fou BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Lead_Fou_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Lead_Fou_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Lead_Fou_BF[i][j][n][k]);

          sprintf(htitle, "Phi sub Fou BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Sub_Fou_BF_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Sub_Fou_BF[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Sub_Fou_BF[i][j][n][k]);

	  sprintf(htitle, "Phi pair Fou Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Pair_Fou_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Pair_Fou[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Pair_Fou[i][j][n][k]);

          sprintf(htitle, "Phi lead Fou Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Lead_Fou_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Lead_Fou[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Lead_Fou[i][j][n][k]);

          sprintf(htitle, "Phi sub Fou Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_MuonPhi_Sub_Fou_%i_%i_%i_%i",i,j,n,k);
          h_MuonPhi_Sub_Fou[i][j][n][k] = new TH1D(histname, htitle, 52, -2.5, 2.5);
          wk()->addOutput (h_MuonPhi_Sub_Fou[i][j][n][k]);

          sprintf(htitle, "CutFlow Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_CutFlow_%i_%i_%i_%i",i,j,n,k);
          h_CutFlow[i][j][n][k] = new TH1D(histname, htitle, 25, 0., 25.);
          wk()->addOutput (h_CutFlow[i][j][n][k]);

          sprintf(htitle, "d0_mu1 Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_d0_mu1_%i_%i_%i_%i",i,j,n,k);
          h_d0_mu1[i][j][n][k] = new TH1D(histname, htitle, 100, -0.1, 0.1);
          wk()->addOutput (h_d0_mu1[i][j][n][k]);

	  sprintf(htitle, "d0_mu1mu2 Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_d0_mu1mu2_%i_%i_%i_%i",i,j,n,k);
          h_d0_mu1mu2[i][j][n][k] = new TH2D(histname, htitle, 100, -0.1, 0.1, 100, -0.1, 0.1);
          wk()->addOutput (h_d0_mu1mu2[i][j][n][k]);

	  sprintf(htitle, "delta_z0_d0_significance Sign=%i Iso=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_delta_z0_d0_significance_%i_%i_%i_%i",i,j,n,k);
	  h_delta_z0_d0_significance[i][j][n][k] = new TH2D(histname, htitle, 50, 0., 10., 50, 0., 10.);
	  wk()->addOutput (h_delta_z0_d0_significance[i][j][n][k]);

	  sprintf(htitle, "d0_significance BOTH Sign=%i Iso=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_d0_significance_BOTH_%i_%i_%i_%i",i,j,n,k);
	  h_d0_significance_BOTH[i][j][n][k] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_BOTH[i][j][n][k]);
 
          sprintf(htitle, "z0_mu1 Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_z0_mu1_%i_%i_%i_%i",i,j,n,k);
          h_z0_mu1[i][j][n][k] = new TH1D(histname, htitle, 100, -200., 200.);
          wk()->addOutput (h_z0_mu1[i][j][n][k]);

	  sprintf(htitle, "delta_z0 Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
	  sprintf(histname, "h_delta_z0_%i_%i_%i_%i",i,j,n,k);
	  h_delta_z0[i][j][n][k] = new TH1D(histname, htitle, 100, -1.25, 1.25);
	  wk()->addOutput (h_delta_z0[i][j][n][k]);
      
	  sprintf(htitle, "d0_mu1 BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_d0_mu1_BF_%i_%i_%i_%i",i,j,n,k);
          h_d0_mu1_BF[i][j][n][k] = new TH1D(histname, htitle, 100, -0.1, 0.1);
          wk()->addOutput (h_d0_mu1_BF[i][j][n][k]);

          sprintf(htitle, "z0_mu1 BF Sign=%i ISO=%i MuQ=%i Trig=%i",i,j,n,k);
          sprintf(histname, "h_z0_mu1_BF_%i_%i_%i_%i",i,j,n,k);
          h_z0_mu1_BF[i][j][n][k] = new TH1D(histname, htitle, 100, -200., 200.);
          wk()->addOutput (h_z0_mu1_BF[i][j][n][k]);

	}
      }
    }
  }

  //Acceptance/Purity/Stability plot A                                             
  for(int j = 0; j < 2; j++) {        //1D acceptance - purity - stability studies                                                         
    for(int k = 0; k < 3; k++) {        //1D acceptance - purity - stability studies                                                         
   
      //sprintf(htitle, "Muon Invariant Mass A Trig=%i",k);
      //sprintf(histname, "h_Invariant_Mass_A_%i",k);
      //h_Invariant_Mass_A[k] = new TH1D(histname, htitle, mass_binnum_A, mass_bins_A);
      //wk()->addOutput (h_Invariant_Mass_A[k]);
      
      //sprintf(htitle, "Muon Invariant Mass B Trig=%i",k);
      //sprintf(histname, "h_Invariant_Mass_B_%i",k);
      //h_Invariant_Mass_B[k] = new TH1D(histname, htitle, mass_binnum_B, mass_bins_B);
      //wk()->addOutput (h_Invariant_Mass_B[k]);
      
      /*sprintf(htitle, "Muon Invariant Mass RECO A Trig=%i",k);
	sprintf(histname, "h_Invariant_Mass_RECO_A_%i",k);
	h_Invariant_Mass_RECO_A[k] = new TH1D(histname, htitle, mass_binnum_A, mass_bins_A);
	wk()->addOutput (h_Invariant_Mass_RECO_A[k]);
	
	sprintf(htitle, "Muon Invariant Mass BOTH A Trig=%i",k);
	sprintf(histname, "h_Invariant_Mass_BOTH_A_%i",k);
	h_Invariant_Mass_BOTH_A[k] = new TH1D(histname, htitle, mass_binnum_A, mass_bins_A);
	wk()->addOutput (h_Invariant_Mass_BOTH_A[k]);*/
      
      sprintf(htitle, "Muon Invariant Mass RECO B Sign=%i Trig=%i",j,k);
      sprintf(histname, "h_Invariant_Mass_RECO_B_%i_%i",j,k);
      h_Invariant_Mass_RECO_B[j][k] = new TH1D(histname, htitle, mass_binnum_B, mass_bins_B);
      wk()->addOutput (h_Invariant_Mass_RECO_B[j][k]);
      
      sprintf(htitle, "Muon Invariant Mass BOTH B Sign=%i Trig=%i",j,k);
      sprintf(histname, "h_Invariant_Mass_BOTH_B_%i_%i",j,k);    
      h_Invariant_Mass_BOTH_B[j][k] = new TH1D(histname, htitle, mass_binnum_B, mass_bins_B);
      wk()->addOutput (h_Invariant_Mass_BOTH_B[j][k]);
      
      /*sprintf(htitle, "Muon Invariant Mass TRUTH A Sign=%i Trig=%i",j,k);
	sprintf(histname, "h_Invariant_Mass_TRUTH_A_%i_%i",j,k);
	h_Invariant_Mass_TRUTH_A[j][k] = new TH1D(histname, htitle, mass_binnum_A, mass_bins_A);
	wk()->addOutput (h_Invariant_Mass_TRUTH_A[j][k]);*/

      sprintf(htitle, "Muon Invariant Mass TRUTH B Sign=%i Trig=%i",j,k);
      sprintf(histname, "h_Invariant_Mass_TRUTH_B_%i_%i",j,k);
      h_Invariant_Mass_TRUTH_B[j][k] = new TH1D(histname, htitle, mass_binnum_B, mass_bins_B);
      wk()->addOutput (h_Invariant_Mass_TRUTH_B[j][k]);

      sprintf(htitle, "Muon Invariant Mass Z Sign=%i Trig=%i",j,k);
      sprintf(histname, "h_Invariant_Mass_Z_%i_%i",j,k);
      h_Invariant_Mass_Z[j][k] = new TH1D(histname, htitle, 25, 66, 116);
      wk()->addOutput (h_Invariant_Mass_Z[j][k]);

      /*sprintf(htitle, "Muon Invariant Mass Res TRUTH A Sign=%i Trig=%i",j,k);
	sprintf(histname, "h_Invariant_Mass_Res_TRUTH_A_%i_%i",j,k);
	h_Invariant_Mass_Res_TRUTH_A[j][k] = new TH2D(histname, htitle, mass_binnum_A, mass_bins_A, 40, -400., 400.);
	wk()->addOutput (h_Invariant_Mass_Res_TRUTH_A[j][k]);*/

      sprintf(htitle, "Muon Invariant Mass Res TRUTH B Sign=%i Trig=%i",j,k);
      sprintf(histname, "h_Invariant_Mass_Res_TRUTH_B_%i_%i",j,k);
      h_Invariant_Mass_Res_TRUTH_B[j][k] = new TH2D(histname, htitle, mass_binnum_B, mass_bins_B, 40, -400., 400.);
      wk()->addOutput (h_Invariant_Mass_Res_TRUTH_B[j][k]);

      /*sprintf(htitle, "Muon Invariant Mass Res RECO A Sign=%i Trig=%i",j,k);
	sprintf(histname, "h_Invariant_Mass_Res_RECO_A_%i_%i",j,k);
	h_Invariant_Mass_Res_RECO_A[j][k] = new TH1D(histname, htitle, mass_binnum_A, mass_bins_A);
	wk()->addOutput (h_Invariant_Mass_Res_RECO_A[j][k]);*/

      sprintf(htitle, "Muon Invariant Mass Res RECO B Sign=%i Trig=%i",j,k);
      sprintf(histname, "h_Invariant_Mass_Res_RECO_B_%i_%i",j,k);
      h_Invariant_Mass_Res_RECO_B[j][k] = new TH1D(histname, htitle, mass_binnum_B, mass_bins_B);
      wk()->addOutput (h_Invariant_Mass_Res_RECO_B[j][k]);

    }
  }  

  for(int j = 0; j < 2; j++) {
    for(int k = 0; k < 8; k++) {        //2D acceptance - purity - stability studies
      for(int i = 0; i < 3; i++) {
	
	sprintf(htitle, "Muon Rapidity TRUTH Sign=%i Bin=%i Trig=%i",j,k,i);
	sprintf(histname, "h_Muon_Rapidity_TRUTH_%i_%i_%i",j,k,i);
	h_Muon_Rapidity_TRUTH[j][k][i] = new TH1D(histname, htitle, 6, 0., 2.4);
	wk()->addOutput (h_Muon_Rapidity_TRUTH[j][k][i]);
      
	sprintf(htitle, "Muon Rapidity RECO Sign=%i Bin=%i Trig=%i",j,k,i);
	sprintf(histname, "h_Muon_Rapidity_RECO_%i_%i_%i",j,k,i);
	h_Muon_Rapidity_RECO[j][k][i] = new TH1D(histname, htitle, 6, 0., 2.4);
	wk()->addOutput (h_Muon_Rapidity_RECO[j][k][i]);

	sprintf(htitle, "Muon Rapidity Res TRUTH Sign=%i Bin=%i Trig=%i",j,k,i);
	sprintf(histname, "h_Muon_Rapidity_Res_TRUTH_%i_%i_%i",j,k,i);
	h_Muon_Rapidity_Res_TRUTH[j][k][i] = new TH2D(histname, htitle, 12, 0., 2.4, 40, -0.2, 0.2);
	wk()->addOutput (h_Muon_Rapidity_Res_TRUTH[j][k][i]);
      
	sprintf(htitle, "Muon Rapidity Res RECO Sign=%i Bin=%i Trig=%i",j,k,i);
	sprintf(histname, "h_Muon_Rapidity_Res_RECO_%i_%i_%i",j,k,i);
	h_Muon_Rapidity_Res_RECO[j][k][i] = new TH1D(histname, htitle, 12, 0., 2.4);
	wk()->addOutput (h_Muon_Rapidity_Res_RECO[j][k][i]);
      
	sprintf(htitle, "Muon Rapidity BOTH Sign=%i Bin=%i Trig=%i",j,k,i);
	sprintf(histname, "h_Muon_Rapidity_BOTH_%i_%i_%i",j,k,i);
	h_Muon_Rapidity_BOTH[j][k][i] = new TH1D(histname, htitle, 6, 0., 2.4);
	wk()->addOutput (h_Muon_Rapidity_BOTH[j][k][i]);

	sprintf(htitle, "Muon Invariant Mass Diff B Sign=%i Bin=%i Trig=%i",j,k,i);
	sprintf(histname, "h_Invariant_Mass_Diff_B_%i_%i_%i",j,k,i);
	h_Invariant_Mass_Diff_B[j][k][i] = new TH1D(histname, htitle, 100, -10., 10.);
	wk()->addOutput (h_Invariant_Mass_Diff_B[j][k][i]);

	//sprintf(htitle, "Muon Rapidity Dimuon Sign=%i Bin=%i Trig=%i",j,k,i);
	//sprintf(histname, "h_Muon_Rapidity_Dimuon_%i_%i_%i",j,k,i);
	//h_Muon_Rapidity_Dimuon[j][k][i] = new TH1D(histname, htitle, 12, 0., 2.4);
	//wk()->addOutput (h_Muon_Rapidity_Dimuon[j][k][i]);

      }      
    }
  }

  //Acceptance/Purity/Stability plot A                                             
  for(int j = 0; j < 2; j++) {
    for(int k = 0; k < 3; k++) {        //1D acceptance - purity - stability studies                                                         
   
      //sprintf(htitle, "Muon Invariant Mass A Trig=%i",k);
      //sprintf(histname, "h_Invariant_Mass_A_%i",k);
      //h_Invariant_Mass_A[j][k] = new TH1D(histname, htitle, mass_binnum_A, mass_bins_A);
      //wk()->addOutput (h_Invariant_Mass_A[j][k]);

      //sprintf(htitle, "Muon Invariant Mass B Trig=%i",k);
      //sprintf(histname, "h_Invariant_Mass_B_%i",k);
      //h_Invariant_Mass_B[j][k] = new TH1D(histname, htitle, mass_binnum_B, mass_bins_B);
      //wk()->addOutput (h_Invariant_Mass_B[j][k]);
    
      /*sprintf(htitle, "Muon Invariant Mass RECO_BARE A Trig=%i",k);
	sprintf(histname, "h_Invariant_Mass_RECO_BARE_A_%i",k);
	h_Invariant_Mass_RECO_BARE_A[j][k] = new TH1D(histname, htitle, mass_binnum_A, mass_bins_A);
	wk()->addOutput (h_Invariant_Mass_RECO_BARE_A[j][k]);

	sprintf(htitle, "Muon Invariant Mass BOTH_BARE A Trig=%i",k);
	sprintf(histname, "h_Invariant_Mass_BOTH_BARE_A_%i",k);
	h_Invariant_Mass_BOTH_BARE_A[j][k] = new TH1D(histname, htitle, mass_binnum_A, mass_bins_A);
	wk()->addOutput (h_Invariant_Mass_BOTH_BARE_A[j][k]);*/
    
      sprintf(htitle, "Muon Invariant Mass RECO_BARE B Sign=%i Trig=%i",j,k);
      sprintf(histname, "h_Invariant_Mass_RECO_BARE_B_%i_%i",j,k);
      h_Invariant_Mass_RECO_BARE_B[j][k] = new TH1D(histname, htitle, mass_binnum_B, mass_bins_B);
      wk()->addOutput (h_Invariant_Mass_RECO_BARE_B[j][k]);
    
      sprintf(htitle, "Muon Invariant Mass BOTH_BARE B Sign=%i Trig=%i",j,k);
      sprintf(histname, "h_Invariant_Mass_BOTH_BARE_B_%i_%i",j,k);    
      h_Invariant_Mass_BOTH_BARE_B[j][k] = new TH1D(histname, htitle, mass_binnum_B, mass_bins_B);
      wk()->addOutput (h_Invariant_Mass_BOTH_BARE_B[j][k]);

      /*sprintf(htitle, "Muon Invariant Mass TRUTH_BARE A Sign=%i Trig=%i",j,k);
	sprintf(histname, "h_Invariant_Mass_TRUTH_BARE_A_%i_%i",j,k);
	h_Invariant_Mass_TRUTH_BARE_A[j][k] = new TH1D(histname, htitle, mass_binnum_A, mass_bins_A);
	wk()->addOutput (h_Invariant_Mass_TRUTH_BARE_A[j][k]);*/

      sprintf(htitle, "Muon Invariant Mass TRUTH_BARE B Sign=%i Trig=%i",j,k);
      sprintf(histname, "h_Invariant_Mass_TRUTH_BARE_B_%i_%i",j,k);
      h_Invariant_Mass_TRUTH_BARE_B[j][k] = new TH1D(histname, htitle, mass_binnum_B, mass_bins_B);
      wk()->addOutput (h_Invariant_Mass_TRUTH_BARE_B[j][k]);

      /*sprintf(htitle, "Muon Invariant Mass Res TRUTH_BARE A Sign=%i Trig=%i",j,k);
	sprintf(histname, "h_Invariant_Mass_Res_TRUTH_BARE_A_%i_%i",j,k);
	h_Invariant_Mass_Res_TRUTH_BARE_A[j][k] = new TH2D(histname, htitle, mass_binnum_A, mass_bins_A, 40, -400., 400.);
	wk()->addOutput (h_Invariant_Mass_Res_TRUTH_BARE_A[j][k]);*/

      sprintf(htitle, "Muon Invariant Mass Res TRUTH_BARE B Sign=%i Trig=%i",j,k);
      sprintf(histname, "h_Invariant_Mass_Res_TRUTH_BARE_B_%i_%i",j,k);
      h_Invariant_Mass_Res_TRUTH_BARE_B[j][k] = new TH2D(histname, htitle, mass_binnum_B, mass_bins_B, 40, -400., 400.);
      wk()->addOutput (h_Invariant_Mass_Res_TRUTH_BARE_B[j][k]);

      /*sprintf(htitle, "Muon Invariant Mass Res RECO_BARE A Sign=%i Trig=%i",j,k);
	sprintf(histname, "h_Invariant_Mass_Res_RECO_BARE_A_%i_%i",j,k);
	h_Invariant_Mass_Res_RECO_BARE_A[j][k] = new TH1D(histname, htitle, mass_binnum_A, mass_bins_A);
	wk()->addOutput (h_Invariant_Mass_Res_RECO_BARE_A[j][k]);*/

      sprintf(htitle, "Muon Invariant Mass Res RECO_BARE B Sign=%i Trig=%i",j,k);
      sprintf(histname, "h_Invariant_Mass_Res_RECO_BARE_B_%i_%i",j,k);
      h_Invariant_Mass_Res_RECO_BARE_B[j][k] = new TH1D(histname, htitle, mass_binnum_B, mass_bins_B);
      wk()->addOutput (h_Invariant_Mass_Res_RECO_BARE_B[j][k]);
    }
  }  

  for(int j = 0; j < 2; j++) {
    for(int k = 0; k < 8; k++) {        //2D acceptance - purity - stability studies    
      for(int i = 0; i < 3; i++) {
       
      sprintf(htitle, "Muon Rapidity TRUTH_BARE Sign=%i Bin=%i Trig=%i",j,k,i);
      sprintf(histname, "h_Muon_Rapidity_TRUTH_BARE_%i_%i_%i",j,k,i);
      h_Muon_Rapidity_TRUTH_BARE[j][k][i] = new TH1D(histname, htitle, 12, 0., 2.4);
      wk()->addOutput (h_Muon_Rapidity_TRUTH_BARE[j][k][i]);
      
      sprintf(htitle, "Muon Rapidity RECO_BARE Sign=%i Bin=%i Trig=%i",j,k,i);
      sprintf(histname, "h_Muon_Rapidity_RECO_BARE_%i_%i_%i",j,k,i);
      h_Muon_Rapidity_RECO_BARE[j][k][i] = new TH1D(histname, htitle, 12, 0., 2.4);
      wk()->addOutput (h_Muon_Rapidity_RECO_BARE[j][k][i]);

      sprintf(htitle, "Muon Rapidity Res TRUTH_BARE Sign=%i Bin=%i Trig=%i",j,k,i);
      sprintf(histname, "h_Muon_Rapidity_Res_TRUTH_BARE_%i_%i_%i",j,k,i);
      h_Muon_Rapidity_Res_TRUTH_BARE[j][k][i] = new TH2D(histname, htitle, 12, 0., 2.4, 40, -0.2, 0.2);
      wk()->addOutput (h_Muon_Rapidity_Res_TRUTH_BARE[j][k][i]);
      
      sprintf(htitle, "Muon Rapidity Res RECO_BARE Sign=%i Bin=%i Trig=%i",j,k,i);
      sprintf(histname, "h_Muon_Rapidity_Res_RECO_BARE_%i_%i_%i",j,k,i);
      h_Muon_Rapidity_Res_RECO_BARE[j][k][i] = new TH1D(histname, htitle, 12, 0., 2.4);
      wk()->addOutput (h_Muon_Rapidity_Res_RECO_BARE[j][k][i]);
      
      sprintf(htitle, "Muon Rapidity BOTH_BARE Sign=%i Bin=%i Trig=%i",j,k,i);
      sprintf(histname, "h_Muon_Rapidity_BOTH_BARE_%i_%i_%i",j,k,i);
      h_Muon_Rapidity_BOTH_BARE[j][k][i] = new TH1D(histname, htitle, 12, 0., 2.4);
      wk()->addOutput (h_Muon_Rapidity_BOTH_BARE[j][k][i]);

      sprintf(htitle, "Muon Invariant Mass Diff BARE B Sign=%i Bin=%i Trig=%i",j,k,i);
      sprintf(histname, "h_Invariant_Mass_Diff_BARE_B_%i_%i_%i",j,k,i);
      h_Invariant_Mass_Diff_BARE_B[j][k][i] = new TH1D(histname, htitle, 100, -10., 10.);
      wk()->addOutput (h_Invariant_Mass_Diff_BARE_B[j][k][i]);

      //sprintf(htitle, "Muon Rapidity Dimuon Bin=%i Trig=%i",j,k,i);
      //sprintf(histname, "h_Muon_Rapidity_Dimuon_%i_%i_%i",j,k,i);
      //h_Muon_Rapidity_Dimuon[j][k][i] = new TH1D(histname, htitle, 12, 0., 2.4);
      //wk()->addOutput (h_Muon_Rapidity_Dimuon[j][k][i]);

      }      
    }
  }

  //Multijet studies
  for(int l = 0; l < 2; l++) {        //2D acceptance - purity - stability studies
    for(int k = 0; k < 4; k++) {
      for(int j = 0; j < 8; j++) {
	for(int i = 0; i < 3; i++) {

	  sprintf(htitle, "chi2 ISO Sign=%i ISO=%i MuQ=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_chi2_ISO_%i_%i_%i_%i",l,k,j,i);
	  h_chi2_ISO[l][k][j][i] = new TH1D(histname, htitle, 150, 0., 30.);
	  wk()->addOutput (h_chi2_ISO[l][k][j][i]);
 
	  sprintf(htitle, "chi2 Prob ISO Sign=%i ISO=%i MuQ=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_chi2_Prob_ISO_%i_%i_%i_%i",l,k,j,i);
	  h_chi2_Prob_ISO[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_chi2_Prob_ISO[l][k][j][i]);
 
	  sprintf(htitle, "chi2 Prob pp ISO Sign=%i ISO=%i MuQ=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_chi2_Prob_pp_ISO_%i_%i_%i_%i",l,k,j,i);
	  h_chi2_Prob_pp_ISO[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_chi2_Prob_pp_ISO[l][k][j][i]);
 
	  sprintf(htitle, "chi2 Prob mm ISO Sign=%i ISO=%i MuQ=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_chi2_Prob_mm_ISO_%i_%i_%i_%i",l,k,j,i);
	  h_chi2_Prob_mm_ISO[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_chi2_Prob_mm_ISO[l][k][j][i]);
 
	  sprintf(htitle, "chi2 Prob pp NOISO Sign=%i ISO=%i MuQ=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_chi2_Prob_pp_NOISO_%i_%i_%i_%i",l,k,j,i);
	  h_chi2_Prob_pp_NOISO[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_chi2_Prob_pp_NOISO[l][k][j][i]);
 
	  sprintf(htitle, "chi2 Prob mm NOISO Sign=%i ISO=%i MuQ=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_chi2_Prob_mm_NOISO_%i_%i_%i_%i",l,k,j,i);
	  h_chi2_Prob_mm_NOISO[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_chi2_Prob_mm_NOISO[l][k][j][i]);
 
	  sprintf(htitle, "chi2 NOISO Sign=%i ISO=%i MuQ=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_chi2_NOISO_%i_%i_%i_%i",l,k,j,i);
	  h_chi2_NOISO[l][k][j][i] = new TH1D(histname, htitle, 150, 0., 30.);
	  wk()->addOutput (h_chi2_NOISO[l][k][j][i]);
 
	  sprintf(htitle, "chi2 Prob NOISO Sign=%i ISO=%i MuQ=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_chi2_Prob_NOISO_%i_%i_%i_%i",l,k,j,i);
	  h_chi2_Prob_NOISO[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_chi2_Prob_NOISO[l][k][j][i]);
 
	  sprintf(htitle, "chi2 HALF Sign=%i ISO=%i MuQ=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_chi2_HALF_%i_%i_%i_%i",l,k,j,i);
	  h_chi2_HALF[l][k][j][i] = new TH1D(histname, htitle, 150, 0., 30.);
	  wk()->addOutput (h_chi2_HALF[l][k][j][i]);
 
	  sprintf(htitle, "chi2 Prob HALF Sign=%i ISO=%i MuQ=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_chi2_Prob_HALF_%i_%i_%i_%i",l,k,j,i);
	  h_chi2_Prob_HALF[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_chi2_Prob_HALF[l][k][j][i]);
 
	  sprintf(htitle, "chi2 Prob noweight Sign=%i ISO=%i MuQ=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_chi2_Prob_noweight_%i_%i_%i_%i",l,k,j,i);
	  h_chi2_Prob_noweight[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_chi2_Prob_noweight[l][k][j][i]);
 
	  sprintf(htitle, "d0sig deltaz0sig Sign=%i ISO=%i MuQ=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0sig_deltaz0sig_%i_%i_%i_%i",l,k,j,i);
	  h_d0sig_deltaz0sig[l][k][j][i] = new TH2D(histname, htitle, 50, 0., 10., 50, 0., 10.);
	  wk()->addOutput (h_d0sig_deltaz0sig[l][k][j][i]);

	  sprintf(htitle, "Eff SF Lead Sign=%i ISO=%i MuQ=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_SF_Eff_Lead_%i_%i_%i_%i",l,k,j,i);
	  h_SF_Eff_Lead[l][k][j][i] = new TH1D(histname,htitle,50,.7,1.2);
	  wk()->addOutput (h_SF_Eff_Lead[l][k][j][i]);

	  sprintf(htitle, "Eff SF Sub Sign=%i ISO=%i MuQ=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_SF_Eff_Sub_%i_%i_%i_%i",l,k,j,i);
	  h_SF_Eff_Sub[l][k][j][i] = new TH1D(histname,htitle,50,.7,1.2);
	  wk()->addOutput (h_SF_Eff_Sub[l][k][j][i]);

	  sprintf(htitle, "Trig SF Lead Sign=%i ISO=%i MuQ=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_SF_Trig_Lead_%i_%i_%i_%i",l,k,j,i);
	  h_SF_Trig_Lead[l][k][j][i] = new TH1D(histname,htitle,50,.7,1.2);
	  wk()->addOutput (h_SF_Trig_Lead[l][k][j][i]);

	  sprintf(htitle, "Trig SF Sub Sign=%i ISO=%i MuQ=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_SF_Trig_Sub_%i_%i_%i_%i",l,k,j,i);
	  h_SF_Trig_Sub[l][k][j][i] = new TH1D(histname,htitle,50,.7,1.2);
	  wk()->addOutput (h_SF_Trig_Sub[l][k][j][i]);

	  sprintf(htitle, "Iso SF Lead Sign=%i ISO=%i MuQ=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_SF_Iso_Lead_%i_%i_%i_%i",l,k,j,i);
	  h_SF_Iso_Lead[l][k][j][i] = new TH1D(histname,htitle,50,.7,1.2);
	  wk()->addOutput (h_SF_Iso_Lead[l][k][j][i]);

	  sprintf(htitle, "Iso SF Sub Sign=%i ISO=%i MuQ=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_SF_Iso_Sub_%i_%i_%i_%i",l,k,j,i);
	  h_SF_Iso_Sub[l][k][j][i] = new TH1D(histname,htitle,50,.7,1.2);
	  wk()->addOutput (h_SF_Iso_Sub[l][k][j][i]);
	
	  /*sprintf(htitle, "d0_significance NOT ISO A Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_NOT_ISO_A_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_NOT_ISO_A[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_NOT_ISO_A[l][k][j][i]);

	  sprintf(htitle, "d0_significance NOT ISO B Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_NOT_ISO_B_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_NOT_ISO_B[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_NOT_ISO_B[l][k][j][i]);

	  sprintf(htitle, "d0_significance NOT ISO C Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_NOT_ISO_C_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_NOT_ISO_C[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_NOT_ISO_C[l][k][j][i]);

	  sprintf(htitle, "d0_significance NOT ISO D Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_NOT_ISO_D_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_NOT_ISO_D[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_NOT_ISO_D[l][k][j][i]);

	  sprintf(htitle, "d0_significance NOT ISO E Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_NOT_ISO_E_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_NOT_ISO_E[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_NOT_ISO_E[l][k][j][i]);

	  sprintf(htitle, "d0_significance NOT ISO F Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_NOT_ISO_F_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_NOT_ISO_F[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_NOT_ISO_F[l][k][j][i]);

	  sprintf(htitle, "d0_significance NOT ISO G Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_NOT_ISO_G_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_NOT_ISO_G[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_NOT_ISO_G[l][k][j][i]);

	  sprintf(htitle, "d0_significance NOT ISO H Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_NOT_ISO_H_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_NOT_ISO_H[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_NOT_ISO_H[l][k][j][i]);

	  sprintf(htitle, "d0_significance NOT ISO Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_NOT_ISO_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_NOT_ISO[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_NOT_ISO[l][k][j][i]);*/

	  //sprintf(htitle, "d0_significance BOTH ISO Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  //sprintf(histname, "h_d0_significance_BOTH_ISO_%i_%i_%i_%i",l,k,j,i);
	  //h_d0_significance_BOTH_ISO[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  //wk()->addOutput (h_d0_significance_BOTH_ISO[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH ISO Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_ISO_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_ISO[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_d0_significance_BOTH_ISO[l][k][j][i]);
	  
	  sprintf(htitle, "d0_significance BOTH ISO Sig Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_ISO_Sig_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_ISO_Sig[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_BOTH_ISO_Sig[l][k][j][i]);

	  sprintf(htitle, "delta_z0_significance ISO Sig Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_delta_z0_significance_ISO_Sig_%i_%i_%i_%i",l,k,j,i);
	  h_delta_z0_significance_ISO_Sig[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_delta_z0_significance_ISO_Sig[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH NOISO Sig Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_NOISO_Sig_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_NOISO_Sig[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_BOTH_NOISO_Sig[l][k][j][i]);

	  sprintf(htitle, "delta_z0_significance NOISO Sig Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_delta_z0_significance_NOISO_Sig_%i_%i_%i_%i",l,k,j,i);
	  h_delta_z0_significance_NOISO_Sig[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_delta_z0_significance_NOISO_Sig[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH HALF Sig Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_HALF_Sig_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_HALF_Sig[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_BOTH_HALF_Sig[l][k][j][i]);

	  sprintf(htitle, "delta_z0_significance HALF Sig Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_delta_z0_significance_HALF_Sig_%i_%i_%i_%i",l,k,j,i);
	  h_delta_z0_significance_HALF_Sig[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_delta_z0_significance_HALF_Sig[l][k][j][i]);

	  /*sprintf(htitle, "d0_significance BOTH ISO rapA Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_ISO_rapA_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_ISO_rapA[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_BOTH_ISO_rapA[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH ISO rapB Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_ISO_rapB_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_ISO_rapB[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_BOTH_ISO_rapB[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH ISO rapC Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_ISO_rapC_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_ISO_rapC[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_BOTH_ISO_rapC[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH ISO rapD Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_ISO_rapD_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_ISO_rapD[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_BOTH_ISO_rapD[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH ISO rapE Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_ISO_rapE_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_ISO_rapE[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_BOTH_ISO_rapE[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH ISO rapF Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_ISO_rapF_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_ISO_rapF[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_BOTH_ISO_rapF[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH NOISO Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_NOISO_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_NOISO[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_BOTH_NOISO[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH NOISO rapA Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_NOISO_rapA_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_NOISO_rapA[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_BOTH_NOISO_rapA[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH NOISO rapB Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_NOISO_rapB_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_NOISO_rapB[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_BOTH_NOISO_rapB[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH NOISO rapC Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_NOISO_rapC_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_NOISO_rapC[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_BOTH_NOISO_rapC[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH NOISO rapD Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_NOISO_rapD_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_NOISO_rapD[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_BOTH_NOISO_rapD[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH NOISO rapE Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_NOISO_rapE_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_NOISO_rapE[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_BOTH_NOISO_rapE[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH NOISO rapF Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_NOISO_rapF_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_NOISO_rapF[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_BOTH_NOISO_rapF[l][k][j][i]);
	  
	  sprintf(htitle, "d0_significance BOTH HALF Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_HALF_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_HALF[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_BOTH_HALF[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH HALF rapA Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_HALF_rapA_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_HALF_rapA[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_BOTH_HALF_rapA[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH HALF rapB Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_HALF_rapB_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_HALF_rapB[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_BOTH_HALF_rapB[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH HALF rapC Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_HALF_rapC_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_HALF_rapC[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_BOTH_HALF_rapC[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH HALF rapD Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_HALF_rapD_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_HALF_rapD[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_BOTH_HALF_rapD[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH HALF rapE Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_HALF_rapE_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_HALF_rapE[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_BOTH_HALF_rapE[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH HALF rapF Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_HALF_rapF_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_HALF_rapF[l][k][j][i] = new TH1D(histname, htitle, 50, 0., 10.);
	  wk()->addOutput (h_d0_significance_BOTH_HALF_rapF[l][k][j][i]);*/

	  sprintf(htitle, "d0_significance BOTH ISO rapA Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_ISO_rapA_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_ISO_rapA[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_d0_significance_BOTH_ISO_rapA[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH ISO rapB Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_ISO_rapB_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_ISO_rapB[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_d0_significance_BOTH_ISO_rapB[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH ISO rapC Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_ISO_rapC_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_ISO_rapC[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_d0_significance_BOTH_ISO_rapC[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH ISO rapD Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_ISO_rapD_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_ISO_rapD[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_d0_significance_BOTH_ISO_rapD[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH ISO rapE Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_ISO_rapE_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_ISO_rapE[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_d0_significance_BOTH_ISO_rapE[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH ISO rapF Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_ISO_rapF_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_ISO_rapF[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_d0_significance_BOTH_ISO_rapF[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH NOISO Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_NOISO_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_NOISO[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_d0_significance_BOTH_NOISO[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH NOISO rapA Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_NOISO_rapA_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_NOISO_rapA[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_d0_significance_BOTH_NOISO_rapA[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH NOISO rapB Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_NOISO_rapB_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_NOISO_rapB[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_d0_significance_BOTH_NOISO_rapB[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH NOISO rapC Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_NOISO_rapC_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_NOISO_rapC[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_d0_significance_BOTH_NOISO_rapC[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH NOISO rapD Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_NOISO_rapD_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_NOISO_rapD[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_d0_significance_BOTH_NOISO_rapD[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH NOISO rapE Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_NOISO_rapE_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_NOISO_rapE[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_d0_significance_BOTH_NOISO_rapE[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH NOISO rapF Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_NOISO_rapF_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_NOISO_rapF[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_d0_significance_BOTH_NOISO_rapF[l][k][j][i]);
	  
	  sprintf(htitle, "d0_significance BOTH HALF Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_HALF_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_HALF[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_d0_significance_BOTH_HALF[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH HALF rapA Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_HALF_rapA_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_HALF_rapA[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_d0_significance_BOTH_HALF_rapA[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH HALF rapB Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_HALF_rapB_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_HALF_rapB[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_d0_significance_BOTH_HALF_rapB[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH HALF rapC Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_HALF_rapC_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_HALF_rapC[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_d0_significance_BOTH_HALF_rapC[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH HALF rapD Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_HALF_rapD_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_HALF_rapD[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_d0_significance_BOTH_HALF_rapD[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH HALF rapE Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_HALF_rapE_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_HALF_rapE[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_d0_significance_BOTH_HALF_rapE[l][k][j][i]);

	  sprintf(htitle, "d0_significance BOTH HALF rapF Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_d0_significance_BOTH_HALF_rapF_%i_%i_%i_%i",l,k,j,i);
	  h_d0_significance_BOTH_HALF_rapF[l][k][j][i] = new TH1D(histname, htitle, 1000, 0., 1.);
	  wk()->addOutput (h_d0_significance_BOTH_HALF_rapF[l][k][j][i]);

	  sprintf(htitle, "SF BOTH ISO Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_SF_BOTH_ISO_%i_%i_%i_%i",l,k,j,i);
	  h_SF_BOTH_ISO[l][k][j][i] = new TH1D(histname, htitle, 100, 0.5, 1.5);
	  wk()->addOutput (h_SF_BOTH_ISO[l][k][j][i]);

	  sprintf(htitle, "SF BOTH ISO rapA Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_SF_BOTH_ISO_rapA_%i_%i_%i_%i",l,k,j,i);
	  h_SF_BOTH_ISO_rapA[l][k][j][i] = new TH1D(histname, htitle, 100, 0.5, 1.5);
	  wk()->addOutput (h_SF_BOTH_ISO_rapA[l][k][j][i]);

	  sprintf(htitle, "SF BOTH ISO rapB Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_SF_BOTH_ISO_rapB_%i_%i_%i_%i",l,k,j,i);
	  h_SF_BOTH_ISO_rapB[l][k][j][i] = new TH1D(histname, htitle, 100, 0.5, 1.5);
	  wk()->addOutput (h_SF_BOTH_ISO_rapB[l][k][j][i]);

	  sprintf(htitle, "SF BOTH ISO rapC Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_SF_BOTH_ISO_rapC_%i_%i_%i_%i",l,k,j,i);
	  h_SF_BOTH_ISO_rapC[l][k][j][i] = new TH1D(histname, htitle, 100, 0.5, 1.5);
	  wk()->addOutput (h_SF_BOTH_ISO_rapC[l][k][j][i]);

	  sprintf(htitle, "SF BOTH ISO rapD Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_SF_BOTH_ISO_rapD_%i_%i_%i_%i",l,k,j,i);
	  h_SF_BOTH_ISO_rapD[l][k][j][i] = new TH1D(histname, htitle, 100, 0.5, 1.5);
	  wk()->addOutput (h_SF_BOTH_ISO_rapD[l][k][j][i]);

	  sprintf(htitle, "SF BOTH ISO rapE Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_SF_BOTH_ISO_rapE_%i_%i_%i_%i",l,k,j,i);
	  h_SF_BOTH_ISO_rapE[l][k][j][i] = new TH1D(histname, htitle, 100, 0.5, 1.5);
	  wk()->addOutput (h_SF_BOTH_ISO_rapE[l][k][j][i]);

	  sprintf(htitle, "SF BOTH ISO rapF Sign=%i Iso=%i Bin=%i Trig=%i",l,k,j,i);
	  sprintf(histname, "h_SF_BOTH_ISO_rapF_%i_%i_%i_%i",l,k,j,i);
	  h_SF_BOTH_ISO_rapF[l][k][j][i] = new TH1D(histname, htitle, 100, 0.5, 1.5);
	  wk()->addOutput (h_SF_BOTH_ISO_rapF[l][k][j][i]);

	}
      }
    }
  }

  std::cout<<"HISTOGRAMMING!"<<std::endl;

  return EL::StatusCode::SUCCESS;
} // end of setting histograms
