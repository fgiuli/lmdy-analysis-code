#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <EventLoop/Algorithm.h>

#include <EventLoop/Algorithm.h>
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"

#include "AsgTools/AsgToolsConf.h"
#include "AsgTools/AsgMetadataTool.h"
#include "AsgTools/ToolHandle.h"

#include "TH1.h"
#include "TH2.h"
#include "TH1D.h"
#include "TH2D.h"

#include "TLorentzVector.h"

#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"

#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionInterface/ITrigDecisionTool.h"
#include "TrigDecisionTool/TrigDecisionToolCore.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/MatchingTool.h"

//PAT Interface
#include "PATInterfaces/SystematicVariation.h"
#include "PATInterfaces/SystematicRegistry.h"
#include "PATInterfaces/SystematicCode.h"
#include "xAODTruth/TruthParticleContainer.h"

#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODTracking/TrackParticleContainer.h"

// Smart slim testing includes:
#include "xAODCore/tools/IOStats.h"
#include "xAODCore/tools/ReadStats.h"

//DY CI Reweight
//#include "DYReweighting/DYCI_XS.h"

// Electron Isolation                                                                 
#include "IsolationSelection/IsolationSelectionTool.h"

// kF
#include "LPXKfactorTool/LPXKfactorTool.h"

// GRL
#include "GoodRunsLists/GoodRunsListSelectionTool.h"

// Muon Corrections and selection
#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"
#include "MuonSelectorTools/MuonSelectionTool.h"

// ROOT
#include <TROOT.h>
#include <TTree.h>
#include <TVector3.h>

// Muon Efficiency Corrections
#include "MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h"
#include "MuonEfficiencyCorrections/MuonTriggerScaleFactors.h"

//PURW
#include "PileupReweighting/PileupReweightingTool.h"

//InDetTrackSystematicsTools
#include "InDetTrackSystematicsTools/InDetTrackSmearingTool.h"
#include "InDetTrackSystematicsTools/InDetTrackBiasingTool.h"
#include "InDetTrackSystematicsTools/InDetTrackTruthFilterTool.h"
#include "InDetTrackSystematicsTools/InDetTrackTruthOriginTool.h"
#include "InDetTrackSystematicsTools/JetTrackFilterTool.h"
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"

// System include(s):
#include <memory>
#include <cstdlib>
#include <cmath>
#include <memory>
#include <cstdlib>
#include <vector>
#include <cmath>
#include <iostream>
#include <exception>

#include "AsgTools/IAsgTool.h"

// ROOT
#include <TROOT.h>
#include <TVector3.h>

/*xAOD includes*/
#include "xAODRootAccess/tools/Message.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/Egamma.h"
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"

// Book Keeper
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"
#include <TTree.h>

// Truth                                                                                                                                                  
#include "xAODTruth/TruthEventContainer.h"                                                                                   
#include "xAODTruth/TruthEvent.h"                                                                                               
#include "xAODTruth/TruthParticle.h"                                                                                                                                
#include "xAODTruth/TruthParticleContainer.h"                                                                                   
#include "xAODRootAccess/TStore.h"                                                                                                                           
#include <EventLoopAlgs/NTupleSvc.h>                                                                                                                                         
#include <EventLoopAlgs/AlgSelect.h>                                                                                                                                         
#include "PathResolver/PathResolver.h"

// Electron Track
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticle.h"

// Electron Isolation
#include "IsolationSelection/IsolationSelectionTool.h"

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <MyAnalysis/MyxAODAnalysis.h>

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"

//Muon Information
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"
#include "MuonSelectorTools/MuonSelectionTool.h" 

// EDM includes:
#include "xAODEventInfo/EventInfo.h"
#include "xAODRootAccess/tools/Message.h"

#include "PATInterfaces/CorrectionCode.h" // to check the return correction code status of tools
#include "xAODCore/ShallowAuxContainer.h"
#include "xAODCore/ShallowCopy.h"
#include "CxxUtils/make_unique.h"

// Trigger SF private class
#include "LowPtMuonTriggerSF.h"

//using CxxUtils::make_unique;

/// Helper macro for checking xAOD::TReturnCode return values
#define EL_RETURN_CHECK( CONTEXT, EXP )                     \
  do {                                                     \
  if( ! EXP.isSuccess() ) {                             \
  Error( CONTEXT,                                    \
    XAOD_MESSAGE( "Failed to execute: %s" ),    \
	 #EXP );                                     \
  return EL::StatusCode::FAILURE;                    \
  }                                                     \
  } while( false )                                   

class MyxAODAnalysis : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:

  //TTree* dimuonTree; //!

  //namespace InDet { class InDetTrackSmearingTool; }
  //std::unique_ptr<InDet::InDetTrackSmearingTool> m_trkSmearingTool; //!

  // float cutValue;
  int m_eventCounter; //!
  int m_numCleanEvents; //!
  double m_nEventsProcessed; //!
  double m_sumOfWeights; //!
  double m_sumOfWeightsSquared; //!
  xAOD::TEvent *m_event; //!
  const xAOD::EventInfo* m_EventInfo; //!     

  bool m_isDerivation; //!
  bool NewFile; //!
  bool CalcOwnMu; //!
  bool m_debug; //!
  bool ApplyCorrections; //!
  bool m_FastSim; //!

  //::: EventInfo
  UInt_t m_RunNumber; //!
  Int_t m_LumiBlock; //!
  ULong64_t m_EventNumber; //!

  //Control plots
  TH1D* h_N_vtx_BF; //!
  TH1D* h_N_vtx_A; //!
  TH1D* h_N_vtx_B; //!
  TH1D* h_PileUpWeight; //!
  TH1D* h_PU; //!
  TH1D* h_PU_BF; //!
  TH1D* h_kF; //!
  TH1D* h_Dummy; //!

  TH1D *h_TRY; //!                                                                                                                                        
  TH1D *h_TRY_2; //!                                                                                                                                        
  TH1D *h_CutFlow_Common; //!                                   
  TH1D *h_Truth_V_Mass; //!                                   
  TH1D *h_Truth_V_Mass_NEW; //!                                   

  TH2D* h_kF_InvMass[2][6][1][3]; //!
  TH1D *h_EventWeight[2][6][1][3]; //!                                                                                                           
  TH1D *h_Multiplicity[2][6][1][3]; //!
  TH1D *h_BornMass[2][6][1][3]; //!                                                                                                                                   
  TH1D *h_DeltaEta[2][6][1][3]; //!                                                                                                                                  
  TH1D *h_DeltaPhi[2][6][1][3]; //!                                                                                                                                  
  TH2D *h_Pt_muons[2][6][1][3]; //!                                                                                                                                  
  TH2D *h_PtEta[2][6][1][3]; //!                                                                                                                                     
  TH2D *h_PtEta_Lead[2][6][1][3]; //!                                                                                                                                
  TH2D *h_PtEta_Sub[2][6][1][3]; //!                                                                                                                                 
  TH2D *h_PtPhi[2][6][1][3]; //!                                                                                                                                     
  TH2D *h_PtPhi_Lead[2][6][1][3]; //!                                                                                                                        
  TH2D *h_PtPhi_Sub[2][6][1][3]; //!                                                                                                                            
  TH1D *h_InvMass_low[2][6][1][3]; //!                                                                                                                               
  TH1D *h_ZpT_A; //!                                                                                                                 
  TH1D *h_ZpT_B; //!                                                                                                                 
  TH1D *h_ZpT_C; //!                                                                                                                 
  TH1D *h_ZpT_D; //!                                                                                                                 
  TH1D *h_ZpT_E; //!                                                                                                                 
  TH1D *h_ZpT_F; //!                                                                                                                 
  TH1D *h_ZpT_G; //!                                                                                                                 
  TH1D *h_ZpT_H; //!                                                                                                                 
  TH1D *h_ZpT_A_After; //!                                                                                                                 
  TH1D *h_ZpT_B_After; //!                                                                                                                 
  TH1D *h_ZpT_C_After; //!                                                                                                                 
  TH1D *h_ZpT_D_After; //!                                                                                                                 
  TH1D *h_ZpT_E_After; //!                                                                                                                 
  TH1D *h_ZpT_F_After; //!                                                                                                                 
  TH1D *h_ZpT_G_After; //!                                                                                                                 
  TH1D *h_ZpT_H_After; //!                                                                                                                 
  TH1D *h_MuonPt_Pair[2][6][1][3]; //!                                                                                                                 
  TH1D *h_MuonPt_Pair_A[2][6][1][3]; //!                                                                                                                 
  TH1D *h_MuonPt_Pair_B[2][6][1][3]; //!                                                                                                                 
  TH1D *h_MuonPt_Pair_C[2][6][1][3]; //!                                                                                                                 
  TH1D *h_MuonPt_Pair_D[2][6][1][3]; //!                                                                                                                 
  TH1D *h_MuonPt_Pair_E[2][6][1][3]; //!                                                                                                                 
  TH1D *h_MuonPt_Pair_F[2][6][1][3]; //!                                                                                                                 
  TH1D *h_MuonPt_Pair_G[2][6][1][3]; //!                                                                                                                 
  TH1D *h_MuonPt_Pair_H[2][6][1][3]; //!                                                                                                                 
  TH1D *h_MuonPt_Lead[2][6][1][3]; //!
  TH1D *h_MuonPt_Sub[2][6][1][3]; //!
  TH1D *h_InvMass_pp_ISO[2][6][1][3]; //!                                                                                                       
  TH1D *h_InvMass_mm_ISO[2][6][1][3]; //!                                                                                                                  
  TH1D *h_InvMass_pp_NOISO[2][6][1][3]; //!                                                                                                       
  TH1D *h_InvMass_mm_NOISO[2][6][1][3]; //!                                                                                               

  TH1D *h_InvMass[2][6][1][3]; //!                                                                                                                                   
  TH1D *h_InvMass_DownIso[2][6][1][3]; //!                                                                                                                                   
  TH1D *h_InvMass_UpIso[2][6][1][3]; //!                                                                                                                                   
  TH1D *h_InvMass_DownAntiIso[2][6][1][3]; //!                                                                                                                      
  TH1D *h_InvMass_UpAntiIso[2][6][1][3]; //!                                                                                                          
  TH1D *h_InvMass_Up[2][6][1][3]; //!                                                                                                                                   
  TH1D *h_InvMass_Down[2][6][1][3]; //!                                                                                                                                   
  TH1D *h_MuonEta_Pair[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Lead[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Sub[2][6][1][3]; //!                                                                                                                               
  TH1D *h_MuonPhi_Pair[2][6][1][3]; //!                                                                                                                             
  TH1D *h_MuonPhi_Lead[2][6][1][3]; //!                                                                                                                         
  TH1D *h_MuonPhi_Sub[2][6][1][3]; //!                                                                                                                 
  TH1D *h_CutFlow[2][6][1][3]; //! 
  TH1D *h_d0_mu1[2][6][1][3]; //!                                                                                                                                    
  TH2D *h_d0_mu1mu2[2][6][1][3]; //!
  TH1D *h_delta_z0_significance[2][6][1][3]; //!                                                                                     
  TH2D *h_delta_z0_d0_significance[2][6][1][3]; //!                                                                                     
  TH1D *h_d0_significance_BOTH[2][6][1][3]; //!                                                                                     
  TH1D *h_z0_mu1[2][6][1][3]; //!                                                                                                               
  TH1D *h_d0_mu1_BF[2][6][1][3]; //!                                                                                                      
  TH1D *h_z0_mu1_BF[2][6][1][3]; //!                                                                                                        
  TH1D *h_Rapidity[2][6][1][3]; //! 
  TH1D *h_DeltaR[2][6][1][3]; //!                                                                                                   
  TH1D *h_delta_z0[2][6][1][3]; //!
  TH1D *h_MuonEta_Inc[2][6][1][3]; //!
  TH1D *h_MuonPhi_Inc[2][6][1][3]; //!
  TH1D *h_MuonRap_Inc[2][6][1][3]; //!
  TH1D *h_MuonPt_Inc[2][6][1][3]; //!

  //Trigger SF studies
  TH1D *h_MuonEta_Inc_Fir[2][6][1][3]; //!
  TH1D *h_MuonPhi_Inc_Fir[2][6][1][3]; //!
  TH1D *h_MuonRap_Inc_Fir[2][6][1][3]; //!
  TH1D *h_MuonEta_Inc_Fir_BF[2][6][1][3]; //!
  TH1D *h_MuonPhi_Inc_Fir_BF[2][6][1][3]; //!
  TH1D *h_MuonRap_Inc_Fir_BF[2][6][1][3]; //!
  TH1D *h_DeltaPhi_Fir_BF[2][6][1][3]; //!                                                                                                                                  
  TH1D *h_DeltaPhi_Fir[2][6][1][3]; //!                                                                                                                                 
  TH1D *h_MuonEta_Pair_Fir[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Lead_Fir[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Sub_Fir[2][6][1][3]; //!                                                                                                                               
  TH1D *h_MuonPhi_Pair_Fir[2][6][1][3]; //!                                                                                                                             
  TH1D *h_MuonPhi_Lead_Fir[2][6][1][3]; //!                                                                                                                         
  TH1D *h_MuonPhi_Sub_Fir[2][6][1][3]; //!                                                                                                                 
  TH1D *h_MuonEta_Pair_Fir_BF[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Lead_Fir_BF[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Sub_Fir_BF[2][6][1][3]; //!                                                                                                                               
  TH1D *h_MuonPhi_Pair_Fir_BF[2][6][1][3]; //!                                                                                                                             
  TH1D *h_MuonPhi_Lead_Fir_BF[2][6][1][3]; //!                                                                                                                         
  TH1D *h_MuonPhi_Sub_Fir_BF[2][6][1][3]; //!                                                                                                                 
  TH1D *h_MuonEta_Inc_Sec[2][6][1][3]; //!
  TH1D *h_MuonPhi_Inc_Sec[2][6][1][3]; //!
  TH1D *h_MuonRap_Inc_Sec[2][6][1][3]; //!
  TH1D *h_MuonEta_Inc_Sec_BF[2][6][1][3]; //!
  TH1D *h_MuonPhi_Inc_Sec_BF[2][6][1][3]; //!
  TH1D *h_MuonRap_Inc_Sec_BF[2][6][1][3]; //!
  TH1D *h_DeltaPhi_Sec_BF[2][6][1][3]; //!                                                                                                                                  
  TH1D *h_DeltaPhi_Sec[2][6][1][3]; //!                                                                                                                                 
  TH1D *h_MuonEta_Pair_Sec[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Lead_Sec[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Sub_Sec[2][6][1][3]; //!                                                                                                                               
  TH1D *h_MuonPhi_Pair_Sec[2][6][1][3]; //!                                                                                                                             
  TH1D *h_MuonPhi_Lead_Sec[2][6][1][3]; //!                                                                                                                         
  TH1D *h_MuonPhi_Sub_Sec[2][6][1][3]; //!                                                                                                                 
  TH1D *h_MuonEta_Pair_Sec_BF[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Lead_Sec_BF[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Sub_Sec_BF[2][6][1][3]; //!                                                                                                                               
  TH1D *h_MuonPhi_Pair_Sec_BF[2][6][1][3]; //!                                                                                                                             
  TH1D *h_MuonPhi_Lead_Sec_BF[2][6][1][3]; //!                                                                                                                         
  TH1D *h_MuonPhi_Sub_Sec_BF[2][6][1][3]; //!                                                                                                                 
  TH1D *h_MuonEta_Inc_Trd[2][6][1][3]; //!
  TH1D *h_MuonPhi_Inc_Trd[2][6][1][3]; //!
  TH1D *h_MuonRap_Inc_Trd[2][6][1][3]; //!
  TH1D *h_MuonEta_Inc_Trd_BF[2][6][1][3]; //!
  TH1D *h_MuonPhi_Inc_Trd_BF[2][6][1][3]; //!
  TH1D *h_MuonRap_Inc_Trd_BF[2][6][1][3]; //!
  TH1D *h_DeltaPhi_Trd_BF[2][6][1][3]; //!                                                                                                                                  
  TH1D *h_DeltaPhi_Trd[2][6][1][3]; //!                                                                                                                                 
  TH1D *h_MuonEta_Pair_Trd[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Lead_Trd[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Sub_Trd[2][6][1][3]; //!                                                                                                                               
  TH1D *h_MuonPhi_Pair_Trd[2][6][1][3]; //!                                                                                                                             
  TH1D *h_MuonPhi_Lead_Trd[2][6][1][3]; //!                                                                                                                         
  TH1D *h_MuonPhi_Sub_Trd[2][6][1][3]; //!                                                                                                                 
  TH1D *h_MuonEta_Pair_Trd_BF[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Lead_Trd_BF[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Sub_Trd_BF[2][6][1][3]; //!                                                                                                                               
  TH1D *h_MuonPhi_Pair_Trd_BF[2][6][1][3]; //!                                                                                                                             
  TH1D *h_MuonPhi_Lead_Trd_BF[2][6][1][3]; //!                                                                                                                         
  TH1D *h_MuonPhi_Sub_Trd_BF[2][6][1][3]; //!                                                                                                                 
  TH1D *h_MuonEta_Inc_Fou[2][6][1][3]; //!
  TH1D *h_MuonPhi_Inc_Fou[2][6][1][3]; //!
  TH1D *h_MuonRap_Inc_Fou[2][6][1][3]; //!
  TH1D *h_MuonEta_Inc_Fou_BF[2][6][1][3]; //!
  TH1D *h_MuonPhi_Inc_Fou_BF[2][6][1][3]; //!
  TH1D *h_MuonRap_Inc_Fou_BF[2][6][1][3]; //!
  TH1D *h_DeltaPhi_Fou_BF[2][6][1][3]; //!                                                                                                                                  
  TH1D *h_DeltaPhi_Fou[2][6][1][3]; //!                                                                                                                                 
  TH1D *h_MuonEta_Pair_Fou[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Lead_Fou[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Sub_Fou[2][6][1][3]; //!                                                                                                                               
  TH1D *h_MuonPhi_Pair_Fou[2][6][1][3]; //!                                                                                                                             
  TH1D *h_MuonPhi_Lead_Fou[2][6][1][3]; //!                                                                                                                         
  TH1D *h_MuonPhi_Sub_Fou[2][6][1][3]; //!                                                                                                                 
  TH1D *h_MuonEta_Pair_Fou_BF[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Lead_Fou_BF[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Sub_Fou_BF[2][6][1][3]; //!                                                                                                                               
  TH1D *h_MuonPhi_Pair_Fou_BF[2][6][1][3]; //!                                                                                                                             
  TH1D *h_MuonPhi_Lead_Fou_BF[2][6][1][3]; //!                                                                                                                         
  TH1D *h_MuonPhi_Sub_Fou_BF[2][6][1][3]; //!

  TH1D *h_MuonEta_Pair_A_BF[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Lead_A_BF[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Sub_A_BF[2][6][1][3]; //!                                                                                                                               
  TH1D *h_MuonPhi_Pair_A_BF[2][6][1][3]; //!                                                                                                                             
  TH1D *h_MuonPhi_Lead_A_BF[2][6][1][3]; //!                                                                                                                         
  TH1D *h_MuonPhi_Sub_A_BF[2][6][1][3]; //!                                                                                                                 
  TH1D *h_DeltaPhi_A_BF[2][6][1][3]; //!                                                                                                                                  
  TH1D *h_MuonEta_Pair_A[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Lead_A[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Sub_A[2][6][1][3]; //!                                                                                                                               
  TH1D *h_MuonPhi_Pair_A[2][6][1][3]; //!                                                                                                                             
  TH1D *h_MuonPhi_Lead_A[2][6][1][3]; //!                                                                                                                         
  TH1D *h_MuonPhi_Sub_A[2][6][1][3]; //!                                                                                                                 
  TH1D *h_DeltaPhi_A[2][6][1][3]; //!                                                                                                                                  
  TH1D *h_MuonRap_Inc_A_BF[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonRap_Inc_A[2][6][1][3]; //!                                                                                                                              

  TH1D *h_MuonEta_Pair_B_BF[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Lead_B_BF[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Sub_B_BF[2][6][1][3]; //!                                                                                                                               
  TH1D *h_MuonPhi_Pair_B_BF[2][6][1][3]; //!                                                                                                                             
  TH1D *h_MuonPhi_Lead_B_BF[2][6][1][3]; //!                                                                                                                         
  TH1D *h_MuonPhi_Sub_B_BF[2][6][1][3]; //!                                                                                                                 
  TH1D *h_DeltaPhi_B_BF[2][6][1][3]; //!                                                                                                                                  
  TH1D *h_MuonEta_Pair_B[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Lead_B[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Sub_B[2][6][1][3]; //!                                                                                                                               
  TH1D *h_MuonPhi_Pair_B[2][6][1][3]; //!                                                                                                                             
  TH1D *h_MuonPhi_Lead_B[2][6][1][3]; //!                                                                                                                         
  TH1D *h_MuonPhi_Sub_B[2][6][1][3]; //!                                                                                                                 
  TH1D *h_DeltaPhi_B[2][6][1][3]; //!                                                                                                                                  
  TH1D *h_MuonRap_Inc_B_BF[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonRap_Inc_B[2][6][1][3]; //!                                                                                                                              

  TH1D *h_MuonEta_Pair_C_BF[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Lead_C_BF[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Sub_C_BF[2][6][1][3]; //!                                                                                                                               
  TH1D *h_MuonPhi_Pair_C_BF[2][6][1][3]; //!                                                                                                                             
  TH1D *h_MuonPhi_Lead_C_BF[2][6][1][3]; //!                                                                                                                         
  TH1D *h_MuonPhi_Sub_C_BF[2][6][1][3]; //!                                                                                                                 
  TH1D *h_DeltaPhi_C_BF[2][6][1][3]; //!                                                                                                                                  
  TH1D *h_MuonEta_Pair_C[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Lead_C[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Sub_C[2][6][1][3]; //!                                                                                                                               
  TH1D *h_MuonPhi_Pair_C[2][6][1][3]; //!                                                                                                                             
  TH1D *h_MuonPhi_Lead_C[2][6][1][3]; //!                                                                                                                         
  TH1D *h_MuonPhi_Sub_C[2][6][1][3]; //!                                                                                                                 
  TH1D *h_DeltaPhi_C[2][6][1][3]; //!                                                                                                                                  
  TH1D *h_MuonRap_Inc_C_BF[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonRap_Inc_C[2][6][1][3]; //!                                                                                                                              

  TH1D *h_MuonEta_Pair_D_BF[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Lead_D_BF[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Sub_D_BF[2][6][1][3]; //!                                                                                                                               
  TH1D *h_MuonPhi_Pair_D_BF[2][6][1][3]; //!                                                                                                                             
  TH1D *h_MuonPhi_Lead_D_BF[2][6][1][3]; //!                                                                                                                         
  TH1D *h_MuonPhi_Sub_D_BF[2][6][1][3]; //!                                                                                                                 
  TH1D *h_DeltaPhi_D_BF[2][6][1][3]; //!                                                                                                                                  
  TH1D *h_MuonEta_Pair_D[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Lead_D[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonEta_Sub_D[2][6][1][3]; //!                                                                                                                               
  TH1D *h_MuonPhi_Pair_D[2][6][1][3]; //!                                                                                                                             
  TH1D *h_MuonPhi_Lead_D[2][6][1][3]; //!                                                                                                                         
  TH1D *h_MuonPhi_Sub_D[2][6][1][3]; //!                                                                                                                 
  TH1D *h_DeltaPhi_D[2][6][1][3]; //!                                                                                                                                  
  TH1D *h_MuonRap_Inc_D_BF[2][6][1][3]; //!                                                                                                                              
  TH1D *h_MuonRap_Inc_D[2][6][1][3]; //!

  //DeltaR studies
  /*TH1D *h_InvMass_DeltaR[2][6][1][3]; //!
  TH1D *h_MuonPt_Pair_DeltaR[2][6][1][3]; //!
  TH1D *h_MuonPt_Lead_DeltaR[2][6][1][3]; //!
  TH1D *h_MuonPt_Sub_DeltaR[2][6][1][3]; //!
  TH1D *h_MuonEta_Lead_DeltaR[2][6][1][3]; //!
  TH1D *h_MuonEta_Sub_DeltaR[2][6][1][3]; //!
  TH1D *h_MuonPhi_Lead_DeltaR[2][6][1][3]; //!
  TH1D *h_MuonPhi_Sub_DeltaR[2][6][1][3]; //!*/

  //Acceptance studies (BORN)
  TH1D *h_Invariant_Mass_RECO_B[2][3]; //!
  TH1D *h_Invariant_Mass_TRUTH_B[2][3]; //!
  TH1D *h_Invariant_Mass_BOTH_B[2][3]; //!
  TH1D *h_Invariant_Mass_Res_RECO_B[2][3]; //!
  TH2D *h_Invariant_Mass_Res_TRUTH_B[2][3]; //!

  TH1D *h_Invariant_Mass_Z[2][3]; //!
  
  TH2D *h_Muon_Rapidity_Res_TRUTH[2][8][3]; //!
  TH1D *h_Muon_Rapidity_Res_RECO[2][8][3]; //!
  TH1D *h_Muon_Rapidity_TRUTH[2][8][3]; //!
  TH1D *h_Muon_Rapidity_RECO[2][8][3]; //!
  TH1D *h_Muon_Rapidity_BOTH[2][8][3]; //!
  TH1D *h_Muon_Rapidity_RECO_SS[2][8][3]; //!
  TH1D *h_Muon_Rapidity_BOTH_SS[2][8][3]; //!
  TH1D *h_Invariant_Mass_Diff_B[2][8][3]; //!

  //Acceptance studies (BARE)
  TH1D *h_Invariant_Mass_RECO_BARE_B[2][3]; //!
  TH1D *h_Invariant_Mass_TRUTH_BARE_B[2][3]; //!
  TH1D *h_Invariant_Mass_BOTH_BARE_B[2][3]; //!
  TH1D *h_Invariant_Mass_Res_RECO_BARE_B[2][3]; //!
  TH2D *h_Invariant_Mass_Res_TRUTH_BARE_B[2][3]; //!
  
  TH2D *h_Muon_Rapidity_Res_TRUTH_BARE[2][8][3]; //!
  TH1D *h_Muon_Rapidity_Res_RECO_BARE[2][8][3]; //!
  TH1D *h_Muon_Rapidity_TRUTH_BARE[2][8][3]; //!
  TH1D *h_Muon_Rapidity_RECO_BARE[2][8][3]; //!
  TH1D *h_Muon_Rapidity_BOTH_BARE[2][8][3]; //!
  TH1D *h_Invariant_Mass_Diff_BARE_B[2][8][3]; //!

  //Multijet bkg studies
  TH1D *h_SF_Eff_Lead[2][4][8][3]; //!
  TH1D *h_SF_Eff_Sub[2][4][8][3]; //!
  TH1D *h_SF_Trig_Lead[2][4][8][3]; //!
  TH1D *h_SF_Trig_Sub[2][4][8][3]; //!
  TH1D *h_SF_Iso_Lead[2][4][8][3]; //!
  TH1D *h_SF_Iso_Sub[2][4][8][3]; //!
  TH1D *h_d0_significance_BOTH_ISO[2][4][8][3]; //!
  TH1D *h_d0_significance_BOTH_ISO_Sig[2][4][8][3]; //!
  TH1D *h_delta_z0_significance_ISO_Sig[2][4][8][3]; //!
  TH1D *h_d0_significance_BOTH_NOISO_Sig[2][4][8][3]; //!
  TH1D *h_delta_z0_significance_NOISO_Sig[2][4][8][3]; //!
  TH1D *h_d0_significance_BOTH_HALF_Sig[2][4][8][3]; //!
  TH1D *h_delta_z0_significance_HALF_Sig[2][4][8][3]; //!
  TH1D *h_d0_significance_BOTH_ISO_rapA[2][4][8][3]; //!
  TH1D *h_d0_significance_BOTH_ISO_rapB[2][4][8][3]; //!
  TH1D *h_d0_significance_BOTH_ISO_rapC[2][4][8][3]; //!
  TH1D *h_d0_significance_BOTH_ISO_rapD[2][4][8][3]; //!
  TH1D *h_d0_significance_BOTH_ISO_rapE[2][4][8][3]; //!
  TH1D *h_d0_significance_BOTH_ISO_rapF[2][4][8][3]; //!
  TH1D *h_d0_significance_BOTH_NOISO[2][4][8][3]; //!
  TH1D *h_d0_significance_BOTH_NOISO_rapA[2][4][8][3]; //!
  TH1D *h_d0_significance_BOTH_NOISO_rapB[2][4][8][3]; //!
  TH1D *h_d0_significance_BOTH_NOISO_rapC[2][4][8][3]; //!
  TH1D *h_d0_significance_BOTH_NOISO_rapD[2][4][8][3]; //!
  TH1D *h_d0_significance_BOTH_NOISO_rapE[2][4][8][3]; //!
  TH1D *h_d0_significance_BOTH_NOISO_rapF[2][4][8][3]; //!
  TH1D *h_d0_significance_BOTH_HALF[2][4][8][3]; //!
  TH1D *h_d0_significance_BOTH_HALF_rapA[2][4][8][3]; //!
  TH1D *h_d0_significance_BOTH_HALF_rapB[2][4][8][3]; //!
  TH1D *h_d0_significance_BOTH_HALF_rapC[2][4][8][3]; //!
  TH1D *h_d0_significance_BOTH_HALF_rapD[2][4][8][3]; //!
  TH1D *h_d0_significance_BOTH_HALF_rapE[2][4][8][3]; //!
  TH1D *h_d0_significance_BOTH_HALF_rapF[2][4][8][3]; //!

  TH1D *h_chi2_ISO[2][4][8][3]; //!
  TH1D *h_chi2_Prob_ISO[2][4][8][3]; //!
  TH1D *h_chi2_Prob_pp_ISO[2][4][8][3]; //!
  TH1D *h_chi2_Prob_mm_ISO[2][4][8][3]; //!
  TH1D *h_chi2_Prob_pp_NOISO[2][4][8][3]; //!
  TH1D *h_chi2_Prob_mm_NOISO[2][4][8][3]; //!
  TH1D *h_chi2_NOISO[2][4][8][3]; //!
  TH1D *h_chi2_Prob_NOISO[2][4][8][3]; //!
  TH1D *h_chi2_HALF[2][4][8][3]; //!
  TH1D *h_chi2_Prob_HALF[2][4][8][3]; //!
  TH1D *h_chi2_Prob_noweight[2][4][8][3]; //!
  TH2D *h_d0sig_deltaz0sig[2][4][8][3]; //!

  TH1D *h_SF_BOTH_ISO[2][4][8][3]; //!
  TH1D *h_SF_BOTH_ISO_rapA[2][4][8][3]; //!
  TH1D *h_SF_BOTH_ISO_rapB[2][4][8][3]; //!
  TH1D *h_SF_BOTH_ISO_rapC[2][4][8][3]; //!
  TH1D *h_SF_BOTH_ISO_rapD[2][4][8][3]; //!
  TH1D *h_SF_BOTH_ISO_rapE[2][4][8][3]; //!
  TH1D *h_SF_BOTH_ISO_rapF[2][4][8][3]; //!

<<<<<<< HEAD
  TH1D *h_BosonPt; //!                                                                                                          
  TH1D *h_kFweight; //!                                                                                                    
  TH1D *h_ZpT; //!    
=======
  TH1D *h_BosonPt; //!

  TH1D *h_kFweight; //!
  TH1D *h_ZpT; //!
>>>>>>> ffa1ca1ae6dbd052eaedc099c8e0518e4329ae6d

  double NormalisationWeight( double mc_channel_number, double lumi ); //!
  double Full_CrossSection( double ZprimeMass, TLorentzVector vec_q1, TLorentzVector vec_q2, TLorentzVector vec_l1, TLorentzVector vec_l2, double quarktype, int model, int mode, double UserAngle); //!
  double MassOnly_CrossSection( double ZprimeMass, double TruthMass, double quarktype, int model, int mode); //!
  double GetMuonRecoSF_Medium(const xAOD::IParticle *p); //!

  double GetMuonIsoSF_Loose(const xAOD::IParticle *p); //!
  double GetMuonIsoSF_Tight(const xAOD::IParticle *p); //!
  double GetMuonIsoSF_FixedCutLoose(const xAOD::IParticle *p); //!
  double GetMuonIsoSF_FixedCutTight(const xAOD::IParticle *p); //!
  double GetMuonIsoSF_Gradient(const xAOD::IParticle *p); //!

  double GetMuonTTVASF(const xAOD::IParticle *p); //!
  double GetMuonRecoSF_Tight(const xAOD::IParticle *p); //!
  void bubblesort(std::vector<double> &array,std::vector<int> &array2); //!
  void SetTreeBranches(); //!
  void ResetVariables();  //!
 
  double GetZReweighting(double pT); //!
  double GetZReweighting_CR1(double pT); //!
  double GetZReweighting_A(double pT); //!
  double GetZReweighting_B(double pT); //!
  double GetZReweighting_C(double pT); //!
  double GetZReweighting_D(double pT); //!
  double GetZReweighting_E(double pT); //!
  double GetZReweighting_F(double pT); //!
  double GetZReweighting_G(double pT); //!
  double GetZReweighting_H(double pT); //!

  double GetMuonTriggerSF_HLT_mu24_imedium(const xAOD::IParticle *p, double m_RandomRN); //!

  double GetMuonTriggerSF_HLT_mu18_MC(const xAOD::IParticle *p, double m_RandomRN); //!
  double GetMuonTriggerSF_HLT_mu8noL1_MC(const xAOD::IParticle *p, double m_RandomRN); //!
  double GetMuonTriggerSF_HLT_mu18_Data(const xAOD::IParticle *p, double m_RandomRN); //!
  double GetMuonTriggerSF_HLT_mu8noL1_Data(const xAOD::IParticle *p, double m_RandomRN); //!

  double GetMuonTriggerSF_TEST(const xAOD::IParticle *p, const xAOD::IParticle *p1, double m_RandomRN); //!

private:
  LowPtMuonTriggerSF * m_sfclass; //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!
  GoodRunsListSelectionTool *m_grl; //!

  // MuonCalibrationAndSmearing and Selection
  CP::MuonCalibrationAndSmearingTool *m_muonCalibrationAndSmearingTool; //!
  CP::MuonSelectionTool *m_muonSelection_MEDIUM; //!
  TrigConf::xAODConfigTool *m_TrigConfigTool; //!
  Trig::TrigDecisionTool *m_TrigDecTool; //!
  CP::IsolationSelectionTool *iso_muon_fixed_tight; //!

  CP::PileupReweightingTool *m_Pileup; //!
  Trig::MatchingTool* m_match_Tool; //!
  LPXKfactorTool *m_kFTool; //!
  CP::MuonEfficiencyScaleFactors *m_mueff_corr_Medium; //!
  CP::MuonEfficiencyScaleFactors *m_muiso_sf_FixedCutTight; //!

  std::vector<CP::SystematicSet> m_sysList; //!
  ToolHandle<CP::IPileupReweightingTool> PRWToolHandle; //!
  ToolHandle<InDet::IInDetTrackTruthOriginTool> trackTruthOriginToolHandle; //!

  xAOD::MuonContainer *SelectedMuon; //!
  xAOD::MuonAuxContainer *SelectedMuonAux; //!
  xAOD::Muon* newMuon; //!
  xAOD::Muon* newMuon1; //!
  std::string m_muTMstring; //!

  xAOD::TrackParticle* trk_Cpy_d0;    //!                                                                                                              
  xAOD::TrackParticle* trk_Cpy_z0;     //!

  xAOD::TrackParticle* trk_leadCpy_d0;    //!  
  xAOD::TrackParticle* trk_subCpy_d0;     //!                                                                                                            
  xAOD::TrackParticle* trk_leadCpy_z0;    //!                                                                                                                  
  xAOD::TrackParticle* trk_subCpy_z0;     //!  
  xAOD::TrackParticle* trk_leadCpy_CR1_d0;     //!                                                                                                             
  xAOD::TrackParticle* trk_subCpy_CR1_d0;      //!  
  xAOD::TrackParticle* trk_leadCpy_CR1_z0;     //!                                                                                                          
  xAOD::TrackParticle* trk_subCpy_CR1_z0;      //!  
  xAOD::TrackParticle* trk_leadCpy_CR2_d0;     //!                                                                                                     
  xAOD::TrackParticle* trk_subCpy_CR2_d0;      //!                                                                                                            
  xAOD::TrackParticle* trk_leadCpy_CR2_z0;     //!                                                                                                            
  xAOD::TrackParticle* trk_subCpy_CR2_z0;      //!  

  InDet::InDetTrackSmearingTool *m_smearingTool_d0; //!
  InDet::InDetTrackSmearingTool *m_smearingTool_z0; //!

  //If needed...                                                                            
  std::string m_muTMstring_1; //!                                                                                                          
  std::string m_muTMstring_2; //!                                                                                                                         
  std::string m_muTMstring_3; //!                                                                                                                        
  std::string m_muTMstring_4; //!                                                                                                                      
  std::string m_muTMstring_5; //! 
  std::string m_muTMstring_6; //! 

  std::string Arg_Sys; //
  std::string sys_name; //!
  std::string Arg_SampleName; //
  Double_t Arg_isMC; //

  std::vector<CP::SystematicSet>::const_iterator sysListItr; //!

  // this is a standard constructor
  MyxAODAnalysis ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(MyxAODAnalysis, 1);
};

#endif
