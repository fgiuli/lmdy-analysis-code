#ifndef LowPtMuonTriggerSF_h
#define  LowPtMuonTriggerSF_h

#include <TROOT.h>
#include <TLorentzVector.h>
#include <TH1.h>
#include <TH2.h>
#include <TFile.h>
#include <list>
#include <string>

class  LowPtMuonTriggerSF {

 public :
  LowPtMuonTriggerSF();

  ~LowPtMuonTriggerSF();

  bool Initialize(const std::string& filename);

  bool getMuonEfficiency(double &eff, const TLorentzVector mu, const int charge, const std::string& trigger, const std::string& systematic, const std::string& dataormc); 

  bool getTriggerSF(double &sf, const TLorentzVector mu, const int charge, const std::string& trigger,  const std::string& systematic);

  bool getDimuonSF(double &sf,
		   const TLorentzVector mu1, const int charge1,
		   const TLorentzVector mu2, const int charge2,
		   const std::string& trigger,  const std::string& systematic);

  void setPhiModulation(bool modulation){
    m_phi_modulation = modulation;
  };

  void setInterpolation(bool interpolate){
    m_interpolate = interpolate;
  };

  void setPtZMin(double ptzmin){
    m_pt_Z_min = ptzmin;
    //std::cout <<"LowPtMuonTriggerSF    INFO                 Using Z maps above pt= " << m_pt_Z_min<<  std::endl;
  };
  
  
 private:
  TFile * m_file;
  TH2D * m_histogram[2][2][2][5][2];
  TH2D*  m_phimod[2][2];

  bool m_apply_id_eff;
  double m_id_eff[2][2];

  
  double m_pt_Z_min;
  bool  m_phi_modulation;
  bool  m_interpolate;


  double m_eps;

  
};

#endif
