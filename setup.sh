cd /afs/cern.ch/user/f/fgiuli/lmdy-analysis-code

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

export CERN_USER=fgiuli
export RUCIO_ACCOUNT=fgiuli
voms-proxy-init -voms atlas -valid 96:00:00
lsetup rucio

rcSetup Base,2.4.43
