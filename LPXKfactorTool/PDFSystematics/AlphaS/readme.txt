10.2.16

Uta Klein and Ellis Kay

Study effect of changing alpha_s from 0.118 within +-0.003 
i.e. a conservative 90% CL in accordance with 68% CL recommendation of 0.0015 from PDF4LHC

Cross section calculated with VRAP 0.9
and PDFs
CT14nnlo_as_0115 ... CT14nnlo_as_0121

The maximum and minimum deviating cross section was indetified per mass bin,
and the resulting postive and negative (raw, not fitted) deviations are 
plotted here for each VB's

Wcomb_CT14nnlo_as_0118_alpha_s.pdf
Wminus_CT14nnlo_as_0118_alpha_s.pdf
Wplus_CT14nnlo_as_0118_alpha_s.pdf
Zgamma_CT14nnlo_as_0118_alpha_s.pdf

There are some funny structures and asymmetries visible, but overall, teh effects are small.

The fits for asymmetric alpha_s uncertainties are per VB in up and down *C files,
supported by control plots *C.pdf

The alpha_s uncertainty has to be added in quadrature to the PDF uncertainty.


