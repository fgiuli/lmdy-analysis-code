double Wall_ct14nnlo_pdf_down(double x) {
   const int fNp = 40, fKstep = 0;
   const double fDelta = -1, fXmin = 9, fXmax = 9703;
   const double fX[40] = { 9, 14, 18, 23, 28,
                        33, 38, 43, 48, 53,
                        58, 63, 71, 81, 91,
                        106, 128, 154, 186, 225,
                        271, 328, 396, 477, 576,
                        696, 840, 1014, 1224, 1478,
                        1784, 2153, 2599, 3137, 3787,
                        4571, 5517, 6660, 8039, 9703 };
   const double fY[40] = { -7.8, -6.82, -6.29, -5.87, -5.57,
                        -5.33, -5.14, -4.99, -4.86, -4.76,
                        -4.66, -4.58, -4.46, -4.37, -4.29,
                        -4.17, -4.03, -3.9, -3.81, -3.75,
                        -3.72, -3.74, -3.83, -4.01, -4.3,
                        -4.72, -5.23, -5.86, -6.57, -7.37,
                        -8.32, -9.56, -11.37, -14.12, -18.4,
                        -24.88, -34.07, -46.58, -65.08, -94.31 };
   const double fB[40] = { 0.234836, 0.159141, 0.107124, 0.0678904, 0.0533142,
                        0.0428527, 0.0332748, 0.028048, 0.0225333, 0.0198187,
                        0.0181918, 0.015414, 0.0125403, 0.00755118, 0.00825497,
                        0.00739838, 0.00568493, 0.00401146, 0.00200492, 0.00111279,
                        0.000184926, -0.000835277, -0.00178348, -0.00258556, -0.00326485,
                        -0.00357346, -0.00358397, -0.00355349, -0.00324241, -0.00309045,
                        -0.00316357, -0.00362892, -0.00450598, -0.00576048, -0.00739636,
                        -0.00903906, -0.0102469, -0.0118709, -0.0151126, -0.0202416 };
   const double fC[40] = { -0.00816245, -0.00697649, -0.00602772, -0.00181901, -0.00109623,
                        -0.000996062, -0.000919522, -0.000125851, -0.000977075, 0.000434151,
                        -0.000759529, 0.000203964, -0.000563177, 6.42662e-05, 6.11207e-06,
                        -6.32177e-05, -1.46663e-05, -4.96981e-05, -1.30062e-05, -9.86903e-06,
                        -1.03018e-05, -7.59645e-06, -6.34764e-06, -3.55462e-06, -3.30696e-06,
                        7.35282e-07, -8.08293e-07, 9.83475e-07, 4.97839e-07, 1.0045e-07,
                        -3.39416e-07, -9.21685e-07, -1.04483e-06, -1.28695e-06, -1.22977e-06,
                        -8.65513e-07, -4.11233e-07, -1.0096e-06, -1.34114e-06, 1664 };
   const double fD[40] = { 7.90642e-05, 7.90642e-05, 0.000280581, 4.81854e-05, 6.678e-06,
                        5.10265e-06, 5.29114e-05, -5.67483e-05, 9.40817e-05, -7.95786e-05,
                        6.42328e-05, -3.19642e-05, 2.09148e-05, -1.93847e-06, -1.54066e-06,
                        7.35627e-07, -4.49125e-07, 3.82207e-07, 2.68137e-08, -3.13637e-09,
                        1.5821e-08, 6.12163e-09, 1.14939e-08, 8.33898e-10, 1.12284e-08,
                        -3.57309e-09, 3.4325e-09, -7.70851e-10, -5.21508e-10, -4.79156e-10,
                        -5.25989e-10, -9.2039e-11, -1.50012e-10, 2.93229e-11, 1.54872e-10,
                        1.60071e-10, -1.74503e-10, -8.01391e-11, -8.01391e-11, 640.495 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}
