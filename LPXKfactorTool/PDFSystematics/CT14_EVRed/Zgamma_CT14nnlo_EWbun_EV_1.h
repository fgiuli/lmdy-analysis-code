double Zgamma_CT14nnlo_EWbun_EV_1(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { -0.686412, 1.50943, 2.49066, 2.48731, 2.65178,
                        2.71352, 2.73705, 2.91655, 3.09004, 3.0303,
                        3.06078, 2.91162, 2.93852, 2.84114, 2.82539,
                        2.85863, 2.77941, 2.89239, 2.97459, 3.07818,
                        3.21852, 3.28204, 3.38853, 3.47313, 3.63291,
                        3.71445, 3.79928, 3.79954, 3.74691, 3.6523,
                        3.6653, 3.61103, 3.47072, 3.28686, 3.20997,
                        2.88194, 2.66667, 2.32531, 1.57148, 0.539291,
                        -0.792952, -2.35664, -4.54009, -7.29685, -11.3198,
                        -17.3759, -26.9548, -43.9313, -76.4294 };
   const double fB[49] = { 0.75907, 0.362081, -0.020661, 0.013926, 0.013293,
                        0.000766264, 0.00922251, 0.023253, 0.00366236, -0.00377776,
                        0.0026724, -0.00430233, -0.00013499, -0.00361537, 0.0010213,
                        -0.000681793, -0.00105318, 0.00156553, 0.000646483, 0.00142236,
                        0.000982074, 0.000765141, 0.0010575, 0.00073762, 0.000455742,
                        0.000335241, 0.000199805, -0.00011338, -0.000374798, -0.000154313,
                        1.28098e-05, -0.000392152, -0.000779256, -0.000380857, -0.000826268,
                        -0.00117309, -0.00100101, -0.00150245, -0.00169877, -0.00241861,
                        -0.00281341, -0.00370331, -0.00485619, -0.00651321, -0.00976907,
                        -0.0148847, -0.0245021, -0.0464392, -0.0865884 };
   const double fC[49] = { -0.0583342, -0.0409132, 0.00263908, 0.000819608, -0.000882908,
                        -0.000369761, 0.00121539, 0.000187661, -0.00214672, 0.00140271,
                        -0.000757696, 0.000478707, -0.000312014, 0.000172798, 1.26683e-05,
                        -4.67301e-05, 3.93023e-05, -1.31151e-05, 3.92467e-06, 3.83412e-06,
                        -8.23699e-06, 6.06767e-06, -3.14407e-06, -5.47316e-08, -1.07278e-06,
                        5.90775e-07, -1.13252e-06, -1.20219e-07, -9.25455e-07, 1.8074e-06,
                        -1.13891e-06, -4.80941e-07, -1.06747e-06, 2.66107e-06, -4.44271e-06,
                        3.05543e-06, -2.36712e-06, 3.61378e-07, -7.54019e-07, -6.85659e-07,
                        -1.03954e-07, -1.67585e-06, -6.29897e-07, -2.68415e-06, -3.82759e-06,
                        -6.40371e-06, -1.28311e-05, -3.10432e-05, 500 };
   const double fD[49] = { 0.00145174, 0.00145174, -6.06492e-05, -5.67505e-05, 1.71049e-05,
                        5.28382e-05, -3.42575e-05, -7.78128e-05, 0.000118315, -7.20136e-05,
                        1.64854e-05, -1.05429e-05, 6.46416e-06, -2.13507e-06, -3.95989e-07,
                        5.73549e-07, -1.74725e-07, 5.67993e-08, -3.01839e-10, -4.0237e-08,
                        4.76822e-08, -3.07058e-08, 1.02978e-08, -1.3574e-09, 2.21807e-09,
                        -2.29772e-09, 1.34973e-09, -1.07365e-09, 3.6438e-09, -3.9284e-09,
                        8.77287e-10, -7.82043e-10, 4.97139e-09, -9.47171e-09, 9.99752e-09,
                        -7.23007e-09, 3.638e-09, -7.43598e-10, 4.55736e-11, 3.87803e-10,
                        -1.04793e-09, 6.97301e-10, -1.3695e-09, -7.62294e-10, -1.71742e-09,
                        -4.28492e-09, -1.21414e-08, -1.21414e-08, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}
