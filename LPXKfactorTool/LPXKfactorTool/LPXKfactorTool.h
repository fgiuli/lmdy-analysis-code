/*
 * WZkfactorTool.h
 *
 *  Created on: Jan 30, 2015
 *      Author: Markus Zinser (markus.zinser@cern.ch)
 */

#ifndef LPXKFACTORTOOL_H_
#define LPXKFACTORTOOL_H_

#include <iostream>
#include <string>

#include "AsgTools/AsgTool.h"

#include "xAODTruth/TruthVertexContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODEventInfo/EventInfo.h"

#include "PATInterfaces/CorrectionCode.h"
#include "PATInterfaces/ISystematicsTool.h"
#include "PATInterfaces/SystematicRegistry.h"
#include "PATInterfaces/SystematicVariation.h"


	class LPXKfactorTool : public asg::AsgTool, public CP::ISystematicsTool {
	public:

		LPXKfactorTool( const std::string& name );
		virtual ~LPXKfactorTool();

	    // Function initialising the tool
		virtual StatusCode initialize();

	    /// Function executing the tool
		virtual StatusCode execute();

	    /// Function to retreive the KFactor
                virtual StatusCode getKFactor( double& kFactorWeight );


	    /// returns: whether this tool is affected by the given systematis
	    virtual bool
	    isAffectedBySystematic( const CP::SystematicVariation& systematic ) const;

	    /// returns: the list of all systematics this tool can be affected by
	    virtual CP::SystematicSet
	    affectingSystematics() const;

	    /// returns: the list of al l systematics this tool recommends to use
	    virtual CP::SystematicSet
	    recommendedSystematics() const;

	    virtual CP::SystematicCode applySystematicVariation
	    ( const CP::SystematicSet& systConfig );

		double getMCCrossSection();
		double getMCFilterEfficiency();

		const char* classname;


	private:
		//const xAOD::TruthVertex* getDecayVertexOfIncomingParticle(const xAOD::TruthVertexContainer* truthVertices, int pdgID);

		void setBosonBornMass(const xAOD::TruthParticleContainer* truthParticles, const xAOD::EventInfo* eventInfo, int pdgID);
		void setCIBornMass(const xAOD::TruthParticleContainer* truthParticles, const xAOD::EventInfo* eventInfo, int pdgID);
		//void storeBornMassW(const xAOD::TruthVertex* decayVertex, const xAOD::EventInfo* eventInfo);
		//void storeBornMassZ(const xAOD::TruthVertex* decayVertex, const xAOD::EventInfo* eventInfo);

		bool isPowhegPythiaZllMC(const xAOD::EventInfo* eventInfo);
		bool isPythiaZllMC(const xAOD::EventInfo* eventInfo);
		bool isPowhegPythiaWlnuMC(const xAOD::EventInfo* eventInfo);

		bool isPythiaZPrimeMC(const xAOD::EventInfo* eventInfo);
		bool isPythiaCIMC(const xAOD::EventInfo* eventInfo);
		bool isPythiaElectronCIMC(const xAOD::EventInfo* eventInfo);
		bool isPythiaMuonCIMC(const xAOD::EventInfo* eventInfo);
		bool isPythiaWPrimeMC(const xAOD::EventInfo* eventInfo);
		bool isPythiaFlatWPrimeMC(const xAOD::EventInfo* eventInfo);

		bool isTTbarUBMC(const xAOD::EventInfo* eventInfo);
		bool isTTbarMBMC(const xAOD::EventInfo* eventInfo);

		bool isDBUBMC(const xAOD::EventInfo* eventInfo);
		bool isDBMBMC(const xAOD::EventInfo* eventInfo);

		double bornMass;
		int charge;

		bool m_isMC15;
		bool m_applyEWCorr;
		bool m_applyPICorr;
		std::string m_truthParticleKey;
		std::string m_sysKey;

	        std::string m_kFactorWeightName;
                const xAOD::EventInfo* m_eventInfo;

		std::map<CP::SystematicSet, CP::SystematicSet> m_filtered_sys_sets;

	};




#endif /* LPXKFACTORTOOL_H_ */
