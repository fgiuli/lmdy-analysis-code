#!/usr/bin/env python
from ROOT import TGraph, TCanvas, TF1
import sys, time

class VrapEntry:
    
    def __init__(self, mass, alphas, xsec, xsecDown, xsecUp, xsecStat, scaleDown, scaleUp, scaleSym):

        self.mass       = mass
        self.alphas     = alphas
        self.xsec       = xsec
        self.xsecDown   = xsecDown
        self.xsecUp     = xsecUp
        self.xsecStat   = xsecStat
        self.scaleDown  = scaleDown
        self.scaleUp    = scaleUp
        self.scaleSym   = scaleSym
        
        
if __name__ == "__main__":
    
    if len(sys.argv) < 3: raise Exception("Please provide two input files for the ratio as argument!")
    
    # Read first file given as first argument     
    inFileName = sys.argv[1]
    inFile = open(inFileName, "r")
    print "\n\nReading: "+inFileName
    
    listVrapEntries1 = []

    for line in inFile:
        # Line is a simple string in the beginning. The function split() splits it into a list of the rows
        line = line.split()
        if len(line) == 9: # Ignore empty lines, Access the entries of each row via [x]
            listVrapEntries1.append(VrapEntry(line[0], line[1], line[2], line[3], line[4], line[5], line[6], line[7], line[8]))
        
    inFile.close()
    
    
    # Read second file given as second argument 
    inFileName = sys.argv[2]
    inFile = open(inFileName, "r")
    print "\n\nReading: "+inFileName
    
    listVrapEntries2 = []

    for line in inFile:
        # Line is a simple string in the beginning. The function split() splits it into a list of the rows
        line = line.split()
        if len(line) == 9: # Ignore empty lines, Access the entries of each row via [x]
            listVrapEntries2.append(VrapEntry(line[0], line[1], line[2], line[3], line[4], line[5], line[6], line[7], line[8]))
        
    inFile.close()
    
    
    # Check that both files have the same amount of mass points and create a dictionary with several TGraphs
    if len(listVrapEntries1) is not len(listVrapEntries2): raise Exception("Both files should have the same number of bins !")
    dictGraphs = {}
    dictGraphs["centralXsec"] = TGraph(len(listVrapEntries1))
    dictGraphs["xsecUp"]      = TGraph(len(listVrapEntries1)) 
    dictGraphs["xsecDown"]    = TGraph(len(listVrapEntries1)) # define as many you want...
    # Loop over VrapEntries and fill TGraph
    for iVrapEntry in range(0, len(listVrapEntries1)):
        mass  = float(listVrapEntries1[iVrapEntry].mass)
        xsec1 = float(listVrapEntries1[iVrapEntry].xsec)
        xsec2 = float(listVrapEntries2[iVrapEntry].xsec)
        dictGraphs["centralXsec"].SetPoint(iVrapEntry, mass, xsec1/xsec2)
        
    # Define fit function
    fa1 = TF1("f1","pol0")
    fa1.SetLineColor(2)
    dictGraphs["centralXsec"].Fit("f1")
    
    # Draw everything
    can = TCanvas()
    dictGraphs["centralXsec"].Draw("ap")
    can.Update()
    
    
    print "Sleeping...exit with Ctrl+C"
    time.sleep(50000)
    
    