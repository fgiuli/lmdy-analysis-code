import sys
import os 
import time

inDir = '/data/atlas/atlasdata/giuli/Samples/Data15_repro207/'
#inDir = '/data/atlas/atlasdata/giuli/Samples/MC15_repro207/'

samplelist = [

  #'mc15_13TeV.363675.Pythia8EvtGen_NNPDF23QED_ggTOmumu_6M18_LeptonFilter.merge.DAOD_STDM7.e4913_s2726_r7773_r7676_p3317',
  #'mc15_13TeV.363676.Pythia8EvtGen_NNPDF23QED_ggTOmumu_18M60_LeptonFilter.merge.DAOD_STDM7.e4913_s2726_r7773_r7676_p3317',
  #'mc15_13TeV.363677.Pythia8EvtGen_NNPDF23QED_ggTOmumu_60M200_LeptonFilter.merge.DAOD_STDM7.e4913_s2726_r7773_r7676_p3317',
  #'mc15_13TeV.363686.HerwigppEvtGen_BudnevQED_ggTOmumu_6M18_LeptonFilter.merge.DAOD_STDM7.e4717_s2726_r7773_r7676_p3317',
  #'mc15_13TeV.363687.HerwigppEvtGen_BudnevQED_ggTOmumu_18M60_LeptonFilter.merge.DAOD_STDM7.e4717_s2726_r7773_r7676_p3317',
  #'mc15_13TeV.363688.HerwigppEvtGen_BudnevQED_ggTOmumu_60M200_LeptonFilter.merge.DAOD_STDM7.e4717_s2726_r7725_r7676_p3317', 
  #'mc15_13TeV.363697.HepMCAsciiEG_Lpair_BSY_SDiss_ggTOmumu_6M18_LeptonFilter.merge.DAOD_STDM7.e4909_s2726_r7773_r7676_p3317',
  #'mc15_13TeV.363698.HepMCAsciiEG_Lpair_BSY_SDiss_ggTOmumu_18M60_LeptonFilter.merge.DAOD_STDM7.e4909_s2726_r7773_r7676_p3317',
  #'mc15_13TeV.363699.HepMCAsciiEG_Lpair_BSY_SDiss_ggTOmumu_60M200_LeptonFilter.merge.DAOD_STDM7.e4909_s2726_r7773_r7676_p3317',
  #'mc15_13TeV.363833.Pythia8B_A14_NNPDF23LO_bbmu3p5mu3p5M6.merge.DAOD_STDM7.e4996_s2726_r7773_r7676_p3317.1',
  #'mc15_13TeV.363833.Pythia8B_A14_NNPDF23LO_bbmu3p5mu3p5M6.merge.DAOD_STDM7.e4996_s2726_r7773_r7676_p3317.2',
  #'mc15_13TeV.363833.Pythia8B_A14_NNPDF23LO_bbmu3p5mu3p5M6.merge.DAOD_STDM7.e4996_s2726_r7773_r7676_p3317.3',
  #'mc15_13TeV.363833.Pythia8B_A14_NNPDF23LO_bbmu3p5mu3p5M6.merge.DAOD_STDM7.e4996_s2726_r7773_r7676_p3317.4',
  #'mc15_13TeV.363833.Pythia8B_A14_NNPDF23LO_bbmu3p5mu3p5M6.merge.DAOD_STDM7.e4996_s2726_r7773_r7676_p3317.5',
  #'mc15_13TeV.363833.Pythia8B_A14_NNPDF23LO_bbmu3p5mu3p5M6.merge.DAOD_STDM7.e4996_s2726_r7773_r7676_p3317.6',
  #'mc15_13TeV.363834.Pythia8B_A14_NNPDF23LO_ccmu3p5mu3p5M6.merge.DAOD_STDM7.e4996_s2726_r7773_r7676_p3317',
  #'mc15_13TeV.361666.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_6M10.merge.DAOD_STDM7.e4770_s2726_r7773_r7676_p3317.1',
  #'mc15_13TeV.361666.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_6M10.merge.DAOD_STDM7.e4770_s2726_r7773_r7676_p3317.2',
  #'mc15_13TeV.361666.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_6M10.merge.DAOD_STDM7.e4770_s2726_r7773_r7676_p3317.3',
  #'mc15_13TeV.361666.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_6M10.merge.DAOD_STDM7.e4770_s2726_r7773_r7676_p3317.4',
  #'mc15_13TeV.361666.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_6M10.merge.DAOD_STDM7.e4770_s2726_r7773_r7676_p3317.5',
  #'mc15_13TeV.361666.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_6M10.merge.DAOD_STDM7.e4770_s2726_r7773_r7676_p3317.6',
  #'mc15_13TeV.361667.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_10M60.merge.DAOD_STDM7.e4770_s2726_r7773_r7676_p3317.1',
  #'mc15_13TeV.361667.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_10M60.merge.DAOD_STDM7.e4770_s2726_r7773_r7676_p3317.2',
  #'mc15_13TeV.361667.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_10M60.merge.DAOD_STDM7.e4770_s2726_r7773_r7676_p3317.3',
  #'mc15_13TeV.361667.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_10M60.merge.DAOD_STDM7.e4770_s2726_r7773_r7676_p3317.4',
  #'mc15_13TeV.361667.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_10M60.merge.DAOD_STDM7.e4770_s2726_r7773_r7676_p3317.5',
  #'mc15_13TeV.361667.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_10M60.merge.DAOD_STDM7.e4770_s2726_r7773_r7676_p3317.6',
  #'mc15_13TeV.361667.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_10M60.merge.DAOD_STDM7.e4770_s2726_r7773_r7676_p3317.7',
  #'mc15_13TeV.361667.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_10M60.merge.DAOD_STDM7.e4770_s2726_r7773_r7676_p3317.8',
  #'mc15_13TeV.361667.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_10M60.merge.DAOD_STDM7.e4770_s2726_r7773_r7676_p3317.9',
  #'mc15_13TeV.361668.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_6M10.merge.DAOD_STDM7.e4784_s2726_r7773_r7676_p3317',
  #'mc15_13TeV.361669.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_10M60.merge.DAOD_STDM7.e4770_s2726_r7773_r7676_p3317',
  #'mc15_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.1',
  #'mc15_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.2',
  #'mc15_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.3',
  #'mc15_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.4',
  #'mc15_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.5',
  #'mc15_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.6',
  #'mc15_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.7',
  #'mc15_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.8',
  #'mc15_13TeV.410503.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_dil.merge.DAOD_STDM7.e5475_s2726_r7773_r7676_p3317.1',
  #'mc15_13TeV.410503.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_dil.merge.DAOD_STDM7.e5475_s2726_r7773_r7676_p3317.2',
  #'mc15_13TeV.410503.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_dil.merge.DAOD_STDM7.e5475_s2726_r7773_r7676_p3317.3',
  #'mc15_13TeV.410503.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_dil.merge.DAOD_STDM7.e5475_s2726_r7773_r7676_p3317.4',
  #'mc15_13TeV.410503.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_dil.merge.DAOD_STDM7.e5475_s2726_r7773_r7676_p3317.5',
  #'mc15_13TeV.410503.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_dil.merge.DAOD_STDM7.e5475_s2726_r7773_r7676_p3317.6',
  #'mc15_13TeV.410503.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_dil.merge.DAOD_STDM7.e5475_s2726_r7773_r7676_p3317.7',
  #'mc15_13TeV.410503.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_dil.merge.DAOD_STDM7.e5475_s2726_r7773_r7676_p3317.8',
  #'mc15_13TeV.410503.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_dil.merge.DAOD_STDM7.e5475_s2726_r7773_r7676_p3317.9',
  #'mc15_13TeV.410503.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_dil.merge.DAOD_STDM7.e5475_s2726_r7773_r7676_p3317.10',
  #'mc15_13TeV.410503.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_dil.merge.DAOD_STDM7.e5475_s2726_r7773_r7676_p3317.11',
  #'mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.1',
  #'mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.2',
  #'mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.3',
  #'mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.4',
  #'mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.5',
  #'mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.6',
  #'mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.7',
  #'mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.8',
  #'mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.9',
  #'mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.10',
  #'mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.11',  
  #'mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.12',
  #'mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.13',
  #'mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.14',
  #'mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.15',
  #'mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.16',
  #'mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.17',
  #'mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.18',
  #'mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.19',
  #'mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.20',
  #'mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7773_r7676_p3317.21',
  #'mc15_13TeV.424105.Pythia8B_A14_CTEQ6L1_Upsilon2S_mu4mu4.merge.DAOD_STDM7.e5104_s2726_r7773_r7676_p3020.1',
  #'mc15_13TeV.424105.Pythia8B_A14_CTEQ6L1_Upsilon2S_mu4mu4.merge.DAOD_STDM7.e5104_s2726_r7773_r7676_p3020.2',
  #'mc15_13TeV.424106.Pythia8B_A14_CTEQ6L1_Upsilon3S_mu4mu4.merge.DAOD_STDM7.e5104_s2726_r7773_r7676_p3020.1',
  #'mc15_13TeV.424106.Pythia8B_A14_CTEQ6L1_Upsilon3S_mu4mu4.merge.DAOD_STDM7.e5104_s2726_r7773_r7676_p3020.2',
  #'mc15_13TeV.424102.Pythia8B_A14_CTEQ6L1_Upsilon1S_mu4mu4.merge.DAOD_STDM7.e4293_s2608_s2183_r7773_r7676_p3020.1',
  #'mc15_13TeV.424102.Pythia8B_A14_CTEQ6L1_Upsilon1S_mu4mu4.merge.DAOD_STDM7.e4293_s2608_s2183_r7773_r7676_p3020.2',
  #'mc15_13TeV.424102.Pythia8B_A14_CTEQ6L1_Upsilon1S_mu4mu4.merge.DAOD_STDM7.e4293_s2608_s2183_r7773_r7676_p3020.3',

  #'mc15_13TeV.364100.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CVetoBVeto.merge.DAOD_STDM7.e5271_s2726_r7773_r7676_p3314',
  #'mc15_13TeV.364101.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CFilterBVeto.merge.DAOD_STDM7.e5271_s2726_r7773_r7676_p3314',
  #'mc15_13TeV.364102.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_BFilter.merge.DAOD_STDM7.e5271_s2726_r7773_r7676_p3314',
  #'mc15_13TeV.364103.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CVetoBVeto.merge.DAOD_STDM7.e5271_s2726_r7773_r7676_p3314',
  #'mc15_13TeV.364104.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CFilterBVeto.merge.DAOD_STDM7.e5271_s2726_r7773_r7676_p3314',
  #'mc15_13TeV.364105.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_BFilter.merge.DAOD_STDM7.e5271_s2726_r7773_r7676_p3314',
  #'mc15_13TeV.364106.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CVetoBVeto.merge.DAOD_STDM7.e5271_s2726_r7773_r7676_p3314',
  #'mc15_13TeV.364107.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CFilterBVeto.merge.DAOD_STDM7.e5271_s2726_r7773_r7676_p3314',
  #'mc15_13TeV.364108.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_BFilter.merge.DAOD_STDM7.e5271_s2726_r7773_r7676_p3314',
  #'mc15_13TeV.364109.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CVetoBVeto.merge.DAOD_STDM7.e5271_s2726_r7773_r7676_p3314',
  #'mc15_13TeV.364110.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CFilterBVeto.merge.DAOD_STDM7.e5271_s2726_r7773_r7676_p3314',
  #'mc15_13TeV.364111.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_BFilter.merge.DAOD_STDM7.e5271_s2726_r7773_r7676_p3314',
  #'mc15_13TeV.364112.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV500_1000.merge.DAOD_STDM7.e5271_s2726_r7773_r7676_p3314',
  #'mc15_13TeV.364198.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV0_70_BVeto.merge.DAOD_STDM7.e5421_s2726_r7773_r7676_p3314',
  #'mc15_13TeV.364199.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV0_70_BFilter.merge.DAOD_STDM7.e5421_s2726_r7773_r7676_p3314',
  #'mc15_13TeV.364200.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV70_280_BVeto.merge.DAOD_STDM7.e5421_s2726_r7773_r7676_p3314',
  #'mc15_13TeV.364201.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV70_280_BFilter.merge.DAOD_STDM7.e5421_s2726_r7773_r7676_p3314',
  #'mc15_13TeV.364202.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV280_E_CMS_BVeto.merge.DAOD_STDM7.e5421_s2726_r7773_r7676_p3314',
  #'mc15_13TeV.364203.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV280_E_CMS_BFilter.merge.DAOD_STDM7.e5421_s2726_r7773_r7676_p3314',
  #'mc15_13TeV.364669.Sherpa_221_NN30NNLO_Zmm_Mll6_10.merge.DAOD_STDM7.e6478_s2726_r7773_r7676_p3314',
  #'mc15_13TeV.364675.Sherpa_221_NN30NNLO_Zmm_Mll10_40.merge.DAOD_STDM7.e6497_s2726_r7773_r7676_p3314',

  'data15_13TeV.00276262.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00276329.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00276336.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00276416.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00276511.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00276689.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00276778.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00276790.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00276952.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00276954.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00278880.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00278912.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00278968.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',  
  'data15_13TeV.00279169.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00279259.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00279279.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00279284.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00279345.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00279515.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00279598.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298', 
  'data15_13TeV.00279685.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00279813.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00279867.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00279928.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00279932.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00279984.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',

  'data15_13TeV.00280231.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00280273.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00280319.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00280368.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00280423.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00280464.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00280500.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00280520.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00280614.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00280673.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00280753.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00280853.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00280862.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00280950.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00280977.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00281070.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00281074.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00281075.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00281317.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00281385.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00281411.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00282625.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00282631.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00282712.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00282784.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00282992.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00283074.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00283155.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00283270.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00283429.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00283608.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00283780.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00284006.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00284154.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00284213.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00284285.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00284420.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00284427.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  'data15_13TeV.00284484.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  
  #CUTFLOW COMPARISON
  #'data15_13TeV.00276262.physics_Main.merge.DAOD_STDM3.r7562_p2521_p2950',
  #'data15_13TeV.00276329.physics_Main.merge.DAOD_STDM3.r7562_p2521_p2950',
  #'data15_13TeV.00276336.physics_Main.merge.DAOD_STDM3.r7562_p2521_p2950',
  #'data15_13TeV.00276416.physics_Main.merge.DAOD_STDM3.r7562_p2521_p2950',
  #'data15_13TeV.00276511.physics_Main.merge.DAOD_STDM3.r7562_p2521_p2950',
  #'data15_13TeV.00276689.physics_Main.merge.DAOD_STDM3.r7562_p2521_p2950',
  #'data15_13TeV.00276778.physics_Main.merge.DAOD_STDM3.r7562_p2521_p2950',
  #'data15_13TeV.00276790.physics_Main.merge.DAOD_STDM3.r7562_p2521_p2950',
  #'data15_13TeV.00276952.physics_Main.merge.DAOD_STDM3.r7562_p2521_p2950',
  #'data15_13TeV.00276954.physics_Main.merge.DAOD_STDM3.r7562_p2521_p2950',

  #'data15_13TeV.00276262.physics_Main.merge.DAOD_STDM3.r7562_p2521_p2667',
  #'data15_13TeV.00276329.physics_Main.merge.DAOD_STDM3.r7562_p2521_p2667',
  #'data15_13TeV.00276336.physics_Main.merge.DAOD_STDM3.r7562_p2521_p2667',
  #'data15_13TeV.00276416.physics_Main.merge.DAOD_STDM3.r7562_p2521_p2667',
  #'data15_13TeV.00276511.physics_Main.merge.DAOD_STDM3.r7562_p2521_p2667',
  #'data15_13TeV.00276689.physics_Main.merge.DAOD_STDM3.r7562_p2521_p2667',
  #'data15_13TeV.00276778.physics_Main.merge.DAOD_STDM3.r7562_p2521_p2667',
  #'data15_13TeV.00276790.physics_Main.merge.DAOD_STDM3.r7562_p2521_p2667',
  #'data15_13TeV.00276952.physics_Main.merge.DAOD_STDM3.r7562_p2521_p2667',
  #'data15_13TeV.00276954.physics_Main.merge.DAOD_STDM3.r7562_p2521_p2667',

  #'data15_13TeV.00276262.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  #'data15_13TeV.00276329.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  #'data15_13TeV.00276336.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  #'data15_13TeV.00276416.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  #'data15_13TeV.00276511.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  #'data15_13TeV.00276689.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  #'data15_13TeV.00276778.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  #'data15_13TeV.00276790.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  #'data15_13TeV.00276952.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',
  #'data15_13TeV.00276954.physics_Main.merge.DAOD_STDM7.r7562_p2521_p3298',

  #'data15_13TeV.00276262.physics_Main.merge.DAOD_STDM3.f620_m1480_p2425',
  #'data15_13TeV.00276329.physics_Main.merge.DAOD_STDM3.f620_m1480_p2425',
  #'data15_13TeV.00276336.physics_Main.merge.DAOD_STDM3.f620_m1480_p2425',
  #'data15_13TeV.00276416.physics_Main.merge.DAOD_STDM3.f620_m1480_p2425',
  #'data15_13TeV.00276511.physics_Main.merge.DAOD_STDM3.f620_m1480_p2425',
  #'data15_13TeV.00276689.physics_Main.merge.DAOD_STDM3.f623_m1480_p2425',
  #'data15_13TeV.00276778.physics_Main.merge.DAOD_STDM3.f620_m1480_p2425',
  #'data15_13TeV.00276790.physics_Main.merge.DAOD_STDM3.f620_m1480_p2425',
  #'data15_13TeV.00276952.physics_Main.merge.DAOD_STDM3.f620_m1480_p2425',
  #'data15_13TeV.00276954.physics_Main.merge.DAOD_STDM3.f620_m1480_p2425',
  ]

workdir = os.getenv('MUON')
#outputdir = workdir + '/corr_deltaZ0_d0_sig/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/MC15_ResBARE/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/MC15_TriggerSF_PhiOn_Int/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/Data15_7M9_August/Run_'

#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/Data15_October/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/MC15_October/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/MC15_October.ZpT.2.Weights/Run_'

#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/Powheg.NEW/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/Sherpa.NEW/Run_'

#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/XSEC_NOV/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/POW_NOV/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/SHERPA_NOV.2/Run_'

outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/Data15_ii_nn/Run_'

#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/Sherpa2_2/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/MC15_September/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/Data15_September/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/MC15_PileUpRwg.2.Py8/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/Data15_PileUpRwg.2.Py8/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/Data15_7M9_September/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/MC15_7M9_September/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/MC15_7M9_September_ZpT.2/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/Data15_7M9_September_ZpT.2/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/MC15_PowhegPythia8_Pesi.2/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/Data15_PileUpRwg.2/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/MC15_QCDonly/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/MC15_noKFnoPU/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/Systematics_lmDY/Nominal_MY_Nochi2cut/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/Systematics_lmDY/Data_MY/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/Data15_Fra_NoZpT/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/Data15_July/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/MC15_ZpT_Mass_Rwg_Max.NEW.DY/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/MC15_7M9.NEW_Rwg/Run_'
#outputdir = '/data/atlas/atlasdata/giuli/low_mass_DY_2016/Output/MC15_ZpT_Mass_NoRwg_Max.NEW.DY_ZpT/Run_'
#outputdir = workdir + '/ttbarZ_cutflow/Run_'
#outputdir = workdir + '/TEST_Rootcore/Run_'
#outputdir = workdir + 'Run_'
#outputdir ='/data/atlas/atlasdata/giuli/low_mass_DY_2016/'

#
# Loop on samples and submit jobs
#
for sample in samplelist:

  #
  # Full path directory to sample files
  #
  inDirSample = inDir + sample
  outDir = outputdir + sample

  #
  # Create temp batch script
  #
  command = 'testRun_Cluster %s %s' % (outDir,inDirSample)

  fName = 'temp_' + sample
  f = open(fName, 'w')
  f.write('#PBS -l cput=34:00:00\n')
  f.write('#PBS -l walltime=34:00:00\n')
  #f.write('#PBS -l walltime=168:00:00,mem=12gb\n')
  #f.write('#PBS -l walltime=700:00:00\n')
  #f.write('#PBS -l nodes=1:ppn=16\n')
  #f.write('#PBS -q short\n')
  #f.write('#PBS -q datatransfer\n')
  f.write('\n')
  #f.write('source /home/giuli/setup_cluster.sh \n')
  f.write('cd '+ workdir +' \n')
  f.write('echo $PWD \n')
  f.write('source dataset.sh \n')
  f.write('echo $ROOTCOREBIN \n')
  #f.write('source /home/giuli/.bashrc \n')
  #f.write('source Make.sh \n')
  #f.write('./Run/Make.sh \n')
  #f.write('rc clean\n')
  #f.write('rc find_packages\n')
  #f.write('rc compile --single-thread\n')
  
  #f.write('cd $TMPDIR\n')
  
  f.write(command + ' \n')
  f.close()

  #
  # Change batch script permission
  #
  preliminary_command = 'chmod +x %s' % fName
  os.system(preliminary_command)
 
  #
  # Send batch script to batch farm
  #
  sendBatchScript_command = 'qsub %s' % fName
  os.system(sendBatchScript_command)
  os.system('rm -f ' + fName)

  time.sleep(30)

